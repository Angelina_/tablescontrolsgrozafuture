﻿using ModelsTablesDBLib;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using TableEvents;
using TableOperations;

namespace ASPControl
{
    /// <summary>
    /// Логика взаимодействия для UserControl1.xaml
    /// </summary>
    public partial class UserControlASP : UserControl, ITableReconEvent, IASPEvents
    {
        public ASPProperty ASPWindow;

        #region Events
        public event EventHandler<TableEvent> OnAddRecord = (object sender, TableEvent data) => { };
        public event EventHandler<TableEvent> OnChangeRecord = (object sender, TableEvent data) => { };
        public event EventHandler<TableEvent> OnDeleteRecord = (object sender, TableEvent data) => { };
        public event EventHandler<NameTable> OnClearRecords = (object sender, NameTable data) => { };

        public event EventHandler<ASPEvents> OnSelectedRow = (object sender, ASPEvents data) => { };

        // Открылось окно с PropertyGrid
        public event EventHandler<ASPProperty> OnIsWindowPropertyOpen = (object sender, ASPProperty data) => { };

        public event EventHandler<TableEventReport> OnAddTableToReport = (object sender, TableEventReport data) => { };
        #endregion

        #region Properties
        public NameTable NameTable { get; } = NameTable.TableASP;
        #endregion

        public UserControlASP()
        {
            InitializeComponent();

            dgvASP.DataContext = new GlobalASP();
        }

        private void ButtonAdd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ((GlobalASP)dgvASP.DataContext).CollectionASP = new ObservableCollection<MyTableASP>();

                this.ASPWindow = new ASPProperty(((GlobalASP)dgvASP.DataContext).CollectionASP);

                if (this.IsRoss)
                {
                    this.IsBrowsablePropertyRoss(this.ASPWindow);
                }
                else
                {
                    this.IsBrowsableProperty(this.ASPWindow);
                }
                
                this.UpdatePropertiesForRoss(this.ASPWindow, this.IsRoss);
               
                OnIsWindowPropertyOpen(this, this.ASPWindow);

                if (!PropIsRecASP.IsRecAdd && !PropIsRecASP.IsRecChange)
                {
                    ASPWindow.Show();
                    PropIsRecASP.IsRecAdd = true;
                    ASPWindow.OnAddRecordPG += this.ASPWindow_OnAddRecordPG;
                }

                //if (ASPWindow.ShowDialog() == true)
                //{
                //    var collection = new ObservableCollection<MyTableASP> { ASPWindow.TableASP };

                //    // Событие добавления одной записи
                //    OnAddRecord(this, new TableEvent(MyTableASPToTableASP(ASPWindow.TableASP)));
                //}
            }
            catch { }
            //catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        private void ASPWindow_OnAddRecordPG(object sender, MyTableASP e)
        {
            PropIsRecASP.IsRecAdd = false;

            // Событие добавления одной записи
            this.OnAddRecord(this, new TableEvent(MyTableASPToTableASP(this.ASPWindow.TableASP)));
        }

        private void ButtonChange_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if(((MyTableASP)dgvASP.SelectedItem) != null)
                {
                    if (((MyTableASP)dgvASP.SelectedItem).Id > 0)
                    {

                        var selected = (MyTableASP)dgvASP.SelectedItem;
                        PropNumberASP.SelectedNumASP = ((MyTableASP)dgvASP.SelectedItem).Id;

                        ASPWindow = new ASPProperty(((GlobalASP)dgvASP.DataContext).CollectionASP, selected.Clone());

                        if (this.IsRoss)
                        {
                            this.IsBrowsablePropertyRoss(this.ASPWindow);
                        }
                        else
                        {
                            this.IsBrowsableProperty(this.ASPWindow);
                        }
                        this.UpdatePropertiesForRoss(this.ASPWindow, this.IsRoss);

                        this.OnIsWindowPropertyOpen(this, this.ASPWindow);

                        if (!PropIsRecASP.IsRecAdd && !PropIsRecASP.IsRecChange)
                        {
                            ASPWindow.Show();
                            PropIsRecASP.IsRecChange = true;
                            this.ASPWindow.OnChangeRecordPG += this.ASPWindow_OnChangeRecordPG;
                        }

                        //if (ASPWindow.ShowDialog() == true)
                        //{
                        //    MyTableASP aspReplace = new MyTableASP();
                        //    aspReplace = ASPWindow.TableASP;

                        //    // Событие изменения одной записи
                        //    OnChangeRecord(this, new TableEvent(MyTableASPToTableASP(aspReplace)));
                        //}
                    }
                }
            }
            catch { }
        }

        private void ASPWindow_OnChangeRecordPG(object sender, MyTableASP e)
        {
            PropIsRecASP.IsRecChange = false;

            // Событие изменения одной записи
            this.OnChangeRecord(this, new TableEvent(MyTableASPToTableASP(this.ASPWindow.TableASP)));
        }

        private void ButtonDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                TableASP tableASP = new TableASP
                {
                    Id = ((MyTableASP)dgvASP.SelectedItem).Id,
                    IsGnssUsed = ((MyTableASP)dgvASP.SelectedItem).IsGnssUsed
                };

                PropNumberASP.SelectedNumASP = ((MyTableASP)dgvASP.SelectedItem).Id;

                //if(tableASP.IsGnssUsed)
                //{
                //    tableASP.IsGnssUsed = false;
                //    OnChangeRecord(this, new TableEvent(tableASP));
                //}

                // Событие удаления одной записи
                OnDeleteRecord(this, new TableEvent(tableASP));
            }
            catch { }
        }

        private void ButtonClear_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // Событие удаления одной записи
                OnClearRecords(this, NameTable);
                //OnClearASP?.Invoke();
            }
            catch { }
        }

        Lib.Lib libNote = new Lib.Lib();
        private void ButtonAddToWord_Click(object sender, RoutedEventArgs e)
        {
            string[,] Table = AddASPToTable();

            //TEST
            //AddTableToReport.AddToExcel("12221", Table, NameTable.TableASP);
            //TEST


            OnAddTableToReport(this, new TableEventReport(Table, NameTable.TableASP));

            // Excel example-------------------------------------------------------------------------------------
            //Microsoft.Office.Interop.Excel.Application excel = new Microsoft.Office.Interop.Excel.Application();
            //excel.Visible = true;
            //Workbook workbook = excel.Workbooks.Add(System.Reflection.Missing.Value);
            //Worksheet sheet1 = (Worksheet)workbook.Sheets[1];

            //for (int j = 0; j < dgvASP.Columns.Count; j++)
            //{
            //    Microsoft.Office.Interop.Excel.Range myRange = (Microsoft.Office.Interop.Excel.Range)sheet1.Cells[1, j + 1];
            //    sheet1.Cells[1, j + 1].Font.Bold = true;
            //    sheet1.Columns[j + 1].ColumnWidth = 15;
            //    myRange.Value2 = dgvASP.Columns[j].Header;
            //}
            // -------------------------------------------------------------------------------------Excel example

            //List<string[]> list = AddTableASPToList();

            //string PathFile = @"D:\MyTest.txt";
            //libNote.AddToNote(PathFile, list);

        }

        private string BytesToString(byte[] bLetters)
        {
            string letters = string.Empty;

            for (int i = 0; i < bLetters.Length; i++)
            {
                if (bLetters[i] == 1)
                    letters += (i + 1) + ",".ToString();
            }
            if (letters != string.Empty)
                letters = letters.Remove(letters.Length - 1, 1);

            return letters;
        }

        //ExcelPrinter.ExclPrinter exclPrinter = new ExcelPrinter.ExclPrinter();
        private void ButtonPrint_Click(object sender, RoutedEventArgs e)
        {
            List<string[]> list = AddTableASPToList();
            //exclPrinter.


        }

        private void DgvASP_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            AddEmptyRows();
        }

        private void dgvASP_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if ((MyTableASP)dgvASP.SelectedItem == null) return;

            if (((MyTableASP)dgvASP.SelectedItem).Id > 0)
            {
                if (((MyTableASP)dgvASP.SelectedItem).Id != PropNumberASP.SelectedNumASP)
                {
                    PropNumberASP.SelectedNumASP = ((MyTableASP)dgvASP.SelectedItem).Id;
                    PropNumberASP.IsSelectedRowASP = true;

                    OnSelectedRow(this, new ASPEvents(PropNumberASP.SelectedNumASP));
                }
            }
            else
            {
                PropNumberASP.SelectedNumASP = 0;
                PropNumberASP.IsSelectedRowASP = false;
            }

                //if ((TableASP)dgvASP.SelectedItem == null) return;

                //if (((TableASP)dgvASP.SelectedItem).Id > 0)
                //{
                //    PropNumberASP.SelectedNumASP = ((TableASP)dgvASP.SelectedItem).Id;
                //    PropNumberASP.IsSelectedRowASP = true;

                //    OnSelectedRow(this, new ASPEvents(PropNumberASP.SelectedNumASP));
                //}
                //else
                //{
                //    PropNumberASP.SelectedNumASP = 0;
                //    PropNumberASP.IsSelectedRowASP = false;
                //}
        }
    }
}
