﻿using ModelsTablesDBLib;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Windows;
using System.Windows.Controls.WpfPropertyGrid;
using TableEvents;

namespace ASPControl
{
    public class GlobalASP : INotifyPropertyChanged
    {
        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }
        #endregion

        public ObservableCollection<MyTableASP> CollectionASP { get; set; }

        public GlobalASP()
        {
            try
            {
                CollectionASP = new ObservableCollection<MyTableASP> { };

                //CollectionASP = new ObservableCollection<MyTableASP>
                //{
                //    new MyTableASP
                //    {
                //        Id = 201,
                //        ISOwn = true,
                //        AddressIP = "127.0.0.1",
                //        AntHeightRec = 45,
                //        AntHeightSup = 60,
                //        Caption = "XXX",
                //        Image = "C:\\Work",
                //        Role = RoleStation.Master,
                //        Type = 0,
                //        IdMission = 3,
                //        CallSign = "1 ASP",
                //        IsConnect = Led.Green,
                ////        Letters = new byte[] { 0, 0, 0, 0, 1, 1, 1, 1, 1, 0 },
                //        Coordinates = new Coord() { Altitude = 1, Latitude = -53.123445, Longitude = 27.456459 },
                //        LPA13 = 235,
                //        LPA24 = 155,
                //        LPA510 = 45,
                //        RRS1 = 30,
                //        RRS2 = 45,
                //        Mode = (byte)1,
                //        MatedStationNumber = 12,
                //        AddressIp3G4G = "127.0.0.1",
                //        AddressPort3G4G = 2345
                //    },
                //    new MyTableASP
                //    {
                //        Id = 203,
                //        ISOwn = false,
                //        AddressIP = "127.0.0.1",
                //        AntHeightRec = 65,
                //        AntHeightSup = 63,
                //        Caption = "AAAAAAA,",
                //        Image = "C:\\Work",
                //        Role = RoleStation.Slave,
                //        Type = 2,
                //        IdMission = 2,
                //        CallSign = "2 ASP",
                //        IsConnect = Led.Green,
                //        //Letters = new byte[] { 1, 0, 1, 0, 1, 0, 1, 1, 0, 0 },
                //        Coordinates = new Coord() { Altitude = 211, Latitude = 56.14577777884, Longitude = -27.845869 },
                //        LPA13 = 135,
                //        LPA24 = 175,
                //        LPA510 = 45,
                //        RRS1 = 60,
                //        RRS2 = 55,
                //        Mode = (byte)0,
                //        MatedStationNumber = 11,
                //        AddressIp3G4G = "127.0.0.1",
                //        AddressPort3G4G = 4444
                //    },
                //    new MyTableASP
                //    {
                //        Id = 207,
                //        ISOwn = false,
                //        AddressIP = "127.0.0.1",
                //        AntHeightRec = 55,
                //        AntHeightSup = 23,
                //        Caption = "JKJJJJJ",
                //        Image = "C:\\Work",
                //        Role = RoleStation.Autonomic,
                //        Type = 2,
                //        IdMission = 2,
                //        CallSign = "3 ASP",
                //        IsConnect = Led.Green,
                //        //Letters = new byte[] { 0, 0, 0, 0, 1, 1, 1, 1, 1, 0 },
                //        Coordinates = new Coord() { Altitude = 223, Latitude = -55.13664234, Longitude = -24.45366469 },
                //        LPA13 = 235,
                //        LPA24 = 155,
                //        LPA510 = 45,
                //        RRS1 = 30,
                //        RRS2 = 45,
                //        Mode = (byte)2,
                //        MatedStationNumber = 42,
                //        AddressIp3G4G = "127.0.0.1",
                //        AddressPort3G4G = 6789
                //    },
                //    new MyTableASP
                //    {
                //        Id = 208,
                //        ISOwn = false,
                //        AddressIP = "127.0.0.1",
                //        AntHeightRec = 55,
                //        AntHeightSup = 23,
                //        Caption = "NNNNN",
                //        Image = "C:\\Work",
                //        Role = RoleStation.Autonomic,
                //        Type = 2,
                //        IdMission = 2,
                //        CallSign = "4 ASP",
                //        IsConnect = Led.Green,
                //        //Letters = new byte[] { 0, 0, 1, 0, 1, 0, 1, 1, 1, 0 },
                //        Coordinates = new Coord() { Altitude = 223, Latitude = 50.13426634, Longitude = 28.47847769 },
                //        LPA13 = 235,
                //        LPA24 = 155,
                //        LPA510 = 45,
                //        RRS1 = 30,
                //        RRS2 = 45,
                //        Mode = (byte)1,
                //        MatedStationNumber = 10,
                //        AddressIp3G4G = "127.0.0.1",
                //        AddressPort3G4G = 2009
                //    }
                //};
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }
    }

    [DataContract]
    [CategoryOrder("ID", 1)]
    [CategoryOrder("Общие", 2)]
    [CategoryOrder("Антенны", 3)]
    [CategoryOrder(nameof(Coordinates), 4)]
    [CategoryOrder("Карта", 5)]
    [CategoryOrder("Прочее", 6)]
    [InfoTable(NameTable.TableASP)]
    [KnownType(typeof(AbstractDependentMission))]
    public class MyTableASP
    {
        [DataMember]
        [Category("ID")]
        [DisplayName("№ АСП"), ReadOnly(false)]
        [Browsable(true)]
        public int Id { get; set; }

        [DataMember]
        [Category("ID")]
        [DisplayName("Своя")]
        [Browsable(true)]
        public bool ISOwn { get; set; }

        [DataMember]
        [Browsable(false)]
        public int IdMission { get; set; }

        [DataMember]
        [Category("Карта")]
        [DisplayName("Подпись")]
        public string Caption { get; set; }

        [DataMember]
        [Category("Карта")]
        [DisplayName("Знак")]
        public string Image { get; set; }

        [DataMember]
        [Category("Общие")]
        [DisplayName("Адрес IP")]
        [PropertyOrder(2)]
        public string AddressIP { get; set; }

        [DataMember]
        [Category("Общие")]
        [DisplayName("Порт")]
        [PropertyOrder(3)]
        public int AddressPort { get; set; }

        [DataMember]
        [Category("Общие")]
        [DisplayName("Подключена"), Browsable(false)]
        public Led IsConnect { get; set; }

        [DataMember]
        [Category("Общие")]
        [DisplayName("Позывной")]
        [PropertyOrder(6)]
        public string CallSign { get; set; }

        [DataMember]
        [Category("Общие")]
        [DisplayName("Режим"), Browsable(false)]
        public byte Mode { get; set; }

        [DataMember]
        [Category("Общие")]
        [DisplayName("Роль")]
        [PropertyOrder(1)]
        public RoleStation Role { get; set; }

        [DataMember]
        [Category("Общие")]
        [DisplayName("Тип"), Browsable(false)]
        public byte Type { get; set; }

        [DataMember]
        [Category("Антенны")]
        [DisplayName("Высота РП, м")]
        public byte AntHeightSup { get; set; }

        [DataMember]
        [Category("Антенны")]
        [DisplayName("Высота РР, м")]
        public byte AntHeightRec { get; set; }

        [DataMember]
        [Category("Общие")]
        [DisplayName("Литеры"), Browsable(false)]
        public byte[] Letters { get; set; }

        [DataMember]
        [Category("Антенны")]
        [DisplayName("РРС 1,°")]
        public short RRS1 { get; set; }

        [DataMember]
        [Category("Антенны")]
        [DisplayName("РРС 2,°")]
        public short RRS2 { get; set; }

        [DataMember]
        [Category("Антенны")]
        [DisplayName("ЛПА 1,3,°")]
        public short LPA13 { get; set; }

        [DataMember]
        [Category("Антенны")]
        [DisplayName("ЛПА 2,4,°")]
        public short LPA24 { get; set; }

        [DataMember]
        [Category("Антенны")]
        [DisplayName("БПСС,°")]
        public short BPSS { get; set; }

        [DataMember]
        [Category("Антенны")]
        [DisplayName("ЛПА 5-7,°")]
        public short LPA57 { get; set; }
        [DataMember]
        [Category("Антенны")]
        [DisplayName("ЛПА 5-9,°")]
        public short LPA59 { get; set; }
        [DataMember]
        [Category("Антенны")]
        [DisplayName("ЛПА 10,°")]
        public short LPA10 { get; set; }

        [DataMember]
        [Category("Антенны")]
        [DisplayName("ЛПА 5-10,°")]
        public short LPA510 { get; set; }

        [DataMember]
        [Category(nameof(Coordinates))]
        [DisplayName(" ")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public Coord Coordinates { get; set; } = new Coord();

        [DataMember]
        [Category(nameof(Coordinates))]
        [DisplayName(" ")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public CoordDDMMSS CoordinatesDDMMSS { get; set; } = new CoordDDMMSS();

        [DataMember]
        [Browsable(false)]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public Sectors Sectors { get; set; } = new Sectors();

        [DataMember]
        [Category("Общие")]
        [DisplayName("Тип связи")]
        [PropertyOrder(7)]
        public TypeConnection TypeConnection { get; set; }

        [DataMember]
        [Category(nameof(Coordinates))]
        [DisplayName("СНС")]
        public bool IsGnssUsed { get; set; }

        [DataMember]
        [Category("ID")]
        [DisplayName("№ АСП сопряж.")]
        public int MatedStationNumber { get; set; }

        [DataMember]
        [Category("Общие")]
        [DisplayName("Адрес IP 3G/4G")]
        [PropertyOrder(4)]
        public string AddressIp3G4G { get; set; } = string.Empty;

        [DataMember]
        [Category("Общие")]
        [DisplayName("Порт 3G/4G")]
        [PropertyOrder(5)]
        public int AddressPort3G4G { get; set; }

        public MyTableASP Clone()
        {
            return new MyTableASP
            {
                Id = this.Id,
                AddressIP = AddressIP,
                AddressPort = AddressPort,
                AntHeightRec = AntHeightRec,
                AntHeightSup = AntHeightSup,
                CallSign = CallSign,
                Caption = Caption,
                Coordinates = new Coord()
                {
                    Altitude = Coordinates.Altitude,
                    Longitude = Coordinates.Longitude,
                    Latitude = Coordinates.Latitude,
                },
                CoordinatesDDMMSS = new CoordDDMMSS
                {
                    LatSign = CoordinatesDDMMSS.LatSign,
                    LonSign = CoordinatesDDMMSS.LonSign,
                    Latitude = CoordinatesDDMMSS.Latitude,
                    Longitude = CoordinatesDDMMSS.Longitude,
                    LatDegrees = CoordinatesDDMMSS.LatDegrees,
                    LonDegrees = CoordinatesDDMMSS.LonDegrees,
                    LatMinutesDDMM = CoordinatesDDMMSS.LatMinutesDDMM,
                    LonMinutesDDMM = CoordinatesDDMMSS.LonMinutesDDMM,
                    LatMinutesDDMMSS = CoordinatesDDMMSS.LatMinutesDDMMSS,
                    LonMinutesDDMMSS = CoordinatesDDMMSS.LonMinutesDDMMSS,
                    LatSeconds = CoordinatesDDMMSS.LatSeconds,
                    LonSeconds = CoordinatesDDMMSS.LonSeconds,
                    Altitude = CoordinatesDDMMSS.Altitude
                },
                IdMission = IdMission,
                Image = Image,
                IsConnect = IsConnect,
                Letters = new byte[] { },
                LPA13 = LPA13,
                LPA24 = LPA24,
                BPSS = BPSS,
                LPA57 = LPA57,
                LPA59 = LPA59,
                LPA10 = LPA10,
                LPA510 = LPA510,
                Mode = Mode,
                ISOwn = ISOwn,
                Role = Role,
                RRS1 = RRS1,
                RRS2 = RRS2,
                Type = Type,
                Sectors = Sectors.Clone(),
                TypeConnection = TypeConnection, 
                IsGnssUsed = IsGnssUsed,
                MatedStationNumber = MatedStationNumber,
                AddressIp3G4G = AddressIp3G4G,
                AddressPort3G4G = AddressPort3G4G
            };
        }
    }

    public class CoordDDMMSS : INotifyPropertyChanged
    {
        #region NotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }
        #endregion

        private LatitudeSign latSign = LatitudeSign.N;
        [DataMember]
        [DisplayName(nameof(LatSign))]
        [NotifyParentProperty(true)]
        [Category(nameof(Coord))]
        public LatitudeSign LatSign
        {
            get { return latSign; }
            set
            {
                if (latSign == value)
                    return;

                latSign = value;
                OnPropertyChanged(nameof(LatSign));
            }
        }

        private LongitudeSign lonSign = LongitudeSign.E;
        [DataMember]
        [DisplayName(nameof(LonSign))]
        [NotifyParentProperty(true)]
        [Category(nameof(Coord))]
        public LongitudeSign LonSign
        {
            get { return lonSign; }
            set
            {
                if (lonSign == value)
                    return;

                lonSign = value;
                OnPropertyChanged(nameof(LonSign));
            }
        }

        private double latitude = 0;
        [DataMember]
        [DisplayName(nameof(Latitude))]
        [NotifyParentProperty(true)]
        [Category(nameof(Coord))]
        public double Latitude
        {
            get { return latitude; }
            set
            {
                if (latitude == value)
                    return;

                latitude = value;
                OnPropertyChanged(nameof(Latitude));
            }
        }

        private double longitude = 0;
        [DataMember]
        [DisplayName(nameof(Longitude))]
        [NotifyParentProperty(true)]
        [Category(nameof(Coord))]
        public double Longitude
        {
            get { return longitude; }
            set
            {
                if (longitude == value)
                    return;

                longitude = value;
                OnPropertyChanged(nameof(Longitude));
            }
        }

        private int latDegrees = 0;
        [DataMember]
        [DisplayName(nameof(LatDegrees))]
        [NotifyParentProperty(true)]
        [Category(nameof(Coord))]
        public int LatDegrees
        {
            get { return latDegrees; }
            set
            {
                if (latDegrees == value)
                    return;

                latDegrees = value;
                OnPropertyChanged(nameof(LatDegrees));
            }
        }

        private int lonDegrees = 0;
        [DataMember]
        [DisplayName(nameof(LonDegrees))]
        [NotifyParentProperty(true)]
        [Category(nameof(Coord))]
        public int LonDegrees
        {
            get { return lonDegrees; }
            set
            {
                if (lonDegrees == value)
                    return;

                lonDegrees = value;
                OnPropertyChanged(nameof(LonDegrees));
            }
        }

        private double latMinutesDDMM = 0;
        [DataMember]
        [DisplayName(nameof(LatMinutesDDMM))]
        [NotifyParentProperty(true)]
        [Category(nameof(Coord))]
        public double LatMinutesDDMM
        {
            get { return latMinutesDDMM; }
            set
            {
                if (latMinutesDDMM == value)
                    return;

                latMinutesDDMM = value;
                OnPropertyChanged(nameof(LatMinutesDDMM));
            }
        }

        private double lonMinutesDDMM = 0;
        [DataMember]
        [DisplayName(nameof(LonMinutesDDMM))]
        [NotifyParentProperty(true)]
        [Category(nameof(Coord))]
        public double LonMinutesDDMM
        {
            get { return lonMinutesDDMM; }
            set
            {
                if (lonMinutesDDMM == value)
                    return;

                lonMinutesDDMM = value;
                OnPropertyChanged(nameof(LonMinutesDDMM));
            }
        }

        private int latMinutesDDMMSS = 0;
        [DataMember]
        [DisplayName(nameof(LatMinutesDDMMSS))]
        [NotifyParentProperty(true)]
        [Category(nameof(Coord))]
        public int LatMinutesDDMMSS
        {
            get { return latMinutesDDMMSS; }
            set
            {
                if (latMinutesDDMMSS == value)
                    return;

                latMinutesDDMMSS = value;
                OnPropertyChanged(nameof(LatMinutesDDMMSS));
            }
        }

        private int lonMinutesDDMMSS = 0;
        [DataMember]
        [DisplayName(nameof(LonMinutesDDMMSS))]
        [NotifyParentProperty(true)]
        [Category(nameof(Coord))]
        public int LonMinutesDDMMSS
        {
            get { return lonMinutesDDMMSS; }
            set
            {
                if (lonMinutesDDMMSS == value)
                    return;

                lonMinutesDDMMSS = value;
                OnPropertyChanged(nameof(LonMinutesDDMMSS));
            }
        }

        private double latSeconds = 0;
        [DataMember]
        [DisplayName(nameof(LatSeconds))]
        [NotifyParentProperty(true)]
        [Category(nameof(Coord))]
        public double LatSeconds
        {
            get { return latSeconds; }
            set
            {
                if (latSeconds == value)
                    return;

                latSeconds = value;
                OnPropertyChanged(nameof(LatSeconds));
            }
        }

        private double lonSeconds = 0;
        [DataMember]
        [DisplayName(nameof(LonSeconds))]
        [NotifyParentProperty(true)]
        [Category(nameof(Coord))]
        public double LonSeconds
        {
            get { return lonSeconds; }
            set
            {
                if (lonSeconds == value)
                    return;

                lonSeconds = value;
                OnPropertyChanged(nameof(LonSeconds));
            }
        }

        private double altitude = -1;
        [DataMember]
        [DisplayName(nameof(Altitude))]
        [NotifyParentProperty(true)]
        [Category(nameof(Coord))]
        public double Altitude
        {
            get { return altitude; }
            set
            {
                if (altitude == value)
                    return;

                altitude = value;
                OnPropertyChanged(nameof(Altitude));
            }
        }
    }
}
