﻿using ModelsTablesDBLib;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls.WpfPropertyGrid;
using System.Windows.Media.Imaging;
using TableEvents;
using ValuesCorrectLib;

namespace ASPControl
{
    /// <summary>
    /// Логика взаимодействия для ASPProperty.xaml
    /// </summary>
    public partial class ASPProperty : Window
    {
        public event EventHandler<MyTableASP> OnAddRecordPG = (object sender, MyTableASP data) => { };
        public event EventHandler<MyTableASP> OnChangeRecordPG = (object sender, MyTableASP data) => { };

        private ObservableCollection<MyTableASP> collectionTemp;
        public MyTableASP TableASP { get; private set; }

        public ASPProperty(ObservableCollection<MyTableASP> collectionASP)
        {
            try
            {
                InitializeComponent();

                InitEditors();

                collectionTemp = collectionASP;
                TableASP = new MyTableASP();

                propertyGrid.SelectedObject = TableASP;

                propertyGrid.Properties[nameof(MyTableASP.Coordinates)].IsBrowsable = false;
                propertyGrid.Properties[nameof(MyTableASP.Id)].IsReadOnly = false;

                Title = SMeaning.meaningAddRecord;
                Icon = new BitmapImage(new Uri(@"pack://application:,,,/"
                                    + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                                    + ";component/"
                                    + "Resources/AddRec.ico", UriKind.Absolute));

                InitProperty();
                propertyGrid.Categories.FirstOrDefault(t => t.Name == "Coordinates").HeaderCategoryName = SMeaning.meaningCoord;
                //ChangeCategories();

            }
            catch { }
        }

        public ASPProperty(ObservableCollection<MyTableASP> collectionASP, MyTableASP ASP)
        {
            try
            {
                InitializeComponent();

                InitEditors();

                collectionTemp = collectionASP;
                TableASP = ASP;

               
                TableASP.CoordinatesDDMMSS.LatSign = TableASP.Coordinates.Latitude < 0 ? LatitudeSign.S : LatitudeSign.N;
                TableASP.CoordinatesDDMMSS.LonSign = TableASP.Coordinates.Longitude < 0 ? LongitudeSign.W : LongitudeSign.E;
                TableASP.CoordinatesDDMMSS.Altitude = TableASP.Coordinates.Altitude;

                if (TableASP.Coordinates.Latitude != -1)
                {
                    TableASP.Coordinates.Latitude = Math.Round(TableASP.Coordinates.Latitude, 6);
                    TableASP.Coordinates.Latitude = TableASP.Coordinates.Latitude < 0 ? TableASP.Coordinates.Latitude * -1 : TableASP.Coordinates.Latitude;
                }
                if (TableASP.Coordinates.Longitude != -1)
                {
                    TableASP.Coordinates.Longitude = Math.Round(TableASP.Coordinates.Longitude, 6);
                    TableASP.Coordinates.Longitude = TableASP.Coordinates.Longitude < 0 ? TableASP.Coordinates.Longitude * -1 : TableASP.Coordinates.Longitude;
                }

                switch (PropViewCoords.ViewCoords)
                {
                    case 1: // format "DD.dddddd"

                        TableASP.CoordinatesDDMMSS.Latitude = TableASP.Coordinates.Latitude;
                        TableASP.CoordinatesDDMMSS.Longitude = TableASP.Coordinates.Longitude;
                        break;

                    case 2: // format "DD MM.mmmm"

                        TableASP.CoordinatesDDMMSS.LatDegrees = (int)Math.Truncate(TableASP.Coordinates.Latitude);
                        TableASP.CoordinatesDDMMSS.LatMinutesDDMM = Math.Round((TableASP.Coordinates.Latitude - Math.Truncate(TableASP.Coordinates.Latitude)) * 60, 4);

                        TableASP.CoordinatesDDMMSS.LonDegrees = (int)Math.Truncate(TableASP.Coordinates.Longitude);
                        TableASP.CoordinatesDDMMSS.LonMinutesDDMM = Math.Round((TableASP.Coordinates.Longitude - Math.Truncate(TableASP.Coordinates.Longitude)) * 60, 4);
                        break;

                    case 3: // format "DD MM SS.ss"

                        double latD = Math.Truncate(TableASP.Coordinates.Latitude);
                        double latM = Math.Truncate((TableASP.Coordinates.Latitude - latD) * 60);
                        double latS = Math.Round((((TableASP.Coordinates.Latitude - latD) * 60) - latM) * 60, 2);

                        TableASP.CoordinatesDDMMSS.LatDegrees = (int)latD;
                        TableASP.CoordinatesDDMMSS.LatMinutesDDMMSS = (int)latM;
                        TableASP.CoordinatesDDMMSS.LatSeconds = latS;

                        double lonD = Math.Truncate(TableASP.Coordinates.Longitude);
                        double lonM = Math.Truncate((TableASP.Coordinates.Longitude - lonD) * 60);
                        double lonS = Math.Round((((TableASP.Coordinates.Longitude - lonD) * 60) - lonM) * 60, 2);

                        TableASP.CoordinatesDDMMSS.LonDegrees = (int)lonD;
                        TableASP.CoordinatesDDMMSS.LonMinutesDDMMSS = (int)lonM;
                        TableASP.CoordinatesDDMMSS.LonSeconds = lonS;
                        break;

                    default:
                        break;
                }

                propertyGrid.SelectedObject = TableASP;

                propertyGrid.Properties[nameof(MyTableASP.Coordinates)].IsBrowsable = false;

                // ОПУ -----------------------------

                // ----------------------------- ОПУ

                propertyGrid.Properties[nameof(MyTableASP.Id)].IsReadOnly = true;

                Title = SMeaning.meaningChangeRecord;
                Icon = new BitmapImage(new Uri(@"pack://application:,,,/"
                                    + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                                    + ";component/"
                                    + "Resources/ChangeRec.ico", UriKind.Absolute));

                InitProperty();
                propertyGrid.Categories.FirstOrDefault(t => t.Name == "Coordinates").HeaderCategoryName = SMeaning.meaningCoord;
                //ChangeCategories();
            }
            catch { }
        }

        public ASPProperty()
        {
            try
            {
                InitializeComponent();

                InitEditors();
                //ChangeCategories();
                propertyGrid.Categories.FirstOrDefault(t => t.Name == "Coordinates").HeaderCategoryName = SMeaning.meaningCoord;
                InitProperty();
            }

            catch { }
        }

        private void ButtonNoApply_Click(object sender, RoutedEventArgs e)
        {
            Close();
            PropIsRecASP.IsRecAdd = false;
            PropIsRecASP.IsRecChange = false;

            //DialogResult = false;
        }

        private void ButtonApply_Click(object sender, RoutedEventArgs e)
        {
            if ((MyTableASP)propertyGrid.SelectedObject != null)
            {
                if (PropIsRecASP.IsRecAdd)
                {
                    OnAddRecordPG?.Invoke(sender, (MyTableASP)propertyGrid.SelectedObject);
                }

                if (PropIsRecASP.IsRecChange)
                {
                    OnChangeRecordPG?.Invoke(sender, (MyTableASP)propertyGrid.SelectedObject);
                }

                Close();
            }
            //DialogResult = true;
        }

        private void InitProperty()
        {
            foreach (var property in propertyGrid.Properties)
            {
                try
                {
                    if (property.IsBrowsable == false)
                        continue;
                    if (property.PropertyValue.SubProperties.Count != 0)
                    {
                        foreach (var subProperty in property.PropertyValue.SubProperties)
                            subProperty.PropertyValue.PropertyValueException += PropertyGridSubException;
                        continue;
                    }
                    property.PropertyValue.PropertyValueException += PropertyGridException;
                }
                catch(Exception ex)
                { MessageBox.Show(ex.Message); }
            }
        }

        private void PropertyGridSubException(object sender, ValueExceptionEventArgs e)
        {
            propertyGrid.Properties[(sender as PropertyItemValue).ParentProperty.ParentValue.ParentProperty.Name].SetValue((sender as PropertyItemValue).Value);
        }

        private void PropertyGridException(object sender, ValueExceptionEventArgs e)
        {
            propertyGrid.Properties[(sender as PropertyItemValue).ParentProperty.Name].SetValue((sender as PropertyItemValue).Value);
        }

        private void ChangeCategories()
        {

            propertyGrid.Categories.FirstOrDefault(t => t.Name == "Coordinates").HeaderCategoryName = "Coordinates";

            //////////////////////// для перевода на английский
            propertyGrid.Categories.FirstOrDefault(t => t.Name == "Общие").HeaderCategoryName = "General";
            propertyGrid.Categories.FirstOrDefault(t => t.Name == "Антенны").HeaderCategoryName = "Antennas";
            propertyGrid.Categories.FirstOrDefault(t => t.Name == "Карта").HeaderCategoryName = "Map";

            propertyGrid.Properties[nameof(TableASP.Id)].DisplayName = "№ AJS";
            propertyGrid.Properties[nameof(TableASP.ISOwn)].DisplayName = "Own";

            propertyGrid.Properties[nameof(TableASP.AddressIP)].DisplayName = "IP address";
            propertyGrid.Properties[nameof(TableASP.AddressPort)].DisplayName = "Port";
            propertyGrid.Properties[nameof(TableASP.CallSign)].DisplayName = "Call sign";
            propertyGrid.Properties[nameof(TableASP.Role)].DisplayName = "Role";
            propertyGrid.Properties[nameof(TableASP.TypeConnection)].DisplayName = "Type connect";

            propertyGrid.Properties[nameof(TableASP.AntHeightRec)].DisplayName = "Height RI, m";
            propertyGrid.Properties[nameof(TableASP.AntHeightSup)].DisplayName = "Height J, m";
            propertyGrid.Properties[nameof(TableASP.LPA13)].DisplayName = "LPA 1,3,°";
            propertyGrid.Properties[nameof(TableASP.LPA24)].DisplayName = "LPA 2,4,°";
            propertyGrid.Properties[nameof(TableASP.LPA510)].DisplayName = "LPA 5-10,°";
            propertyGrid.Properties[nameof(TableASP.RRS1)].DisplayName = "RRC 1,°";
            propertyGrid.Properties[nameof(TableASP.RRS2)].DisplayName = "RRC 2,°";

            propertyGrid.Properties[nameof(TableASP.IsGnssUsed)].DisplayName = "SNS";

            propertyGrid.Properties[nameof(TableASP.Caption)].DisplayName = "Signature";
            propertyGrid.Properties[nameof(TableASP.Image)].DisplayName = "Sign";


            ///////////////////////////
        }

        public void SetLanguagePropertyGrid(Languages language)
        {
            LoadTranslatorPropertyGrid(language);
            TranslatorTables.ChangeLanguagePropertyGrid(language, propertyGrid);
        }

        public void SetLanguagePropertyGrid(Languages language, ResourceDictionary dict)
        {
            LoadTranslatorPropertyGrid(dict);
            TranslatorTables.ChangeLanguagePropertyGrid(language, propertyGrid);
        }

        private void LoadTranslatorPropertyGrid(Languages language)
        {
            ResourceDictionary dict = new ResourceDictionary();
            try
            {
                switch (language)
                {
                    case Languages.Eng:

                        dict.Source = new Uri("/GrozaFuture;component/Languages/TranslatorTables/TranslatorTables.EN.xaml",
                                      UriKind.Relative);
                        break;
                    case Languages.Rus:
                        dict.Source = new Uri("/GrozaFuture;component/Languages/TranslatorTables/TranslatorTables.RU.xaml",
                                            UriKind.Relative);
                        break;
                    case Languages.Azr:
                        dict.Source = new Uri("/GrozaFuture;component/Languages/TranslatorTables/TranslatorTables.AZ.xaml",
                                            UriKind.Relative);
                        break;
                    default:
                        dict.Source = new Uri("/GrozaFuture;component/Languages/TranslatorTables/TranslatorTables.RU.xaml",
                                          UriKind.Relative);
                        break;
                }

                // Отладка ---------------------------------------------------------------------------------------
                //switch (language)
                //{
                //    case Languages.Eng:

                //        dict.Source = new Uri("/ProjectTestTables;component/TranslatorTables.EN.xaml",
                //                      UriKind.Relative);
                //        break;
                //    case Languages.Rus:
                //        dict.Source = new Uri("/ProjectTestTables;component/TranslatorTables.RU.xaml",
                //                            UriKind.Relative);
                //        break;
                //    default:
                //        dict.Source = new Uri("/ProjectTestTables;component/TranslatorTables.RU.xaml",
                //                          UriKind.Relative);
                //        break;
                //}
                //--------------------------------------------------------------------------------------- Отладка

                this.Resources.MergedDictionaries.Add(dict);
            }
            catch (Exception)
            { }
        }

        private void LoadTranslatorPropertyGrid(ResourceDictionary dict)
        {
            try
            {
                this.Resources.MergedDictionaries.Add(dict);
            }
            catch (Exception)
            { }
        }

        private void InitEditors()
        {
            //propertyGrid.Editors.Add(new CoordinatesASPEditor(nameof(Coord), typeof(TableASP)));

            switch (PropViewCoords.ViewCoords)
            {
                case 1: // format "DD.dddddd"
                    propertyGrid.Editors.Add(new CoordinatesDDASPEditor(nameof(TableASP.CoordinatesDDMMSS), typeof(MyTableASP)));
                    propertyGrid.Editors.Add(new RoleDDASPEditor(nameof(TableASP.Role), typeof(MyTableASP)));
                    break;

                case 2: // format "DD MM.mmmm"
                    propertyGrid.Editors.Add(new CoordinatesDDMMASPEditor(nameof(TableASP.CoordinatesDDMMSS), typeof(MyTableASP)));
                    propertyGrid.Editors.Add(new RoleDDMMASPEditor(nameof(TableASP.Role), typeof(MyTableASP)));
                    break;

                case 3: // format "DD MM SS.ss"
                    propertyGrid.Editors.Add(new CoordinatesDDMMSSASPEditor(nameof(TableASP.CoordinatesDDMMSS), typeof(MyTableASP)));
                    propertyGrid.Editors.Add(new RoleDDMMSSASPEditor(nameof(TableASP.Role), typeof(MyTableASP)));
                    break;

                default:
                    break;
            }

           // propertyGrid.Editors.Add(new CoordinatesASPEditor(nameof(tableASP.Coordinates), typeof(TableASP)));
            //propertyGrid.Editors.Add(new RoleASPEditor(nameof(tableASP.Role), typeof(MyTableASP)));

        }

        private void gridProperty_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Enter)
            {
                if ((MyTableASP)propertyGrid.SelectedObject != null)
                {
                    if (PropIsRecASP.IsRecAdd)
                    {
                        OnAddRecordPG?.Invoke(sender, (MyTableASP)propertyGrid.SelectedObject);
                    }

                    if (PropIsRecASP.IsRecChange)
                    {
                        OnChangeRecordPG?.Invoke(sender, (MyTableASP)propertyGrid.SelectedObject);
                    }

                    Close();
                }
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            PropIsRecASP.IsRecAdd = false;
            PropIsRecASP.IsRecChange = false;
        }
    }
}
