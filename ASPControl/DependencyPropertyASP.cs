﻿using ModelsTablesDBLib;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.WpfPropertyGrid;
using TableEvents;

namespace ASPControl
{
    public partial class UserControlASP : UserControl, INotifyPropertyChanged
    {
        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }
        #endregion

        #region FormatViewCoord

        public static readonly DependencyProperty ViewCoordProperty = DependencyProperty.Register("ViewCoord", typeof(byte), typeof(UserControlASP),
                                                                       new PropertyMetadata((byte)1, new PropertyChangedCallback(ViewCoordChanged)));

        public byte ViewCoord
        {
            get { return (byte)GetValue(ViewCoordProperty); }
            set { SetValue(ViewCoordProperty, value); }
        }

        private static void ViewCoordChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                UserControlASP userControlASP = (UserControlASP)d;
                userControlASP.UpdateFormatCoords();
            }
            catch
            { }

        }

        /// <summary>
        /// Обновить вид координат в таблице
        /// </summary>
        private void UpdateFormatCoords()
        {
            try
            {
                if (TempListASP == null)
                {
                    return;
                }

                PropViewCoords.ViewCoords = ViewCoord;

                UpdateASPs(TempListASP);
            }
            catch { }
        }
        #endregion

        #region ОПУ1

        public static readonly DependencyProperty StateOPU1Property = DependencyProperty.Register("StateOPU1", typeof(bool), typeof(UserControlASP),
                                                                     new PropertyMetadata(false, new PropertyChangedCallback(StateOPU1Changed)));

        public bool StateOPU1
        {
            get { return (bool)GetValue(StateOPU1Property); }
            set { SetValue(StateOPU1Property, value); }
        }

        private static void StateOPU1Changed(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                UserControlASP userControlASP = (UserControlASP)d;
                userControlASP.ColumnVisible();
            }
            catch
            { }

        }

        public static readonly DependencyProperty OPU1Property = DependencyProperty.Register("OPU1", typeof(DeviceRSD), typeof(UserControlASP),
                                                                       new PropertyMetadata(DeviceRSD.RRS_PC, new PropertyChangedCallback(OPU1Changed)));

        public DeviceRSD OPU1
        {
            get { return (DeviceRSD)GetValue(OPU1Property); }
            set { SetValue(OPU1Property, value); }
        }

        private static void OPU1Changed(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                UserControlASP userControlASP = (UserControlASP)d;
                userControlASP.ColumnVisible();
            }
            catch
            { }

        }

        #endregion

        #region ОПУ2

        public static readonly DependencyProperty StateOPU2Property = DependencyProperty.Register("StateOPU2", typeof(bool), typeof(UserControlASP),
                                                             new PropertyMetadata(false, new PropertyChangedCallback(StateOPU2Changed)));

        public bool StateOPU2
        {
            get { return (bool)GetValue(StateOPU2Property); }
            set { SetValue(StateOPU2Property, value); }
        }

        private static void StateOPU2Changed(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                UserControlASP userControlASP = (UserControlASP)d;
                userControlASP.ColumnVisible();
            }
            catch
            { }

        }

        public static readonly DependencyProperty OPU2Property = DependencyProperty.Register("OPU2", typeof(DeviceRSD), typeof(UserControlASP),
                                                                       new PropertyMetadata(DeviceRSD.RRS_Lincked, new PropertyChangedCallback(OPU2Changed)));

        public DeviceRSD OPU2
        {
            get { return (DeviceRSD)GetValue(OPU2Property); }
            set { SetValue(OPU2Property, value); }
        }

        private static void OPU2Changed(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                UserControlASP userControlASP = (UserControlASP)d;
                userControlASP.ColumnVisible();
            }
            catch
            { }

        }
        #endregion

        #region ОПУ3

        public static readonly DependencyProperty StateOPU3Property = DependencyProperty.Register("StateOPU3", typeof(bool), typeof(UserControlASP),
                                                          new PropertyMetadata(false, new PropertyChangedCallback(StateOPU3Changed)));

        public bool StateOPU3
        {
            get { return (bool)GetValue(StateOPU3Property); }
            set { SetValue(StateOPU3Property, value); }
        }

        private static void StateOPU3Changed(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                UserControlASP userControlASP = (UserControlASP)d;
                userControlASP.ColumnVisible();
            }
            catch
            { }

        }

        public static readonly DependencyProperty OPU3Property = DependencyProperty.Register("OPU3", typeof(DeviceRSD), typeof(UserControlASP),
                                                                       new PropertyMetadata(DeviceRSD.LPA_5_10, new PropertyChangedCallback(OPU3Changed)));

        public DeviceRSD OPU3
        {
            get { return (DeviceRSD)GetValue(OPU3Property); }
            set { SetValue(OPU3Property, value); }
        }

        private static void OPU3Changed(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                UserControlASP userControlASP = (UserControlASP)d;
                userControlASP.ColumnVisible();
            }
            catch
            { }

        }
        #endregion

        private void CheckDeviceRSD(DeviceRSD OPU)
        {
            switch (OPU)
            {
                case DeviceRSD.RRS_Lincked:
                    RRS1.Visibility = Visibility.Visible;
                    break;

                case DeviceRSD.RRS_PC:
                    RRS2.Visibility = Visibility.Visible;
                    break;

                case DeviceRSD.SSTU:
                    BPSS.Visibility = Visibility.Visible;
                    break;

                case DeviceRSD.LPA_5_10:
                    LPA510.Visibility = Visibility.Visible;
                    break;

                case DeviceRSD.LPA_5_7:
                    LPA57.Visibility = Visibility.Visible;
                    break;

                case DeviceRSD.LPA_5_9:
                    LPA59.Visibility = Visibility.Visible;
                    break;

                case DeviceRSD.LPA_10б:
                    LPA10.Visibility = Visibility.Visible;
                    break;
            }
        }

        /// <summary>
        /// Обновить заголовки в таблице
        /// </summary>
        private void ColumnVisible()
        {
            try
            {
                RRS1.Visibility = Visibility.Hidden;
                RRS2.Visibility = Visibility.Hidden;
                BPSS.Visibility = Visibility.Hidden;
                LPA57.Visibility = Visibility.Hidden;
                LPA59.Visibility = Visibility.Hidden;
                LPA510.Visibility = Visibility.Hidden;
                LPA10.Visibility = Visibility.Hidden;

                if (StateOPU1)
                {
                    CheckDeviceRSD(OPU1);
                }
                if (StateOPU2)
                {
                    CheckDeviceRSD(OPU2);
                }
                if (StateOPU3)
                {
                    CheckDeviceRSD(OPU3);
                }
            }
            catch { }
        }
    }
}
