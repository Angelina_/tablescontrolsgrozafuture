﻿using Bearing;
using ModelsTablesDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using TableEvents;
using ValuesConverter;
using ValuesCorrectLib;

namespace ASPControl
{
    public partial class UserControlASP : UserControl
    {
        public List<TableASP> TempListASP { get; set; }
        /// <summary>
        /// Обновить все записи в контроле
        /// </summary>m
        /// <param name="listASP"></param>
        public void UpdateASPs(List<TableASP> listASP)
        {
            try
            {
                TempListASP = listASP;

                if (listASP == null)
                    return;

                ((GlobalASP)dgvASP.DataContext).CollectionASP.Clear();

                List<MyTableASP> list = TableASPToMyTableASP(listASP);

                for (int i = 0; i < list.Count; i++)
                {
                    ((GlobalASP)dgvASP.DataContext).CollectionASP.Add(list[i]);
                }

                dgvASP.ItemsSource = ((GlobalASP)dgvASP.DataContext).CollectionASP;

                AddEmptyRows();

                int ind = ((GlobalASP)dgvASP.DataContext).CollectionASP.ToList()
                    .FindIndex(x => x.Id == PropNumberASP.SelectedNumASP);
                if (ind != -1)
                {
                    dgvASP.SelectedIndex = ind;
                }
                else
                {
                    dgvASP.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// Обновить все записи в контроле
        /// </summary>
        /// <param name="listASP"></param>
        public void UpdateASPsGNSS(List<TableASP> listASP, Coord coord, short gnssInaccuracy)
        {
            try
            {
                if (listASP == null || listASP.Count == 0)
                    return;

                ((GlobalASP)dgvASP.DataContext).CollectionASP.Clear();

                List<MyTableASP> list = TableASPToMyTableASP(listASP);

                for (int i = 0; i < listASP.Count; i++)
                {
                    if (listASP[i].ISOwn && listASP[i].IsGnssUsed)
                    {
                        double Distance = ClassBearing.f_D_2Points(list[i].Coordinates.Latitude, list[i].Coordinates.Longitude, coord.Latitude, coord.Longitude, 1); 
                        if(Distance > gnssInaccuracy)
                        {
                            list[i].Coordinates.Latitude = coord.Latitude;
                            list[i].Coordinates.Longitude = coord.Longitude;
                            ((GlobalASP)dgvASP.DataContext).CollectionASP.Add(list[i]);
                        }
                        else
                        {
                            ((GlobalASP)dgvASP.DataContext).CollectionASP.Add(list[i]);
                        }
                    }
                    else
                    {
                        ((GlobalASP)dgvASP.DataContext).CollectionASP.Add(list[i]);
                    }
                }
                dgvASP.ItemsSource = ((GlobalASP)dgvASP.DataContext).CollectionASP;

                AddEmptyRows();

                int ind = ((GlobalASP)dgvASP.DataContext).CollectionASP.ToList().FindIndex(x => x.Id == PropNumberASP.SelectedNumASP);
                if (ind != -1)
                {
                    dgvASP.SelectedIndex = ind;
                }
                else
                {
                    dgvASP.SelectedIndex = 0;
                }
            }
            catch { }
        }

        /// <summary>
        /// Обновить АСП
        /// </summary>
        /// <param name="listASP"></param>
        public TableASP UpdateRowASPsGNSS(List<TableASP> listASP, Coord coord, short gnssInaccuracy)
        {
            try
            {
                if (listASP == null || listASP.Count == 0)
                    return null;

                //List<MyTableASP> list = TableASPToMyTableASP(listASP);
                List<TableASP> list = new List<TableASP>(listASP);
                for (int i = 0; i < list.Count; i++)
                {
                    if (list[i].ISOwn && listASP[i].IsGnssUsed)
                    {
                        double Distance = ClassBearing.f_D_2Points(list[i].Coordinates.Latitude, list[i].Coordinates.Longitude, coord.Latitude, coord.Longitude, 1);
                        if (Distance > gnssInaccuracy)
                        {
                            list[i].Coordinates.Latitude = coord.Latitude;
                            list[i].Coordinates.Longitude = coord.Longitude;
                            return list[i];
                        }
                    }
                }
            }
            catch { }

            return null;
        }

        /// <summary>
        /// Добавить несколько записей в контрол
        /// </summary>
        /// <param name="listASP"></param>
        public void AddASPs(List<TableASP> listASP)
        {
            try
            {
                DeleteEmptyRows();

                List<MyTableASP> list = TableASPToMyTableASP(listASP);

                for (int i = 0; i < listASP.Count; i++)
                {
                    ((GlobalASP)dgvASP.DataContext).CollectionASP.Add(list[i]);
                }

                AddEmptyRows();
            }
            catch { }
        }

        /// <summary>
        /// Добавить одну запись в контрол
        /// </summary>
        /// <param name="listASP"></param>
        public void AddASP(TableASP tableASP)
        {
            try
            {
                DeleteEmptyRows();

                MyTableASP table = TableASPToMyTableASP(tableASP);

                ((GlobalASP)dgvASP.DataContext).CollectionASP.Add(table);

                AddEmptyRows();
            }
            catch { }
        }

        /// <summary>
        /// Изменить одну запись в контроле
        /// </summary>
        /// <param name="ind"> индекс записи, которую надо заменить </param>
        /// <param name="replaceASP"> запись, на которую надо заменить </param>
        public void ChangeASP(int id, TableASP replaceASP)
        {
            try
            {
                MyTableASP table = TableASPToMyTableASP(replaceASP);

                int ind = ((GlobalASP)dgvASP.DataContext).CollectionASP.ToList().FindIndex(x => x.Id == table.Id);
                if (ind != -1)
                {
                    ((GlobalASP)dgvASP.DataContext).CollectionASP.RemoveAt(ind);
                    ((GlobalASP)dgvASP.DataContext).CollectionASP.Insert(ind, table);
                }
            }
            catch { }
        }

        /// <summary>
        /// Удалить одну запись из контрола
        /// </summary>
        public void DeleteASP(TableASP tableASP)
        {
            try
            {
                int ind = ((GlobalASP)dgvASP.DataContext).CollectionASP.ToList().FindIndex(x => x.Id == tableASP.Id);
                if (ind != -1)
                {
                    ((GlobalASP)dgvASP.DataContext).CollectionASP.RemoveAt(ind);

                    AddEmptyRows();
                }
            }
            catch { }
        }

        /// <summary>
        /// Удалить все записи из контрола
        /// </summary>
        public void ClearASP()
        {
            try
            {
                ((GlobalASP)dgvASP.DataContext).CollectionASP.Clear();

                AddEmptyRows();
            }
            catch { }
        }

        /// <summary>
        /// Добавить пустые строки в таблицу
        /// </summary>
        private void AddEmptyRows()
        {
            //try
            //{
                int сountRowsAll = dgvASP.Items.Count; // количество всех строк в таблице
                double hs = 23; // высота строки
                double ah = dgvASP.ActualHeight; // визуализированная высота dataGrid
                double chh = dgvASP.ColumnHeaderHeight; // высота заголовка

                int countRows = Convert.ToInt32(Math.Floor(Convert.ToDouble(ah) - Convert.ToDouble(chh)) / hs); // количество строк, которое помещается в область видимости = (высота таблицы - заголовок таблицы) / высота строки

                int count = сountRowsAll - countRows; // сколько пустых строк нужно удалить
                int index = -1;
                for (int i = 0; i < count; i++)
                {
                    // Удалить пустые строки в dgv
                    index = ((GlobalASP)dgvASP.DataContext).CollectionASP.ToList().FindIndex(x => x.IdMission == -1);
                    if (index != -1)
                    {
                        ((GlobalASP)dgvASP.DataContext).CollectionASP.RemoveAt(index);
                    }
                }

                List<MyTableASP> list = new List<MyTableASP>();
                for (int i = 0; i < countRows - сountRowsAll; i++)
                {
                    MyTableASP strASP = new MyTableASP
                    {
                        Id = -2,
                        Coordinates = new Coord() { Latitude = -2, Longitude = -2, Altitude = -2 },
                        LPA13 = -2,
                        LPA24 = -2,
                        LPA510 = -2,
                        BPSS = -2,
                        LPA57 = -2,
                        LPA59 = -2,
                        LPA10 = -2,
                        RRS1 = -2,
                        RRS2 = -2,
                        IsConnect = Led.Empty,
                        Letters = new byte[] { 2, 2, 2, 2, 2, 2, 2, 2, 2, 2 },
                        AntHeightRec = 255,
                        AntHeightSup = 255,
                        Mode = 3,
                        Role = RoleStation.Autonomic,
                        AddressIP = string.Empty,

                        Caption = string.Empty,
                        Image = string.Empty,
                        Type = 255,
                        IdMission = -2,
                        CallSign = string.Empty,
                        MatedStationNumber = -2,
                        AddressIp3G4G = string.Empty,
                        AddressPort3G4G = -2
                    };

                    list.Add(strASP);
                }

                for (int i = 0; i < list.Count; i++)
                {
                    ((GlobalASP)dgvASP.DataContext).CollectionASP.Add(list[i]);
                }
            //}
            //catch(Exception ex) { MessageBox.Show(ex.Message); }
        }

        /// <summary>
        /// Удалить пустые строки из таблицы
        /// </summary>
        private void DeleteEmptyRows()
        {
            try
            {
                int countEmptyRows = ((GlobalASP)dgvASP.DataContext).CollectionASP.Count(s => s.Id < 0);
                int countAllRows = ((GlobalASP)dgvASP.DataContext).CollectionASP.Count;
                int iCount = countAllRows - countEmptyRows;
                for (int i = iCount; i < countAllRows; i++)
                {
                    ((GlobalASP)dgvASP.DataContext).CollectionASP.RemoveAt(iCount);
                }
            }
            catch { }
        }

        /// <summary>
        /// Преобразование TableASP к MyTableASP
        /// </summary>
        /// <param name="listASP"></param>
        /// <returns></returns>
        private List<MyTableASP> TableASPToMyTableASP(List<TableASP> listASP)
        {
            List<MyTableASP> list = new List<MyTableASP>();
            for (int i = 0; i < listASP.Count; i++)
            {
                MyTableASP table = new MyTableASP();

                double minutesLat = (listASP[i].Coordinates.Latitude - Math.Truncate(listASP[i].Coordinates.Latitude)) * 60;
                double minutesLon = (listASP[i].Coordinates.Longitude - Math.Truncate(listASP[i].Coordinates.Longitude)) * 60;

                table.Id = listASP[i].Id;
                table.AddressIP = listASP[i].AddressIP;
                table.AddressPort = listASP[i].AddressPort;
                table.AntHeightRec = listASP[i].AntHeightRec;
                table.AntHeightSup = listASP[i].AntHeightSup;
                table.CallSign = listASP[i].CallSign;
                table.Caption = listASP[i].Caption;
                table.Coordinates.Altitude = listASP[i].Coordinates.Altitude;
                table.Coordinates.Latitude = listASP[i].Coordinates.Latitude;
                table.Coordinates.Longitude = listASP[i].Coordinates.Longitude;
                table.CoordinatesDDMMSS.LatSign = listASP[i].Coordinates.Latitude < 0 ? LatitudeSign.S : LatitudeSign.N;
                table.CoordinatesDDMMSS.LonSign = listASP[i].Coordinates.Longitude < 0 ? LongitudeSign.W : LongitudeSign.E;
                table.CoordinatesDDMMSS.Latitude = listASP[i].Coordinates.Latitude;
                table.CoordinatesDDMMSS.Latitude = listASP[i].Coordinates.Latitude;
                table.CoordinatesDDMMSS.Altitude = listASP[i].Coordinates.Altitude;
                table.CoordinatesDDMMSS.Latitude = listASP[i].Coordinates.Latitude;
                table.CoordinatesDDMMSS.Longitude = listASP[i].Coordinates.Longitude;
                table.CoordinatesDDMMSS.LatDegrees = (int)Math.Truncate(listASP[i].Coordinates.Latitude);
                table.CoordinatesDDMMSS.LonDegrees = (int)Math.Truncate(listASP[i].Coordinates.Longitude);
                table.CoordinatesDDMMSS.LatMinutesDDMM = Math.Round((listASP[i].Coordinates.Latitude - Math.Truncate(listASP[i].Coordinates.Latitude)) * 60, 4);
                table.CoordinatesDDMMSS.LonMinutesDDMM = Math.Round((listASP[i].Coordinates.Longitude - Math.Truncate(listASP[i].Coordinates.Longitude)) * 60, 4);
                table.CoordinatesDDMMSS.LatMinutesDDMMSS = (int)((listASP[i].Coordinates.Latitude - Math.Truncate(listASP[i].Coordinates.Latitude)) * 60);
                table.CoordinatesDDMMSS.LonMinutesDDMMSS = (int)((listASP[i].Coordinates.Longitude - Math.Truncate(listASP[i].Coordinates.Longitude)) * 60);
                table.CoordinatesDDMMSS.LatSeconds = Math.Round((minutesLat - Math.Truncate(minutesLat)) * 60, 2);
                table.CoordinatesDDMMSS.LonSeconds = Math.Round((minutesLon - Math.Truncate(minutesLon)) * 60, 2);
                table.CoordinatesDDMMSS.Altitude = listASP[i].Coordinates.Altitude;
                table.IdMission = listASP[i].IdMission;
                table.Image = listASP[i].Image;
                table.IsConnect = listASP[i].IsConnect;
                table.Letters = listASP[i].Letters;
                table.LPA13 = listASP[i].LPA13;
                table.LPA24 = listASP[i].LPA24;
                table.BPSS = listASP[i].BPSS;
                table.LPA57 = listASP[i].LPA57;
                table.LPA59 = listASP[i].LPA59;
                table.LPA10 = listASP[i].LPA10;
                table.LPA510 = listASP[i].LPA510;
                table.Mode = listASP[i].Mode;
                table.ISOwn = listASP[i].ISOwn;
                table.Role = listASP[i].Role;
                table.RRS1 = listASP[i].RRS1;
                table.RRS2 = listASP[i].RRS2;
                table.Type = listASP[i].Type;
                table.Sectors = listASP[i].Sectors;
                table.TypeConnection = listASP[i].TypeConnection;
                table.IsGnssUsed = listASP[i].IsGnssUsed;
                table.MatedStationNumber = listASP[i].MatedStationNumber;
                table.AddressIp3G4G = listASP[i].AddressIp3G4G;
                table.AddressPort3G4G = listASP[i].AddressPort3G4G;

                list.Add(table);
            }

            return list;
        }

        /// <summary>
        /// Преобразование MyTableASP к TableASP
        /// </summary>
        /// <param name="tableASP"></param>
        /// <returns> List<TableASP> </returns>
        private TableASP MyTableASPToTableASP(MyTableASP tableASP)
        {
            TableASP table = new TableASP();

            table.Id = tableASP.Id;
            table.AddressIP = tableASP.AddressIP;
            table.AddressPort = tableASP.AddressPort;
            table.AntHeightRec = tableASP.AntHeightRec;
            table.AntHeightSup = tableASP.AntHeightSup;
            table.CallSign = tableASP.CallSign;
            table.Caption = tableASP.Caption;

            switch (PropViewCoords.ViewCoords)
            {
                case 1: // format "DD.dddddd"
                    table.Coordinates.Altitude = tableASP.CoordinatesDDMMSS.Altitude;
                    table.Coordinates.Latitude = tableASP.CoordinatesDDMMSS.Latitude;
                    table.Coordinates.Longitude = tableASP.CoordinatesDDMMSS.Longitude;
                    break;

                case 2: // format "DD MM.mmmm"
                    table.Coordinates.Altitude = tableASP.CoordinatesDDMMSS.Altitude;
                    table.Coordinates.Latitude = Math.Round(tableASP.CoordinatesDDMMSS.LatDegrees + ((double)tableASP.CoordinatesDDMMSS.LatMinutesDDMM / 60), 6);
                    table.Coordinates.Longitude = Math.Round(tableASP.CoordinatesDDMMSS.LonDegrees + ((double)tableASP.CoordinatesDDMMSS.LonMinutesDDMM / 60), 6);
                    break;

                case 3: // format "DD MM SS.ss"

                    table.Coordinates.Altitude = tableASP.CoordinatesDDMMSS.Altitude;
                    table.Coordinates.Latitude = Math.Round(tableASP.CoordinatesDDMMSS.LatDegrees + ((double)tableASP.CoordinatesDDMMSS.LatMinutesDDMMSS / 60) + (tableASP.CoordinatesDDMMSS.LatSeconds / 3600), 6);
                    table.Coordinates.Longitude = Math.Round(tableASP.CoordinatesDDMMSS.LonDegrees + ((double)tableASP.CoordinatesDDMMSS.LonMinutesDDMMSS / 60) + (tableASP.CoordinatesDDMMSS.LonSeconds / 3600), 6);
                    break;
                default:
                    break;
            }

            table.Coordinates.Latitude = tableASP.CoordinatesDDMMSS.LatSign == LatitudeSign.S ? table.Coordinates.Latitude * -1 : table.Coordinates.Latitude;
            table.Coordinates.Longitude = tableASP.CoordinatesDDMMSS.LonSign == LongitudeSign.W ? table.Coordinates.Longitude * -1 : table.Coordinates.Longitude;

            table.IdMission = tableASP.IdMission;
            table.Image = tableASP.Image;
            table.IsConnect = tableASP.IsConnect;
            table.Letters = tableASP.Letters;
            table.LPA13 = tableASP.LPA13;
            table.LPA24 = tableASP.LPA24;
            table.LPA510 = tableASP.LPA510;
            table.BPSS = tableASP.BPSS;
            table.LPA57 = tableASP.LPA57;
            table.LPA59 = tableASP.LPA59;
            table.LPA10 = tableASP.LPA10;
            table.Mode = tableASP.Mode;
            table.ISOwn = tableASP.ISOwn;
            table.Role = tableASP.Role;
            table.RRS1 = tableASP.RRS1;
            table.RRS2 = tableASP.RRS2;
            table.Type = tableASP.Type;
            table.Sectors = tableASP.Sectors;
            table.TypeConnection = tableASP.TypeConnection;
            table.IsGnssUsed = tableASP.IsGnssUsed;
            table.MatedStationNumber = tableASP.MatedStationNumber;
            table.AddressIp3G4G = tableASP.AddressIp3G4G;
            table.AddressPort3G4G = tableASP.AddressPort3G4G;

            return table;
        }
        private MyTableASP TableASPToMyTableASP(TableASP tableASP)
        {
            MyTableASP table = new MyTableASP();

            table.Id = tableASP.Id;
            table.AddressIP = tableASP.AddressIP;
            table.AddressPort = tableASP.AddressPort;
            table.AntHeightRec = tableASP.AntHeightRec;
            table.AntHeightSup = tableASP.AntHeightSup;
            table.CallSign = tableASP.CallSign;
            table.Caption = tableASP.Caption;
            table.Coordinates.Altitude = tableASP.Coordinates.Altitude;
            table.Coordinates.Latitude = tableASP.Coordinates.Latitude;
            table.Coordinates.Longitude = tableASP.Coordinates.Longitude;
            table.IdMission = tableASP.IdMission;
            table.Image = tableASP.Image;
            table.IsConnect = tableASP.IsConnect;
            table.Letters = tableASP.Letters;
            table.LPA13 = tableASP.LPA13;
            table.LPA24 = tableASP.LPA24;
            table.LPA510 = tableASP.LPA510;
            table.BPSS = tableASP.BPSS;
            table.LPA57 = tableASP.LPA57;
            table.LPA59 = tableASP.LPA59;
            table.LPA10 = tableASP.LPA10;
            table.Mode = tableASP.Mode;
            table.ISOwn = tableASP.ISOwn;
            table.Role = tableASP.Role;
            table.RRS1 = tableASP.RRS1;
            table.RRS2 = tableASP.RRS2;
            table.Type = tableASP.Type;
            table.Sectors = tableASP.Sectors;
            table.TypeConnection = tableASP.TypeConnection;
            table.IsGnssUsed = tableASP.IsGnssUsed;
            table.MatedStationNumber = tableASP.MatedStationNumber;
            table.AddressIp3G4G = tableASP.AddressIp3G4G;
            table.AddressPort3G4G = tableASP.AddressPort3G4G;

            return table;
        }
        public List<string[]> AddTableASPToList()
        {
            try
            {
                List<string[]> list = new List<string[]>();
                string[] str = new string[dgvASP.Columns.Count(x => x.MaxWidth > 0)];

                for (int j = 0, k = 0; j < dgvASP.Columns.Count; j++, k++)
                {
                    if (dgvASP.Columns[j].MaxWidth != 0)
                        str[k] = dgvASP.Columns[j].Header.ToString();
                    else k--;
                }

                list.Add(str);

                for (int i = 0; i < dgvASP.Items.Count; i++)
                {
                    string[] row =
                    {
                        ((MyTableASP)dgvASP.Items[i]).Id.ToString(),
                        ((MyTableASP)dgvASP.Items[i]).IsConnect.ToString(),
                        ((TableASP)dgvASP.Items[i]).AddressIP.ToString(),
                        ((MyTableASP)dgvASP.Items[i]).Mode.ToString(),
                        ((MyTableASP)dgvASP.Items[i]).CallSign.ToString(),
                        ((MyTableASP)dgvASP.Items[i]).Role.ToString(),
                        BytesToString(((MyTableASP)dgvASP.Items[i]).Letters),
                        ((MyTableASP)dgvASP.Items[i]).Coordinates.Latitude.ToString() + "   " +
                        ((MyTableASP)dgvASP.Items[i]).Coordinates.Longitude.ToString(),
                        ((MyTableASP)dgvASP.Items[i]).Coordinates.Altitude.ToString(),
                        ((MyTableASP)dgvASP.Items[i]).RRS1.ToString(),
                        ((MyTableASP)dgvASP.Items[i]).RRS2.ToString(),
                        ((MyTableASP)dgvASP.Items[i]).LPA13.ToString(),
                        ((MyTableASP)dgvASP.Items[i]).LPA24.ToString(),
                        ((MyTableASP)dgvASP.Items[i]).LPA510.ToString(),
                        ((MyTableASP)dgvASP.Items[i]).BPSS.ToString(),
                        ((MyTableASP)dgvASP.Items[i]).LPA57.ToString(),
                        ((MyTableASP)dgvASP.Items[i]).LPA59.ToString(),
                        ((MyTableASP)dgvASP.Items[i]).LPA10.ToString(),
                        ((MyTableASP)dgvASP.Items[i]).Caption.ToString(),
                        ((MyTableASP)dgvASP.Items[i]).MatedStationNumber.ToString(),
                        ((MyTableASP)dgvASP.Items[i]).AddressIp3G4G.ToString(),
                        ((MyTableASP)dgvASP.Items[i]).AddressPort3G4G.ToString(),
                    };
                    if (((MyTableASP)dgvASP.Items[i]).Id != -2)
                        list.Add(row);
                }
                return list;

            }
            catch { }

            return null;
        }

        /// <summary>
        /// Добавить таблицу для отчета
        /// </summary>
        /// <returns></returns>
        private string[,] AddASPToTable()
        {
            int Columns = 13;// dgvSRanges.Columns.Count;
            int Rows = (((GlobalASP)dgvASP.DataContext).CollectionASP.Count(x => x.Id > 0)) + 1;
            string[,] Table = new string[Rows, Columns];

            try
            {
                Table[0, 0] = SHeaders.headerNumAJS;
                Table[0, 1] = SHeaders.headerIPaddress;
                Table[0, 2] = SHeaders.headerMode;
                Table[0, 3] = SHeaders.headerCallSign;
                Table[0, 4] = SHeaders.headerRole;
                Table[0, 5] = SHeaders.headerLatLon;
                Table[0, 6] = SHeaders.headerAlt;
                Table[0, 7] = SHeaders.headerRRC1;
                Table[0, 8] = SHeaders.headerRRC2;
                Table[0, 9] = SHeaders.headerLPA1_3;
                Table[0, 10] = SHeaders.headerLPA2_4;
                Table[0, 11] = SHeaders.headerBPSS;
                Table[0, 12] = SHeaders.headerLPA5_7;
                Table[0, 13] = SHeaders.headerLPA5_9;
                Table[0, 14] = SHeaders.headerLPA10;
                Table[0, 15] = SHeaders.headerLPA5_10;
                Table[0, 16] = SHeaders.headerSignature;

                Table[0, 17] = "MatedStationNumber"; //SHeaders.headerSignature;
                Table[0, 18] = "AddressIp3G4G"; //SHeaders.headerSignature;
                Table[0, 19] = "AddressPort3G4G"; //SHeaders.headerSignature;


                for (int i = 1; i < Rows; i++)
                {
                    Table[i, 0] = IsOwnIdASPConverter.NumASP(((GlobalASP)dgvASP.DataContext).CollectionASP[i - 1].Id, ((GlobalASP)dgvASP.DataContext).CollectionASP[i - 1].ISOwn);//((GlobalASP)dgvASP.DataContext).CollectionASP[i - 1].Id.ToString();
                    Table[i, 1] = ((GlobalASP)dgvASP.DataContext).CollectionASP[i - 1].AddressIP == null ? string.Empty : ((GlobalASP)dgvASP.DataContext).CollectionASP[i - 1].AddressIP.ToString();
                    Table[i, 2] = ((GlobalASP)dgvASP.DataContext).CollectionASP[i - 1].Mode.ToString();
                    Table[i, 3] = ((GlobalASP)dgvASP.DataContext).CollectionASP[i - 1].CallSign == null ? string.Empty : ((GlobalASP)dgvASP.DataContext).CollectionASP[i - 1].CallSign.ToString();
                    Table[i, 4] = RoleConverter.RoleStationParam(((GlobalASP)dgvASP.DataContext).CollectionASP[i - 1].Role);
                    Table[i, 5] = ((GlobalASP)dgvASP.DataContext).CollectionASP[i - 1].Coordinates.Latitude.ToString() + "   " + ((GlobalASP)dgvASP.DataContext).CollectionASP[i - 1].Coordinates.Longitude.ToString();
                    Table[i, 6] = ((GlobalASP)dgvASP.DataContext).CollectionASP[i - 1].Coordinates.Altitude.ToString();
                    Table[i, 7] = ((GlobalASP)dgvASP.DataContext).CollectionASP[i - 1].RRS1.ToString();
                    Table[i, 8] = ((GlobalASP)dgvASP.DataContext).CollectionASP[i - 1].RRS2.ToString();
                    Table[i, 9] = ((GlobalASP)dgvASP.DataContext).CollectionASP[i - 1].LPA13.ToString();
                    Table[i, 10] = ((GlobalASP)dgvASP.DataContext).CollectionASP[i - 1].LPA24.ToString();
                    Table[i, 11] = ((GlobalASP)dgvASP.DataContext).CollectionASP[i - 1].BPSS.ToString();
                    Table[i, 12] = ((GlobalASP)dgvASP.DataContext).CollectionASP[i - 1].LPA57.ToString();
                    Table[i, 13] = ((GlobalASP)dgvASP.DataContext).CollectionASP[i - 1].LPA59.ToString();
                    Table[i, 14] = ((GlobalASP)dgvASP.DataContext).CollectionASP[i - 1].LPA10.ToString();
                    Table[i, 15] = ((GlobalASP)dgvASP.DataContext).CollectionASP[i - 1].LPA510.ToString();
                    Table[i, 16] = ((GlobalASP)dgvASP.DataContext).CollectionASP[i - 1].Caption == null ? string.Empty : ((GlobalASP)dgvASP.DataContext).CollectionASP[i - 1].Caption.ToString();
                    Table[i, 17] = ((GlobalASP)dgvASP.DataContext).CollectionASP[i - 1].MatedStationNumber.ToString();
                    Table[i, 18] = ((GlobalASP)dgvASP.DataContext).CollectionASP[i - 1].AddressIp3G4G.ToString();
                    Table[i, 19] = ((GlobalASP)dgvASP.DataContext).CollectionASP[i - 1].AddressPort3G4G.ToString();
                }
            }
            catch { }

            return Table;
        }

        private void SetLanguageTables(Languages language)
        {
            ResourceDictionary dict = new ResourceDictionary();
            try
            {
                switch (language)
                {
                    case Languages.Eng:

                        dict.Source = new Uri("/ProjectTestTables;component/TranslatorTables.EN.xaml",
                                      UriKind.Relative);
                        break;
                    case Languages.Rus:
                       dict.Source = new Uri("/ProjectTestTables;component/TranslatorTables.RU.xaml",
                                           UriKind.Relative);
                        break;
                    default:
                        dict.Source = new Uri("/ProjectTestTables;component/TranslatorTables.RU.xaml",
                                          UriKind.Relative);
                        break;
                }

                this.Resources.MergedDictionaries.Add(dict);
            }
            catch (Exception)
            { }
        }

        private void IsBrowsableProperty(ASPProperty ASPWindow)
        {

            ASPWindow.propertyGrid.Properties[nameof(MyTableASP.RRS1)].IsBrowsable = true ? RRS1.Visibility == Visibility.Visible : ASPWindow.propertyGrid.Properties[nameof(MyTableASP.RRS1)].IsBrowsable = false;
            ASPWindow.propertyGrid.Properties[nameof(MyTableASP.RRS2)].IsBrowsable = true ? RRS2.Visibility == Visibility.Visible : ASPWindow.propertyGrid.Properties[nameof(MyTableASP.RRS2)].IsBrowsable = false;
            ASPWindow.propertyGrid.Properties[nameof(MyTableASP.BPSS)].IsBrowsable = true ? BPSS.Visibility == Visibility.Visible : ASPWindow.propertyGrid.Properties[nameof(MyTableASP.BPSS)].IsBrowsable = false;
            ASPWindow.propertyGrid.Properties[nameof(MyTableASP.LPA57)].IsBrowsable = true ? LPA57.Visibility == Visibility.Visible : ASPWindow.propertyGrid.Properties[nameof(MyTableASP.LPA57)].IsBrowsable = false;
            ASPWindow.propertyGrid.Properties[nameof(MyTableASP.LPA59)].IsBrowsable = true ? LPA59.Visibility == Visibility.Visible : ASPWindow.propertyGrid.Properties[nameof(MyTableASP.LPA59)].IsBrowsable = false;
            ASPWindow.propertyGrid.Properties[nameof(MyTableASP.LPA510)].IsBrowsable = true ? LPA510.Visibility == Visibility.Visible : ASPWindow.propertyGrid.Properties[nameof(MyTableASP.LPA510)].IsBrowsable = false;
            ASPWindow.propertyGrid.Properties[nameof(MyTableASP.LPA10)].IsBrowsable = true ? LPA10.Visibility == Visibility.Visible : ASPWindow.propertyGrid.Properties[nameof(MyTableASP.LPA10)].IsBrowsable = false;
        }

        private void IsBrowsablePropertyRoss(ASPProperty ASPWindow)
        {
            ASPWindow.propertyGrid.Properties[nameof(MyTableASP.RRS1)].IsBrowsable = false;
            ASPWindow.propertyGrid.Properties[nameof(MyTableASP.RRS2)].IsBrowsable = false;
            ASPWindow.propertyGrid.Properties[nameof(MyTableASP.BPSS)].IsBrowsable = false;
            ASPWindow.propertyGrid.Properties[nameof(MyTableASP.LPA57)].IsBrowsable = false;
            ASPWindow.propertyGrid.Properties[nameof(MyTableASP.LPA59)].IsBrowsable = false;
            ASPWindow.propertyGrid.Properties[nameof(MyTableASP.LPA510)].IsBrowsable = false;
            ASPWindow.propertyGrid.Properties[nameof(MyTableASP.LPA10)].IsBrowsable = false;
        }

        private bool isRoss = false;
        public bool IsRoss
        {
            get { return isRoss; }
            set
            {
                if (isRoss == value)
                    return;

                isRoss = value;
                UpdateTableForRoss(isRoss);
            }
        }

        private void UpdateTableForRoss(bool IsVisible)
        { 
            if (IsVisible)
            {
                NumASP.Visibility = Visibility.Hidden;
                NumASPRoss.Visibility = Visibility.Visible;
                MatedStationNumber.Visibility = Visibility.Visible;
                AddressIp3G4G.Visibility = Visibility.Visible;
                AddressPort3G4G.Visibility = Visibility.Visible;

                RRS1.Visibility = Visibility.Hidden;
                RRS2.Visibility = Visibility.Hidden;
                BPSS.Visibility = Visibility.Hidden;
                LPA57.Visibility = Visibility.Hidden;
                LPA59.Visibility = Visibility.Hidden;
                LPA510.Visibility = Visibility.Hidden;
                LPA10.Visibility = Visibility.Hidden;
            }
            else
            {
                NumASP.Visibility = Visibility.Visible;
                NumASPRoss.Visibility = Visibility.Hidden;
                MatedStationNumber.Visibility = Visibility.Hidden;
                AddressIp3G4G.Visibility = Visibility.Hidden;
                AddressPort3G4G.Visibility = Visibility.Hidden;

                RRS1.Visibility = Visibility.Visible;
                RRS2.Visibility = Visibility.Visible;
                BPSS.Visibility = Visibility.Visible;
                LPA57.Visibility = Visibility.Visible;
                LPA59.Visibility = Visibility.Visible;
                LPA510.Visibility = Visibility.Visible;
                LPA10.Visibility = Visibility.Visible;
            }
        }

        private void UpdatePropertiesForRoss(ASPProperty ASPWindow, bool IsVisible)
        {
            ASPWindow.propertyGrid.Properties[nameof(MyTableASP.ISOwn)].IsBrowsable = true ? !IsVisible : ASPWindow.propertyGrid.Properties[nameof(MyTableASP.ISOwn)].IsBrowsable = false;
            ASPWindow.propertyGrid.Properties[nameof(MyTableASP.MatedStationNumber)].IsBrowsable = true ? IsVisible : ASPWindow.propertyGrid.Properties[nameof(MyTableASP.MatedStationNumber)].IsBrowsable = false;
            ASPWindow.propertyGrid.Properties[nameof(MyTableASP.AddressIp3G4G)].IsBrowsable = true ? IsVisible : ASPWindow.propertyGrid.Properties[nameof(MyTableASP.AddressIp3G4G)].IsBrowsable = false;
            ASPWindow.propertyGrid.Properties[nameof(MyTableASP.AddressPort3G4G)].IsBrowsable = true ? IsVisible : ASPWindow.propertyGrid.Properties[nameof(MyTableASP.AddressPort3G4G)].IsBrowsable = false;
        }

        public void SetCoordsASPToPG(Coord coord)
        {
            try
            {
                if (this.ASPWindow != null)
                {
                    ASPWindow.TableASP.CoordinatesDDMMSS.LatSign = coord.Latitude < 0 ? LatitudeSign.S : LatitudeSign.N;
                    ASPWindow.TableASP.CoordinatesDDMMSS.LonSign = coord.Longitude < 0 ? LongitudeSign.W : LongitudeSign.E;
                    ASPWindow.TableASP.CoordinatesDDMMSS.Altitude = coord.Altitude;

                    coord.Latitude = Math.Round(coord.Latitude, 6);
                    coord.Latitude = coord.Latitude < 0 ? coord.Latitude * -1 : coord.Latitude;
                    coord.Longitude = Math.Round(coord.Longitude, 6);
                    coord.Longitude = coord.Longitude < 0 ? coord.Longitude * -1 : coord.Longitude;

                    switch (PropViewCoords.ViewCoords)
                    {
                        case 1: // format "DD.dddddd"

                            ASPWindow.TableASP.CoordinatesDDMMSS.Latitude = coord.Latitude;
                            ASPWindow.TableASP.CoordinatesDDMMSS.Longitude = coord.Longitude;
                            break;

                        case 2: // format "DD MM.mmmm"

                            ASPWindow.TableASP.CoordinatesDDMMSS.LatDegrees = (int)Math.Truncate(coord.Latitude);
                            ASPWindow.TableASP.CoordinatesDDMMSS.LatMinutesDDMM = Math.Round((coord.Latitude - Math.Truncate(coord.Latitude)) * 60, 4);

                            ASPWindow.TableASP.CoordinatesDDMMSS.LonDegrees = (int)Math.Truncate(coord.Longitude);
                            ASPWindow.TableASP.CoordinatesDDMMSS.LonMinutesDDMM = Math.Round((coord.Longitude - Math.Truncate(coord.Longitude)) * 60, 4);
                            break;

                        case 3: // format "DD MM SS.ss"

                            double latD = Math.Truncate(coord.Latitude);
                            double latM = Math.Truncate((coord.Latitude - latD) * 60);
                            double latS = Math.Round(((coord.Latitude - latD) * 60 - latM) * 60, 2);

                            ASPWindow.TableASP.CoordinatesDDMMSS.LatDegrees = (int)latD;
                            ASPWindow.TableASP.CoordinatesDDMMSS.LatMinutesDDMMSS = (int)latM;
                            ASPWindow.TableASP.CoordinatesDDMMSS.LatSeconds = latS;

                            double lonD = Math.Truncate(coord.Longitude);
                            double lonM = Math.Truncate((coord.Longitude - lonD) * 60);
                            double lonS = Math.Round(((coord.Longitude - lonD) * 60 - lonM) * 60, 2);

                            ASPWindow.TableASP.CoordinatesDDMMSS.LonDegrees = (int)lonD;
                            ASPWindow.TableASP.CoordinatesDDMMSS.LonMinutesDDMMSS = (int)lonM;
                            ASPWindow.TableASP.CoordinatesDDMMSS.LonSeconds = lonS;
                            break;

                        default:
                            break;
                    }
                }
            }
            catch { }
        }
    }
}
