﻿using InheritorsEventArgs;
using ModelsTablesDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Threading;
using TableEvents;

namespace ProjectTestTables
{
    public partial class MainWindow
    {
        private void HandlerUpdate_ButtonsNAV(object sender, TableEventArs<ButtonsNAV> e)
        {
        }

        private void HandlerError_ClientDb(object sender, OperationTableEventArgs e)
        {
            MessageBox.Show(e.GetMessage);
        }

        private void HandlerUpdate_TableMission(object sender, TableEventArs<TableMission> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                MessageBox.Show($"Пришла таблица TableMission кол-во строк: {e.Table.Count}");
            });
        }

        private void HandlerUpdate_TableASP(object sender, TableEventArs<TableASP> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                //MessageBox.Show($"Пришла таблица TableASP кол-во строк: {e.Table.Count}");
                lASP = e.Table;
                ucASP.UpdateASPs(lASP);

                ucReconFHSS.UpdateASPRP(UpdateASPRPRecon(lASP));
                ucReconFWS.UpdateASPRP(lASP, lReconFWS);
            });
        }

        private void HandlerUpdate_TableSectorRangesRecon(object sender, TableEventArs<TableSectorsRangesRecon> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                //MessageBox.Show($"Пришла таблица TableSectorsRangesRecon кол-во строк: {e.Table.Count}");
                //List<TableSectorsRanges> listSRanges = (from t in e.Table let a = t as TableSectorsRanges select a).ToList();
                lSRangeRecon = (from t in e.Table let a = t as TableSectorsRanges select a).ToList().Where(x => x.NumberASP == PropNumberASP.SelectedNumASP).ToList();

                ucSRangesRecon.UpdateSRanges(lSRangeRecon);

            });
        }

        private void HandlerUpdate_TableSectorRangesSuppr(object sender, TableEventArs<TableSectorsRangesSuppr> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                //MessageBox.Show($"Пришла таблица TableSectorsRangesSuppr кол-во строк: {e.Table.Count}");

                //List<TableSectorsRanges> listSRanges = (from t in e.Table let a = t as TableSectorsRanges select a).ToList();
                //List<TableSectorsRanges> listSRanges = (from t in e.Table let a = t as TableSectorsRanges select a).ToList().Where(x => x.NumberASP == PropNumberASP.SelectedNumASP).ToList();
                //ucSRangesSuppr.UpdateSRanges(listSRanges);
                //PanoramaImportRSFreqs(listSRanges);

                lSRangeSuppr = (from t in e.Table let a = t as TableSectorsRanges select a).ToList().Where(x => x.NumberASP == PropNumberASP.SelectedNumASP).ToList();
                ucSRangesSuppr.UpdateSRanges(lSRangeSuppr);

            });
        }

        private void HandlerUpdate_TableFreqForbidden(object sender, TableEventArs<TableFreqForbidden> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                lSpecFreqForbidden = (from t in e.Table let a = t as TableFreqSpec select a).ToList().Where(x => x.NumberASP == PropNumberASP.SelectedNumASP).ToList();
                ucSpecFreqForbidden.UpdateSpecFreqs(lSpecFreqForbidden);
            });
        }

        private void HandlerUpdate_TableFreqImportant(object sender, TableEventArs<TableFreqImportant> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                lSpecFreqImportant = (from t in e.Table let a = t as TableFreqSpec select a).ToList().Where(x => x.NumberASP == PropNumberASP.SelectedNumASP).ToList();
                ucSpecFreqImportant.UpdateSpecFreqs(lSpecFreqImportant);
            });
        }

        private void HandlerUpdate_TableFreqKnown(object sender, TableEventArs<TableFreqKnown> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                lSpecFreqKnown = (from t in e.Table let a = t as TableFreqSpec select a).ToList().Where(x => x.NumberASP == PropNumberASP.SelectedNumASP).ToList();
                ucSpecFreqKnown.UpdateSpecFreqs(lSpecFreqKnown);
            });
        }

        private void HandlerUpdate_TempWFS(object sender, TableEventArs<TempFWS> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                //ucTempFWS.UpdateTempFWS(e.Table);
            });
        }

        private void HandlerChangeFWS(object sender, TempFWS e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                ucTempFWS.ChangeTempFWS(e.Id, e);
            });
        }

        private void HandlerAddFWS(object sender, TempFWS e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                //ucTempFWS.AddTempFWSs(new List<TempFWS>() { e });

            });
        }

        private void HandlerAddRangeFWS(object sender, TableEventArs<TempFWS> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                //ucTempFWS.AddTempFWSs(ExcludeKnownFreq(e.Table));
                //ucTempFWS.ColorFreqImportantForbidden(lSpecFreqImportant.Where(x => x.NumberASP == PropNumberASP.SelectedNumASP).ToList(), lSpecFreqForbidden.Where(x => x.NumberASP == PropNumberASP.SelectedNumASP).ToList());

            });
        }

        private void HandlerUpdate_GlobalProperties(object sender, TableEventArs<GlobalProperties> e)
        {
            //Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            //{
            //    var globalProperties = e.Table.FirstOrDefault();
            //    basicProperties.Global = globalProperties;
            //    UpdateGlobalProperties4MainPanel(globalProperties);
            //    UpdateGlobalProperties4LeftRIButtons(globalProperties);
            //});
        }

        private void HandlerUpdate_TableSuppressFWS(object sender, TableEventArs<TableSuppressFWS> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                lSuppressFWS = e.Table;
                ucSuppressFWS.UpdateSuppressFWS(lSuppressFWS.Where(x => x.NumberASP == PropNumberASP.SelectedNumASP).ToList());
            });
        }

        private void HandlerUpdate_TempSuppressFWS(object sender, TableEventArs<TempSuppressFWS> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                List<TempSuppressFWS> listTempSuppressFWS = e.Table;
                ucSuppressFWS.UpdateRadioJamState(listTempSuppressFWS);
            });
        }

        private void HandlerUpdate_TableReconFWS(object sender, TableEventArs<TableReconFWS> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                lReconFWS = e.Table;
                ucReconFWS.UpdateReconFWS(lReconFWS);
                ucReconFWS.UpdateASPRP(lASP, lReconFWS);
            });
        }

        private void HandlerAddRangeReconFWS(object sender, TableEventArs<TableReconFWS> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                for (int i = 0; i < e.Table.Count; i++)
                {
                    int ind = lReconFWS.FindIndex(x => x.Id == e.Table[i].Id);
                    if (ind != -1)
                    {
                        lReconFWS[ind] = e.Table[i];
                    }
                }
                ucReconFWS.AddReconFWSs(e.Table);
                ucReconFWS.UpdateASPRP(lASP, lReconFWS);
            });
        }

        private void HandlerUpdate_TableSuppressFHSS(object sender, TableEventArs<TableSuppressFHSS> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                lSuppressFHSS = (from t in e.Table let a = t as TableSuppressFHSS select a).ToList().Where(x => x.NumberASP == PropNumberASP.SelectedNumASP).ToList();
                ucSuppressFHSS.UpdateSuppressFHSS(lSuppressFHSS);
            });
        }

        private void HandlerUpdate_TableFHSSExcludedFreq(object sender, TableEventArs<TableFHSSExcludedFreq> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                lFHSSExcludedFreq = e.Table;
                ucSuppressFHSS.UpdateFHSSExcludedFreq(lFHSSExcludedFreq);
            });
        }

        private void HandlerUpdate_TempSuppressFHSS(object sender, TableEventArs<TempSuppressFHSS> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                List<TempSuppressFHSS> listTempSuppressFHSS = e.Table;
                ucSuppressFHSS.UpdateRadioJamState(listTempSuppressFHSS);
            });
        }

        private void HandlerUpdate_TableReconFHSS(object sender, TableEventArs<TableReconFHSS> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                lReconFHSS = e.Table;
                ucReconFHSS.UpdateReconFHSS(lReconFHSS);
                ucReconFHSS.UpdateASPRP(UpdateASPRPRecon(lASP));
            });
        }

        private void HandlerAddRangeReconFHSS(object sender, TableEventArs<TableReconFHSS> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                lReconFHSS = e.Table;
                ucReconFHSS.AddReconFHSSs(lReconFHSS);
                ucReconFHSS.UpdateASPRP(UpdateASPRPRecon(lASP));
            });
        }

        private void HandlerUpdate_TableSourceFHSS(object sender, TableEventArs<TableSourceFHSS> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                lSourceFHSS = e.Table;
                ucReconFHSS.UpdateSourceFHSS(e.Table);
            });
        }

        private void HandlerUpdate_TableFHSSReconExcluded(object sender, TableEventArs<TableFHSSReconExcluded> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                lFHSSReconExcluded = e.Table;
                ucReconFHSS.UpdateTableFHSSReconExcluded(lFHSSReconExcluded);
            });
        }

        private async void LoadTables()
        {
            try
            {
                lASP = await clientDB.Tables[NameTable.TableASP].LoadAsync<TableASP>();
                ucASP.UpdateASPs(lASP);

                lSRangeRecon = await clientDB.Tables[NameTable.TableSectorsRangesRecon].LoadAsync<TableSectorsRanges>();
                lSRangeSuppr = await clientDB.Tables[NameTable.TableSectorsRangesSuppr].LoadAsync<TableSectorsRanges>();

                ucSRangesRecon.UpdateSRanges(lSRangeRecon.Where(x => x.NumberASP == PropNumberASP.SelectedNumASP).ToList());
                ucSRangesSuppr.UpdateSRanges(lSRangeSuppr.Where(x => x.NumberASP == PropNumberASP.SelectedNumASP).ToList());

                lSpecFreqForbidden = await clientDB.Tables[NameTable.TableFreqForbidden].LoadAsync<TableFreqSpec>();
                ucSpecFreqForbidden.UpdateSpecFreqs((lSpecFreqForbidden).Where(x => x.NumberASP == PropNumberASP.SelectedNumASP).ToList());

                lSpecFreqImportant = await clientDB.Tables[NameTable.TableFreqImportant].LoadAsync<TableFreqSpec>();
                ucSpecFreqImportant.UpdateSpecFreqs((lSpecFreqImportant).Where(x => x.NumberASP == PropNumberASP.SelectedNumASP).ToList());

                lSpecFreqKnown = await clientDB.Tables[NameTable.TableFreqKnown].LoadAsync<TableFreqSpec>();
                ucSpecFreqKnown.UpdateSpecFreqs((lSpecFreqKnown).Where(x => x.NumberASP == PropNumberASP.SelectedNumASP).ToList());

                lSuppressFWS = await clientDB.Tables[NameTable.TableSuppressFWS].LoadAsync<TableSuppressFWS>();
                ucSuppressFWS.UpdateSuppressFWS(lSuppressFWS.Where(x => x.NumberASP == PropNumberASP.SelectedNumASP).ToList());
               
                lReconFWS = await clientDB.Tables[NameTable.TableReconFWS].LoadAsync<TableReconFWS>();
                ucReconFWS.UpdateReconFWS(lReconFWS);
                ucReconFWS.UpdateASPRP(lASP, lReconFWS);

                lReconFHSS = await clientDB.Tables[NameTable.TableReconFHSS].LoadAsync<TableReconFHSS>();
                ucReconFHSS.UpdateReconFHSS(lReconFHSS);
                ucReconFHSS.UpdateASPRP(UpdateASPRPRecon(lASP));
                lSourceFHSS = await clientDB.Tables[NameTable.TableSourceFHSS].LoadAsync<TableSourceFHSS>();
                ucReconFHSS.UpdateSourceFHSS(lSourceFHSS);
                lFHSSReconExcluded = await clientDB.Tables[NameTable.TableFHSSReconExcluded].LoadAsync<TableFHSSReconExcluded>();
                ucReconFHSS.UpdateTableFHSSReconExcluded(lFHSSReconExcluded);


                //ucTempFWS.UpdateTempFWS(await clientDB.Tables[NameTable.TempFWS].LoadAsync<TempFWS>());

                lSuppressFHSS = (await clientDB.Tables[NameTable.TableSuppressFHSS].LoadAsync<TableSuppressFHSS>()).Where(x => x.NumberASP == PropNumberASP.SelectedNumASP).ToList();
                ucSuppressFHSS.UpdateSuppressFHSS(lSuppressFHSS);
                lFHSSExcludedFreq = await clientDB.Tables[NameTable.TableFHSSExcludedFreq].LoadAsync<TableFHSSExcludedFreq>();
                ucSuppressFHSS.UpdateFHSSExcludedFreq(lFHSSExcludedFreq);
            }
            catch (ClientDataBase.Exceptions.ExceptionClient exeptClient)
            {
                MessageBox.Show(exeptClient.Message);
            }
            catch (ClientDataBase.Exceptions.ExceptionDatabase excpetService)
            {
                MessageBox.Show(excpetService.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
