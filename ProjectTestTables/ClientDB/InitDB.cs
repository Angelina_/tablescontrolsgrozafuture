﻿using System;
using ClientDataBase;
using InheritorsEventArgs;
using ModelsTablesDBLib;

namespace ProjectTestTables
{
    public partial class MainWindow
    {
        ClientDB clientDB;
        string endPoint = "127.0.0.1:8302";

        //TODO: по номеру арма из настроек брать
        private new string Name { get; set; } = "ARM";

        void InitClientDB()
        {
            clientDB.OnConnect += HandlerConnect_ClientDb;
            clientDB.OnDisconnect += HandlerDisconnect_ClientDb;
            clientDB.OnErrorDataBase += HandlerError_ClientDb;
            (clientDB.Tables[NameTable.TableMission] as ITableUpdate<TableMission>).OnUpTable += HandlerUpdate_TableMission;
            (clientDB.Tables[NameTable.TableASP] as ITableUpdate<TableASP>).OnUpTable += HandlerUpdate_TableASP;
            (clientDB.Tables[NameTable.TableSectorsRangesRecon] as ITableUpdate<TableSectorsRangesRecon>).OnUpTable += HandlerUpdate_TableSectorRangesRecon;
            (clientDB.Tables[NameTable.TableSectorsRangesSuppr] as ITableUpdate<TableSectorsRangesSuppr>).OnUpTable += HandlerUpdate_TableSectorRangesSuppr;
            (clientDB.Tables[NameTable.TableFreqForbidden] as ITableUpdate<TableFreqForbidden>).OnUpTable += HandlerUpdate_TableFreqForbidden;
            (clientDB.Tables[NameTable.TableFreqImportant] as ITableUpdate<TableFreqImportant>).OnUpTable += HandlerUpdate_TableFreqImportant;
            (clientDB.Tables[NameTable.TableFreqKnown] as ITableUpdate<TableFreqKnown>).OnUpTable += HandlerUpdate_TableFreqKnown;
            (clientDB.Tables[NameTable.TempFWS] as ITableUpdate<TempFWS>).OnUpTable += HandlerUpdate_TempWFS;
            (clientDB.Tables[NameTable.TempFWS] as ITableUpRecord<TempFWS>).OnAddRecord += HandlerAddFWS;
            (clientDB.Tables[NameTable.TempFWS] as ITableUpRecord<TempFWS>).OnChangeRecord += HandlerChangeFWS;
            (clientDB.Tables[NameTable.TempFWS] as ITableAddRange<TempFWS>).OnAddRange += HandlerAddRangeFWS; ;
            (clientDB.Tables[NameTable.GlobalProperties] as ITableUpdate<GlobalProperties>).OnUpTable += HandlerUpdate_GlobalProperties;
            (clientDB.Tables[NameTable.TableSuppressFWS] as ITableUpdate<TableSuppressFWS>).OnUpTable += HandlerUpdate_TableSuppressFWS;
            (clientDB.Tables[NameTable.TempSuppressFWS] as ITableUpdate<TempSuppressFWS>).OnUpTable += HandlerUpdate_TempSuppressFWS;
            (clientDB.Tables[NameTable.TableReconFWS] as ITableUpdate<TableReconFWS>).OnUpTable += HandlerUpdate_TableReconFWS;
            (clientDB.Tables[NameTable.TableReconFWS] as ITableAddRange<TableReconFWS>).OnAddRange += HandlerAddRangeReconFWS;
            (clientDB.Tables[NameTable.TableSuppressFHSS] as ITableUpdate<TableSuppressFHSS>).OnUpTable += HandlerUpdate_TableSuppressFHSS;
            (clientDB.Tables[NameTable.TableFHSSExcludedFreq] as ITableUpdate<TableFHSSExcludedFreq>).OnUpTable += HandlerUpdate_TableFHSSExcludedFreq;
            (clientDB.Tables[NameTable.TempSuppressFHSS] as ITableUpdate<TempSuppressFHSS>).OnUpTable += HandlerUpdate_TempSuppressFHSS;

            (clientDB.Tables[NameTable.TableReconFHSS] as ITableUpdate<TableReconFHSS>).OnUpTable += HandlerUpdate_TableReconFHSS;
            (clientDB.Tables[NameTable.TableReconFHSS] as ITableAddRange<TableReconFHSS>).OnAddRange += HandlerAddRangeReconFHSS;
            (clientDB.Tables[NameTable.TableSourceFHSS] as ITableUpdate<TableSourceFHSS>).OnUpTable += HandlerUpdate_TableSourceFHSS;
            (clientDB.Tables[NameTable.TableFHSSReconExcluded] as ITableUpdate<TableFHSSReconExcluded>).OnUpTable += HandlerUpdate_TableFHSSReconExcluded;

            (clientDB.Tables[NameTable.ButtonsNAV] as ITableUpdate<ButtonsNAV>).OnUpTable += HandlerUpdate_ButtonsNAV;
        }

        
    }
}
