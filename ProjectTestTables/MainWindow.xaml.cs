﻿using ASPControl;
using ModelsTablesDBLib;
using RadarRodnikControl;
using SectorsRangesControl;
using StateNAVControl;
using StateUAVControl;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Windows;
using TableEvents;
using TempFWSControl;
using ValuesCorrectLib;

namespace ProjectTestTables
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    /// 
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }
        #endregion
        public MainWindow()
        {
            InitializeComponent();

            InitTables();

            SetLanguageTables(Languages.Rus);
            TranslatorTables.LoadDictionary(Languages.Rus);

            ucState.SetTranslation(StateNAVControl.Language.RU);
            ucRodnik.SetTranslation(RadarRodnikControl.Language.EN);

            PropViewCoords.ViewCoords = 2;

            
        }

        private void ButtonAddState_Click(object sender, RoutedEventArgs e)
        {
            Random rand = new Random();
            List<StateModel> list = new List<StateModel>()
            {
                    new StateModel
                    {
                        Id = -2,
                        Type = StateNAVControl.Type.NAV,
                        Temperature = (short)rand.Next(1, 100),
                        Amperage = (float)(rand.Next(1, 100) + rand.NextDouble()),
                        Snt = StateNAVControl.Led.Red,
                        Rad = StateNAVControl.Led.Green,
                        Pow = StateNAVControl.Led.Green
                    },
                    new StateModel
                    {
                        Id = -2,
                        Type = StateNAVControl.Type.SPUF,
                        Temperature = (short)rand.Next(1, 100),
                        Amperage = (float)(rand.Next(1, 100) + rand.NextDouble()),
                        Snt = StateNAVControl.Led.Gray,
                        Rad = StateNAVControl.Led.Green,
                        Pow = StateNAVControl.Led.Red
                    }
            };

            ucState.ListStateModel = list;

        }

        private void ucRodnik_OnDeleteRecord(object sender, RadarRodnikControl.RodnikModel e)
        {
            int ind = listRodnik.FindIndex(x => x.Id == e.Id);
            if(ind != -1)
            {
                listRodnik.RemoveAt(ind);
                ucRodnik.ListRodnikModel = listRodnik;
            }

            bool b = this.ucRodnik.IsCheckAll;

        }

        private void ucRodnik_OnClearRecords(object sender, EventArgs e)
        {
            listRodnik = new List<RodnikModel>();
            ucRodnik.ListRodnikModel = listRodnik;
        }

        private void ucRodnik_OnChangeRecord(object sender, RadarRodnikControl.RodnikModel e)
        {
            int ind = listRodnik.FindIndex(x => x.Id == e.Id);
            if(ind != -1)
            {
                listRodnik[ind].IsCheck = e.IsCheck;
                ucRodnik.ListRodnikModel = listRodnik;
            }
        }

        public List<RodnikModel> listRodnik = new List<RodnikModel>();
        private void ButtonAddRodnik_Click(object sender, RoutedEventArgs e)
        {
            Random rand = new Random();
            listRodnik = new List<RodnikModel>();

            for (int i = 0; i < 5; i++)
            {
                RodnikModel model = new RodnikModel
                {
                    IsCheck = false,
                    Id = (short)(i + 1),
                    Azimuth = (float)(rand.Next(1, 100) + rand.NextDouble()),
                    Range = (float)(rand.Next(1, 100) + rand.NextDouble()),
                    Class = "AAA",
                    Velocity = (float)(rand.Next(1, 100) + rand.NextDouble()),
                    Aspect = (float)(rand.Next(1, 100) + rand.NextDouble()),
                    Time = DateTime.Now,
                    Mode = "BBB",
                    Flag = "CCC"
                };

                listRodnik.Add(model);
            }

            ucRodnik.ListRodnikModel = listRodnik;

            this.ucStateUAV.Language = 2;
        }

        private void ucRodnik_OnCheckAll(object sender, bool IsCheckAll)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                this.listRodnik.ForEach(x => x.IsCheck = IsCheckAll);

                ucRodnik.ListRodnikModel = this.listRodnik;
            });
        }

        public List<StateUAVModel> listStateUAV = new List<StateUAVModel>();
        private void UcStateUAV_OnClickJamUAV(object sender, List<StateUAVModel> e)
        {
            //this.listStateUAV = e;
            //this.ucStateUAV.ListStateUAV = this.listStateUAV;
        }
    }
}
