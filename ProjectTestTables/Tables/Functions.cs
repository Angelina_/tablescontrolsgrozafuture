﻿using ModelsTablesDBLib;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using TableEvents;
using ValuesCorrectLib;

namespace ProjectTestTables
{
    public partial class MainWindow
    {
        ObservableCollection<ObservableCollection<string>> lASPRP = new ObservableCollection<ObservableCollection<string>>();

        /// <summary>
        /// Обновить поле АСП РП в таблице 
        /// </summary>
        /// <param name="tableASP"></param>
        /// <returns></returns>
        private ObservableCollection<string> UpdateASPRPRecon(List<TableASP> tableASP)
        {
            ObservableCollection<string> lASPRP = new ObservableCollection<string>();

            for(int i = 0; i < tableASP.Count; i ++)
            {
                if(tableASP[i].ISOwn)
                {
                    lASPRP.Add("*" + (tableASP[i].Id).ToString());
                }
                else
                {
                    lASPRP.Add(tableASP[i].Id.ToString());
                }
            }
            return lASPRP;
        }

        /// <summary>
        /// Обновить колонку АСП РП в таблице 
        /// </summary>
        /// <param name="tableASP"></param>
        /// <returns></returns>
        private ObservableCollection<ObservableCollection<string>> UpdateASPRPDistribution(List<TableASP> tableASP, List<TableReconFWS> tableReconFWS)
        {
            lASPRP = new ObservableCollection<ObservableCollection<string>>();
            for(int i = 0; i < tableReconFWS.Count; i++)
            {
                ObservableCollection<string> list = new ObservableCollection<string>();
                if (tableReconFWS[i].ASPSuppr == -2 || tableReconFWS[i].ASPSuppr == 0)
                {
                    list.Add(" ");
                    for (int j = 0; j < tableASP.Count; j++)
                    {
                        if (tableASP[j].ISOwn) { list.Add("*" + (tableASP[j].Id).ToString()); }
                        else { list.Add(tableASP[j].Id.ToString()); }
                    }
                }
                else
                {
                    int ind = tableASP.FindIndex(x => x.Id == tableReconFWS[i].ASPSuppr);
                    if (ind != -1)
                    {
                        if (tableASP[ind].ISOwn) { list.Add("*" + (tableASP[ind].Id).ToString()); }
                        else { list.Add(tableASP[ind].Id.ToString()); }
                    }

                    for (int j = 0; j < tableASP.Count; j++)
                    {
                        if(tableASP[j].Id != tableReconFWS[i].ASPSuppr)
                        {
                            if (tableASP[j].ISOwn) { list.Add("*" + (tableASP[j].Id).ToString()); }
                            else { list.Add(tableASP[j].Id.ToString()); }
                        }
                    }
                }
                lASPRP.Add(list);
            }
           
            return lASPRP;
        }

        /// <summary>
        /// Проверка возможности добавления диапазона в таблицу
        /// </summary>
        /// <param name="lTemp"> Текущий список диапазонов </param>
        /// <param name="recAdd"> Диапазон для добавления в таблицу </param>
        /// <param name="IsChange"> true - проверка на возможность изменения записи,
        ///                         false - проверка на возможность добавления записи </param>
        /// <returns> true - диапазон можно добавить </returns>
        private bool IsAddSpecFreq(List<TableFreqSpec> lTemp, TableFreqSpec recAdd, bool IsChange)
        {
            List<TableFreqSpec> tempListSpecFreq = new List<TableFreqSpec>();
            if (IsChange)
            {
                tempListSpecFreq = lTemp.Where(rec => rec.Id != recAdd.Id).ToList();
            }
            else
            {
                tempListSpecFreq = lTemp;
            }

            CorrectMinMax.IsCorrectMinMax(recAdd, PropNameTable.SpecFreqsName);
            if (CorrectMinMax.IsCorrectRangeMinMax(recAdd))
            {
                ObservableCollection<TableFreqSpec> tempSpecFreq = new ObservableCollection<TableFreqSpec>(tempListSpecFreq);
                ObservableCollection<TableFreqSpec> addSpecFreq = new ObservableCollection<TableFreqSpec>();
                addSpecFreq.Add(recAdd);
                if (CorrectSpecFreq.IsAddRecordsToCollection(tempSpecFreq, addSpecFreq))
                {
                    return true;
                }
            }
            return false;
        }


        /// <summary>
        /// Добавление/Изменение в TableSuppressFWS
        /// </summary>
        /// <param name="SuppressFWS"> модель ИРИ для РП </param>
        /// <returns> сообщение об ошибке (пустая строка - успешно) </returns>
        public string IsUpdateTableSuppressFWS(TableSuppressFWS SuppressFWS, bool IsChange)
        {
            List<TableSuppressFWS> tempListSuppressFWS = new List<TableSuppressFWS>();
            if (IsChange)
            {
                tempListSuppressFWS = lSuppressFWS.Where(rec => rec.Id != SuppressFWS.Id).ToList();
            }
            else
            {
                tempListSuppressFWS = lSuppressFWS;
            }

            string sMessage = CorrectSuppress.IsCheckRangesSuppress(SuppressFWS.FreqKHz.Value, lSRangeSuppr);
            if (sMessage != string.Empty)
                return sMessage;

            sMessage = CorrectSuppress.IsCheckForbiddenFreq(SuppressFWS.FreqKHz.Value, lSpecFreqForbidden);
            if (sMessage != string.Empty)
                return sMessage;

            sMessage = CorrectSuppress.CountFreqLetter(SuppressFWS.Letter.Value, 3, tempListSuppressFWS);/*basicProperties.Global.NumberIri*/
            if (sMessage != string.Empty)
                return sMessage;

            return sMessage;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="SuppressFHSS"></param>
        /// <param name="IsChange"> true - проверка на возможность изменения записи,
        ///                         false - проверка на возможность добавления записи </param>
        /// <returns></returns>
        public string IsUpdateTableSuppressFHSS(TableSuppressFHSS SuppressFHSS, bool IsChange)
        {
            string sMessage = CorrectSuppress.IsCheckRangesSuppress(SuppressFHSS.FreqMinKHz, SuppressFHSS.FreqMaxKHz, lSRangeSuppr);
            if (sMessage != string.Empty)
                return sMessage;

            sMessage = CorrectSuppress.IsCheckForbiddenRange(SuppressFHSS.FreqMinKHz, SuppressFHSS.FreqMaxKHz, lSpecFreqForbidden.Where(x => x.NumberASP == PropNumberASP.SelectedNumASP).ToList());
            if (sMessage != string.Empty)
                return sMessage;

            List<TableSuppressFHSS> tempListSuppressFHSS = new List<TableSuppressFHSS>();
            if (IsChange)
            {
                tempListSuppressFHSS = lSuppressFHSS.Where(rec => rec.Id != SuppressFHSS.Id).ToList();
            }
            else
            {
                tempListSuppressFHSS = lSuppressFHSS;
            }

            // Проверка возможности добавления записи ------------------------------------------------------------------------------------
            if (tempListSuppressFHSS.Count == 4)
            {
                sMessage = SMessages.mesUnableAddRecord;
            }
            else
            {
                if (!CorrectSuppress.IsCheckCountChannel(tempListSuppressFHSS, SuppressFHSS))
                {
                    sMessage = SMessages.mesCountChannelsMax;
                }
            }
            //if (lSuppressFHSS.Count == 4)
            //{
            //    sMessage = "Невозможно добавить запись! Количество записей превысит допустимое!";
            //}
            //else
            //{
            //    if (!CorrectSuppress.IsCheckCountChannel(lSuppressFHSS, SuppressFHSS))
            //    {
            //        sMessage = "Суммарное количество каналов превышает допустимое!";
            //    }
            //}
            //----------------------------------------------------------------------------------------------------------------------------
            return sMessage;
        }

        /// <summary>
        /// Убрать Известные частоты из списка
        /// </summary>
        /// <param name="listTempFWS"></param>
        /// <returns></returns>
        private List<TempFWS> ExcludeKnownFreq(List<TempFWS> listTempFWS)
        {
            List<TableFreqSpec> tempFreqKnown = lSpecFreqKnown.Where(x => x.NumberASP == PropNumberASP.SelectedNumASP).ToList();
            List<TempFWS> listExclude = new List<TempFWS> (listTempFWS);
            bool f = false;

            if (listTempFWS.Count > 0 && tempFreqKnown.Count > 0)
            {
                for (int i = 0; i < listTempFWS.Count; i++)
                {
                    for (int j = 0; j < tempFreqKnown.Count; j++)
                    {
                        if ((listTempFWS[i].FreqKHz >= tempFreqKnown[j].FreqMinKHz && listTempFWS[i].FreqKHz <= tempFreqKnown[j].FreqMaxKHz))
                        {
                            f = true;
                        }
                       
                    }
                    if(f)
                    {
                        listExclude.Remove(listTempFWS[i]);
                        f = false;
                    }
                }

                return listExclude;
            }
            return listTempFWS;

            //if (listTempFWS.Count > 0 && tempFreqKnown.Count > 0)
            //{
            //    for (int i = 0; i < listTempFWS.Count; i++)
            //    {
            //        for (int j = 0; j < tempFreqKnown.Count; j++)
            //        {
            //            if (!(listTempFWS[i].FreqKHz >= tempFreqKnown[j].FreqMinKHz && listTempFWS[i].FreqKHz <= tempFreqKnown[j].FreqMaxKHz))
            //            {
            //                TempFWS temp = listTempFWS[i];
            //                listExclude.Add(temp);
            //            }
            //        }
            //    }
            //}
            //else
            //{
            //    return listTempFWS;
            //}

            //return listExclude;
        }
    }
}
