﻿using ClientDataBase;
using Formation_RN_RD_CC;
using ModelsTablesDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using TableEvents;
using ValuesCorrectLib;

namespace ProjectTestTables
{
    public partial class MainWindow
    {
        // АСП
        public List<TableASP> lASP = new List<TableASP>();
        // ИРИ ФРЧ ЦР
        public List<TableReconFWS> lReconFWS = new List<TableReconFWS>();
        public List<SRNet> lUS = new List<SRNet>(); // Узлы связи 
        public List<SRNet> lRS = new List<SRNet>(); // Радиосети
        // ИРИ ФРЧ РП
        public List<TableSuppressFWS> lSuppressFWS = new List<TableSuppressFWS>();
        // Специальные частоты
        public List<TableFreqSpec> lSpecFreqForbidden = new List<TableFreqSpec>();
        public List<TableFreqSpec> lSpecFreqImportant = new List<TableFreqSpec>();
        public List<TableFreqSpec> lSpecFreqKnown = new List<TableFreqSpec>();
        // Сектора и диапазоны
        public List<TableSectorsRanges> lSRangeSuppr = new List<TableSectorsRanges>();
        public List<TableSectorsRanges> lSRangeRecon = new List<TableSectorsRanges>();
        // ИРИ ППРЧ РП
        public List<TableSuppressFHSS> lSuppressFHSS = new List<TableSuppressFHSS>();
        public List<TableFHSSExcludedFreq> lFHSSExcludedFreq = new List<TableFHSSExcludedFreq>();
        // ИРИ ППРЧ
        public List<TableReconFHSS> lReconFHSS = new List<TableReconFHSS>();
        public List<TableSourceFHSS> lSourceFHSS = new List<TableSourceFHSS>();
        public List<TableFHSSReconExcluded> lFHSSReconExcluded = new List<TableFHSSReconExcluded>();

        private int selectedASP = -2;

        public ButtonsNAV buttonsNAV = new ButtonsNAV();

        /// <summary>
        /// Update tables SectorsRanges, SpecFreqs
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UcASP_OnSelectedRow(object sender, ASPEvents e)
        {
            if (e.Id > 0)
            {
                selectedASP = e.Id;
                LoadTablesByFilter(e.Id);
            }
        }

        private async void LoadTablesByFilter(int id)
        {
            try
            {
                if (clientDB == null) 
                    return;

                lSRangeRecon = await (clientDB.Tables[NameTable.TableSectorsRangesRecon] as IDependentAsp).LoadByFilterAsync<TableSectorsRanges>(id); 
                ucSRangesRecon.UpdateSRanges(lSRangeRecon);

                lSRangeSuppr = await (clientDB.Tables[NameTable.TableSectorsRangesSuppr] as IDependentAsp).LoadByFilterAsync<TableSectorsRanges>(id);
                ucSRangesSuppr.UpdateSRanges(lSRangeSuppr);

                lSpecFreqForbidden = await (clientDB.Tables[NameTable.TableFreqForbidden] as IDependentAsp).LoadByFilterAsync<TableFreqSpec>(id);
                ucSpecFreqForbidden.UpdateSpecFreqs(lSpecFreqForbidden);

                lSpecFreqImportant = await (clientDB.Tables[NameTable.TableFreqImportant] as IDependentAsp).LoadByFilterAsync<TableFreqSpec>(id);
                ucSpecFreqImportant.UpdateSpecFreqs(lSpecFreqImportant);

                lSpecFreqKnown = await (clientDB.Tables[NameTable.TableFreqKnown] as IDependentAsp).LoadByFilterAsync<TableFreqSpec>(id);
                ucSpecFreqKnown.UpdateSpecFreqs(lSpecFreqKnown);

                lSuppressFWS = await (clientDB.Tables[NameTable.TableSuppressFWS] as IDependentAsp).LoadByFilterAsync<TableSuppressFWS>(id);
                ucSuppressFWS.UpdateSuppressFWS(lSuppressFWS);

                lSuppressFHSS = await (clientDB.Tables[NameTable.TableSuppressFHSS] as IDependentAsp).LoadByFilterAsync<TableSuppressFHSS>(id);
                ucSuppressFHSS.UpdateSuppressFHSS(lSuppressFHSS);

            }
            catch (ClientDataBase.Exceptions.ExceptionClient exeptClient)
            {
                MessageBox.Show(exeptClient.Message);
            }
            catch (ClientDataBase.Exceptions.ExceptionDatabase excpetService)
            {
                MessageBox.Show(excpetService.Message);
            }
        }
        private void UcTemsFWS_OnDeleteRecord(object sender, TableEvent e)
        {
            if (clientDB != null)
            {
                clientDB.Tables[e.NameTable].Delete(e.Record);
            }

        }

        private void OnClearRecords(object sender, NameTable nameTable)
        {
            if (clientDB != null)
            {
                 clientDB.Tables[nameTable].Clear();
            }
        }

        private void OnClearRecordsByFilter(object sender, NameTable nameTable)
        {
            try
            {
                if (selectedASP > 0)
                    (clientDB.Tables[nameTable] as IDependentAsp).ClearByFilter(selectedASP);
            }
            catch(Exception)
            {

            }

        }

        private void OnDeleteRecord(object sender, TableEvent e)
        {
            if (clientDB != null)
            {
                clientDB.Tables[e.NameTable].Delete(e.Record);
            }
        }

        private void OnChangeRecord(object sender, TableEvent e)
        {
            if (clientDB != null)
            {
                switch (e.NameTable)
                {
                    case NameTable.TableSuppressFWS:
                        // Проверка возможности добавления в таблицу---------------------------------------------------
                        string sMessage = IsUpdateTableSuppressFWS(e.Record as TableSuppressFWS, true);
                        if (sMessage == string.Empty)
                        {
                            clientDB.Tables[e.NameTable].Change(e.Record);
                        }
                        else
                        {
                            MessageBox.Show(sMessage, SMessages.mesMessage, MessageBoxButton.OK, MessageBoxImage.Exclamation);
                        }
                        break;

                    case NameTable.TableFreqForbidden:
                        if (IsAddSpecFreq(lSpecFreqForbidden, e.Record as TableFreqSpec, true))
                        {
                            clientDB.Tables[e.NameTable].Change(e.Record);
                        }
                        break;

                    case NameTable.TableFreqKnown:
                        if (IsAddSpecFreq(lSpecFreqKnown, e.Record as TableFreqSpec, true))
                        {
                            clientDB.Tables[e.NameTable].Change(e.Record);
                        }
                        break;

                    case NameTable.TableFreqImportant:
                        if (IsAddSpecFreq(lSpecFreqImportant, e.Record as TableFreqSpec, true))
                        {
                            clientDB.Tables[e.NameTable].Change(e.Record);
                        }
                        break;

                    case NameTable.TableSuppressFHSS:
                        // Проверка возможности добавления в таблицу---------------------------------------------------
                        sMessage = IsUpdateTableSuppressFHSS(e.Record as TableSuppressFHSS, true);
                        if (sMessage == string.Empty)
                        {
                            clientDB.Tables[e.NameTable].Change(e.Record);
                        }
                        else
                        {
                            MessageBox.Show(sMessage, SMessages.mesMessage, MessageBoxButton.OK, MessageBoxImage.Exclamation);
                        }
                        // --------------------------------------------------------------------------------------------
                        break;

                    default:
                        clientDB.Tables[e.NameTable].Change(e.Record);
                        break;

                }
            }
        }

        private void OnAddRecord(object sender, TableEvent e)
        {
            if (clientDB != null)
            {
                string sMessage = string.Empty;
                switch (e.NameTable)
                {
                    case NameTable.TableSuppressFWS:
                        // Проверка возможности добавления в таблицу---------------------------------------------------
                        sMessage = IsUpdateTableSuppressFWS(e.Record as TableSuppressFWS, false);
                        if (sMessage == string.Empty)
                        {
                            clientDB?.Tables[e.NameTable].Add(e.Record);
                        }
                        else
                        {
                            MessageBox.Show(sMessage, SMessages.mesMessage, MessageBoxButton.OK, MessageBoxImage.Exclamation);
                        }
                        break;

                    case NameTable.TableFreqForbidden:
                        if (IsAddSpecFreq(lSpecFreqForbidden, e.Record as TableFreqSpec, false))
                        {
                            clientDB?.Tables[e.NameTable].Add(e.Record);
                        }
                        break;

                    case NameTable.TableFreqKnown:
                        if (IsAddSpecFreq(lSpecFreqKnown, e.Record as TableFreqSpec, false))
                        {
                            clientDB?.Tables[e.NameTable].Add(e.Record);
                        }
                        break;

                    case NameTable.TableFreqImportant:
                        if (IsAddSpecFreq(lSpecFreqImportant, e.Record as TableFreqSpec, false))
                        {
                            clientDB?.Tables[e.NameTable].Add(e.Record);
                        }
                        break;

                    case NameTable.TableSuppressFHSS:
                        // Проверка возможности добавления в таблицу---------------------------------------------------
                        sMessage = IsUpdateTableSuppressFHSS(e.Record as TableSuppressFHSS, false);
                        if (sMessage == string.Empty)
                        {
                            clientDB?.Tables[e.NameTable].Add(e.Record);
                        }
                        else
                        {
                            MessageBox.Show(sMessage, SMessages.mesMessage, MessageBoxButton.OK, MessageBoxImage.Exclamation);
                        }
                        // --------------------------------------------------------------------------------------------
                        break;

                    default:
                        clientDB?.Tables[e.NameTable].Add(e.Record);
                        break;
                }
            }
        }

        private void UcTemsFWS_OnAddFWS_RS(object sender, TableSuppressFWS e)
        {
            if (PropNumberASP.SelectedNumASP > 0)
            {
                e.NumberASP = PropNumberASP.SelectedNumASP;

                // Проверка возможности добавления в таблицу---------------------------------------------------
                string sMessage = IsUpdateTableSuppressFWS(e, false);
                if (sMessage == string.Empty)
                {
                    clientDB.Tables[NameTable.TableSuppressFWS].Add(e);
                }
                else
                {
                    MessageBox.Show(sMessage, SMessages.mesMessage, MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                // --------------------------------------------------------------------------------------------
            }
        }

        private void UcTemsFWS_OnAddFWS_TD(object sender, TableReconFWS e)
        {
            if (PropNumberASP.SelectedNumASP > 0)
            {
                clientDB.Tables[NameTable.TableReconFWS].Add(e);


            }
        }
        private void UcTemsFWS_OnSelectedRow(object sender, TableEvent e)
        {
            clientDB.Tables[e.NameTable].Change(e.Record);
        }

        private void UcReconFWS_OnAddFWS_RS(object sender, TableSuppressFWS e)
        {
            if (PropNumberASP.SelectedNumASP > 0)
            {
                e.NumberASP = PropNumberASP.SelectedNumASP;

                // Проверка возможности добавления в таблицу---------------------------------------------------
                string sMessage = IsUpdateTableSuppressFWS(e, false);
                if (sMessage == string.Empty)
                {
                    clientDB.Tables[NameTable.TableSuppressFWS].Add(e);
                }
                else
                {
                    MessageBox.Show(sMessage, SMessages.mesMessage, MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                // --------------------------------------------------------------------------------------------
            }
        }

        private void UcReconFWS_OnSelectedASPSuppr(object sender, TableReconFWS e)
        {
            clientDB.Tables[NameTable.TableReconFWS].Change(e);
        }

        private async void UcReconFWS_OnClickTDistribution(object sender, EventArgs e)
        {
            try
            {
                List<TableReconFWS> listDistrib = new List<TableReconFWS>();
                List<TableSectorsRanges> listSRangeSuppr = await clientDB.Tables[NameTable.TableSectorsRangesSuppr].LoadAsync<TableSectorsRanges>();
                List<TableFreqSpec> listSpecFreqForbidden = await clientDB.Tables[NameTable.TableFreqForbidden].LoadAsync<TableFreqSpec>();
                lReconFWS = await clientDB.Tables[NameTable.TableReconFWS].LoadAsync<TableReconFWS>();
                //listDistrib = IRI_Distribution.ClassTargetDistribution.Distribution(lASP, listSRangeSuppr, listSpecFreqForbidden, lReconFWS, basicProperties.Global.NumberIri); //Distribution(lASP, lSRangeSuppr, lSpecFreqForbidden, lReconFWS, basicProperties.Global.NumberIri);

                //if (listDistrib.Count != 0)
                //    clientDB.Tables[NameTable.TableReconFWS].AddRange(listDistrib);
            }
            catch { }
        }

        private void UcReconFWS_OnSendFWS_TD_RS(object sender, List<TableSuppressFWS> e)
        {
            lSuppressFWS = e;
            clientDB.Tables[NameTable.TableSuppressFWS].Clear();
            clientDB.Tables[NameTable.TableSuppressFWS].AddRange(lSuppressFWS);
        }

        private async void UcReconFWS_OnClickRS(object sender, bool e)
        {
            try
            {
                if (e)
                {
                    //lRS = new List<SRNet>();
                    lReconFWS = await clientDB.Tables[NameTable.TableReconFWS].LoadAsync<TableReconFWS>();
                    lRS = ClassFormation_RN_RD_CC.Organization_RNet(lReconFWS, 0, 0, 0);
                    //ClassFormation_RN_RD_CC.Organization_RNet(lASP, lReconFWS, 0, 0, 0, ref lRS);
                    ucReconFWS.UpdateRS(lRS);
                }
                else
                {
                    ucReconFWS.ClearRS(lUS);
                }
            }
            catch { }
        }

        private async void UcReconFWS_OnClickUS(object sender, bool e)
        {
            try
            {
                if (e)
                {
                    //lUS = new List<SRNet>();
                    lReconFWS = await clientDB.Tables[NameTable.TableReconFWS].LoadAsync<TableReconFWS>();
                    lUS = ClassFormation_RN_RD_CC.Organization_Communication(lReconFWS, 0, 0);
                    //ClassFormation_RN_RD_CC.Organization_Communication(lASP, lReconFWS, 0, 0, ref lUS);
                    ucReconFWS.UpdateUS(lUS);
                }
                else
                {
                    ucReconFWS.ClearUS(lRS);
                }
            }
            catch { }
        }

        private void UcSRangesSuppr_OnLoadDefaultSRanges(object sender, NameTable e)
        {
            List<TableSectorsRanges> sr = new List<TableSectorsRanges>();
            object Ranges = new object();
            if (e == ModelsTablesDBLib.NameTable.TableSectorsRangesSuppr)
            {
                sr = ucSRangesSuppr.LoadDefaultSRangesSuppr(Languages.Rus);
                if (sr.Count > 0)
                {
                    for (int i = 0; i < sr.Count; i++)
                    {
                        sr[i].IsCheck = true;
                        sr[i].NumberASP = PropNumberASP.SelectedNumASP;
                    }
                }
                Ranges = (from t in sr let c = t.ToRangesSuppr() select c).ToList();
            }
            clientDB.Tables[e].AddRange(Ranges);

        }

        private void UcSRangesRecon_OnLoadDefaultSRanges(object sender, NameTable e)
        {
            List<TableSectorsRanges> sr = new List<TableSectorsRanges>();
            object Ranges = new object();
            if (e == NameTable.TableSectorsRangesRecon)
            {
                sr = ucSRangesRecon.LoadDefaultSRangesRecon(Languages.Rus);
                if (sr.Count > 0)
                {
                    for (int i = 0; i < sr.Count; i++)
                    {
                        sr[i].IsCheck = true;
                        sr[i].NumberASP = PropNumberASP.SelectedNumASP;
                    }
                }
                Ranges = (from t in sr let c = t.ToRangesRecon() select c).ToList();
            }
            clientDB.Tables[e].AddRange(Ranges);
        }

        private void UcSuppressFWS_OnDeleteRange(object sender, List<TableSuppressFWS> e)
        {
            if (clientDB != null)
            {
                clientDB.Tables[NameTable.TableSuppressFWS].RemoveRange(e);
            }
        }

        private void UcReconFHSS_OnSelectedRow(object sender, TableEvent e)
        {
            if (clientDB != null)
            {
                clientDB.Tables[e.NameTable].Change(e.Record);
            }
        }

        private void UcReconFHSS_OnAddFHSS_RS_Recon(object sender, TableSuppressFHSS e)
        {
            // Проверка возможности добавления в таблицу---------------------------------------------------
            string sMessage = IsUpdateTableSuppressFHSS(e, false);
            if (sMessage == string.Empty)
            {
                clientDB.Tables[NameTable.TableSuppressFHSS].Add(e);
            }
            else
            {
                MessageBox.Show(sMessage, SMessages.mesMessage, MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
            // --------------------------------------------------------------------------------------------
        }

        private void UcSuppressFWS_OnCheckButtonsNAV(object sender, TableEvent e)
        {
            if (clientDB != null)
            {
                clientDB.Tables[NameTable.ButtonsNAV].Add(e);
            }
        }

        private void OnAddTableToReport(object sender, TableEventReport e)
        {
            //switch (basicProperties.Local.Common.FileType)
            //{
            //    case FileType.Word:
            //        AddTableToReport.AddToWord("First", e.Table, e.NameTable);
            //        break;

            //    case FileType.Excel:
            //        AddTableToReport.AddToExcel("First", e.Table, e.NameTable);
            //        break;

            //    case FileType.Txt:
            //        AddTableToReport.AddToTxt("First", e.Table, e.NameTable);
            //        break;
            //}

        }
    }
}
