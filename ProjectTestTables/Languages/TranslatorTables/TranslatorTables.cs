﻿using System.ComponentModel;
using System.Windows;
using ModelsTablesDBLib;

namespace ProjectTestTables
{
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        private void UcSRangesRecon_OnIsWindowPropertyOpen(object sender, SectorsRangesControl.SectorsRangesProperty e)
        {
            e.SetLanguagePropertyGrid(Languages.Rus);
            //e.SetLanguagePropertyGrid(basicProperties.Local.Common.Language);
        }

        private void UcSpecFreqKnown_OnIsWindowPropertyOpen(object sender, SpecFreqControl.SpecFreqProperty e)
        {
            e.SetLanguagePropertyGrid(Languages.Rus);
            //e.SetLanguagePropertyGrid(basicProperties.Local.Common.Language);
        }

        private void UcSpecFreqImportant_OnIsWindowPropertyOpen(object sender, SpecFreqControl.SpecFreqProperty e)
        {
            e.SetLanguagePropertyGrid(Languages.Rus);
            //e.SetLanguagePropertyGrid(basicProperties.Local.Common.Language);
        }

        private void UcSpecFreqForbidden_OnIsWindowPropertyOpen(object sender, SpecFreqControl.SpecFreqProperty e)
        {
            e.SetLanguagePropertyGrid(Languages.Rus);
            //e.SetLanguagePropertyGrid(basicProperties.Local.Common.Language);
        }

        private void UcSRangesSuppr_OnIsWindowPropertyOpen(object sender, SectorsRangesControl.SectorsRangesProperty e)
        {
            e.SetLanguagePropertyGrid(Languages.Rus);
            //e.SetLanguagePropertyGrid(basicProperties.Local.Common.Language);
        }

        private void UcASP_OnIsWindowPropertyOpen(object sender, ASPControl.ASPProperty e)
        {
            e.SetLanguagePropertyGrid(Languages.Rus);
            //e.SetLanguagePropertyGrid(basicProperties.Local.Common.Language);
        }

        private void UcSuppressFHSS_OnIsWindowPropertyOpen(object sender, SuppressFHSSControl.SuppressFHSSProperty e)
        {
            e.SetLanguagePropertyGrid(Languages.Rus);
            //e.SetLanguagePropertyGrid(basicProperties.Local.Common.Language);
        }

        private void UcSuppressFHSS_OnIsWindowPropertyOpenExc(object sender, SuppressFHSSControl.ExcludedFreqProperty e)
        {
            e.SetLanguagePropertyGrid(Languages.Rus);
            //e.SetLanguagePropertyGrid(basicProperties.Local.Common.Language);
        }

        private void UcSuppressFWS_OnIsWindowPropertyOpen(object sender, SuppressFWSControl.SuppressFWSProperty e)
        {
            e.SetLanguagePropertyGrid(Languages.Rus);
            //e.SetLanguagePropertyGrid(basicProperties.Local.Common.Language);
        }

      
    }
}
