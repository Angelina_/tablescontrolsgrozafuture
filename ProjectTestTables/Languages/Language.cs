﻿using ModelsTablesDBLib;
using System;
using System.ComponentModel;
using System.Windows;
using ValuesCorrectLib;

namespace ProjectTestTables
{
    public partial class MainWindow : Window, INotifyPropertyChanged
    {

        private void SetLanguageTables(Languages language)
        {
            ResourceDictionary dict = new ResourceDictionary();
            try
            {
                switch (language)
                {
                    case Languages.Eng:
                        dict.Source = new Uri("/ProjectTestTables;component/Languages/TranslatorTables/TranslatorTables.EN.xaml",
                                      UriKind.Relative);
                        break;
                    case Languages.Rus:
                        dict.Source = new Uri("/ProjectTestTables;component/Languages/TranslatorTables/TranslatorTables.RU.xaml",
                                           UriKind.Relative);
                        break;
                    case Languages.Azr:
                        dict.Source = new Uri("/ProjectTestTables;component/Languages/TranslatorTables/TranslatorTables.AZ.xaml",
                                           UriKind.Relative);
                        break;
                    default:
                        dict.Source = new Uri("/ProjectTestTables;component/Languages/TranslatorTables/TranslatorTables.RU.xaml",
                                          UriKind.Relative);
                        break;
                }

                this.Resources.MergedDictionaries.Add(dict);
            }
            catch (Exception ex)
            { }
        }

        private void basicProperties_OnLanguageChanged(object sender, Languages language)
        {
            SetLanguageTables(language);
            TranslatorTables.LoadDictionary(language);
        }

       
    }
}
