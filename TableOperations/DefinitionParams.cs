﻿using System;
using System.Collections.Generic;
using TableEvents;
using ValuesCorrectLib;

namespace TableOperations
{
    public class DefinitionParams
    {
        /// <summary>
        /// Определить литеру
        /// </summary>
        /// <param name="dFreq"></param>
        /// <returns></returns>
        public static byte DefineLetter(double dFreq)
        {
            return GetLetter(dFreq);
        }

        /// <summary>
        /// Определить литеры
        /// </summary>
        /// <param name="dFreqMin"></param>
        /// <param name="ddFreqMax"></param>
        /// <returns></returns>
        public static byte[] DefineLetters(double dFreqMin, double dFreqMax)
        {
            byte[] bMasLetters = null;

            // определить литеру для Fmin
            byte bLetterMin = GetLetter(dFreqMin);

            // определить литеру для Fmax
            byte bLetterMax = GetLetter(dFreqMax);

            if (bLetterMin != bLetterMax)
            {
                bMasLetters = new byte[2];
                bMasLetters[0] = bLetterMin;
                bMasLetters[1] = bLetterMax;
            }
            else if (bLetterMin == bLetterMax)
            {
                bMasLetters = new byte[1];
                bMasLetters[0] = bLetterMin;
            }

            return bMasLetters;
        }

        /// <summary>
        /// Определить полосу обзора
        /// </summary>
        /// <param name="dFreqMin"></param>
        /// <param name="dFreqMax"></param>
        /// <returns></returns>
        public static byte[] DefineEPO(double dFreqMin, double dFreqMax)
        {


            byte[] bMasEPO = null;

            int iEPO = 30000;

            byte bEPOmin = 0;
            byte bEPOmax = 0;

            ////////////////////////////
            List<FreqsEPO> ListEPO = GetFreqsEPO();

            // определить полосу для Fmin
            bEPOmin = Convert.ToByte(ListEPO.FindIndex(x => x.freqMin <= dFreqMin && dFreqMin < x.freqMax) + 1);

            // определить полосу для Fmax
            bEPOmax = Convert.ToByte(ListEPO.FindIndex(x => x.freqMin <= dFreqMax && dFreqMax < x.freqMax) + 1);

            if (bEPOmin != bEPOmax)
            {
                bMasEPO = new byte[2];
                bMasEPO[0] = bEPOmin;
                bMasEPO[1] = bEPOmax;
            }
            else if (bEPOmin == bEPOmax)
            {
                bMasEPO = new byte[1];
                bMasEPO[0] = bEPOmin;
            }
           
            return bMasEPO;
        }

        private static List<FreqsEPO> GetFreqsEPO()
        {
            List<FreqsEPO> list = new List<FreqsEPO>();

            switch (PropGlobalProperty.RangeJamming)
            {
                case 7:
                case 9:
                case 10:
                    for (int i = 25000; i < 6000000; i += ConstValues.Band_30000)
                    {
                        FreqsEPO freqsEPO = new FreqsEPO
                        {
                            freqMin = i,
                            freqMax = i + ConstValues.Band_30000
                        };
                
                        list.Add(freqsEPO);
                    }
                    break;
                    
                case 11:
                    for (int i = 0; i < 6000000; i += ConstValues.Band_80000)
                    {
                        FreqsEPO freqsEPO = new FreqsEPO
                        {
                            freqMin = i,
                            freqMax = i + ConstValues.Band_80000
                        };

                        list.Add(freqsEPO);
                    }
                    break;
            }
            return list;
        }

        /// <summary>
        /// Получить литеру
        /// </summary>
        /// <param name="dFreq"></param>
        /// <returns></returns>
        private static byte GetLetter(double dFreq)
        {
            byte bLetter = 0;

            if (dFreq >= RangesLetters.FREQ_START_LETTER_1 & dFreq < RangesLetters.FREQ_START_LETTER_2)
                bLetter = 1;
            if (dFreq >= RangesLetters.FREQ_START_LETTER_2 & dFreq < RangesLetters.FREQ_START_LETTER_3)
                bLetter = 2;
            if (dFreq >= RangesLetters.FREQ_START_LETTER_3 & dFreq < RangesLetters.FREQ_START_LETTER_4)
                bLetter = 3;
            if (dFreq >= RangesLetters.FREQ_START_LETTER_4 & dFreq < RangesLetters.FREQ_START_LETTER_5)
                bLetter = 4;
            if (dFreq >= RangesLetters.FREQ_START_LETTER_5 & dFreq < RangesLetters.FREQ_START_LETTER_6)
                bLetter = 5;
            if (dFreq >= RangesLetters.FREQ_START_LETTER_6 & dFreq < RangesLetters.FREQ_START_LETTER_7)
                bLetter = 6;
            if (dFreq >= RangesLetters.FREQ_START_LETTER_7 & dFreq < RangesLetters.FREQ_START_LETTER_8)
                bLetter = 7;
            if (dFreq >= RangesLetters.FREQ_START_LETTER_8 & dFreq < RangesLetters.FREQ_START_LETTER_9)
                bLetter = 8;
            switch (PropGlobalProperty.Amplifiers)
            {
                case 0:
                    if (dFreq >= RangesLetters.FREQ_START_LETTER_9 & dFreq <= RangesLetters.FREQ_START_LETTER_10)
                        bLetter = 9;
                    if (dFreq >= RangesLetters.FREQ_START_LETTER_10 & dFreq <= RangesLetters.FREQ_STOP_LETTER_10)
                        bLetter = 10;
                    break;

                case 1:
                    if (dFreq >= RangesLetters.FREQ_START_LETTER_9 & dFreq <= RangesLetters.FREQ_START_LETTER_10_MODIF)
                        bLetter = 9;
                    if (dFreq >= RangesLetters.FREQ_START_LETTER_10_MODIF & dFreq <= RangesLetters.FREQ_STOP_LETTER_10)
                        bLetter = 10;
                    break;
            }

            return bLetter;
        }
    }

    public class FreqsEPO
    {
        public int freqMin { get; set; }
        public int freqMax { get; set; }
    }
}
