﻿using ModelsTablesDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using TableEvents;
using TableOperations;
using ValuesCorrectLib;

namespace SuppressFWSControl
{
    public partial class UserControlSuppressFWS : UserControl
    {
        private bool IsSetFreq(byte bLetter)
        {
            int ind = ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS.ToList().FindIndex(x => x.Letter == bLetter);
            if (ind != -1)
            {
                int indFreqIridium = ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS.ToList().FindIndex(x => x.FreqKHz == ConstValues.dFreqIridium);
                int indFreqInmarsat = ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS.ToList().FindIndex(x => x.FreqKHz == ConstValues.dFreqInmarsat);
                int indFreq868 = ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS.ToList().FindIndex(x => x.FreqKHz == ConstValues.dFreq868);
                int indFreq920 = ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS.ToList().FindIndex(x => x.FreqKHz == ConstValues.dFreq920);
                int indFreqGS = ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS.ToList().FindIndex(x => x.FreqKHz == ConstValues.dFreqGlobalStar);

                int indFreq5_2 = ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS.ToList().FindIndex(x => x.FreqKHz == ConstValues.dFreq5_2);
                int indFreq5_8 = ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS.ToList().FindIndex(x => x.FreqKHz == ConstValues.dFreq5_8);

                switch (bLetter)
                {
                    case 7:

                        if ((indFreq868 != -1 && ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS[indFreq868].Sender == SignSender.UAV_860) ||
                            (indFreq920 != -1 && ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS[indFreq920].Sender == SignSender.UAV_920))
                        {
                            return true;
                        }
                        break;

                    case 8:

                        if ((indFreqIridium != -1 && ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS[indFreqIridium].Sender == SignSender.IR) ||
                            (indFreqInmarsat != -1 && ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS[indFreqInmarsat].Sender == SignSender.IN))
                        {
                            return true;
                        }
                        break;

                    case 9:

                        if (indFreqGS != -1 && ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS[indFreqGS].Sender == SignSender.GS)
                        {
                            return true;
                        }
                        break;

                    case 10:

                        if ((indFreq5_2 != -1 && ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS[indFreq5_2].Sender == SignSender.UAV_5200) ||
                            (indFreq5_8 != -1 && ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS[indFreq5_8].Sender == SignSender.UAV_5800))
                        {
                            return true;
                        }
                        break;
                }

                System.Windows.Forms.MessageBox.Show(SMessages.mesInBandLetterRESJamming + bLetter.ToString() + "!", SMessages.mesMessage, System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information);

                return false;
            }
            return true;
        }

        /// <summary>
        /// Проверка на наличие частот в таблице из литер 5, 7, 8, 9, 10
        /// </summary>
        /// <returns></returns>
        private bool IsSetFreqsTB(double Freq)
        {
            try
            {
                if (b433.IsChecked.Value && id433[0] != -2)
                {
                    if (Freq > RangesLetters.FREQ_START_LETTER_5 && Freq < RangesLetters.FREQ_START_LETTER_6)
                    {
                        System.Windows.Forms.MessageBox.Show(SMessages.mesInBandLetterRESJamming + "5!", SMessages.mesMessage, System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information);
                        return true;
                    }
                }

                if ((b868.IsChecked.Value && id868[0] != -2) || (b920.IsChecked.Value && id920[0] != -2))
                {
                    if (Freq > RangesLetters.FREQ_START_LETTER_7 && Freq < RangesLetters.FREQ_START_LETTER_8)
                    {
                        System.Windows.Forms.MessageBox.Show(SMessages.mesInBandLetterRESJamming + "7!", SMessages.mesMessage, System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information);
                        return true;
                    }
                }

                if ((bIridium.IsChecked.Value && idIridium[0] != -2) || (bInmarsat.IsChecked.Value && idInmarsat[0] != -2) || (bNAV.IsChecked.Value && idNAV_L1[0] != -2 && idNAV_L2[0] != -2))
                {
                    if (Freq > RangesLetters.FREQ_START_LETTER_8 && Freq < RangesLetters.FREQ_START_LETTER_9)
                    {
                        System.Windows.Forms.MessageBox.Show(SMessages.mesInBandLetterRESJamming + "8!", SMessages.mesMessage, System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information);
                        return true;
                    }
                }

                if ((b2_4.IsChecked.Value && id2_4[0] != -2) || (bGS.IsChecked.Value && idGlobalStar[0] != -2))
                {
                    switch(PropGlobalProperty.Amplifiers)
                    {
                        case 0: 
                            if (Freq > RangesLetters.FREQ_START_LETTER_9 && Freq < RangesLetters.FREQ_START_LETTER_10)
                            {
                                System.Windows.Forms.MessageBox.Show(SMessages.mesInBandLetterRESJamming + "9!", SMessages.mesMessage, System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information);
                                return true;
                            }
                            break;

                        case 1:
                            if (Freq > RangesLetters.FREQ_START_LETTER_9 && Freq < RangesLetters.FREQ_START_LETTER_10_MODIF)
                            {
                                System.Windows.Forms.MessageBox.Show(SMessages.mesInBandLetterRESJamming + "9!", SMessages.mesMessage, System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information);
                                return true;
                            }
                            break;
                    }
                }

                if ((b5_8.IsChecked.Value && id5_8[0] != -2) || (b5_2.IsChecked.Value && id5_2[0] != -2))
                {
                    switch (PropGlobalProperty.Amplifiers)
                    {
                        case 0:
                            if (Freq > RangesLetters.FREQ_START_LETTER_10 && Freq < RangesLetters.FREQ_STOP_LETTER_10)
                            {
                                System.Windows.Forms.MessageBox.Show(SMessages.mesInBandLetterRESJamming + "10!", SMessages.mesMessage, System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information);
                                return true;
                            }
                            break;

                        case 1:
                            if (Freq > RangesLetters.FREQ_START_LETTER_10_MODIF && Freq < RangesLetters.FREQ_STOP_LETTER_10)
                            {
                                System.Windows.Forms.MessageBox.Show(SMessages.mesInBandLetterRESJamming + "10!", SMessages.mesMessage, System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information);
                                return true;
                            }
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }

            return false;
        }

        /// <summary>
        /// Обновить состояние ToggleButtons
        /// </summary>
        /// <param name="list"></param>
        private void ToggleButtonsUpdate(List<TableSuppressFWS> list)
        {
            try
            {
                int indFreqIidium = list.FindIndex(x => x.FreqKHz == ConstValues.dFreqIridium && x.Sender == SignSender.IR);
                int indFreqInmarsat = list.FindIndex(x => x.FreqKHz == ConstValues.dFreqInmarsat && x.Sender == SignSender.IN);
                int indFreqGS = list.FindIndex(x => x.FreqKHz == ConstValues.dFreqGlobalStar && x.Sender == SignSender.GS);
                int indFreq433 = list.FindIndex(x => x.FreqKHz == ConstValues.dFreq433 && x.Sender == SignSender.UAV_433);
                int indFreq868 = list.FindIndex(x => x.FreqKHz == ConstValues.dFreq868 && x.Sender == SignSender.UAV_860);
                int indFreq920 = list.FindIndex(x => x.FreqKHz == ConstValues.dFreq920 && x.Sender == SignSender.UAV_920);
                int indFreq2_4 = list.FindIndex(x => x.FreqKHz == ConstValues.dFreq2_4 && x.Sender == SignSender.UAV_2400);
                int indFreq5_2 = list.FindIndex(x => x.FreqKHz == ConstValues.dFreq5_2 && x.Sender == SignSender.UAV_5200);
                int indFreq5_8 = list.FindIndex(x => x.FreqKHz == ConstValues.dFreq5_8 && x.Sender == SignSender.UAV_5800);
                int indFreqNAV_L1 = list.FindIndex(x => x.FreqKHz == ConstValues.dFreqNAV_L1 && x.Sender == SignSender.NAV_L1);
                int indFreqNAV_L2 = list.FindIndex(x => x.FreqKHz == ConstValues.dFreqNAV_L2 && x.Sender == SignSender.NAV_L2);

                if (indFreqIidium != -1) bIridium.IsChecked = true;
                else bIridium.IsChecked = false;

                if (indFreqInmarsat != -1) bInmarsat.IsChecked = true;
                else bInmarsat.IsChecked = false;

                if (indFreqGS != -1) bGS.IsChecked = true;
                else bGS.IsChecked = false;

                if (indFreq433 != -1) b433.IsChecked = true;
                else b433.IsChecked = false;
                
                if (indFreq868 != -1) b868.IsChecked = true;
                else b868.IsChecked = false;

                if (indFreq920 != -1) b920.IsChecked = true;
                else b920.IsChecked = false;

                if (indFreq2_4 != -1) b2_4.IsChecked = true;
                else b2_4.IsChecked = false;

                 if (indFreq5_2 != -1) b5_2.IsChecked = true;
                else b5_2.IsChecked = false;

                if (indFreq5_8 != -1) b5_8.IsChecked = true;
                else b5_8.IsChecked = false;

                if (indFreqNAV_L1 != -1 && indFreqNAV_L2 != -1) bNAV.IsChecked = true;
                else bNAV.IsChecked = false;

                if (indFreq433 != -1 && indFreq868 != -1 && indFreq920 != -1 && indFreq2_4 != -1 && indFreq5_8 != -1) bBPLA.IsChecked = true;
                else bBPLA.IsChecked = false;
            }
            catch { }
        }

        /// <summary>
        /// Получить запись для подавления в литерах 5, 7, 8, 9, 10
        /// </summary>
        /// <param name="dFreq"> Частота </param>
        /// <param name="bModulation"> Код модуляции </param>
        /// <returns></returns>
        private TableSuppressFWS GetSuppressFWS5_7_8_9_10(double dFreq, byte bModulation, byte bManipulation, byte bDeviation, byte bDuration, SignSender signSender)
        {
            return new TableSuppressFWS
            {
                Id = 0,
                NumberASP = PropNumberASP.SelectedNumASP,
                FreqKHz = dFreq,
                Bearing = 0,
                Letter = DefinitionParams.DefineLetter(dFreq),
                Threshold = -130,
                Priority = 1,
                Sender = signSender,
                Coordinates = new Coord
                {
                    Latitude = -1,
                    Longitude = -1,
                    Altitude = -1
                },
                InterferenceParam = new InterferenceParam
                {
                    Modulation = bModulation,
                    Manipulation = bManipulation,
                    Deviation = bDeviation,
                    Duration = 1 //4 //bDuration
                }
            };
        }

        /// <summary>
        /// Удалить частоту из литер 5, 7, 8, 9, 10
        /// </summary>
        /// <param name="id"></param>
        /// <param name="numASP"></param>
        private TableSuppressFWS DeleteFreq5_7_8_9_10(int id, int numASP, SignSender signSender)
        {
            if (id > 0 && numASP > 0)
            {
                TableSuppressFWS tableSuppressFWS = new TableSuppressFWS
                {
                    NumberASP = numASP,
                    Id = id,
                    Sender = signSender
                };

                return tableSuppressFWS;
            }

            return null;
        }

        /// <summary>
        /// Видимость кнопок в зависимости от заданного диапазона РП
        /// </summary>
        public void ToggleButtonVisible()
        {
            gridButtons.ColumnDefinitions[15].Width = new GridLength(30); // b433
            gridButtons.ColumnDefinitions[16].Width = new GridLength(30); // b868
            gridButtons.ColumnDefinitions[17].Width = new GridLength(30); // b920

            switch (PropGlobalProperty.RangeJamming)
            {
                case 7:

                    gridButtons.ColumnDefinitions[12].Width = new GridLength(0); // bIridium
                    gridButtons.ColumnDefinitions[13].Width = new GridLength(0); // bInmarsat
                    gridButtons.ColumnDefinitions[14].Width = new GridLength(0); // bGS
                    gridButtons.ColumnDefinitions[18].Width = new GridLength(0); // b2_4
                    gridButtons.ColumnDefinitions[19].Width = new GridLength(0); // b5_2
                    gridButtons.ColumnDefinitions[20].Width = new GridLength(0); // b5_8
                    gridButtons.ColumnDefinitions[21].Width = new GridLength(0); // bBPLA
                    gridButtons.ColumnDefinitions[22].Width = new GridLength(0); // bNAV

                    break;

                case 9:

                    gridButtons.ColumnDefinitions[12].Width = new GridLength(30); // bIridium
                    gridButtons.ColumnDefinitions[13].Width = new GridLength(30); // bInmarsat
                    gridButtons.ColumnDefinitions[14].Width = new GridLength(30); // bGS
                    gridButtons.ColumnDefinitions[18].Width = new GridLength(30); // b2_4
                    gridButtons.ColumnDefinitions[19].Width = new GridLength(0); // b5_2
                    gridButtons.ColumnDefinitions[20].Width = new GridLength(0); // b5_8
                    gridButtons.ColumnDefinitions[21].Width = new GridLength(0); // bBPLA
                    gridButtons.ColumnDefinitions[22].Width = new GridLength(46); // bNAV

                    break;

                case 10:

                    gridButtons.ColumnDefinitions[12].Width = new GridLength(30); // bIridium
                    gridButtons.ColumnDefinitions[13].Width = new GridLength(30); // bInmarsat
                    gridButtons.ColumnDefinitions[14].Width = new GridLength(30); // bGS
                    gridButtons.ColumnDefinitions[18].Width = new GridLength(30); // b2_4
                    gridButtons.ColumnDefinitions[19].Width = new GridLength(30); // b5_2
                    gridButtons.ColumnDefinitions[20].Width = new GridLength(30); // b5_8
                    gridButtons.ColumnDefinitions[21].Width = new GridLength(46); // bBPLA
                    gridButtons.ColumnDefinitions[22].Width = new GridLength(46); // bNAV

                    break;

                default:

                    gridButtons.ColumnDefinitions[12].Width = new GridLength(30); // bIridium
                    gridButtons.ColumnDefinitions[13].Width = new GridLength(30); // bInmarsat
                    gridButtons.ColumnDefinitions[14].Width = new GridLength(30); // bGS
                    gridButtons.ColumnDefinitions[18].Width = new GridLength(30); // b2_4
                    gridButtons.ColumnDefinitions[19].Width = new GridLength(30); // b5_2
                    gridButtons.ColumnDefinitions[20].Width = new GridLength(30); // b5_8
                    gridButtons.ColumnDefinitions[21].Width = new GridLength(46); // bBPLA
                    gridButtons.ColumnDefinitions[22].Width = new GridLength(46); // bNAV

                    break;
            }
        }

        private void AddIridium()
        {
            if (IsSetFreq(8))
            {
                TableSuppressFWS tableSuppressFWS = GetSuppressFWS5_7_8_9_10(ConstValues.dFreqIridium, 0x0B, 148, 130, 0, SignSender.IR);
                OnAddRecord(this, new TableEvent(tableSuppressFWS));
            }
        }

        private TableSuppressFWS DeleteIridium(int indFreqIridium, IsBPLA_NAV isNAV)
        {
            TableSuppressFWS tableSuppressFWS = DeleteFreq5_7_8_9_10(((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS[indFreqIridium].Id,
                ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS[indFreqIridium].NumberASP, SignSender.IR);

            switch (isNAV)
            {
                case IsBPLA_NAV.ClickNAV:

                    return tableSuppressFWS;
            }

            OnDeleteRecord(this, new TableEvent(tableSuppressFWS));

            return null;
        }

        private void AddInmarsat()
        {
            if (IsSetFreq(8))
            {
                TableSuppressFWS tableSuppressFWS = GetSuppressFWS5_7_8_9_10(ConstValues.dFreqInmarsat, 0x0B, 138, 145, 0, SignSender.IN);
                OnAddRecord(this, new TableEvent(tableSuppressFWS));
            }
        }

        private TableSuppressFWS DeleteInmarsat(int indFreqInmarsat, IsBPLA_NAV isNAV)
        {
            TableSuppressFWS tableSuppressFWS = DeleteFreq5_7_8_9_10(((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS[indFreqInmarsat].Id,
                ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS[indFreqInmarsat].NumberASP, SignSender.IN);

            switch (isNAV)
            {
                case IsBPLA_NAV.ClickNAV:

                    return tableSuppressFWS;
            }

            OnDeleteRecord(this, new TableEvent(tableSuppressFWS));

            return null;
        }

        private TableSuppressFWS Add2_4(IsBPLA_NAV isBPLA)
        {
            if (IsSetFreq(9))
            {
                TableSuppressFWS tableSuppressFWS = GetSuppressFWS5_7_8_9_10(ConstValues.dFreq2_4, 0x0B, 178, 171, 0, SignSender.UAV_2400);

                switch (isBPLA)
                {
                    case IsBPLA_NAV.ClickBPLA:

                        return tableSuppressFWS;
                }

                OnAddRecord(this, new TableEvent(tableSuppressFWS));
            }

            return null;
        }

        private TableSuppressFWS Delete2_4(int indFreq2_4, IsBPLA_NAV isBPLA)
        {
            TableSuppressFWS tableSuppressFWS = DeleteFreq5_7_8_9_10(((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS[indFreq2_4].Id,
                ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS[indFreq2_4].NumberASP, SignSender.UAV_2400);

            switch(isBPLA)
            {
                case IsBPLA_NAV.ClickBPLA:
                    
                    return tableSuppressFWS;
            }

            OnDeleteRecord(this, new TableEvent(tableSuppressFWS));

            return null;
        }

        private TableSuppressFWS Add5_2(IsBPLA_NAV isBPLA)
        {
            if (IsSetFreq(10))
            {
                TableSuppressFWS tableSuppressFWS = GetSuppressFWS5_7_8_9_10(ConstValues.dFreq5_2, 0x0B, 178, 193, 0, SignSender.UAV_5200);

                switch (isBPLA)
                {
                    case IsBPLA_NAV.ClickBPLA:

                        return tableSuppressFWS;
                }

                OnAddRecord(this, new TableEvent(tableSuppressFWS));
            }

            return null;
        }

        private TableSuppressFWS Delete5_2(int indFreq5_2, IsBPLA_NAV isBPLA)
        {
            TableSuppressFWS tableSuppressFWS = DeleteFreq5_7_8_9_10(((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS[indFreq5_2].Id,
                ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS[indFreq5_2].NumberASP, SignSender.UAV_5200);

            switch(isBPLA)
            {
                case IsBPLA_NAV.ClickBPLA:
                    
                    return tableSuppressFWS;
            }

            OnDeleteRecord(this, new TableEvent(tableSuppressFWS));

            return null;
        }

        private TableSuppressFWS Add5_8(IsBPLA_NAV isBPLA)
        {
            if (IsSetFreq(10))
            {
                TableSuppressFWS tableSuppressFWS = GetSuppressFWS5_7_8_9_10(ConstValues.dFreq5_8, 0x0B, 178, 203, 0, SignSender.UAV_5800);

                switch (isBPLA)
                {
                    case IsBPLA_NAV.ClickBPLA:

                        return tableSuppressFWS;
                }

                OnAddRecord(this, new TableEvent(tableSuppressFWS));
            }

            return null;
        }

        private TableSuppressFWS Delete5_8(int indFreq5_8, IsBPLA_NAV isBPLA)
        {
            TableSuppressFWS tableSuppressFWS = DeleteFreq5_7_8_9_10(((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS[indFreq5_8].Id,
                ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS[indFreq5_8].NumberASP, SignSender.UAV_5800);

            switch (isBPLA)
            {
                case IsBPLA_NAV.ClickBPLA:

                    return tableSuppressFWS;
            }

            OnDeleteRecord(this, new TableEvent(tableSuppressFWS));

            return null;
        }

        private void AddGS()
        {
            if (IsSetFreq(9))
            {
                TableSuppressFWS tableSuppressFWS = GetSuppressFWS5_7_8_9_10(ConstValues.dFreqGlobalStar, 0x0B, 148, 136, 0, SignSender.GS);
                OnAddRecord(this, new TableEvent(tableSuppressFWS));
            }
        }

        private void DeleteGS(int indFreqGlobalStar)
        {
            TableSuppressFWS tableSuppressFWS = DeleteFreq5_7_8_9_10(((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS[indFreqGlobalStar].Id,
                ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS[indFreqGlobalStar].NumberASP, SignSender.GS);

            OnDeleteRecord(this, new TableEvent(tableSuppressFWS));
        }

        private TableSuppressFWS Add433(IsBPLA_NAV isBPLA )
        {
            if (IsSetFreq(5))
            {
                TableSuppressFWS tableSuppressFWS = GetSuppressFWS5_7_8_9_10(ConstValues.dFreq433, 0x0B, 148, 130, 0, SignSender.UAV_433);

                switch (isBPLA)
                {
                    case IsBPLA_NAV.ClickBPLA:

                        return tableSuppressFWS;
                }

                OnAddRecord(this, new TableEvent(tableSuppressFWS));
            }
           
            return null;
        }

        private TableSuppressFWS Delete433(int indFreq433, IsBPLA_NAV isBPLA)
        {
            TableSuppressFWS tableSuppressFWS = DeleteFreq5_7_8_9_10(((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS[indFreq433].Id,
                ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS[indFreq433].NumberASP, SignSender.UAV_433);

            switch (isBPLA)
            {
                case IsBPLA_NAV.ClickBPLA:

                    return tableSuppressFWS;
            }

            OnDeleteRecord(this, new TableEvent(tableSuppressFWS));

            return null;
        }

        private TableSuppressFWS Add868(IsBPLA_NAV isBPLA)
        {
            if (IsSetFreq(7))
            {
                TableSuppressFWS tableSuppressFWS = GetSuppressFWS5_7_8_9_10(ConstValues.dFreq868, 0x0B, 148, 132, 0, SignSender.UAV_860);

                switch (isBPLA)
                {
                    case IsBPLA_NAV.ClickBPLA:

                        return tableSuppressFWS;
                }

                OnAddRecord(this, new TableEvent(tableSuppressFWS));
            }

            return null;
        }

        private TableSuppressFWS Delete868(int indFreq868, IsBPLA_NAV isBPLA)
        {
            TableSuppressFWS tableSuppressFWS = DeleteFreq5_7_8_9_10(((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS[indFreq868].Id,
                ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS[indFreq868].NumberASP, SignSender.UAV_860);

            switch (isBPLA)
            {
                case IsBPLA_NAV.ClickBPLA:

                    return tableSuppressFWS;
            }

            OnDeleteRecord(this, new TableEvent(tableSuppressFWS));

            return null;
        }

        private TableSuppressFWS Add920(IsBPLA_NAV isBPLA)
        {
            if (IsSetFreq(7))
            {
                TableSuppressFWS tableSuppressFWS = GetSuppressFWS5_7_8_9_10(ConstValues.dFreq920, 0x0B, 148, 141, 0, SignSender.UAV_920);

                switch (isBPLA)
                {
                    case IsBPLA_NAV.ClickBPLA:

                        return tableSuppressFWS;
                }

                OnAddRecord(this, new TableEvent(tableSuppressFWS));
            }

            return null;
        }

        private TableSuppressFWS Delete920(int indFreq920, IsBPLA_NAV isBPLA)
        {
            TableSuppressFWS tableSuppressFWS = DeleteFreq5_7_8_9_10(((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS[indFreq920].Id,
                ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS[indFreq920].NumberASP, SignSender.UAV_920);

            switch (isBPLA)
            {
                case IsBPLA_NAV.ClickBPLA:

                    return tableSuppressFWS;
            }

            OnDeleteRecord(this, new TableEvent(tableSuppressFWS));

            return null;
        }

        private void DeleteBPLA(int indFreq433, int indFreq868, int indFreq920, int indFreq2_4, int indFreq5_2, int indFreq5_8, IsBPLA_NAV isBPLA)
        {
            List<TableSuppressFWS> list = new List<TableSuppressFWS>();

            TableSuppressFWS table2_4 = Delete2_4(indFreq2_4, isBPLA);
            if (table2_4 != null)
                list.Add(table2_4);

            TableSuppressFWS table5_2 = Delete5_2(indFreq5_2, isBPLA);
            if (table5_2 != null)
                list.Add(table5_2);

            TableSuppressFWS table5_8 = Delete5_8(indFreq5_8, isBPLA);
            if (table5_8 != null)
                list.Add(table5_8);

            TableSuppressFWS table433 = Delete433(indFreq433, isBPLA);
            if (table433 != null)
                list.Add(table433);

            TableSuppressFWS table868 = Delete868(indFreq868, isBPLA);
            if (table868 != null)
                list.Add(table868);

            TableSuppressFWS table920 = Delete920(indFreq920, isBPLA);
            if (table920 != null)
                list.Add(table920);

            if (list.Count > 0)
            {
                OnDeleteRange(this, list);
            }
        }

        private void AddNAV()
        {
            MessageBoxResult result = MessageBox.Show(SMessages.mesNavigationBroken, SMessages.mesMessage, MessageBoxButton.YesNo, MessageBoxImage.Warning);
            if (result == MessageBoxResult.Yes)
            {
                List<TableSuppressFWS> listNAV = new List<TableSuppressFWS>()
                {
                    GetSuppressFWS5_7_8_9_10(ConstValues.dFreqNAV_L1, 0x0B, 228, 136, 0, SignSender.NAV_L1),
                    GetSuppressFWS5_7_8_9_10(ConstValues.dFreqNAV_L2, 0x0B, 228, 132, 0, SignSender.NAV_L2)
                };

                if(listNAV.Count > 0)
                {
                    OnAddRange(this, listNAV);
                }
            }
        }

        private void DeleteNAV(int indFreqNAV_L1, int indFreqNAV_L2)
        {
            List<TableSuppressFWS> list = new List<TableSuppressFWS>();

            TableSuppressFWS tableNAV_L1 = DeleteFreq5_7_8_9_10(((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS[indFreqNAV_L1].Id,
                ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS[indFreqNAV_L1].NumberASP, SignSender.NAV_L1);
            if(tableNAV_L1 != null)
                list.Add(tableNAV_L1);

            TableSuppressFWS tableNAV_L2 = DeleteFreq5_7_8_9_10(((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS[indFreqNAV_L2].Id,
                ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS[indFreqNAV_L2].NumberASP, SignSender.NAV_L2);
            if(tableNAV_L2 != null)
                list.Add(tableNAV_L2);

            if (list.Count > 0)
            {
                OnDeleteRange(this, list);
            }
        }
    }
}
