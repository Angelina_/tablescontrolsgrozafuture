﻿using ModelsTablesDBLib;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using TableEvents;
using TableOperations;
using ValuesCorrectLib;

namespace SuppressFWSControl
{
    /// <summary>
    /// Логика взаимодействия для SuppressFWSControl.xaml
    /// </summary>
    public partial class UserControlSuppressFWS : UserControl
    {
        #region Events
        public event EventHandler<TableEvent> OnAddRecord = (object srnder, TableEvent data) => { };
        public event EventHandler<TableEvent> OnChangeRecord = (object srnder, TableEvent data) => { };
        public event EventHandler<TableEvent> OnDeleteRecord = (object srnder, TableEvent data) => { };
        public event EventHandler<NameTable> OnClearRecords = (object srnder, NameTable data) => { };
        public event EventHandler<List<TableSuppressFWS>> OnDeleteRange = (object sender, List<TableSuppressFWS> data) => { };
        public event EventHandler<List<TableSuppressFWS>> OnAddRange = (object sender, List<TableSuppressFWS> data) => { };
        // Открылось окно с PropertyGrid
        public event EventHandler<SuppressFWSProperty> OnIsWindowPropertyOpen = (object sender, SuppressFWSProperty data) => { };
        // Отправить частоту на КРПУ1
        public event EventHandler<TempFWS> OnSendFreqCRRD = (object sender, TempFWS data) => { };
        // Отправить частоту на КРПУ2
        public event EventHandler<TempFWS> OnSendFreqCRRD2 = (object sender, TempFWS data) => { };
        // Отправить запрос на исполнительное пеленгование
        public event EventHandler<TableSuppressFWS> OnGetExecBear = (object sender, TableSuppressFWS data) => { };

        public event EventHandler<TableEventReport> OnAddTableToReport = (object sender, TableEventReport data) => { };

        public event EventHandler<TableEvent> OnCheckButtonsNAV = (object sender, TableEvent data) => { };
        #endregion

        #region Properties
        public NameTable NameTable { get; } = NameTable.TableSuppressFWS;

        #endregion

        public UserControlSuppressFWS()
        {
            InitializeComponent();

            dgvSuppressFWS.DataContext = new GlobalSuppressFWS();


        }

        private void ButtonAdd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (PropNumberASP.SelectedNumASP != 0 && PropNumberASP.IsSelectedRowASP)
                {
                    var SuppressFWSWindow = new SuppressFWSProperty(((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS);
                    SuppressFWSWindow.TableSuppressFWSAll.NumberASP = PropNumberASP.SelectedNumASP;

                    ComboBoxEditor.IndComboBoxModulation = 4;
                    ComboBoxEditor.IndComboBoxManipulation = 8;
                    ComboBoxEditor.IndComboBoxDeviation = 0;
                    ComboBoxEditor.IndComboBoxDuration = 0;
                    ComboBoxEditor.ValueDecUpDownDeviation = 10;
                    ComboBoxEditor.ValueDecUpDownManipulation = 10;

                    OnIsWindowPropertyOpen(this, SuppressFWSWindow);

                    if (SuppressFWSWindow.ShowDialog() == true)
                    {
                        if (!IsSetFreqsTB(SuppressFWSWindow.TableSuppressFWSAll.FreqKHz.Value))
                        {
                            SuppressFWSWindow.TableSuppressFWSAll.Sender = SignSender.Hand;

                                //Событие добавления одной записи
                                OnAddRecord(this, new TableEvent(TableSuppressFWSAllToTableSuppressFWS(SuppressFWSWindow.TableSuppressFWSAll)));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void ButtonChange_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if ((dgvSuppressFWS.DataContext as GlobalSuppressFWS).SelectedSuppressFWS != null)
                {
                    if ((dgvSuppressFWS.DataContext as GlobalSuppressFWS).SelectedSuppressFWS.Id > 0)
                    {
                        var selected = (dgvSuppressFWS.DataContext as GlobalSuppressFWS).SelectedSuppressFWS;

                        var SuppressFWSWindow = new SuppressFWSProperty(((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS, selected.Clone());

                        OnIsWindowPropertyOpen(this, SuppressFWSWindow);

                        if (SuppressFWSWindow.ShowDialog() == true)
                        {
                            TableSuppressFWSAll supprReplace = new TableSuppressFWSAll();

                            supprReplace = SuppressFWSWindow.TableSuppressFWSAll;

                            if (selected != supprReplace)
                            {
                                supprReplace.Sender = SignSender.Hand;
                            }

                            // Событие изменения одной записи
                            OnChangeRecord(this, new TableEvent(TableSuppressFWSAllToTableSuppressFWS(supprReplace)));
                        }
                    }
                }
            }
            catch { }
        }

        private void ButtonDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!IsSelectedRowEmpty())
                    return;

                TableSuppressFWS tableSuppressFWS = new TableSuppressFWS
                {
                    Id = ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).SelectedSuppressFWS.Id,
                    NumberASP = ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).SelectedSuppressFWS.NumberASP
                };

                // Событие удаления одной записи
                OnDeleteRecord(this, new TableEvent(tableSuppressFWS));
            }
            catch { }
        }

        private void ButtonClear_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // Событие удаления всех записей
                OnClearRecords(this, NameTable);
            }
            catch { }
        }

        private void ButtonWord_Click(object sender, RoutedEventArgs e)
        {
            string[,] Table = AddSuppressFWSToTable();

            OnAddTableToReport(this, new TableEventReport(Table, NameTable.TableSuppressFWS));
        }

        private void ButtonPrint_Click(object sender, RoutedEventArgs e)
        {

        }

        private void DgvSuppressFWS_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            AddEmptyRows();
        }

        private void dgvSuppressFWS_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            try
            {
                TableSuppressFWSAll row = (TableSuppressFWSAll)e.Row.DataContext;

                if ((row.FreqKHz == ConstValues.dFreqIridium) ||
                    (row.FreqKHz == ConstValues.dFreqInmarsat) ||
                    (row.FreqKHz == ConstValues.dFreq2_4) ||
                    (row.FreqKHz == ConstValues.dFreq5_8) ||
                    (row.FreqKHz == ConstValues.dFreqGlobalStar) ||
                    (row.FreqKHz == ConstValues.dFreq433) ||
                    (row.FreqKHz == ConstValues.dFreq868) ||
                    (row.FreqKHz == ConstValues.dFreq920) ||
                    (row.FreqKHz == ConstValues.dFreqNAV_L1) ||
                    (row.FreqKHz == ConstValues.dFreqNAV_L2))
                {
                    if (row.FreqKHz == ConstValues.dFreqIridium && bIridium.IsChecked == true)
                    {
                        idIridium[0] = row.Id;
                        idIridium[1] = row.NumberASP;

                        //e.Row.Visibility = Visibility.Hidden;
                        //AddOneEmptyRow(row);
                    }

                    if (row.FreqKHz == ConstValues.dFreqInmarsat && bInmarsat.IsChecked == true)
                    {
                        idInmarsat[0] = row.Id;
                        idInmarsat[1] = row.NumberASP;

                        //e.Row.Visibility = Visibility.Hidden;
                        //AddOneEmptyRow(row);
                    }

                    if (row.FreqKHz == ConstValues.dFreq2_4 && b2_4.IsChecked == true)
                    {
                        id2_4[0] = row.Id;
                        id2_4[1] = row.NumberASP;

                        //e.Row.Visibility = Visibility.Hidden;
                        //AddOneEmptyRow(row);
                    }

                    if (row.FreqKHz == ConstValues.dFreq5_2 && b5_2.IsChecked == true)
                    {
                        id5_2[0] = row.Id;
                        id5_2[1] = row.NumberASP;

                        //e.Row.Visibility = Visibility.Hidden;
                        //AddOneEmptyRow(row);
                    }

                    if (row.FreqKHz == ConstValues.dFreq5_8 && b5_8.IsChecked == true)
                    {
                        id5_8[0] = row.Id;
                        id5_8[1] = row.NumberASP;

                        //e.Row.Visibility = Visibility.Hidden;
                        //AddOneEmptyRow(row);
                    }

                    if (row.FreqKHz == ConstValues.dFreqGlobalStar && bGS.IsChecked == true)
                    {
                        idGlobalStar[0] = row.Id;
                        idGlobalStar[1] = row.NumberASP;

                        //e.Row.Visibility = Visibility.Hidden;
                        //AddOneEmptyRow(row);
                    }

                    if (row.FreqKHz == ConstValues.dFreq433 && b433.IsChecked == true)
                    {
                        id433[0] = row.Id;
                        id433[1] = row.NumberASP;

                        //e.Row.Visibility = Visibility.Hidden;
                        //AddOneEmptyRow(row);
                    }

                    if (row.FreqKHz == ConstValues.dFreq868 && b868.IsChecked == true)
                    {
                        id868[0] = row.Id;
                        id868[1] = row.NumberASP;

                        //e.Row.Visibility = Visibility.Hidden;
                        //AddOneEmptyRow(row);
                    }

                    if (row.FreqKHz == ConstValues.dFreq920 && b920.IsChecked == true)
                    {
                        id920[0] = row.Id;
                        id920[1] = row.NumberASP;

                        //e.Row.Visibility = Visibility.Hidden;
                        //AddOneEmptyRow(row);
                    }

                    if (row.FreqKHz == ConstValues.dFreqNAV_L1 && bNAV.IsChecked == true)
                    {
                        idNAV_L1[0] = row.Id;
                        idNAV_L1[1] = row.NumberASP;

                        //e.Row.Visibility = Visibility.Hidden;
                        //AddOneEmptyRow(row);
                    }

                    if (row.FreqKHz == ConstValues.dFreqNAV_L2 && bNAV.IsChecked == true)
                    {
                        idNAV_L2[0] = row.Id;
                        idNAV_L2[1] = row.NumberASP;

                        //e.Row.Visibility = Visibility.Hidden;
                        //AddOneEmptyRow(row);
                    }
                }
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }
        }

        private void ButtonAddFWS_CRRD_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if ((TableSuppressFWSAll)dgvSuppressFWS.SelectedItem != null)
                {
                    if (!IsSelectedRowEmpty())
                        return;

                    TempFWS tempFWS = new TempFWS
                    {
                        FreqKHz = ((TableSuppressFWSAll)dgvSuppressFWS.SelectedItem).FreqKHz.Value
                    };

                    // Событие На КРПУ
                    OnSendFreqCRRD(this, tempFWS);
                }
            }
            catch { }
        }

        private void ButtonAddFWS_CRRD2_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if ((TableSuppressFWSAll)dgvSuppressFWS.SelectedItem != null)
                {
                    if (!IsSelectedRowEmpty())
                        return;

                    TempFWS tempFWS = new TempFWS
                    {
                        FreqKHz = ((TableSuppressFWSAll)dgvSuppressFWS.SelectedItem).FreqKHz.Value
                    };

                    // Событие На КРПУ
                    OnSendFreqCRRD2(this, tempFWS);
                }
            }
            catch { }
        }

        private void ButtonGetExecBear_Click(object sender, RoutedEventArgs e)
        {
            if ((TableSuppressFWSAll)dgvSuppressFWS.SelectedItem != null)
            {
                if (!IsSelectedRowEmpty())
                    return;

                TableSuppressFWS tableSuppressFWS = new TableSuppressFWS
                {
                    Id = ((TableSuppressFWSAll)dgvSuppressFWS.SelectedItem).Id,
                    NumberASP = ((TableSuppressFWSAll)dgvSuppressFWS.SelectedItem).NumberASP,
                    FreqKHz = ((TableSuppressFWSAll)dgvSuppressFWS.SelectedItem).FreqKHz.Value,
                    Bearing = ((TableSuppressFWSAll)dgvSuppressFWS.SelectedItem).Bearing.Value,
                    Letter = ((TableSuppressFWSAll)dgvSuppressFWS.SelectedItem).Letter,
                    Threshold = ((TableSuppressFWSAll)dgvSuppressFWS.SelectedItem).Threshold,
                    Priority = ((TableSuppressFWSAll)dgvSuppressFWS.SelectedItem).Priority,
                    Coordinates = new Coord
                    {
                        Latitude = ((TableSuppressFWSAll)dgvSuppressFWS.SelectedItem).Coordinates.Latitude,
                        Longitude = ((TableSuppressFWSAll)dgvSuppressFWS.SelectedItem).Coordinates.Longitude,
                        Altitude = ((TableSuppressFWSAll)dgvSuppressFWS.SelectedItem).Coordinates.Altitude
                    },
                    InterferenceParam = new InterferenceParam
                    {
                        Modulation = ((TableSuppressFWSAll)dgvSuppressFWS.SelectedItem).InterferenceParam.Modulation,
                        Manipulation = ((TableSuppressFWSAll)dgvSuppressFWS.SelectedItem).InterferenceParam.Manipulation,
                        Deviation = ((TableSuppressFWSAll)dgvSuppressFWS.SelectedItem).InterferenceParam.Deviation,
                        Duration = ((TableSuppressFWSAll)dgvSuppressFWS.SelectedItem).InterferenceParam.Duration
                    }
                };

                // Событие Запрос на исполнительное пеленгование
                OnGetExecBear(this, tableSuppressFWS);
            }
        }

        private void grid_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            try
            {
                if (e.Key == System.Windows.Input.Key.Space)
                    ButtonAddFWS_CRRD_Click(this, null);

                if (e.Key == System.Windows.Input.Key.E)
                    ButtonGetExecBear_Click(this, null);
            }
            catch { }
        }

        private void dgvSuppressFWS_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if ((TableSuppressFWSAll)dgvSuppressFWS.SelectedItem == null) return;

                if (((TableSuppressFWSAll)dgvSuppressFWS.SelectedItem).Id > 0)
                {
                    if (((TableSuppressFWSAll)dgvSuppressFWS.SelectedItem).Id != PropNumSuppressFWS.SelectedIdSuppressFWS)
                    {
                        int ind = ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS.ToList().FindIndex(x => x.Id == ((TableSuppressFWSAll)dgvSuppressFWS.SelectedItem).Id);
                        if (ind != -1)
                        {
                            PropNumSuppressFWS.SelectedNumSuppressFWS = ind;
                            PropNumSuppressFWS.SelectedIdSuppressFWS = ((TableSuppressFWSAll)dgvSuppressFWS.SelectedItem).Id;
                        }
                    }
                }
                else
                {
                    PropNumSuppressFWS.SelectedNumSuppressFWS = 0;
                    PropNumSuppressFWS.SelectedIdSuppressFWS = 0;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }
        }

        #region Init id NAV
        public int[] idIridium = new int[2]; // [0] - id, [1] - numASP
        public int[] idInmarsat = new int[2];
        public int[] id2_4 = new int[2];
        public int[] id5_2 = new int[2];
        public int[] id5_8 = new int[2];
        public int[] idGlobalStar = new int[2];
        public int[] id433 = new int[2];
        public int[] id868 = new int[2];
        public int[] id920 = new int[2];
        public int[] idNAV_L1 = new int[2];
        public int[] idNAV_L2 = new int[2];

        #endregion

        private void bIridium_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                bIridium.IsChecked = false;

                int indFreqIridium = ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS.ToList().FindIndex(x => x.FreqKHz == ConstValues.dFreqIridium && x.Sender == SignSender.IR);
                if (indFreqIridium != -1)
                {
                    DeleteIridium(indFreqIridium, IsBPLA_NAV.ClickButtonUAV);
                }
                else
                {
                    int indFreqNAV_L1 = ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS.ToList().FindIndex(x => x.FreqKHz == ConstValues.dFreqNAV_L1 && x.Sender == SignSender.NAV_L1);
                    int indFreqNAV_L2 = ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS.ToList().FindIndex(x => x.FreqKHz == ConstValues.dFreqNAV_L2 && x.Sender == SignSender.NAV_L2);

                    if (indFreqNAV_L1 != -1 && indFreqNAV_L2 != -1)
                    {
                        return;
                    }

                    AddIridium();
                }
            }
            catch { }
        }

        private void bInmarsat_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                bInmarsat.IsChecked = false;

                int indFreqInmarsat = ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS.ToList().FindIndex(x => x.FreqKHz == ConstValues.dFreqInmarsat && x.Sender == SignSender.IN);
                if (indFreqInmarsat != -1)
                {
                    DeleteInmarsat(indFreqInmarsat, IsBPLA_NAV.ClickButtonUAV);
                }
                else
                {
                    int indFreqNAV_L1 = ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS.ToList().FindIndex(x => x.FreqKHz == ConstValues.dFreqNAV_L1 && x.Sender == SignSender.NAV_L1);
                    int indFreqNAV_L2 = ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS.ToList().FindIndex(x => x.FreqKHz == ConstValues.dFreqNAV_L2 && x.Sender == SignSender.NAV_L2);

                    if (indFreqNAV_L1 != -1 && indFreqNAV_L2 != -1)
                    {
                        return;
                    }

                    AddInmarsat();
                }
            }
            catch { }
        }

        private void bGS_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                bGS.IsChecked = false;

                int indFreq2_4 = ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS.ToList().FindIndex(x => x.FreqKHz == ConstValues.dFreq2_4 && x.Sender == SignSender.UAV_2400);
                if (indFreq2_4 != -1)
                {
                    return;
                }

                int indFreqGlobalStar = ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS.ToList().FindIndex(x => x.FreqKHz == ConstValues.dFreqGlobalStar && x.Sender == SignSender.GS);
                if (indFreqGlobalStar != -1)
                {
                    DeleteGS(indFreqGlobalStar);
                }
                else
                {
                    AddGS();
                }
            }
            catch { }
        }

        private void b433_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                b433.IsChecked = false;

                int indFreq433 = ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS.ToList().FindIndex(x => x.FreqKHz == ConstValues.dFreq433 && x.Sender == SignSender.UAV_433);
                if (indFreq433 != -1)
                {
                    Delete433(indFreq433, IsBPLA_NAV.ClickButtonUAV);
                }
                else
                {
                    Add433(IsBPLA_NAV.ClickButtonUAV);
                }
            }
            catch { }
        }

        private void b868_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                b868.IsChecked = false;

                int indFreq868 = ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS.ToList().FindIndex(x => x.FreqKHz == ConstValues.dFreq868 && x.Sender == SignSender.UAV_860);
                if (indFreq868 != -1)
                {
                    Delete868(indFreq868, IsBPLA_NAV.ClickButtonUAV);
                }
                else
                {
                    Add868(IsBPLA_NAV.ClickButtonUAV);
                }
            }
            catch { }
        }

        private void b920_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                b920.IsChecked = false;

                int indFreq920 = ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS.ToList().FindIndex(x => x.FreqKHz == ConstValues.dFreq920 && x.Sender == SignSender.UAV_920);
                if (indFreq920 != -1)
                {
                    Delete920(indFreq920, IsBPLA_NAV.ClickButtonUAV);
                }
                else
                {
                    Add920(IsBPLA_NAV.ClickButtonUAV);
                }
            }
            catch { }
        }

        private void b2_4_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                b2_4.IsChecked = false;

                int indFreq2_4 = ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS.ToList().FindIndex(x => x.FreqKHz == ConstValues.dFreq2_4 && x.Sender == SignSender.UAV_2400);
                if (indFreq2_4 != -1)
                {
                    Delete2_4(indFreq2_4, IsBPLA_NAV.ClickButtonUAV);
                }
                else
                {
                    int indFreqGlobalStar = ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS.ToList().FindIndex(x => x.FreqKHz == ConstValues.dFreqGlobalStar && x.Sender == SignSender.GS);
                    if (indFreqGlobalStar != -1)
                    {
                        DeleteGS(indFreqGlobalStar);
                    }

                    Add2_4(IsBPLA_NAV.ClickButtonUAV);
                }
            }
            catch { }
        }

        private void b5_2_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                b5_2.IsChecked = false;

                int indFreq5_2 = ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS.ToList().FindIndex(x => x.FreqKHz == ConstValues.dFreq5_2 && x.Sender == SignSender.UAV_5200);
                if (indFreq5_2 != -1)
                {
                    Delete5_2(indFreq5_2, IsBPLA_NAV.ClickButtonUAV);
                }
                else
                {
                    Add5_2(IsBPLA_NAV.ClickButtonUAV);
                }
            }
            catch { }
        }

        private void b5_8_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                b5_8.IsChecked = false;

                int indFreq5_8 = ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS.ToList().FindIndex(x => x.FreqKHz == ConstValues.dFreq5_8 && x.Sender == SignSender.UAV_5800);
                if (indFreq5_8 != -1)
                {
                    Delete5_8(indFreq5_8, IsBPLA_NAV.ClickButtonUAV);
                }
                else
                {
                    Add5_8(IsBPLA_NAV.ClickButtonUAV);
                }
            }
            catch { }
        }

        private void bBPLA_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                bBPLA.IsChecked = false;

                List<TableSuppressFWS> list = new List<TableSuppressFWS>();

                int indFreqGlobalStar = ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS.ToList().FindIndex(x => x.FreqKHz == ConstValues.dFreqGlobalStar && x.Sender == SignSender.GS);

                int indFreq433 = ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS.ToList().FindIndex(x => x.FreqKHz == ConstValues.dFreq433 && x.Sender == SignSender.UAV_433);
                int indFreq868 = ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS.ToList().FindIndex(x => x.FreqKHz == ConstValues.dFreq868 && x.Sender == SignSender.UAV_860);
                int indFreq920 = ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS.ToList().FindIndex(x => x.FreqKHz == ConstValues.dFreq920 && x.Sender == SignSender.UAV_920);
                int indFreq2_4 = ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS.ToList().FindIndex(x => x.FreqKHz == ConstValues.dFreq2_4 && x.Sender == SignSender.UAV_2400);
                int indFreq5_2 = ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS.ToList().FindIndex(x => x.FreqKHz == ConstValues.dFreq5_2 && x.Sender == SignSender.UAV_5200);
                int indFreq5_8 = ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS.ToList().FindIndex(x => x.FreqKHz == ConstValues.dFreq5_8 && x.Sender == SignSender.UAV_5800);

                if(indFreq433 != -1 && indFreq868 != -1 && indFreq920 != -1 && indFreq2_4 != -1 && indFreq5_8 != -1)
                {
                    DeleteBPLA(indFreq433, indFreq868, indFreq920, indFreq2_4, indFreq5_2, indFreq5_8, IsBPLA_NAV.ClickBPLA);
                }
                else
                {
                    if(indFreqGlobalStar != -1 && indFreq2_4 == -1)
                    {
                        DeleteGS(indFreqGlobalStar);
                    }
                    if(indFreq433 == -1)
                    {
                        TableSuppressFWS table = Add433(IsBPLA_NAV.ClickBPLA);
                        if(table != null)
                            list.Add(table);
                    }
                    if (indFreq868 == -1)
                    {
                        TableSuppressFWS table = Add868(IsBPLA_NAV.ClickBPLA);
                        if (table != null)
                            list.Add(table);
                    }
                    if (indFreq920 == -1)
                    {
                        TableSuppressFWS table = Add920(IsBPLA_NAV.ClickBPLA);
                        if (table != null)
                            list.Add(table);
                    }
                    if (indFreq2_4 == -1)
                    {
                        TableSuppressFWS table = Add2_4(IsBPLA_NAV.ClickBPLA);
                        if (table != null)
                            list.Add(table);
                    }
                    if (indFreq5_2 == -1)
                    {
                        TableSuppressFWS table = Add5_2(IsBPLA_NAV.ClickBPLA);
                        if (table != null)
                            list.Add(table);
                    }
                    if (indFreq5_8 == -1)
                    {
                        TableSuppressFWS table = Add5_8(IsBPLA_NAV.ClickBPLA);
                        if (table != null)
                            list.Add(table);
                    }

                    if (list.Count > 0)
                    {
                        OnAddRange(this, list);
                    }
                }
            }
            catch { }
        }

        private void bNAV_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                bNAV.IsChecked = false;

                int indFreqNAV_L1 = ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS.ToList().FindIndex(x => x.FreqKHz == ConstValues.dFreqNAV_L1 && x.Sender == SignSender.NAV_L1);
                int indFreqNAV_L2 = ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS.ToList().FindIndex(x => x.FreqKHz == ConstValues.dFreqNAV_L2 && x.Sender == SignSender.NAV_L2);

                if (indFreqNAV_L1 != -1 && indFreqNAV_L2 != -1)
                {
                    DeleteNAV(indFreqNAV_L1, indFreqNAV_L2);
                }
                else if(indFreqNAV_L1 == -1 && indFreqNAV_L2 == -1)
                {
                    List<TableSuppressFWS> list = new List<TableSuppressFWS>();
                    int indFreqIridium = ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS.ToList().FindIndex(x => x.FreqKHz == ConstValues.dFreqIridium && x.Sender == SignSender.IR);
                    if (indFreqIridium != -1)
                    {
                        list.Add(DeleteIridium(indFreqIridium, IsBPLA_NAV.ClickNAV));
                    }
                    int indFreqInmarsat = ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS.ToList().FindIndex(x => x.FreqKHz == ConstValues.dFreqInmarsat && x.Sender == SignSender.IN);
                    if (indFreqInmarsat != -1)
                    {
                        list.Add(DeleteInmarsat(indFreqInmarsat, IsBPLA_NAV.ClickNAV));
                    }
                    if (list.Count > 0)
                    {
                        OnDeleteRange(this, list);
                    }

                    AddNAV();
                }
            }
            catch { }
        }
    }
}
