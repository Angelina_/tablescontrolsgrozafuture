﻿using ModelsTablesDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using TableEvents;
using ValuesConverter;
using ValuesCorrectLib;

namespace SuppressFWSControl
{
    public partial class UserControlSuppressFWS : UserControl
    {
        private bool bManager = false;

        public List<TableSuppressFWS> TempListSuppressFWS { get; set; }

        /// <summary>
        /// Обновить записи в контроле
        /// </summary>
        /// <param name="listSuppressFWS"></param>
        public void UpdateSuppressFWS(List<TableSuppressFWS> listSuppressFWS)
        {
            try
            {
                //if (listSuppressFWS.Count == 0)
                //{
                //    TButtonUnchecked();
                //}

                TempListSuppressFWS = listSuppressFWS;

                ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS.Clear();

                List<TableSuppressFWSAll> list = SuppressFWSToSuppressFWSAll(listSuppressFWS);

                IEnumerable<TableSuppressFWSAll> sortLetter = list.OrderBy(l => l.Letter);                    // сортировка по литере
                IEnumerable<TableSuppressFWSAll> sortPriority = sortLetter.OrderByDescending(p => p.Priority); // сортировка по приоритету

               list = sortPriority.ToList();

                for (int i = 0; i < listSuppressFWS.Count; i++)
                {
                    ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS.Add(list[i]);
                }

                AddEmptyRows();

                ToggleButtonsUpdate(listSuppressFWS);

                UpdateRadioJamState(answerLed);

            }
            catch { }
        }

        public List<TempSuppressFWS> answerLed = new List<TempSuppressFWS>();
        /// <summary>
        /// ControlState -- КР
        /// RadiationState -- Излучение
        /// SuppressState -- РП
        /// </summary>
        /// <param name="answer"></param>
        public void UpdateRadioJamState(List<TempSuppressFWS> answer)
        {
            try
            {
                Led ControlState;
                Led SuppressState;
                Led RadiationState;

                int ind = -1;

                if (answer.Count == 0)
                {
                    ClearRadioJamState();
                }

                if (answer.Count > 0)
                {
                    answerLed = answer;

                    //try
                    //{
                    //    answer[1].Radiation = Led.Empty;
                    //    answer[2].Radiation = Led.Empty;
                    //}
                    //catch { }

                    for (int i = 0; i < answer.Count; i++)
                    {
                        ind = ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS.ToList().FindIndex(x => x.Id == answer[i].Id);

                        if (ind != -1)
                        {
                            ControlState = answer[i].Control;
                            switch (ControlState)
                            {
                                case Led.Empty: // нет сигнала (ничего)
                                    ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS[ind].TempSuppressFWS.Control = Led.Empty;
                                    break;

                                case Led.Red: // есть сигнал (красный)
                                    ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS[ind].TempSuppressFWS.Control = Led.Red;
                                    break;

                                case Led.Green: // давно есть сигнал (зеленый)
                                    ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS[ind].TempSuppressFWS.Control = Led.Green;
                                    break;

                                case Led.Yellow: // давно нет сигнала (желтый)
                                    ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS[ind].TempSuppressFWS.Control = Led.Yellow;
                                    break;
                            }

                            SuppressState = answer[i].Suppress;
                            switch (SuppressState)
                            {
                                case Led.Empty: // нет подавления (ничего)
                                    ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS[ind].TempSuppressFWS.Suppress = Led.Empty;
                                    break;

                                case Led.Red: // есть подавление (красный)
                                    ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS[ind].TempSuppressFWS.Suppress = Led.Red;
                                    break;

                                case Led.Green: // давно есть подавление (зеленый)
                                    ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS[ind].TempSuppressFWS.Suppress = Led.Green;
                                    break;
                            }

                            RadiationState = answer[i].Radiation;
                            switch (RadiationState)
                            {
                                case Led.Empty: // нет излучения (ничего)
                                    ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS[ind].TempSuppressFWS.Radiation = Led.Empty;
                                    break;

                                case Led.Blue: // есть излучение (синий)
                                    ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS[ind].TempSuppressFWS.Radiation = Led.Blue;
                                    break;

                                case Led.Red: //  излучение (красный)
                                    ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS[ind].TempSuppressFWS.Radiation = Led.Red;
                                    break;

                                case Led.White: //  излучение (белый)
                                    ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS[ind].TempSuppressFWS.Radiation = Led.White;
                                    break;
                            }

                            ChangeSuppressFWS(ind, ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS[ind]);
                        }
                    }
                }
            }
            catch { }
        }

        private int FoundIndCollectionSuppressFWS(List<TableSuppressFWSAll> tableSuppressFWS, List<TempSuppressFWS> tempSuppressFWS, int iNumAnswer)
        {
            //try
            //{
            //    int ind = tableSuppressFWS.FindIndex(x => x.TableSuppressFWS.Id == tempSuppressFWS[iNumAnswer].Id);


            //for (int i = 0; i < tableSuppressFWS.Count; i++)
            //{
            //    if (tableSuppressFWS[i].TableSuppressFWS.Id == tempSuppressFWS[iNumAnswer].Id)
            //    {
            //        return i;
            //    }
            //}
            //}
            //catch { }

            //return ind;

            return -1;
        }

        /// <summary>
        /// Добавить несколько записей в контрол
        /// </summary>
        /// <param name="listSuppressFWS"></param>
        public void AddSuppressFWSs(List<TableSuppressFWS> listSuppressFWS)
        {
            try
            {
                DeleteEmptyRows();

                List<TableSuppressFWSAll> list = SuppressFWSToSuppressFWSAll(listSuppressFWS);

                for (int i = 0; i < listSuppressFWS.Count; i++)
                {
                    ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS.Add(list[i]);
                }

                List<TableSuppressFWSAll> listSort = ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS.ToList();


                IEnumerable<TableSuppressFWSAll> sortLetter = list.OrderBy(l => l.Letter);                    // сортировка по литере
                IEnumerable<TableSuppressFWSAll> sortPriority = sortLetter.OrderByDescending(p => p.Priority); // сортировка по приоритету

                listSort = sortPriority.ToList();

                ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS.Clear();

                for (int i = 0; i < listSort.Count; i++)
                {
                    ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS.Add(listSort[i]);
                }

                AddEmptyRows();
            }
            catch { }
        }

        /// <summary>
        /// Преобразование TableSuppressFWS к TableSuppressFWSAll
        /// </summary>
        /// <param name="listSuppressFWS"></param>
        /// <returns> List<TableSuppressFWSAll> </returns>
        private List<TableSuppressFWSAll> SuppressFWSToSuppressFWSAll(List<TableSuppressFWS> listSuppressFWS)
        {
            List<TableSuppressFWSAll> list = new List<TableSuppressFWSAll>();
            for (int i = 0; i < listSuppressFWS.Count; i++)
            {
                TableSuppressFWSAll table = new TableSuppressFWSAll();
                
                double minutesLat = (listSuppressFWS[i].Coordinates.Latitude - Math.Truncate(listSuppressFWS[i].Coordinates.Latitude)) * 60;
                double minutesLon = (listSuppressFWS[i].Coordinates.Longitude - Math.Truncate(listSuppressFWS[i].Coordinates.Longitude)) * 60;

                table.Id = listSuppressFWS[i].Id;
                table.IdMission = listSuppressFWS[i].IdMission;
                table.NumberASP = listSuppressFWS[i].NumberASP;
                table.Sender = listSuppressFWS[i].Sender;
                table.FreqKHz = listSuppressFWS[i].FreqKHz;
                table.Bearing = listSuppressFWS[i].Bearing;
                table.Letter = listSuppressFWS[i].Letter;
                table.Threshold = listSuppressFWS[i].Threshold;
                table.Priority = listSuppressFWS[i].Priority;
                table.InterferenceParam.Modulation = listSuppressFWS[i].InterferenceParam.Modulation;
                table.InterferenceParam.Manipulation = listSuppressFWS[i].InterferenceParam.Manipulation;
                table.InterferenceParam.Duration = listSuppressFWS[i].InterferenceParam.Duration;
                table.InterferenceParam.Deviation = listSuppressFWS[i].InterferenceParam.Deviation;
                table.Coordinates.Latitude = listSuppressFWS[i].Coordinates.Latitude;
                table.Coordinates.Longitude = listSuppressFWS[i].Coordinates.Longitude;
                table.Coordinates.Altitude = listSuppressFWS[i].Coordinates.Altitude;
                table.CoordinatesDDMMSS.LatSign = listSuppressFWS[i].Coordinates.Latitude < 0 ? LatitudeSign.S : LatitudeSign.N;
                table.CoordinatesDDMMSS.LonSign = listSuppressFWS[i].Coordinates.Longitude < 0 ? LongitudeSign.W : LongitudeSign.E;
                table.CoordinatesDDMMSS.Latitude = listSuppressFWS[i].Coordinates.Latitude;
                table.CoordinatesDDMMSS.Latitude = listSuppressFWS[i].Coordinates.Latitude;
                table.CoordinatesDDMMSS.Altitude = listSuppressFWS[i].Coordinates.Altitude;
                table.CoordinatesDDMMSS.Latitude = listSuppressFWS[i].Coordinates.Latitude;
                table.CoordinatesDDMMSS.Longitude = listSuppressFWS[i].Coordinates.Longitude;
                table.CoordinatesDDMMSS.LatDegrees = (int)Math.Truncate(listSuppressFWS[i].Coordinates.Latitude);
                table.CoordinatesDDMMSS.LonDegrees = (int)Math.Truncate(listSuppressFWS[i].Coordinates.Longitude);
                table.CoordinatesDDMMSS.LatMinutesDDMM = Math.Round((listSuppressFWS[i].Coordinates.Latitude - Math.Truncate(listSuppressFWS[i].Coordinates.Latitude)) * 60, 4);
                table.CoordinatesDDMMSS.LonMinutesDDMM = Math.Round((listSuppressFWS[i].Coordinates.Longitude - Math.Truncate(listSuppressFWS[i].Coordinates.Longitude)) * 60, 4);
                table.CoordinatesDDMMSS.LatMinutesDDMMSS = (int)((listSuppressFWS[i].Coordinates.Latitude - Math.Truncate(listSuppressFWS[i].Coordinates.Latitude)) * 60);
                table.CoordinatesDDMMSS.LonMinutesDDMMSS = (int)((listSuppressFWS[i].Coordinates.Longitude - Math.Truncate(listSuppressFWS[i].Coordinates.Longitude)) * 60);
                table.CoordinatesDDMMSS.LatSeconds = Math.Round((minutesLat - Math.Truncate(minutesLat)) * 60, 2);
                table.CoordinatesDDMMSS.LonSeconds = Math.Round((minutesLon - Math.Truncate(minutesLon)) * 60, 2);
                table.CoordinatesDDMMSS.Altitude = listSuppressFWS[i].Coordinates.Altitude;

                list.Add(table);
            }

            return list;
        }

        /// <summary>
        /// Добавить одну запись в контрол
        /// </summary>
        /// <param name="suppressFWS"></param>
        public void AddSuppressFWS(TableSuppressFWS suppressFWS)
        {
            try
            {
                DeleteEmptyRows();

                TableSuppressFWSAll table = new TableSuppressFWSAll();
                table = TableSuppressFWSToTableSuppressFWSAll(suppressFWS);

                ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS.Add(table);
 
                List<TableSuppressFWSAll> list = ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS.ToList();

                IEnumerable<TableSuppressFWSAll> sortLetter = list.OrderBy(l => l.Letter);                    // сортировка по литере
                IEnumerable<TableSuppressFWSAll> sortPriority = sortLetter.OrderByDescending(p => p.Priority); // сортировка по приоритету

                list = sortPriority.ToList();

                ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS.Clear();

                for (int i = 0; i < list.Count; i++)
                {
                    ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS.Add(list[i]);
                }

                AddEmptyRows();
            }
            catch { }

        }

        /// <summary>
        /// Изменить одну запись в контроле
        /// </summary>
        /// <param name="ind"> индекс записи, которую надо заменить </param>
        /// <param name="supprReplace"> запись, на которую надо заменить </param>
        public void ChangeSuppressFWS(int id, TableSuppressFWS supprReplace)
        {
            try
            {
                TableSuppressFWSAll table = new TableSuppressFWSAll();
                table = TableSuppressFWSToTableSuppressFWSAll(supprReplace);

                int index = (((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS).ToList().FindIndex(x => x.Id == id);

                if (index != -1)
                {
                    ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS.RemoveAt(index);
                    ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS.Insert(index, table);

                    AddEmptyRows();
                }
            }
            catch { }
        }

        public void ChangeSuppressFWS(int index, TableSuppressFWSAll supprReplace)
        {
            try
            {
                //int index = (((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS).ToList().FindIndex(x => x.TableSuppressFWS.Id == id);

                ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS.RemoveAt(index);
                ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS.Insert(index, supprReplace);

                AddEmptyRows();

                dgvSuppressFWS.SelectedIndex = PropNumSuppressFWS.SelectedNumSuppressFWS;
            }
            catch { }
        }

        /// <summary>
        /// Удалить одну запись из контрола
        /// </summary>
        /// <param name="specFreq"></param>
        public void DeleteSuppressFWS(TableSuppressFWS suppressFWS)
        {
            try
            {
                int index = ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS.ToList().FindIndex(x => x.Id == suppressFWS.Id);

                ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS.RemoveAt(index);

                AddEmptyRows();
            }
            catch { }
        }

        /// <summary>
        /// Удалить все записи из контрола
        /// </summary>
        public void ClearSuppressFWS()
        {
            try
            {
                ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS.Clear();

                AddEmptyRows();
            }
            catch { }
        }

        /// <summary>
        /// Очистить RadioJamState
        /// </summary>
        public void ClearRadioJamState()
        {
            try
            {
                for (int i = 0; i < ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS.Count; i++)
                {
                    ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS[i].TempSuppressFWS.Control = Led.Empty;
                    ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS[i].TempSuppressFWS.Suppress = Led.Empty;
                    ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS[i].TempSuppressFWS.Radiation = Led.Empty;
                }
            }
            catch { }
        }

        /// <summary>
        /// Добавить пустые строки в таблицу
        /// </summary>
        private void AddEmptyRows()
        {
            int сountRowsAll = dgvSuppressFWS.Items.Count; // количество всех строк в таблице
            double hs = 23; // высота строки
            double ah = dgvSuppressFWS.ActualHeight; // визуализированная высота dataGrid
            double chh = dgvSuppressFWS.ColumnHeaderHeight; // высота заголовка

            int countRows = Convert.ToInt32(Math.Floor(Convert.ToDouble(ah) - Convert.ToDouble(chh)) / hs); // количество строк, которое помещается в область видимости = (высота таблицы - заголовок таблицы) / высота строки

            int count = сountRowsAll - countRows; // сколько пустых строк нужно удалить
            int index = -1;
            for (int i = 0; i < count; i++)
            {
                // Удалить пустые строки в dgv
                index = ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS.ToList().FindIndex(x => x.Id < 0);
                if (index != -1)
                {
                    ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS.RemoveAt(index);
                }
            }

            List<TableSuppressFWSAll> list = new List<TableSuppressFWSAll>();
            for (int i = 0; i < countRows - сountRowsAll; i++)
            {
                TableSuppressFWSAll strSuppressFWSAll = new TableSuppressFWSAll
                { 
                    Id = -2,
                    FreqKHz = -2F,
                    NumberASP = -2,
                    Bearing = -2,
                    Letter = 0,
                    Threshold = 255,
                    Priority = 0,
                    Coordinates = new Coord
                    {
                        Latitude = -2,
                        Longitude = -2,
                        Altitude = -2
                    },
                    InterferenceParam = new InterferenceParam
                    {
                           
                    },
                    TempSuppressFWS = new TempSuppressFWS
                    {
                        Control = Led.Empty,
                        Suppress = Led.Empty,
                        Radiation = Led.Empty
                    }
                };

                list.Add(strSuppressFWSAll);
            }

            for (int i = 0; i < list.Count; i++)
            {
                ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS.Add(list[i]);
            }
        }

        /// <summary>
        /// Добавить одну пустую строку вместо скрытой в таблицу
        /// </summary>
        private void AddOneEmptyRow(TableSuppressFWSAll row)
        {
            int ind = ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS.ToList().FindIndex(x => x.FreqKHz == row.FreqKHz);

            TableSuppressFWSAll strSuppressFWSAll = new TableSuppressFWSAll
            {
                Id = -2,
                NumberASP = -2,
                FreqKHz = 0,
                Bearing = -2,
                Letter = 0,
                Threshold = 255,
                Priority = 0,
                Coordinates = new Coord
                {
                    Latitude = -2,
                    Longitude = -2,
                    Altitude = -2
                },
                InterferenceParam = new InterferenceParam
                {

                },
                TempSuppressFWS = new TempSuppressFWS
                {
                    Control = Led.Empty,
                    Suppress = Led.Empty,
                    Radiation = Led.Empty
                }
            };
            ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS.RemoveAt(ind);
            ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS.Insert(ind, strSuppressFWSAll);
        }

        /// <summary>
        /// Удалить пустые строки из таблиц
        /// </summary>
        private void DeleteEmptyRows()
        {
            int countEmptyRows = ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS.Count(s => s.Id < 0);
            int countAllRows = ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS.Count;
            int iCount = countAllRows - countEmptyRows;
            for (int i = iCount; i < countAllRows; i++)
            {
                ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS.RemoveAt(iCount);
            }
        }

        /// <summary>
        /// Проверка на наличие частот в таблице из литер 5, 7, 8, 9, 10
        /// </summary>
        /// <param name="tb"> ToggleButton </param>
        /// <param name="bLetter"> Литера </param>
        /// <returns></returns>
        private bool IsSelectedRowEmpty()
        {
            try
            {
                if (((TableSuppressFWSAll)dgvSuppressFWS.SelectedItem).Id == -2)
                    return false;
            }
            catch { }

            return true;
        }
      
        private void SetLanguageTables(Languages language)
        {
            ResourceDictionary dict = new ResourceDictionary();
            try
            {
                switch (language)
                {
                    case Languages.Eng:

                        dict.Source = new Uri("/ProjectTestTables;component/TranslatorTables.EN.xaml",
                                      UriKind.Relative);
                        break;
                    case Languages.Rus:
                        dict.Source = new Uri("/ProjectTestTables;component/TranslatorTables.RU.xaml",
                                            UriKind.Relative);
                        break;
                    default:
                        dict.Source = new Uri("/ProjectTestTables;component/TranslatorTables.RU.xaml",
                                          UriKind.Relative);
                        break;
                }

                this.Resources.MergedDictionaries.Add(dict);
            }
            catch (Exception)
            { }
        }

        /// <summary>
        /// Добавить таблицу для отчета
        /// </summary>
        /// <returns></returns>
        private string[,] AddSuppressFWSToTable()
        {
            int Columns = 9;
            int Rows = ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS.Count(x => x.Id > 0) + 1;
            string[,] Table = new string[Rows, Columns];

            try
            {
                Table[0, 0] = SHeaders.headerNumAJS;
                Table[0, 1] = SHeaders.headerFreq;
                Table[0, 2] = SHeaders.headerBearing;
                Table[0, 3] = SHeaders.headerLetters;
                Table[0, 4] = SHeaders.headerJammerParams;
                Table[0, 5] = SHeaders.headerLevel;
                Table[0, 6] = SHeaders.headerPriority;
                Table[0, 7] = SHeaders.headerLatLon;
                Table[0, 8] = SHeaders.headerAlt;

                for (int i = 1; i < Rows; i++)
                {
                    Table[i, 0] = ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS[i - 1].NumberASP.ToString();
                    Table[i, 1] = ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS[i - 1].FreqKHz.ToString();
                    Table[i, 2] = ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS[i - 1].Bearing.ToString();
                    Table[i, 3] = ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS[i - 1].Letter.ToString();
                    Table[i, 4] = ParamsNoiseConverter.ParamHindrance(((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS[i - 1].InterferenceParam.Modulation,
                        ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS[i - 1].InterferenceParam.Deviation,
                        ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS[i - 1].InterferenceParam.Manipulation,
                        ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS[i - 1].InterferenceParam.Duration);
                    Table[i, 5] = ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS[i - 1].Threshold.ToString();
                    Table[i, 6] = ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS[i - 1].Priority.ToString();
                    Table[i, 7] = ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS[i - 1].Coordinates.Latitude.ToString() + "   " + ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS[i - 1].Coordinates.Longitude.ToString();
                    Table[i, 8] = ((GlobalSuppressFWS)dgvSuppressFWS.DataContext).CollectionSuppressFWS[i - 1].Coordinates.Altitude.ToString();
                }
            }
            catch { }

            return Table;
        }

        /// <summary>
        /// Видимость кнопок в зависимости от заданного состояния КРПУ
        /// </summary>
        public void ButtonsCRRDVisible()
        {
            // КРПУ1
            if (PropLocalCRRD.CRRD1)
            {
                gridButtons.ColumnDefinitions[8].Width = new GridLength(30);

                if (IsRoss)
                    gridButtons.ColumnDefinitions[11].Width = new GridLength(7);
                
            }
            else
            {
                gridButtons.ColumnDefinitions[8].Width = new GridLength(0);
            }

            // КРПУ2
            if (PropLocalCRRD.CRRD2)
            {
                gridButtons.ColumnDefinitions[9].Width = new GridLength(30);

                if (IsRoss)
                    gridButtons.ColumnDefinitions[11].Width = new GridLength(7);
            }
            else
            {
                gridButtons.ColumnDefinitions[9].Width = new GridLength(0);
            }
        }

        public void ChangeFreqHeaderMHz_kHz()
        {
            switch (PropLocalFreqMHz_kHz.FreqMHz_kHz)
            {
                case 0: // kHz
                    lHeaderFreq.Content = SMeaning.meaningFkHz;
                    break;

                case 1: // MHz
                    lHeaderFreq.Content = SMeaning.meaningFMHz;
                    break;
            }
        }

        private TableSuppressFWS TableSuppressFWSAllToTableSuppressFWS(TableSuppressFWSAll tableSuppressFWSAll)
        {
            TableSuppressFWS table = new TableSuppressFWS();

            table.Id = tableSuppressFWSAll.Id;
            table.IdMission = tableSuppressFWSAll.IdMission;
            table.Sender = tableSuppressFWSAll.Sender;
            table.FreqKHz = tableSuppressFWSAll.FreqKHz;
            table.Bearing = tableSuppressFWSAll.Bearing;
            table.Threshold = tableSuppressFWSAll.Threshold;
            table.InterferenceParam.Modulation = tableSuppressFWSAll.InterferenceParam.Modulation;
            table.InterferenceParam.Manipulation = tableSuppressFWSAll.InterferenceParam.Manipulation;
            table.InterferenceParam.Deviation = tableSuppressFWSAll.InterferenceParam.Deviation;
            table.InterferenceParam.Duration = tableSuppressFWSAll.InterferenceParam.Duration;
            table.Letter = tableSuppressFWSAll.Letter;
            table.Priority = tableSuppressFWSAll.Priority;
            table.NumberASP = tableSuppressFWSAll.NumberASP;

            switch (PropViewCoords.ViewCoords)
            {
                case 1: // format "DD.dddddd"
                    table.Coordinates.Altitude = tableSuppressFWSAll.CoordinatesDDMMSS.Altitude;
                    table.Coordinates.Latitude = tableSuppressFWSAll.CoordinatesDDMMSS.Latitude;
                    table.Coordinates.Longitude = tableSuppressFWSAll.CoordinatesDDMMSS.Longitude;
                    break;

                case 2: // format "DD MM.mmmm"
                    table.Coordinates.Altitude = tableSuppressFWSAll.CoordinatesDDMMSS.Altitude;
                    table.Coordinates.Latitude = Math.Round(tableSuppressFWSAll.CoordinatesDDMMSS.LatDegrees + ((double)tableSuppressFWSAll.CoordinatesDDMMSS.LatMinutesDDMM / 60), 6);
                    table.Coordinates.Longitude = Math.Round(tableSuppressFWSAll.CoordinatesDDMMSS.LonDegrees + ((double)tableSuppressFWSAll.CoordinatesDDMMSS.LonMinutesDDMM / 60), 6);
                    break;

                case 3: // format "DD MM SS.ss"

                    table.Coordinates.Altitude = tableSuppressFWSAll.CoordinatesDDMMSS.Altitude;
                    table.Coordinates.Latitude = Math.Round(tableSuppressFWSAll.CoordinatesDDMMSS.LatDegrees + ((double)tableSuppressFWSAll.CoordinatesDDMMSS.LatMinutesDDMMSS / 60) + (tableSuppressFWSAll.CoordinatesDDMMSS.LatSeconds / 3600), 6);
                    table.Coordinates.Longitude = Math.Round(tableSuppressFWSAll.CoordinatesDDMMSS.LonDegrees + ((double)tableSuppressFWSAll.CoordinatesDDMMSS.LonMinutesDDMMSS / 60) + (tableSuppressFWSAll.CoordinatesDDMMSS.LonSeconds / 3600), 6);
                    break;
                default:
                    break;
            }

            table.Coordinates.Latitude = tableSuppressFWSAll.CoordinatesDDMMSS.LatSign == LatitudeSign.S ? table.Coordinates.Latitude * -1 : table.Coordinates.Latitude;
            table.Coordinates.Longitude = tableSuppressFWSAll.CoordinatesDDMMSS.LonSign == LongitudeSign.W ? table.Coordinates.Longitude * -1 : table.Coordinates.Longitude;

            return table;
        }

        private TableSuppressFWSAll TableSuppressFWSToTableSuppressFWSAll(TableSuppressFWS tableSuppressFWS)
        {
            TableSuppressFWSAll table = new TableSuppressFWSAll();

            double minutesLat = (tableSuppressFWS.Coordinates.Latitude - Math.Truncate(tableSuppressFWS.Coordinates.Latitude)) * 60;
            double minutesLon = (tableSuppressFWS.Coordinates.Longitude - Math.Truncate(tableSuppressFWS.Coordinates.Longitude)) * 60;

            table.Id = tableSuppressFWS.Id;
            table.IdMission = tableSuppressFWS.IdMission;
            table.Sender = tableSuppressFWS.Sender;
            table.NumberASP = tableSuppressFWS.NumberASP;
            table.FreqKHz = tableSuppressFWS.FreqKHz;
            table.Bearing = tableSuppressFWS.Bearing;
            table.Coordinates.Latitude = tableSuppressFWS.Coordinates.Latitude;
            table.Coordinates.Longitude = tableSuppressFWS.Coordinates.Longitude;
            table.Coordinates.Altitude = tableSuppressFWS.Coordinates.Altitude;
            table.CoordinatesDDMMSS.LatSign = tableSuppressFWS.Coordinates.Latitude < 0 ? LatitudeSign.S : LatitudeSign.N;
            table.CoordinatesDDMMSS.LonSign = tableSuppressFWS.Coordinates.Longitude < 0 ? LongitudeSign.W : LongitudeSign.E;
            table.CoordinatesDDMMSS.Latitude = tableSuppressFWS.Coordinates.Latitude;
            table.CoordinatesDDMMSS.Latitude = tableSuppressFWS.Coordinates.Latitude;
            table.CoordinatesDDMMSS.Altitude = tableSuppressFWS.Coordinates.Altitude;
            table.CoordinatesDDMMSS.Latitude = tableSuppressFWS.Coordinates.Latitude;
            table.CoordinatesDDMMSS.Longitude = tableSuppressFWS.Coordinates.Longitude;
            table.CoordinatesDDMMSS.LatDegrees = (int)Math.Truncate(tableSuppressFWS.Coordinates.Latitude);
            table.CoordinatesDDMMSS.LonDegrees = (int)Math.Truncate(tableSuppressFWS.Coordinates.Longitude);
            table.CoordinatesDDMMSS.LatMinutesDDMM = Math.Round((tableSuppressFWS.Coordinates.Latitude - Math.Truncate(tableSuppressFWS.Coordinates.Latitude)) * 60, 4);
            table.CoordinatesDDMMSS.LonMinutesDDMM = Math.Round((tableSuppressFWS.Coordinates.Longitude - Math.Truncate(tableSuppressFWS.Coordinates.Longitude)) * 60, 4);
            table.CoordinatesDDMMSS.LatMinutesDDMMSS = (int)((tableSuppressFWS.Coordinates.Latitude - Math.Truncate(tableSuppressFWS.Coordinates.Latitude)) * 60);
            table.CoordinatesDDMMSS.LonMinutesDDMMSS = (int)((tableSuppressFWS.Coordinates.Longitude - Math.Truncate(tableSuppressFWS.Coordinates.Longitude)) * 60);
            table.CoordinatesDDMMSS.LatSeconds = Math.Round((minutesLat - Math.Truncate(minutesLat)) * 60, 2);
            table.CoordinatesDDMMSS.LonSeconds = Math.Round((minutesLon - Math.Truncate(minutesLon)) * 60, 2);
            table.CoordinatesDDMMSS.Altitude = tableSuppressFWS.Coordinates.Altitude;
            table.InterferenceParam.Modulation = tableSuppressFWS.InterferenceParam.Modulation;
            table.InterferenceParam.Manipulation = tableSuppressFWS.InterferenceParam.Manipulation;
            table.InterferenceParam.Deviation = tableSuppressFWS.InterferenceParam.Deviation;
            table.InterferenceParam.Duration = tableSuppressFWS.InterferenceParam.Duration;
            table.Letter = tableSuppressFWS.Letter;
            table.Priority = tableSuppressFWS.Priority;
            table.Threshold = tableSuppressFWS.Threshold;
          
            return table;
        }

        private bool isRoss = false;
        public bool IsRoss
        {
            get { return isRoss; }
            set
            {
                if (isRoss == value)
                    return;

                isRoss = value;
                UpdateButtonsForRoss(isRoss);
            }
        }

        private void UpdateButtonsForRoss(bool isRoss)
        {
            if (isRoss)
            {
                gridButtons.ColumnDefinitions[10].Width = new GridLength(0);
                gridButtons.ColumnDefinitions[11].Width = new GridLength(0);
            }
            else
            {
                gridButtons.ColumnDefinitions[10].Width = new GridLength(30);
                gridButtons.ColumnDefinitions[11].Width = new GridLength(7);
            }

            //if (isRoss)
            //{
            //    gridButtons.ColumnDefinitions[10].Width = new GridLength(0);
                
            //    if (gridButtons.ColumnDefinitions[8].Width.Value == 30 || gridButtons.ColumnDefinitions[9].Width.Value == 30)
            //    {
            //        gridButtons.ColumnDefinitions[11].Width = new GridLength(7);
            //    }
            //    else
            //    {
            //        gridButtons.ColumnDefinitions[11].Width = new GridLength(0);
            //    }

            //    if (gridButtons.ColumnDefinitions[8].Width.Value == 0 && gridButtons.ColumnDefinitions[9].Width.Value == 0)
            //    {
            //        gridButtons.ColumnDefinitions[11].Width = new GridLength(0);
            //    }
            //    else
            //    {
            //        gridButtons.ColumnDefinitions[11].Width = new GridLength(7);
            //    }
                
            //}
            //else
            //{
            //    gridButtons.ColumnDefinitions[10].Width = new GridLength(30);
            //    gridButtons.ColumnDefinitions[11].Width = new GridLength(7);
            //}
        }
    }
}
