﻿using ModelsTablesDBLib;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls.WpfPropertyGrid;
using System.Windows.Media.Imaging;
using TableEvents;
using TableOperations;
using ValuesCorrectLib;

namespace SuppressFWSControl
{
    /// <summary>
    /// Логика взаимодействия для SuppressFWSProperty.xaml
    /// </summary>
    public partial class SuppressFWSProperty : Window
    {
        private ObservableCollection<TableSuppressFWSAll> collectionTemp;
        public TableSuppressFWSAll TableSuppressFWSAll { get; private set; }
       
        public SuppressFWSProperty(ObservableCollection<TableSuppressFWSAll> collectionSuppressFWS)
        {
            try
            {
                InitializeComponent();

                InitEditors();

                collectionTemp = collectionSuppressFWS;
                TableSuppressFWSAll = new TableSuppressFWSAll();
                TableSuppressFWSAll.Bearing = -1;
                TableSuppressFWSAll.Priority = 2;
                TableSuppressFWSAll.Threshold = -130;
              
                propertyGrid.SelectedObject = TableSuppressFWSAll;
                propertyGrid.Properties[nameof(TableSuppressFWSAll.Coordinates)].IsBrowsable = false;
                propertyGrid.ShowReadOnlyProperties = true;

                Title = SMeaning.meaningAddRecord;
                Icon = new BitmapImage(new Uri(@"pack://application:,,,/"
                                    + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                                    + ";component/"
                                    + "Resources/AddRec.ico", UriKind.Absolute));
               
                InitProperty();
                propertyGrid.Categories.FirstOrDefault(t => t.Name == "InterferenceParam").HeaderCategoryName = SMeaning.meaningInterferenceParam;
                propertyGrid.Categories.FirstOrDefault(t => t.Name == "Coordinates").HeaderCategoryName = SMeaning.meaningCoord;
                //ChangeCategories();


            }
            catch { }
        }

        public SuppressFWSProperty(ObservableCollection<TableSuppressFWSAll> collectionSuppressFWS, TableSuppressFWSAll SuppressFWS)
        {
            try
            {
                InitializeComponent();

                InitEditors();

                collectionTemp = collectionSuppressFWS;
                TableSuppressFWSAll = SuppressFWS;

                switch (PropLocalFreqMHz_kHz.FreqMHz_kHz)
                {
                    case 0: // kHz
                        TableSuppressFWSAll.FreqKHz = Math.Round(TableSuppressFWSAll.FreqKHz.Value, 1);
                        break;

                    case 1: // MHz
                        TableSuppressFWSAll.FreqKHz = Math.Round(TableSuppressFWSAll.FreqKHz.Value / 1000, 1);
                        break;

                    default:
                        TableSuppressFWSAll.FreqKHz = Math.Round(TableSuppressFWSAll.FreqKHz.Value, 1);
                        break;
                }

                TableSuppressFWSAll.CoordinatesDDMMSS.LatSign = TableSuppressFWSAll.Coordinates.Latitude < 0 ? LatitudeSign.S : LatitudeSign.N;
                TableSuppressFWSAll.CoordinatesDDMMSS.LonSign = TableSuppressFWSAll.Coordinates.Longitude < 0 ? LongitudeSign.W : LongitudeSign.E;
                TableSuppressFWSAll.CoordinatesDDMMSS.Altitude = TableSuppressFWSAll.Coordinates.Altitude;

                if (TableSuppressFWSAll.Coordinates.Latitude != -1)
                {
                    TableSuppressFWSAll.Coordinates.Latitude = Math.Round(TableSuppressFWSAll.Coordinates.Latitude, 6);
                    TableSuppressFWSAll.Coordinates.Latitude = TableSuppressFWSAll.Coordinates.Latitude < 0 ? TableSuppressFWSAll.Coordinates.Latitude * -1 : TableSuppressFWSAll.Coordinates.Latitude;
                }
                if (TableSuppressFWSAll.Coordinates.Longitude != -1)
                {
                    TableSuppressFWSAll.Coordinates.Longitude = Math.Round(TableSuppressFWSAll.Coordinates.Longitude, 6);
                    TableSuppressFWSAll.Coordinates.Longitude = TableSuppressFWSAll.Coordinates.Longitude < 0 ? TableSuppressFWSAll.Coordinates.Longitude * -1 : TableSuppressFWSAll.Coordinates.Longitude;
                }

                switch (PropViewCoords.ViewCoords)
                {
                    case 1: // format "DD.dddddd"

                        TableSuppressFWSAll.CoordinatesDDMMSS.Latitude = TableSuppressFWSAll.Coordinates.Latitude;
                        TableSuppressFWSAll.CoordinatesDDMMSS.Longitude = TableSuppressFWSAll.Coordinates.Longitude;
                        break;

                    case 2: // format "DD MM.mmmm"

                        TableSuppressFWSAll.CoordinatesDDMMSS.LatDegrees = (int)Math.Truncate(TableSuppressFWSAll.Coordinates.Latitude);
                        TableSuppressFWSAll.CoordinatesDDMMSS.LatMinutesDDMM = Math.Round((TableSuppressFWSAll.Coordinates.Latitude - Math.Truncate(TableSuppressFWSAll.Coordinates.Latitude)) * 60, 4);

                        TableSuppressFWSAll.CoordinatesDDMMSS.LonDegrees = (int)Math.Truncate(TableSuppressFWSAll.Coordinates.Longitude);
                        TableSuppressFWSAll.CoordinatesDDMMSS.LonMinutesDDMM = Math.Round((TableSuppressFWSAll.Coordinates.Longitude - Math.Truncate(TableSuppressFWSAll.Coordinates.Longitude)) * 60, 4);
                        break;

                    case 3: // format "DD MM SS.ss"

                        double latD = Math.Truncate(TableSuppressFWSAll.Coordinates.Latitude);
                        double latM = Math.Truncate((TableSuppressFWSAll.Coordinates.Latitude - latD) * 60);
                        double latS = Math.Round((((TableSuppressFWSAll.Coordinates.Latitude - latD) * 60) - latM) * 60, 2);

                        TableSuppressFWSAll.CoordinatesDDMMSS.LatDegrees = (int)latD;
                        TableSuppressFWSAll.CoordinatesDDMMSS.LatMinutesDDMMSS = (int)latM;
                        TableSuppressFWSAll.CoordinatesDDMMSS.LatSeconds = latS;

                        double lonD = Math.Truncate(TableSuppressFWSAll.Coordinates.Longitude);
                        double lonM = Math.Truncate((TableSuppressFWSAll.Coordinates.Longitude - lonD) * 60);
                        double lonS = Math.Round((((TableSuppressFWSAll.Coordinates.Longitude - lonD) * 60) - lonM) * 60, 2);

                        TableSuppressFWSAll.CoordinatesDDMMSS.LonDegrees = (int)lonD;
                        TableSuppressFWSAll.CoordinatesDDMMSS.LonMinutesDDMMSS = (int)lonM;
                        TableSuppressFWSAll.CoordinatesDDMMSS.LonSeconds = lonS;
                        break;

                    default:
                        break;
                }

                propertyGrid.SelectedObject = TableSuppressFWSAll;
                propertyGrid.Properties[nameof(TableSuppressFWSAll.Coordinates)].IsBrowsable = false;
                propertyGrid.ShowReadOnlyProperties = false;

                Title = SMeaning.meaningChangeRecord;
                Icon = new BitmapImage(new Uri(@"pack://application:,,,/"
                                    + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                                    + ";component/"
                                    + "Resources/ChangeRec.ico", UriKind.Absolute));

                propertyGrid.Categories.FirstOrDefault(t => t.Name == "InterferenceParam").HeaderCategoryName = SMeaning.meaningInterferenceParam;
                propertyGrid.Categories.FirstOrDefault(t => t.Name == "Coordinates").HeaderCategoryName = SMeaning.meaningCoord;
                //ChangeCategories();
                InitProperty();

                PropChangeJammerParams.IsChangeJammerParams = true;
                CorrectJammerParams.PropertyGridJammerParams(TableSuppressFWSAll.InterferenceParam);
            }
            catch { }
        }

        public SuppressFWSProperty()
        {
            try
            {
                InitializeComponent();

                InitEditors();
                propertyGrid.Categories.FirstOrDefault(t => t.Name == "InterferenceParam").HeaderCategoryName = SMeaning.meaningInterferenceParam;
                propertyGrid.Categories.FirstOrDefault(t => t.Name == "Coordinates").HeaderCategoryName = SMeaning.meaningCoord;
                //ChangeCategories();
                InitProperty();
            }
            catch { }
        }

        private void ButtonApply_Click(object sender, RoutedEventArgs e)
        {
            if (IsAddClick((TableSuppressFWSAll)propertyGrid.SelectedObject) != null)
            {
                DialogResult = true;
            }
        }

        public TableSuppressFWSAll IsAddClick(TableSuppressFWSAll SuppressFWSWindow)
        {
            //if (PropLocalFreqMHz_kHz.FreqMHz_kHz == 1)
            //{
            //    SuppressFWSWindow.FreqKHz = SuppressFWSWindow.FreqKHz * 1000;
            //}

            if (SuppressFWSWindow.FreqKHz.HasValue && SuppressFWSWindow.Bearing.HasValue)
            {
                SuppressFWSWindow.Threshold = SuppressFWSWindow.Threshold > 0 ? (Int16)(SuppressFWSWindow.Threshold * -1) : SuppressFWSWindow.Threshold;
                SuppressFWSWindow.Threshold = CorrectAddRecord.IsCorrectThreshold(SuppressFWSWindow.Threshold.Value);

                if (CorrectAddRecord.IsCorrectFreqRanges(PropLocalFreqMHz_kHz.FreqMHz_kHz == 1 ? SuppressFWSWindow.FreqKHz.Value * 1000 : SuppressFWSWindow.FreqKHz.Value) &&
                    CorrectAddRecord.IsCorrectPeleng(SuppressFWSWindow.Bearing.Value) &&
                    CorrectParamsNoise.GetParamsNoise(SuppressFWSWindow.InterferenceParam, PropLocalFreqMHz_kHz.FreqMHz_kHz == 1 ? SuppressFWSWindow.FreqKHz.Value * 1000 : SuppressFWSWindow.FreqKHz.Value, DefinitionParams.DefineLetter(SuppressFWSWindow.FreqKHz.Value)))
                {

                    SuppressFWSWindow.FreqKHz = PropLocalFreqMHz_kHz.FreqMHz_kHz == 1 ? SuppressFWSWindow.FreqKHz * 1000 : SuppressFWSWindow.FreqKHz;

                    SuppressFWSWindow.Letter = DefinitionParams.DefineLetter(SuppressFWSWindow.FreqKHz.Value);

                    return SuppressFWSWindow;
                }
                //if (CorrectAddRecord.IsCorrectFreqRanges(SuppressFWSWindow.FreqKHz.Value) &&
                //CorrectAddRecord.IsCorrectPeleng(SuppressFWSWindow.Bearing.Value) &&
                //CorrectParamsNoise.GetParamsNoise(SuppressFWSWindow.InterferenceParam, SuppressFWSWindow.FreqKHz.Value, DefinitionParams.DefineLetter(SuppressFWSWindow.FreqKHz.Value)))
                //{
                //    SuppressFWSWindow.Letter = DefinitionParams.DefineLetter(SuppressFWSWindow.FreqKHz.Value);

                //    return SuppressFWSWindow;
                //}
            }
            else
            {
                MessageBox.Show(SMessageError.mesErrEnterValues, SMessageError.mesErr, MessageBoxButton.OK, MessageBoxImage.Error);
                return null;
            }

            return null;
        }

        private void ButtonNoApply_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void InitProperty()
        {
            foreach (var property in propertyGrid.Properties)
            {
                try
                {
                    if (property.IsBrowsable == false)
                        continue;
                    if (property.PropertyValue.SubProperties.Count != 0)
                    {
                        foreach (var subProperty in property.PropertyValue.SubProperties)
                            subProperty.PropertyValue.PropertyValueException += PropertyGridSubException;
                        continue;
                    }
                    property.PropertyValue.PropertyValueException += PropertyGridException;
                }
                catch (Exception ex)
                { MessageBox.Show(ex.Message); }
            }
        }

        private void PropertyGridSubException(object sender, ValueExceptionEventArgs e)
        {
            propertyGrid.Properties[(sender as PropertyItemValue).ParentProperty.ParentValue.ParentProperty.Name].SetValue((sender as PropertyItemValue).Value);
        }

        private void PropertyGridException(object sender, ValueExceptionEventArgs e)
        {
            propertyGrid.Properties[(sender as PropertyItemValue).ParentProperty.Name].SetValue((sender as PropertyItemValue).Value);
        }

        private void ChangeCategories()
        {
            //propertyGrid.Categories.FirstOrDefault(t => t.Name == "InterferenceParam").HeaderCategoryName = "Параметры помехи";
            //propertyGrid.Categories.FirstOrDefault(t => t.Name == "Coordinates").HeaderCategoryName = "Координаты";
            //propertyGrid.Categories.FirstOrDefault(t => t.Name == "Прочее").HeaderCategoryName = "Общие";

            propertyGrid.Categories.FirstOrDefault(t => t.Name == "InterferenceParam").HeaderCategoryName = "Jammer parameters";
            propertyGrid.Categories.FirstOrDefault(t => t.Name == "Coordinates").HeaderCategoryName = "Coordinates";
            propertyGrid.Categories.FirstOrDefault(t => t.Name == "Прочее").HeaderCategoryName = "General";

            propertyGrid.Properties[nameof(TableSuppressFWS.FreqKHz)].DisplayName = "F, kHz";
            propertyGrid.Properties[nameof(TableSuppressFWS.Bearing)].DisplayName = "θ, °";
            propertyGrid.Properties[nameof(TableSuppressFWS.Threshold)].DisplayName = "U, dB";
            propertyGrid.Properties[nameof(TableSuppressFWS.Priority)].DisplayName = "Priority";

        }

        public void SetLanguagePropertyGrid(Languages language)
        {
            LoadTranslatorPropertyGrid(language);
            TranslatorTables.ChangeLanguagePropertyGrid(language, propertyGrid);
            
            SetDisplayNameFreqs();
        }

        public void SetLanguagePropertyGrid(Languages language, ResourceDictionary dict)
        {
            LoadTranslatorPropertyGrid(dict);
            TranslatorTables.ChangeLanguagePropertyGrid(language, propertyGrid);

            SetDisplayNameFreqs();
        }

        private void LoadTranslatorPropertyGrid(ResourceDictionary dict)
        {
            try
            {
                this.Resources.MergedDictionaries.Add(dict);
            }
            catch (Exception)
            { }
        }

        private void SetDisplayNameFreqs()
        {
            switch (PropLocalFreqMHz_kHz.FreqMHz_kHz)
            {
                case 0: // kHz
                    propertyGrid.Properties[nameof(TableSuppressFWSAll.FreqKHz)].DisplayName = SMeaning.meaningFkHz;
                    break;

                case 1: // MHz
                    propertyGrid.Properties[nameof(TableSuppressFWSAll.FreqKHz)].DisplayName = SMeaning.meaningFMHz;
                    break;

                default:
                    break;
            }
        }

        private void LoadTranslatorPropertyGrid(Languages language)
        {
            ResourceDictionary dict = new ResourceDictionary();
            try
            {
                switch (language)
                {
                    case Languages.Eng:
                        dict.Source = new Uri("/GrozaFuture;component/Languages/TranslatorTables/TranslatorTables.EN.xaml",
                                      UriKind.Relative);
                        break;
                    case Languages.Rus:
                        dict.Source = new Uri("/GrozaFuture;component/Languages/TranslatorTables/TranslatorTables.RU.xaml",
                                            UriKind.Relative);
                        break;
                    case Languages.Azr:
                        dict.Source = new Uri("/GrozaFuture;component/Languages/TranslatorTables/TranslatorTables.AZ.xaml",
                                            UriKind.Relative);
                        break;
                    default:
                        dict.Source = new Uri("/GrozaFuture;component/Languages/TranslatorTables/TranslatorTables.RU.xaml",
                                          UriKind.Relative);
                        break;
                }

                // Отладка ----------------------------------------------------------------------------------
                //switch (language)
                //{
                //    case Languages.Eng:

                //        dict.Source = new Uri("/ProjectTestTables;component/TranslatorTables.EN.xaml",
                //                      UriKind.Relative);
                //        break;
                //    case Languages.Rus:
                //        dict.Source = new Uri("/ProjectTestTables;component/TranslatorTables.RU.xaml",
                //                            UriKind.Relative);
                //        break;
                //    default:
                //        dict.Source = new Uri("/ProjectTestTables;component/TranslatorTables.RU.xaml",
                //                          UriKind.Relative);
                //        break;
                //}
                // ---------------------------------------------------------------------------------- Отладка

                this.Resources.MergedDictionaries.Add(dict);
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
        }

        private void InitEditors()
        {
            //propertyGrid.Editors.Add(new GeneralFreqEditor(nameof(TableSuppressFWSAll.FreqKHz), typeof(TableSuppressFWSAll)));
            //propertyGrid.Editors.Add(new GeneralBearingEditor(nameof(TableSuppressFWSAll.Bearing), typeof(TableSuppressFWSAll)));
            //propertyGrid.Editors.Add(new GeneralThresholdFWSEditor(nameof(TableSuppressFWSAll.Threshold), typeof(TableSuppressFWSAll)));
            //propertyGrid.Editors.Add(new GeneralPriorityEditor(nameof(TableSuppressFWSAll.Priority), typeof(TableSuppressFWSAll)));
            //propertyGrid.Editors.Add(new InterferenceParamFWSEditor(nameof(TableSuppressFWSAll.InterferenceParam), typeof(TableSuppressFWSAll)));

           // propertyGrid.Editors.Add(new CoordinatesFWSEditor(nameof(tableSuppressFWS.TableSuppressFWS.Coordinates), typeof(TableSuppressFWS)));

            switch (PropViewCoords.ViewCoords)
            {
                case 1: // format "DD.dddddd"
                    propertyGrid.Editors.Add(new CoordinatesFWSDDEditor(nameof(TableSuppressFWSAll.CoordinatesDDMMSS), typeof(TableSuppressFWSAll)));
                    propertyGrid.Editors.Add(new FreqFWSDDEditor(nameof(TableSuppressFWSAll.FreqKHz), typeof(TableSuppressFWSAll)));
                    propertyGrid.Editors.Add(new BearingFWSDDEditor(nameof(TableSuppressFWSAll.Bearing), typeof(TableSuppressFWSAll)));
                    propertyGrid.Editors.Add(new ThresholdFWSDDEditor(nameof(TableSuppressFWSAll.Threshold), typeof(TableSuppressFWSAll)));
                    propertyGrid.Editors.Add(new PriorityFWSDDEditor(nameof(TableSuppressFWSAll.Priority), typeof(TableSuppressFWSAll)));
                    propertyGrid.Editors.Add(new InterferenceParamFWSDDEditor(nameof(TableSuppressFWSAll.InterferenceParam), typeof(TableSuppressFWSAll)));
                    break;

                case 2: // format "DD MM.mmmm"
                    propertyGrid.Editors.Add(new CoordinatesFWSDDMMEditor(nameof(TableSuppressFWSAll.CoordinatesDDMMSS), typeof(TableSuppressFWSAll)));
                    propertyGrid.Editors.Add(new FreqFWSDDMMEditor(nameof(TableSuppressFWSAll.FreqKHz), typeof(TableSuppressFWSAll)));
                    propertyGrid.Editors.Add(new BearingFWSDDMMEditor(nameof(TableSuppressFWSAll.Bearing), typeof(TableSuppressFWSAll)));
                    propertyGrid.Editors.Add(new ThresholdFWSDDMMEditor(nameof(TableSuppressFWSAll.Threshold), typeof(TableSuppressFWSAll)));
                    propertyGrid.Editors.Add(new PriorityFWSDDMMEditor(nameof(TableSuppressFWSAll.Priority), typeof(TableSuppressFWSAll)));
                    propertyGrid.Editors.Add(new InterferenceParamFWSDDMMEditor(nameof(TableSuppressFWSAll.InterferenceParam), typeof(TableSuppressFWSAll)));
                    break;

                case 3: // format "DD MM SS.ss"
                    propertyGrid.Editors.Add(new CoordinatesFWSDDMMSSEditor(nameof(TableSuppressFWSAll.CoordinatesDDMMSS), typeof(TableSuppressFWSAll)));
                    propertyGrid.Editors.Add(new FreqFWSDDMMSSEditor(nameof(TableSuppressFWSAll.FreqKHz), typeof(TableSuppressFWSAll)));
                    propertyGrid.Editors.Add(new BearingFWSDDMMSSEditor(nameof(TableSuppressFWSAll.Bearing), typeof(TableSuppressFWSAll)));
                    propertyGrid.Editors.Add(new ThresholdFWSDDMMSSEditor(nameof(TableSuppressFWSAll.Threshold), typeof(TableSuppressFWSAll)));
                    propertyGrid.Editors.Add(new PriorityFWSDDMMSSEditor(nameof(TableSuppressFWSAll.Priority), typeof(TableSuppressFWSAll)));
                    propertyGrid.Editors.Add(new InterferenceParamFWSDDMMSSEditor(nameof(TableSuppressFWSAll.InterferenceParam), typeof(TableSuppressFWSAll)));
                    break;

                default:
                    break;
            }
        }

        private void gridProperty_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Enter)
            {
                if (IsAddClick((TableSuppressFWSAll)propertyGrid.SelectedObject) != null)
                {
                    DialogResult = true;
                }
            }
        }
    }
}
