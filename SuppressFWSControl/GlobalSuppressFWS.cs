﻿using ModelsTablesDBLib;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Windows.Controls.WpfPropertyGrid;
using TableEvents;

namespace SuppressFWSControl
{
    [DataContract]
    [CategoryOrder(nameof(InterferenceParam), 2)]
    [CategoryOrder(nameof(Coordinates), 3)]
    [KnownType(typeof(AbstractDependentASP))]
    [InfoTable(NameTable.TableSuppressFWS)]
    public class TableSuppressFWSAll
    {
        //   public TableSuppressFWS TableSuppressFWS { get; set; } = new TableSuppressFWS();
        [DataMember]
        [Browsable(false)]
        public TempSuppressFWS TempSuppressFWS { get; set; } = new TempSuppressFWS();

        [DataMember]
        [Browsable(false)]
        public int Id { get; set; } // уникальный номер записи

        [DataMember]
        [Browsable(false)]
        public int NumberASP { get; set; } // номер станции

        [DataMember]
        [Browsable(false)]
        public int IdMission { get; set; } // номер обстановки 

        [DataMember]
        [Category(nameof(Coordinates))]
        [DisplayName(" ")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Browsable(false)]
        public Coord Coordinates { get; set; } = new Coord();

        [DataMember]
        [Browsable(false)]
        public SignSender? Sender { get; set; } // знак отправителя

        [DataMember]
        [DisplayName("F, кГц")]
        public double? FreqKHz { get; set; } // частота

        [DataMember]
        [DisplayName("θ, °")]
        public float? Bearing { get; set; } //пеленг

        [DataMember]
        [Browsable(false)]
        public byte? Letter { get; set; } // Литера

        [DataMember]
        [DisplayName("U, дБ")]
        public short? Threshold { get; set; } // порог

        [DataMember]
        [DisplayName("Приоритет")]
        public byte? Priority { get; set; }

        [DataMember]
        [Category(nameof(InterferenceParam))]
        [DisplayName(" ")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public InterferenceParam InterferenceParam { get; set; } = new InterferenceParam();

        [DataMember]
        [Category(nameof(Coordinates))]
        [DisplayName(" ")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public CoordDDMMSS CoordinatesDDMMSS { get; set; } = new CoordDDMMSS();

        public TableSuppressFWSAll Clone()
        {
            return new TableSuppressFWSAll()
            {
                Bearing = Bearing,
                FreqKHz = FreqKHz,
                Id = Id,
                IdMission = IdMission,
                InterferenceParam = InterferenceParam.Clone(),
                Coordinates = new Coord()
                {
                    Altitude = Coordinates.Altitude,
                    Longitude = Coordinates.Longitude,
                    Latitude = Coordinates.Latitude,
                },
                CoordinatesDDMMSS = new CoordDDMMSS
                {
                    LatSign = CoordinatesDDMMSS.LatSign,
                    LonSign = CoordinatesDDMMSS.LonSign,
                    Latitude = CoordinatesDDMMSS.Latitude,
                    Longitude = CoordinatesDDMMSS.Longitude,
                    LatDegrees = CoordinatesDDMMSS.LatDegrees,
                    LonDegrees = CoordinatesDDMMSS.LonDegrees,
                    LatMinutesDDMM = CoordinatesDDMMSS.LatMinutesDDMM,
                    LonMinutesDDMM = CoordinatesDDMMSS.LonMinutesDDMM,
                    LatMinutesDDMMSS = CoordinatesDDMMSS.LatMinutesDDMMSS,
                    LonMinutesDDMMSS = CoordinatesDDMMSS.LonMinutesDDMMSS,
                    LatSeconds = CoordinatesDDMMSS.LatSeconds,
                    LonSeconds = CoordinatesDDMMSS.LonSeconds,
                    Altitude = CoordinatesDDMMSS.Altitude
                },
                Letter = Letter,
                Threshold = Threshold,
                NumberASP = NumberASP,
                Sender = Sender,
                Priority = Priority
            };
        }
    }

    public class GlobalSuppressFWS : INotifyPropertyChanged
    {
        public ObservableCollection<TableSuppressFWSAll> CollectionSuppressFWS { get; set; }

        public GlobalSuppressFWS()
        {
            CollectionSuppressFWS = new ObservableCollection<TableSuppressFWSAll>() { };

            //CollectionSuppressFWS = new ObservableCollection<TableSuppressFWSAll>()
            //{
            //    new TableSuppressFWSAll
            //    {
            //        TableSuppressFWS = new TableSuppressFWS
            //        {
            //            Id = 1,
            //            NumberASP = 1,
            //            FreqKHz = 35000.0F,
            //            Bearing = 345,
            //            Letter = 1,
            //            Threshold = -80,
            //            Priority = 1,
            //            Coordinates = new Coord
            //            {
            //                Latitude = 52.456788,
            //                Longitude = 25.111977,
            //                Altitude = 45
            //            },
            //            InterferenceParam = new InterferenceParam
            //            {
            //                Modulation = 2,
            //                Manipulation = 0,
            //                Deviation = 1,
            //                Duration = 2
            //            }
            //        },
            //        TempSuppressFWS = new TempSuppressFWS
            //        {
            //            Control = Led.Green,
            //            Suppress = Led.Green,
            //            Radiation = Led.Blue
            //        }
            //    },
            //    new TableSuppressFWSAll
            //    {
            //        TableSuppressFWS = new TableSuppressFWS
            //        {
            //            Id = 2,
            //            NumberASP = 2,
            //            FreqKHz = 55055.8,
            //            Bearing = 55,
            //            Letter = 1,
            //            Threshold = -70,
            //            Priority = 2,
            //            Coordinates = new Coord
            //            {
            //                Latitude = 56.560099,
            //                Longitude = 32.567878,
            //                Altitude = 200
            //            },
            //            InterferenceParam = new InterferenceParam
            //            {
            //                Modulation = 1,
            //                Manipulation = 1,
            //                Deviation = 2,
            //                Duration = 1
            //            }
            //        },
            //        TempSuppressFWS = new TempSuppressFWS
            //        {
            //            Control = Led.Red,
            //            Suppress = Led.Empty,
            //            Radiation = Led.Red
            //        }
            //    },
            //    new TableSuppressFWSAll
            //    {
            //        TableSuppressFWS = new TableSuppressFWS
            //        {
            //            Id = 3,
            //            NumberASP = 3,
            //            FreqKHz = 43009.5,
            //            Bearing = 70,
            //            Letter = 1,
            //            Threshold = -70,
            //            Priority = 2,
            //            Coordinates = new Coord
            //            {
            //                Latitude = 55.546788,
            //                Longitude = 29.349809,
            //                Altitude = 200
            //            },
            //            InterferenceParam = new InterferenceParam
            //            {
            //                Modulation = 1,
            //                Manipulation = 9,
            //                Deviation = 2,
            //                Duration = 1
            //            }
            //        },
            //        TempSuppressFWS = new TempSuppressFWS
            //        {
            //            Control = Led.Empty,
            //            Suppress = Led.Empty,
            //            Radiation = Led.Empty
            //        }
            //    },
            //    new TableSuppressFWSAll
            //    {
            //        TableSuppressFWS = new TableSuppressFWS
            //        {
            //            Id = 4,
            //            NumberASP = 4,
            //            FreqKHz = 87009.5,
            //            Bearing = 235,
            //            Letter = 2,
            //            Threshold = -60,
            //            Priority = 2,
            //            Coordinates = new Coord
            //            {
            //                Latitude = 52.005668,
            //                Longitude = 30.345566,
            //                Altitude = 1500
            //            },
            //            InterferenceParam = new InterferenceParam
            //            {
            //                Modulation = 1,
            //                Manipulation = 6,
            //                Deviation = 2,
            //                Duration = 1
            //            }
            //        },
            //        TempSuppressFWS = new TempSuppressFWS
            //        {
            //            Control = Led.Green,
            //            Suppress = Led.Green,
            //            Radiation = Led.White
            //        }
            //    }
            //};

            //Example////////////////////////////////////////////////////////////////////
            //var a = (from t in CollectionSuppressFWS select t.TableSuppressFWS).ToList();
            //////////////////////////////////////////////////////////////////////Example

        }

        public TableSuppressFWSAll SelectedSuppressFWS
        {
            get { return selectedSuppressFWS; }
            set
            {
                selectedSuppressFWS = value;
                OnPropertyChanged("SelectedSuppressFWS");
            }
        }
        private TableSuppressFWSAll selectedSuppressFWS;

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }

    }

    public class CoordDDMMSS : INotifyPropertyChanged
    {
        #region NotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }
        #endregion

        private LatitudeSign latSign = LatitudeSign.N;
        [DataMember]
        [DisplayName(nameof(LatSign))]
        [NotifyParentProperty(true)]
        [Category(nameof(Coord))]
        public LatitudeSign LatSign
        {
            get { return latSign; }
            set
            {
                if (latSign == value)
                    return;

                latSign = value;
                OnPropertyChanged(nameof(LatSign));
            }
        }

        private LongitudeSign lonSign = LongitudeSign.E;
        [DataMember]
        [DisplayName(nameof(LonSign))]
        [NotifyParentProperty(true)]
        [Category(nameof(Coord))]
        public LongitudeSign LonSign
        {
            get { return lonSign; }
            set
            {
                if (lonSign == value)
                    return;

                lonSign = value;
                OnPropertyChanged(nameof(LonSign));
            }
        }

        private double latitude = 0;
        [DataMember]
        [DisplayName(nameof(Latitude))]
        [NotifyParentProperty(true)]
        [Category(nameof(Coord))]
        public double Latitude
        {
            get { return latitude; }
            set
            {
                if (latitude == value)
                    return;

                latitude = value;
                OnPropertyChanged(nameof(Latitude));
            }
        }

        private double longitude = 0;
        [DataMember]
        [DisplayName(nameof(Longitude))]
        [NotifyParentProperty(true)]
        [Category(nameof(Coord))]
        public double Longitude
        {
            get { return longitude; }
            set
            {
                if (longitude == value)
                    return;

                longitude = value;
                OnPropertyChanged(nameof(Longitude));
            }
        }

        private int latDegrees = 0;
        [DataMember]
        [DisplayName(nameof(LatDegrees))]
        [NotifyParentProperty(true)]
        [Category(nameof(Coord))]
        public int LatDegrees
        {
            get { return latDegrees; }
            set
            {
                if (latDegrees == value)
                    return;

                latDegrees = value;
                OnPropertyChanged(nameof(LatDegrees));
            }
        }

        private int lonDegrees = 0;
        [DataMember]
        [DisplayName(nameof(LonDegrees))]
        [NotifyParentProperty(true)]
        [Category(nameof(Coord))]
        public int LonDegrees
        {
            get { return lonDegrees; }
            set
            {
                if (lonDegrees == value)
                    return;

                lonDegrees = value;
                OnPropertyChanged(nameof(LonDegrees));
            }
        }

        private double latMinutesDDMM = 0;
        [DataMember]
        [DisplayName(nameof(LatMinutesDDMM))]
        [NotifyParentProperty(true)]
        [Category(nameof(Coord))]
        public double LatMinutesDDMM
        {
            get { return latMinutesDDMM; }
            set
            {
                if (latMinutesDDMM == value)
                    return;

                latMinutesDDMM = value;
                OnPropertyChanged(nameof(LatMinutesDDMM));
            }
        }

        private double lonMinutesDDMM = 0;
        [DataMember]
        [DisplayName(nameof(LonMinutesDDMM))]
        [NotifyParentProperty(true)]
        [Category(nameof(Coord))]
        public double LonMinutesDDMM
        {
            get { return lonMinutesDDMM; }
            set
            {
                if (lonMinutesDDMM == value)
                    return;

                lonMinutesDDMM = value;
                OnPropertyChanged(nameof(LonMinutesDDMM));
            }
        }

        private int latMinutesDDMMSS = 0;
        [DataMember]
        [DisplayName(nameof(LatMinutesDDMMSS))]
        [NotifyParentProperty(true)]
        [Category(nameof(Coord))]
        public int LatMinutesDDMMSS
        {
            get { return latMinutesDDMMSS; }
            set
            {
                if (latMinutesDDMMSS == value)
                    return;

                latMinutesDDMMSS = value;
                OnPropertyChanged(nameof(LatMinutesDDMMSS));
            }
        }

        private int lonMinutesDDMMSS = 0;
        [DataMember]
        [DisplayName(nameof(LonMinutesDDMMSS))]
        [NotifyParentProperty(true)]
        [Category(nameof(Coord))]
        public int LonMinutesDDMMSS
        {
            get { return lonMinutesDDMMSS; }
            set
            {
                if (lonMinutesDDMMSS == value)
                    return;

                lonMinutesDDMMSS = value;
                OnPropertyChanged(nameof(LonMinutesDDMMSS));
            }
        }

        private double latSeconds = 0;
        [DataMember]
        [DisplayName(nameof(LatSeconds))]
        [NotifyParentProperty(true)]
        [Category(nameof(Coord))]
        public double LatSeconds
        {
            get { return latSeconds; }
            set
            {
                if (latSeconds == value)
                    return;

                latSeconds = value;
                OnPropertyChanged(nameof(LatSeconds));
            }
        }

        private double lonSeconds = 0;
        [DataMember]
        [DisplayName(nameof(LonSeconds))]
        [NotifyParentProperty(true)]
        [Category(nameof(Coord))]
        public double LonSeconds
        {
            get { return lonSeconds; }
            set
            {
                if (lonSeconds == value)
                    return;

                lonSeconds = value;
                OnPropertyChanged(nameof(LonSeconds));
            }
        }

        private double altitude = -1;
        [DataMember]
        [DisplayName(nameof(Altitude))]
        [NotifyParentProperty(true)]
        [Category(nameof(Coord))]
        public double Altitude
        {
            get { return altitude; }
            set
            {
                if (altitude == value)
                    return;

                altitude = value;
                OnPropertyChanged(nameof(Altitude));
            }
        }
    }
}
