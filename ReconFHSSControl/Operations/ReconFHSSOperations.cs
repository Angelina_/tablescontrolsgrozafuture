﻿using ModelsTablesDBLib;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Controls;
using TableEvents;
using ValuesCorrectLib;

namespace ReconFHSSControl
{
    public partial class UserControlReconFHSS : UserControl
    {
        /// <summary>
        /// Добавить записи в контрол
        /// </summary>
        public void AddReconFHSSs(List<TableReconFHSS> listReconFHSS)
        {
            try
            {
                DeleteEmptyRows(dgvReconFHSS);

                for (int i = 0; i < listReconFHSS.Count; i++)
                {
                    int ind = ((GlobalReconFHSS)dgvReconFHSS.DataContext).CollectionReconFHSS.ToList().FindIndex(x => x.TableReconFHSS.Id == listReconFHSS[i].Id);
                    if (ind != -1)
                    {    
                        ((GlobalReconFHSS)dgvReconFHSS.DataContext).CollectionReconFHSS[ind].TableReconFHSS = listReconFHSS[i];
                    }
                    else
                    {
                        TableReconFHSSAll table = ReconFHSSToReconFHSSAll(listReconFHSS[i]);
                        ((GlobalReconFHSS)dgvReconFHSS.DataContext).CollectionReconFHSS.Add(table);
                    }
                }

                int indIsSelectedRow = ((GlobalReconFHSS)dgvReconFHSS.DataContext).CollectionReconFHSS.ToList().FindIndex(x => (x.TableReconFHSS.IsSelected.HasValue && x.TableReconFHSS.IsSelected.Value));
                if (indIsSelectedRow != -1)
                {
                    if (((GlobalReconFHSSCurRow)dgvReconFHSSCurRow.DataContext).CollectionReconFHSSCurRow.Count == 0)
                    {
                        AddEmptyRows(dgvReconFHSSCurRow);
                        ((GlobalReconFHSSCurRow)dgvReconFHSSCurRow.DataContext).CollectionReconFHSSCurRow[0] = ((GlobalReconFHSS)dgvReconFHSS.DataContext).CollectionReconFHSS[indIsSelectedRow];
                    }
                    else
                    {
                        ((GlobalReconFHSSCurRow)dgvReconFHSSCurRow.DataContext).CollectionReconFHSSCurRow[0] = ((GlobalReconFHSS)dgvReconFHSS.DataContext).CollectionReconFHSS[indIsSelectedRow];
                    }
                }

                AddEmptyRows(dgvReconFHSS);
            }
            catch { }
        }

        public void UpdateASPRP(ObservableCollection<string> lASPRP)
        {
            DeleteEmptyRows(dgvReconFHSS);

            for (int i = 0; i < ((GlobalReconFHSS)dgvReconFHSS.DataContext).CollectionReconFHSS.Count; i++)
            {
                ((GlobalReconFHSS)dgvReconFHSS.DataContext).CollectionReconFHSS[i].TableNumASP.NumberASP = lASPRP;


                /////////////////
                
                    ((GlobalReconFHSS)dgvReconFHSS.DataContext).CollectionReconFHSS[i].TableNumASP.SelectedASPRP = lASPRP[0];
                    ////////////
            }

            AddEmptyRows(dgvReconFHSS);
        }

        /// <summary>
        /// Преобразование TableReconFHSS к TableReconFHSSAll
        /// </summary>
        /// <param name="listSuppressFHSS"></param>
        /// <returns> List<TableSuppressFWSAll> </returns>
        private TableReconFHSSAll ReconFHSSToReconFHSSAll(TableReconFHSS tableReconFHSS)
        {
            TableReconFHSSAll table = new TableReconFHSSAll();
            table.TableReconFHSS.Id = tableReconFHSS.Id;
            table.TableReconFHSS.IdMission = tableReconFHSS.IdMission;
            table.TableReconFHSS.FreqMinKHz = tableReconFHSS.FreqMinKHz;
            table.TableReconFHSS.FreqMaxKHz = tableReconFHSS.FreqMaxKHz;
            table.TableReconFHSS.Deviation = tableReconFHSS.Deviation;
            table.TableReconFHSS.ImpulseDuration = tableReconFHSS.ImpulseDuration;
            table.TableReconFHSS.StepKHz = tableReconFHSS.StepKHz;
            table.TableReconFHSS.IsSelected = tableReconFHSS.IsSelected;
            table.TableReconFHSS.Modulation = tableReconFHSS.Modulation;
            table.TableReconFHSS.QuantitySignal = tableReconFHSS.QuantitySignal;
            table.TableReconFHSS.Time = tableReconFHSS.Time;

            return table;
        }

        /// <summary>
        /// Преобразование List<TableReconFHSS> к List<TableReconFHSSAll>
        /// </summary>
        /// <param name="listSuppressFHSS"></param>
        /// <returns> List<TableSuppressFWSAll> </returns>
        private List<TableReconFHSSAll> ReconFHSSToReconFHSSAll(List<TableReconFHSS> listReconFHSS)
        {
            List<TableReconFHSSAll> list = new List<TableReconFHSSAll>();
            for (int i = 0; i < listReconFHSS.Count; i++)
            {
                TableReconFHSSAll table = new TableReconFHSSAll();
                table.TableReconFHSS.Id = listReconFHSS[i].Id;
                table.TableReconFHSS.IdMission = listReconFHSS[i].IdMission;
                table.TableReconFHSS.FreqMinKHz = listReconFHSS[i].FreqMinKHz;
                table.TableReconFHSS.FreqMaxKHz = listReconFHSS[i].FreqMaxKHz;
                table.TableReconFHSS.Deviation = listReconFHSS[i].Deviation;
                table.TableReconFHSS.ImpulseDuration = listReconFHSS[i].ImpulseDuration;
                table.TableReconFHSS.StepKHz = listReconFHSS[i].StepKHz;
                table.TableReconFHSS.IsSelected = listReconFHSS[i].IsSelected;
                table.TableReconFHSS.Modulation = listReconFHSS[i].Modulation;
                table.TableReconFHSS.QuantitySignal = listReconFHSS[i].QuantitySignal;
                table.TableReconFHSS.Time = listReconFHSS[i].Time;

                list.Add(table);
            }

            return list;
        }

        /// <summary>
        /// Обновить записи в контроле
        /// </summary>
        public void UpdateReconFHSS(List<TableReconFHSS> listReconFHSS)
        {
            try
            {
                ((GlobalReconFHSSCurRow)dgvReconFHSSCurRow.DataContext).CollectionReconFHSSCurRow.Clear();
                ((GlobalReconFHSS)dgvReconFHSS.DataContext).CollectionReconFHSS.Clear();

                List<TableReconFHSSAll> list = ReconFHSSToReconFHSSAll(listReconFHSS);
                for (int i = 0; i < list.Count; i++)
                {
                    ((GlobalReconFHSS)dgvReconFHSS.DataContext).CollectionReconFHSS.Add(list[i]);
                }

                AddEmptyRows(dgvReconFHSS);
                AddEmptyRows(dgvReconFHSSCurRow);
            }
            catch { }
        }

        /// <summary>
        /// Удалить одну запись из контрола
        /// </summary>
        public void DeleteReconFHSS(TableReconFHSS tableReconFHSS)
        {
            try
            {
                int index = ((GlobalReconFHSS)dgvReconFHSS.DataContext).CollectionReconFHSS.ToList().FindIndex(x => x.TableReconFHSS.Id == tableReconFHSS.Id);
                ((GlobalReconFHSS)dgvReconFHSS.DataContext).CollectionReconFHSS.RemoveAt(index);

                ((GlobalReconFHSSCurRow)dgvReconFHSSCurRow.DataContext).CollectionReconFHSSCurRow.RemoveAt(0);
            }
            catch { }
        }

        /// <summary>
        /// Удалить все записи из контрола
        /// </summary>
        public void ClearReconFHSS()
        {
            try
            {
                ((GlobalReconFHSS)dgvReconFHSS.DataContext).CollectionReconFHSS.Clear();
                AddEmptyRows(dgvReconFHSS);

                ((GlobalReconFHSSCurRow)dgvReconFHSSCurRow.DataContext).CollectionReconFHSSCurRow.Clear();
                AddEmptyRows(dgvReconFHSSCurRow);
            }
            catch { }
        }

        /// <summary>
        /// Добавить / удалить пустую строку в dgvTempFWSCurRow или dgvTempFWS
        /// </summary>
        private void AddEmptyRows(DataGrid dataGrid)
        {
            try
            {
                int сountRowsAll = dataGrid.Items.Count; // количество всех строк в таблице
                double hs = 23; // высота строки
                double ah = dataGrid.ActualHeight; // визуализированная высота dataGrid
                double chh = dataGrid.ColumnHeaderHeight; // высота заголовка
                int countRows = -1;
                int index = -1;
                int count = -1;

                List<TableReconFHSSAll> listReconFHSS;

                switch (dataGrid.Name)
                {
                    case "dgvReconFHSS":

                        countRows = Convert.ToInt32(Math.Floor(Convert.ToDouble(ah) - Convert.ToDouble(chh)) / hs); // количество строк, которое помещается в область видимости = (высота таблицы - заголовок таблицы) / высота строки
                        count = сountRowsAll - countRows; // сколько пустых строк нужно удалить

                        for (int i = 0; i < count; i++)
                        {
                            // Удалить пустые строки в dgv
                            index = ((GlobalReconFHSS)dgvReconFHSS.DataContext).CollectionReconFHSS.ToList().FindIndex(x => x.TableReconFHSS.Id < 0);
                            if (index != -1)
                            {
                                ((GlobalReconFHSS)dgvReconFHSS.DataContext).CollectionReconFHSS.RemoveAt(index);
                            }
                        }

                        listReconFHSS = new List<TableReconFHSSAll>();

                        for (int i = 0; i < (countRows - сountRowsAll) + 1; i++)
                        {
                            TableReconFHSSAll strReconFHSS = new TableReconFHSSAll
                            {
                                TableReconFHSS = new TableReconFHSS
                                {
                                    Id = -2,
                                    FreqMinKHz = -2F,
                                    FreqMaxKHz = -2F,
                                    Deviation = -2F,
                                    StepKHz = -2,
                                    Modulation = 33, // 33 - пустое значение
                                    ImpulseDuration = 65535, // ushort От 0 до 65535
                                    QuantitySignal = -2
                                },
                                TableNumASP = new TableNumASP
                                {
                                    NumberASP = new ObservableCollection<string> { }
                                    //NumberASP = new List<int>
                                    //{
                                    //   -2
                                    //},
                                    //IsOwn = new List<bool>
                                    //{
                                    //    false
                                    //}
                                }
                                //lTableNumASP = new List<TableNumASP>
                                //{
                                //    new TableNumASP { NumberASP = -2, IsOwn = false },
                                //}
                            };

                            listReconFHSS.Add(strReconFHSS);
                        }

                        for (int i = 0; i < listReconFHSS.Count; i++)
                        {
                            ((GlobalReconFHSS)dgvReconFHSS.DataContext).CollectionReconFHSS.Add(listReconFHSS[i]);
                        }
                        break;

                    case "dgvReconFHSSCurRow":

                        countRows = Convert.ToInt32(Math.Floor(Convert.ToDouble(ah) - Convert.ToDouble(chh)) / hs); // количество строк, которое помещается в область видимости = (высота таблицы - заголовок таблицы) / высота строки

                        listReconFHSS = new List<TableReconFHSSAll>();
                        for (int i = 0; i < countRows; i++)
                        {
                            TableReconFHSSAll strReconFHSS = new TableReconFHSSAll
                            {
                                TableReconFHSS = new TableReconFHSS
                                {
                                    Id = -2,
                                    Deviation = -2F,
                                    FreqMinKHz = -2F,
                                    FreqMaxKHz = -2F,
                                    StepKHz = -2,
                                    Modulation = 33, // 33 - пустое значение
                                    ImpulseDuration = 65535, // ushort От 0 до 65535
                                    QuantitySignal = -2
                                },
                                TableNumASP = new TableNumASP
                                {
                                    NumberASP = new ObservableCollection<string> { }
                                    //NumberASP = new List<int>
                                    //{
                                    //   -2
                                    //},
                                    //IsOwn = new List<bool>
                                    //{
                                    //    false
                                    //}
                                }
                                //lTableNumASP = new List<TableNumASP>
                                //{
                                //    new TableNumASP { NumberASP = -2, IsOwn = false },
                                //}
                            };

                            listReconFHSS.Add(strReconFHSS);
                        }

                        for (int i = 0; i < listReconFHSS.Count; i++)
                        {
                            ((GlobalReconFHSSCurRow)dgvReconFHSSCurRow.DataContext).CollectionReconFHSSCurRow.Add(listReconFHSS[i]);
                        }
                        break;

                    default:
                        break;
                }
            }
            catch { }
        }

        /// <summary>
        /// Удалить пустые строки из таблицы
        /// </summary>
        private void DeleteEmptyRows(DataGrid dataGrid)
        {
            int countEmptyRows = 0;
            int countAllRows = 0;
            int iCount = 0;

            switch (dataGrid.Name)
            {
                case "dgvReconFHSS":

                    countEmptyRows = ((GlobalReconFHSS)dgvReconFHSS.DataContext).CollectionReconFHSS.Count(s => s.TableReconFHSS.Id < 0);
                    countAllRows = ((GlobalReconFHSS)dgvReconFHSS.DataContext).CollectionReconFHSS.Count;
                    iCount = countAllRows - countEmptyRows;

                    for (int i = iCount; i < countAllRows; i++)
                    {
                        ((GlobalReconFHSS)dgvReconFHSS.DataContext).CollectionReconFHSS.RemoveAt(iCount);
                    }

                    break;

                case "dgvReconFHSSCurRow":

                    countEmptyRows = ((GlobalReconFHSSCurRow)dgvReconFHSSCurRow.DataContext).CollectionReconFHSSCurRow.Count(s => s.TableReconFHSS.Id < 0);
                    countAllRows = ((GlobalReconFHSSCurRow)dgvReconFHSSCurRow.DataContext).CollectionReconFHSSCurRow.Count;
                    iCount = countAllRows - countEmptyRows;

                    for (int i = iCount; i < countAllRows; i++)
                    {
                        ((GlobalReconFHSSCurRow)dgvReconFHSSCurRow.DataContext).CollectionReconFHSSCurRow.RemoveAt(iCount);
                    }

                    break;

                default:
                    break;

            }
           
        }

        private bool IsSelectedRowEmpty()
        {
            try
            {
                if (((GlobalReconFHSSCurRow)dgvReconFHSSCurRow.DataContext).CollectionReconFHSSCurRow != null)
                {
                    if (((GlobalReconFHSSCurRow)dgvReconFHSSCurRow.DataContext).CollectionReconFHSSCurRow[0].TableReconFHSS.Id == -2)
                        return false;
                }
            }
            catch { }

            return true;
        }

        /// <summary>
        /// Записать в TableSourceFHSS выделенную строку 
        /// </summary>
        /// <returns></returns>
        private TableSourceFHSS SelectedRowSourceForSend()
        {
            int index = ((GlobalSourceFHSS)dgvSourceFHSS.DataContext).CollectionSourceFHSS.ToList().FindIndex(x => x.IdFHSS == ((TableReconFHSS)dgvReconFHSS.SelectedItem).Id);

            ObservableCollection<TableJamDirect> listJamDirect = new ObservableCollection<TableJamDirect>();
            for (int i = 0; i < ((GlobalSourceFHSS)dgvSourceFHSS.DataContext).CollectionSourceFHSS[index].ListJamDirect.Count; i++)
            {
                TableJamDirect tableJamDirect =  new TableJamDirect
                {
                    JamDirect = new JamDirect
                    {
                        NumberASP = ((GlobalSourceFHSS)dgvSourceFHSS.DataContext).CollectionSourceFHSS[index].ListJamDirect[i].JamDirect.NumberASP,
                        Bearing = ((GlobalSourceFHSS)dgvSourceFHSS.DataContext).CollectionSourceFHSS[index].ListJamDirect[i].JamDirect.Bearing,
                        Level = ((GlobalSourceFHSS)dgvSourceFHSS.DataContext).CollectionSourceFHSS[index].ListJamDirect[i].JamDirect.Level,
                        Std = ((GlobalSourceFHSS)dgvSourceFHSS.DataContext).CollectionSourceFHSS[index].ListJamDirect[i].JamDirect.Std,
                        DistanceKM = ((GlobalSourceFHSS)dgvSourceFHSS.DataContext).CollectionSourceFHSS[index].ListJamDirect[i].JamDirect.DistanceKM,
                        IsOwn = ((GlobalSourceFHSS)dgvSourceFHSS.DataContext).CollectionSourceFHSS[index].ListJamDirect[i].JamDirect.IsOwn
                    }
                };

               listJamDirect.Add(tableJamDirect);
                
            }

            TableSourceFHSS tableSourceFHSS = new TableSourceFHSS
            {
                //Id = ((GlobalSourceFHSS)dgvSourceFHSS.DataContext).CollectionSourceFHSS[index].Id,
                Coordinates = new Coord
                {
                    Latitude = ((GlobalSourceFHSS)dgvSourceFHSS.DataContext).CollectionSourceFHSS[index].Coordinates.Latitude,
                    Longitude = ((GlobalSourceFHSS)dgvSourceFHSS.DataContext).CollectionSourceFHSS[index].Coordinates.Longitude,
                    Altitude = ((GlobalSourceFHSS)dgvSourceFHSS.DataContext).CollectionSourceFHSS[index].Coordinates.Altitude
                },
                ListJamDirect = listJamDirect
                
            };

            return tableSourceFHSS;
        }

        private TableReconFHSS SelectedRowReconForSend()
        {
            TableReconFHSS reconFHSS = new TableReconFHSS()
            {
                Id = ((TableReconFHSSAll)dgvReconFHSS.SelectedItem).TableReconFHSS.Id,
                FreqMinKHz = ((TableReconFHSSAll)dgvReconFHSS.SelectedItem).TableReconFHSS.FreqMinKHz,
                FreqMaxKHz = ((TableReconFHSSAll)dgvReconFHSS.SelectedItem).TableReconFHSS.FreqMaxKHz,
                Deviation = ((TableReconFHSSAll)dgvReconFHSS.SelectedItem).TableReconFHSS.Deviation,
                Modulation = ((TableReconFHSSAll)dgvReconFHSS.SelectedItem).TableReconFHSS.Modulation,
                Time = ((TableReconFHSSAll)dgvReconFHSS.SelectedItem).TableReconFHSS.Time,
                StepKHz = ((TableReconFHSSAll)dgvReconFHSS.SelectedItem).TableReconFHSS.StepKHz,
                ImpulseDuration = ((TableReconFHSSAll)dgvReconFHSS.SelectedItem).TableReconFHSS.ImpulseDuration,
                QuantitySignal = ((TableReconFHSSAll)dgvReconFHSS.SelectedItem).TableReconFHSS.QuantitySignal,
                IsSelected = true
                
            };

            return reconFHSS;
        }

        /// <summary>
        /// Добавить таблицу для отчета
        /// </summary>
        /// <returns></returns>
        private string[,] AddReconFHSSToTable()
        {
            int Columns = 9;
            int Rows = (((GlobalReconFHSS)dgvReconFHSS.DataContext).CollectionReconFHSS.Count(x => x.TableReconFHSS.Id > 0)) + 1;
            string[,] Table = new string[Rows, Columns];

            try
            {
                Table[0, 0] = SHeaders.headerFreqMin;
                Table[0, 1] = SHeaders.headerFreqMax;
                Table[0, 2] = SHeaders.headerDeltaF;
                Table[0, 3] = SHeaders.headerType;
                Table[0, 4] = SHeaders.headerTime;
                Table[0, 5] = SHeaders.headerStep;
                Table[0, 6] = SHeaders.headerPulseWidth;
                Table[0, 7] = SHeaders.headerNumFreq;
                Table[0, 8] = SHeaders.headerJ_AJS;

                for (int i = 1; i < Rows; i++)
                {
                    Table[i, 0] = ((GlobalReconFHSS)dgvReconFHSS.DataContext).CollectionReconFHSS[i - 1].TableReconFHSS.FreqMinKHz.ToString();
                    Table[i, 1] = ((GlobalReconFHSS)dgvReconFHSS.DataContext).CollectionReconFHSS[i - 1].TableReconFHSS.FreqMaxKHz.ToString();
                    Table[i, 2] = ((GlobalReconFHSS)dgvReconFHSS.DataContext).CollectionReconFHSS[i - 1].TableReconFHSS.Deviation.ToString();
                    Table[i, 3] = ((GlobalReconFHSS)dgvReconFHSS.DataContext).CollectionReconFHSS[i - 1].TableReconFHSS.Modulation.ToString();
                    Table[i, 4] = ((GlobalReconFHSS)dgvReconFHSS.DataContext).CollectionReconFHSS[i - 1].TableReconFHSS.Time.ToString();
                    Table[i, 5] = ((GlobalReconFHSS)dgvReconFHSS.DataContext).CollectionReconFHSS[i - 1].TableReconFHSS.StepKHz.ToString();
                    Table[i, 6] = ((GlobalReconFHSS)dgvReconFHSS.DataContext).CollectionReconFHSS[i - 1].TableReconFHSS.ImpulseDuration.ToString();
                    Table[i, 7] = ((GlobalReconFHSS)dgvReconFHSS.DataContext).CollectionReconFHSS[i - 1].TableReconFHSS.QuantitySignal.ToString();
                    Table[i, 8] = ((GlobalReconFHSS)dgvReconFHSS.DataContext).CollectionReconFHSS[i - 1].TableNumASP.SelectedASPRP.ToString();
                }
            }
            catch { }

            return Table;
        }

        public void ChangeFreqHeaderMHz_kHz()
        {
            switch (PropLocalFreqMHz_kHz.FreqMHz_kHz)
            {
                case 0: // kHz
                    lHeaderFreq.Content = SMeaning.meaningFkHz;
                    lHeaderFreqMin.Content = SMeaning.meaningFminkHz;
                    lHeaderFreqMax.Content = SMeaning.meaningFmaxkHz;
                    break;

                case 1: // MHz
                    lHeaderFreq.Content = SMeaning.meaningFMHz;
                    lHeaderFreqMin.Content = SMeaning.meaningFminMHz;
                    lHeaderFreqMax.Content = SMeaning.meaningFmaxMHz;
                    break;
            }

        }
    }
}
