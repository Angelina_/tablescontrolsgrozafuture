﻿using ModelsTablesDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using TableEvents;

namespace ReconFHSSControl
{
    public partial class UserControlReconFHSS : UserControl
    {
        public List<TableFHSSReconExcluded> listReconExcluded = new List<TableFHSSReconExcluded>();

        /// <summary>
        /// Обновть таблицу (FHSSReconExcluded)
        /// </summary>
        /// <param name="id"></param>
        private void UpdateTableFHSSReconExcluded(int id)
        {
            try
            {
                if ((GlobalFHSSReconExcluded)dgvFHSSReconExcluded.DataContext == null) { return; }

                ((GlobalFHSSReconExcluded)dgvFHSSReconExcluded.DataContext).CollectionFHSSReconExcluded.Clear();

                List<TableFHSSReconExcluded> list = listReconExcluded.Where(x => x.IdFHSS == id).ToList();

                for (int i = 0; i < list.Count; i++)
                {
                    ((GlobalFHSSReconExcluded)dgvFHSSReconExcluded.DataContext).CollectionFHSSReconExcluded.Add(list[i]);
                }

                AddEmptyRowsFHSSReconExcluded();
            }
            catch { }
        }

        /// <summary>
        /// Обновить записи в контроле (Выколотые частоты)
        /// </summary>
        /// <param name="listFHSSReconExcluded"></param>
        public void UpdateTableFHSSReconExcluded(List<TableFHSSReconExcluded> listFHSSReconExcluded)
        {
            try
            {
                if (listFHSSReconExcluded == null)
                    return;

                listReconExcluded = listFHSSReconExcluded;

                List<TableFHSSReconExcluded> list = listReconExcluded.Where(x => x.IdFHSS == PropSelectedIdFHSS.SelectedIdReconFHSS).ToList();

                if (list != null && list.Count > 0)
                {
                    ((GlobalFHSSReconExcluded)dgvFHSSReconExcluded.DataContext).CollectionFHSSReconExcluded.Clear();

                    for (int i = 0; i < list.Count; i++)
                    {
                        ((GlobalFHSSReconExcluded)dgvFHSSReconExcluded.DataContext).CollectionFHSSReconExcluded.Add(list[i]);
                    }

                    AddEmptyRowsFHSSReconExcluded();
                }
            }
            catch { }
        }
       
        /// <summary>
        /// Добавить пустую строку в dgvFHSSReconExcluded
        /// </summary>
        private void AddEmptyRowsFHSSReconExcluded()
        {
            try
            {
                int сountRowsAll = dgvFHSSReconExcluded.Items.Count; // количество всех строк в таблице
                double hs = 23; // высота строки
                double ah = dgvFHSSReconExcluded.ActualHeight; // визуализированная высота dataGrid
                double chh = dgvFHSSReconExcluded.ColumnHeaderHeight; // высота заголовка
                int countRows = Convert.ToInt32(Math.Floor(Convert.ToDouble(ah) - Convert.ToDouble(chh)) / hs); // количество строк, которое помещается в область видимости = (высота таблицы - заголовок таблицы) / высота строки

                int count = сountRowsAll - countRows; // сколько пустых строк нужно удалить
                int index = -1;
                for (int i = 0; i < count; i++)
                {
                    // Удалить пустые строки в dgv
                    index = ((GlobalFHSSReconExcluded)dgvFHSSReconExcluded.DataContext).CollectionFHSSReconExcluded.ToList().FindIndex(x => x.Id < 0);
                    if (index != -1)
                    {
                        ((GlobalFHSSReconExcluded)dgvFHSSReconExcluded.DataContext).CollectionFHSSReconExcluded.RemoveAt(index);
                    }
                }

                List<TableFHSSReconExcluded> list = new List<TableFHSSReconExcluded>();
                for (int i = 0; i < countRows - сountRowsAll; i++)
                {
                    TableFHSSReconExcluded strFHSSReconExcluded = new TableFHSSReconExcluded
                    {
                        Id = -2,
                        IdFHSS = -2,
                        FreqKHz = -2F,
                        Deviation = -2
                    };

                    list.Add(strFHSSReconExcluded);
                }

                for (int i = 0; i < list.Count; i++)
                {
                    ((GlobalFHSSReconExcluded)dgvFHSSReconExcluded.DataContext).CollectionFHSSReconExcluded.Add(list[i]);
                }
            }

            catch { }
        }

        /// <summary>
        /// Удалить пустые строки из таблицы
        /// </summary>
        private void DeleteEmptyRowsFHSSReconExcluded()
        {
            int countEmptyRows = ((GlobalFHSSReconExcluded)dgvFHSSReconExcluded.DataContext).CollectionFHSSReconExcluded.Count(s => s.Id < 0);
            int countAllRows = ((GlobalFHSSReconExcluded)dgvFHSSReconExcluded.DataContext).CollectionFHSSReconExcluded.Count;
            int iCount = countAllRows - countEmptyRows;

            for (int i = iCount; i < countAllRows; i++)
            {
                ((GlobalFHSSReconExcluded)dgvFHSSReconExcluded.DataContext).CollectionFHSSReconExcluded.RemoveAt(iCount);
            }
        }

    }
}
