﻿using ModelsTablesDBLib;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using TableEvents;

namespace ReconFHSSControl
{
    public partial class UserControlReconFHSS : UserControl
    {
        public List<TableSourceFHSSRow> listSourceFHSS = new List<TableSourceFHSSRow>();
        public List<TableSourceFHSS> TempListSourceFHSS { get; set; }

        /// <summary>
        /// Обновть таблицу (SourceFHSS) по выделенной строке
        /// </summary>
        /// <param name="id"></param>
        private void UpdateSourceFHSS(int id)
        {
            try
            {
                if (((GlobalSourceFHSS)dgvSourceFHSS.DataContext) == null) { return; }

                ((GlobalSourceFHSS)dgvSourceFHSS.DataContext).CollectionSourceFHSS.Clear();

                List<TableSourceFHSSRow> list = listSourceFHSS.Where(x => x.IdFHSS == id).ToList();

                for (int i = 0; i < list.Count; i++)
                {
                    ((GlobalSourceFHSS)dgvSourceFHSS.DataContext).CollectionSourceFHSS.Add(list[i]);
                }

                AddEmptyRowsSource();
            }
            catch { }
        }

        public void UpdateSourceFHSS(List<TableSourceFHSS> lSourceFHSS)
        {
            try
            {
                TempListSourceFHSS = lSourceFHSS;

                if (lSourceFHSS == null)
                    return;

                listSourceFHSS = TableSourceFHSSToTableSourceFHSSRow(lSourceFHSS);

                List<TableSourceFHSSRow> list = listSourceFHSS.Where(x => x.IdFHSS == PropSelectedIdFHSS.SelectedIdReconFHSS).ToList();

                if (list != null && list.Count > 0)
                {
                    ((GlobalSourceFHSS)dgvSourceFHSS.DataContext).CollectionSourceFHSS.Clear();

                    for (int i = 0; i < list.Count; i++)
                    {
                        ((GlobalSourceFHSS)dgvSourceFHSS.DataContext).CollectionSourceFHSS.Add(list[i]);
                    }

                    AddEmptyRowsSource();
                }
            }
            catch { }
        }

        /// <summary>
        /// Привести TableSourceFHSS к TableSourceFHSSRow
        /// </summary>
        /// <param name="listSourceFHSS"></param>
        /// <returns></returns>
        private List<TableSourceFHSSRow> TableSourceFHSSToTableSourceFHSSRow(List<TableSourceFHSS> listSourceFHSS)
        {
            List<TableSourceFHSSRow> list = new List<TableSourceFHSSRow>();
            for (int i = 0; i < listSourceFHSS.Count; i++)
            {
                TableSourceFHSSRow table = new TableSourceFHSSRow();
                table.Id = listSourceFHSS[i].Id;
                table.IdFHSS = listSourceFHSS[i].IdFHSS;
                table.Coordinates = new Coord
                {
                    Latitude = listSourceFHSS[i].Coordinates.Latitude,
                    Longitude = listSourceFHSS[i].Coordinates.Longitude,
                    Altitude = listSourceFHSS[i].Coordinates.Altitude
                };

                ObservableCollection<TableJamDirect> listJamDirect = new ObservableCollection<TableJamDirect>();
                for (int j = 0; j < listSourceFHSS[i].ListJamDirect.Count; j++)
                {
                    TableJamDirect tableJamDirect = new TableJamDirect
                    {
                        JamDirect = new JamDirect
                        {
                            NumberASP = listSourceFHSS[i].ListJamDirect[j].JamDirect.NumberASP,
                            Bearing = listSourceFHSS[i].ListJamDirect[j].JamDirect.Bearing,
                            Level = listSourceFHSS[i].ListJamDirect[j].JamDirect.Level,
                            Std = listSourceFHSS[i].ListJamDirect[j].JamDirect.Std,
                            DistanceKM = listSourceFHSS[i].ListJamDirect[j].JamDirect.DistanceKM,
                            IsOwn = listSourceFHSS[i].ListJamDirect[j].JamDirect.IsOwn
                        }
                    };

                    listJamDirect.Add(tableJamDirect);
                }
                table.ListJamDirect = listJamDirect;

                list.Add(table);
            }

            return list;
        }   
        
        /// <summary>
        /// Добавить / удалить пустую строку в dgvTempFWSCurRow или dgvTempFWS
        /// </summary>
        private void AddEmptyRowsSource()
        {
            try
            {
                int сountRowsAll = dgvSourceFHSS.Items.Count; // количество всех строк в таблице
                double hs = 23; // высота строки
                double ah = dgvSourceFHSS.ActualHeight; // визуализированная высота dataGrid
                double chh = dgvSourceFHSS.ColumnHeaderHeight; // высота заголовка
                int countRows = Convert.ToInt32(Math.Floor(Convert.ToDouble(ah) - Convert.ToDouble(chh)) / hs); // количество строк, которое помещается в область видимости = (высота таблицы - заголовок таблицы) / высота строки

                int count = сountRowsAll - countRows; // сколько пустых строк нужно удалить
                int index = -1;
                for (int i = 0; i < count; i++)
                {
                    // Удалить пустые строки в dgv
                    index = ((GlobalSourceFHSS)dgvSourceFHSS.DataContext).CollectionSourceFHSS.ToList().FindIndex(x => x.Id < 0);
                    if (index != -1)
                    {
                        ((GlobalSourceFHSS)dgvSourceFHSS.DataContext).CollectionSourceFHSS.RemoveAt(index);
                    }
                }

                List<TableSourceFHSSRow> list = new List<TableSourceFHSSRow>();
                for (int i = 0; i < countRows - сountRowsAll; i++)
                {
                    TableSourceFHSSRow strSourceFHSS = new TableSourceFHSSRow
                    {
                        Id = -2,
                        IdFHSS = -2,
                        Coordinates = new Coord
                        {
                            Latitude = -2,
                            Longitude = -2,
                            Altitude = -2
                        },
                        ListJamDirect = new ObservableCollection<TableJamDirect>
                        {
                             new TableJamDirect
                             {
                                 JamDirect = new JamDirect
                                 {
                                     NumberASP = -2,
                                     Bearing = -2F,
                                     Level = -2,
                                     Std = -2,
                                     DistanceKM = -2

                                 }
                             }
                         }
                    };

                    list.Add(strSourceFHSS);
                }

                for (int i = 0; i < list.Count; i++)
                {
                    ((GlobalSourceFHSS)dgvSourceFHSS.DataContext).CollectionSourceFHSS.Add(list[i]);
                }
            }

            catch { }
        }

        /// <summary>
        /// Удалить пустые строки из таблицы
        /// </summary>
        private void DeleteEmptyRowsSource()
        {
            int countEmptyRows = ((GlobalSourceFHSS)dgvSourceFHSS.DataContext).CollectionSourceFHSS.Count(s => s.Id < 0);
            int countAllRows = ((GlobalSourceFHSS)dgvSourceFHSS.DataContext).CollectionSourceFHSS.Count;
            int iCount = countAllRows - countEmptyRows;

            for (int i = iCount; i < countAllRows; i++)
            {
                ((GlobalSourceFHSS)dgvSourceFHSS.DataContext).CollectionSourceFHSS.RemoveAt(iCount);
            }
        }

    }
}
