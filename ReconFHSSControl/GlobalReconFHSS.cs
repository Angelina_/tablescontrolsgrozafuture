﻿using ModelsTablesDBLib;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace ReconFHSSControl
{
    public class TableReconFHSSAll
    {
        public TableReconFHSS TableReconFHSS { get; set; } = new TableReconFHSS();
        //public List<TableNumASP> lTableNumASP { get; set; } = new List<TableNumASP>();
        public TableNumASP TableNumASP { get; set; } = new TableNumASP();
    }

    public class GlobalReconFHSS
    {
        public ObservableCollection<TableReconFHSSAll> CollectionReconFHSS { get; set; }

        public GlobalReconFHSS()
        {
            CollectionReconFHSS = new ObservableCollection<TableReconFHSSAll> { };

            //CollectionReconFHSS = new ObservableCollection<TableReconFHSSAll>
            //{
            //    new TableReconFHSSAll
            //    {
            //        TableReconFHSS = new TableReconFHSS
            //        {
            //            Id = 3,
            //            FreqMinKHz = 35000.9,
            //            FreqMaxKHz = 156666.7,
            //            Deviation = 100.4F,
            //            StepKHz = 100,
            //            Time = DateTime.Now.ToLocalTime(),
            //            Modulation = 1,
            //            ImpulseDuration = 30,
            //            IsSelected = true,
            //            QuantitySignal = 50
            //        },
            //        TableNumASP = new TableNumASP
            //        {
            //            NumberASP = new ObservableCollection<string>{"*209", "210", "211"}
            //        }
            //    },
            //    new TableReconFHSSAll
            //    {
            //        TableReconFHSS = new TableReconFHSS
            //        {
            //            Id = 4,
            //            FreqMinKHz = 245000.9,
            //            FreqMaxKHz = 356797.77,
            //            Deviation = 234.7F,
            //            StepKHz = 25,
            //            Time = DateTime.Now.ToLocalTime(),
            //            Modulation = 3,
            //            ImpulseDuration = 60,
            //            IsSelected = false,
            //            QuantitySignal = 88
            //        },
            //        TableNumASP = new TableNumASP
            //        {
            //            NumberASP = new ObservableCollection<string>{"*209", "210", "211"}
            //        }
            //    }
            //};
        }
    }

    public class GlobalReconFHSSCurRow
    {
        public ObservableCollection<TableReconFHSSAll> CollectionReconFHSSCurRow { get; set; }

        public GlobalReconFHSSCurRow()
        {
            CollectionReconFHSSCurRow = new ObservableCollection<TableReconFHSSAll> { };
        }
    }

    public class TableNumASP : INotifyPropertyChanged
    {
        public ObservableCollection<string> NumberASP { get; set; }

        private string selectedASPRP = string.Empty;

        public string SelectedASPRP
        {
            get
            {
                return selectedASPRP;
            }
            set
            {
                if (selectedASPRP == value) return;
                selectedASPRP = value;
                OnPropertyChanged("SelectedASPRP");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
    }

    public class GlobalFHSSReconExcluded
    {
        public ObservableCollection<TableFHSSReconExcluded> CollectionFHSSReconExcluded { get; set; }

        public GlobalFHSSReconExcluded()
        {
            CollectionFHSSReconExcluded = new ObservableCollection<TableFHSSReconExcluded> { };

            //CollectionFHSSReconExcluded = new ObservableCollection<TableFHSSReconExcluded>()
            //{
            //    new TableFHSSReconExcluded
            //    {
            //        Id = 1,
            //        IdFHSS = 1,
            //        Deviation = 300,
            //        FreqKHz = 36000.0F
            //    },
            //    new TableFHSSReconExcluded
            //    {
            //        Id = 2,
            //        IdFHSS = 1,
            //        Deviation = 700,
            //        FreqKHz = 79005.6F
            //    },
            //    new TableFHSSReconExcluded
            //    {
            //        Id = 3,
            //        IdFHSS = 2,
            //        Deviation = 250,
            //        FreqKHz = 111000.0F
            //    },
            //    new TableFHSSReconExcluded
            //    {
            //        Id = 4,
            //        IdFHSS = 2,
            //        Deviation = 700,
            //        FreqKHz = 152300.6F
            //    }
            //};
        }
    }

    public class GlobalSourceFHSS
    {
        public ObservableCollection<TableSourceFHSSRow> CollectionSourceFHSS { get; set; }

        public GlobalSourceFHSS()
        {
            CollectionSourceFHSS = new ObservableCollection<TableSourceFHSSRow> { };

            //CollectionSourceFHSS = new ObservableCollection<TableSourceFHSSRow>
            //{
            //    new TableSourceFHSSRow
            //    {
            //        Id = 1,
            //        IdFHSS = 2,
            //        Coordinates = new Coord
            //        {
            //            Latitude = 55.675566,
            //            Longitude = 27.565656,
            //            Altitude = 150
            //        },
            //        ListJamDirect = new ObservableCollection<TableJamDirect>
            //        {
            //            new TableJamDirect
            //            {
            //                JamDirect = new JamDirect
            //                {
            //                    NumberASP = 209,
            //                    Bearing = 150,
            //                    Level = -70,
            //                    Std = 100.9F,
            //                    DistanceKM = 2000,
            //                    IsOwn = true
            //                }
            //            },
            //            new TableJamDirect
            //            {
            //                JamDirect = new JamDirect
            //                {
            //                    NumberASP = 210,
            //                    Bearing = 50,
            //                    Level = -80,
            //                    Std = 200.9F,
            //                    DistanceKM = 1250,
            //                    IsOwn = false
            //                }
            //            },
            //            new TableJamDirect
            //            {
            //                JamDirect = new JamDirect
            //                {
            //                    NumberASP = 211,
            //                    Bearing = 40,
            //                    Level = -90,
            //                    Std = 30.5F,
            //                    DistanceKM = 30,
            //                    IsOwn = false
            //                }
            //            },
            //        }
            //    },
            //    new TableSourceFHSSRow
            //    {
            //        Id = 2,
            //        IdFHSS = 3,
            //        Coordinates = new Coord
            //        {
            //            Latitude = 55.675566,
            //            Longitude = 27.565656,
            //            Altitude = 150
            //        },
            //        ListJamDirect = new ObservableCollection<TableJamDirect>
            //        {
            //            new TableJamDirect
            //            {
            //                JamDirect = new JamDirect
            //                {
            //                    NumberASP = 210,
            //                    Bearing = 50,
            //                    Level = -80,
            //                    Std = 200.9F,
            //                    DistanceKM = 1250,
            //                    IsOwn = false
            //                }
            //            },
            //            new TableJamDirect
            //            {
            //                JamDirect = new JamDirect
            //                {
            //                    NumberASP = 211,
            //                    Bearing = 40,
            //                    Level = -90,
            //                    Std = 30.5F,
            //                    DistanceKM = 30,
            //                    IsOwn = false
            //                }
            //            }
            //        }
            //    }
            //};
        }
    }

    public class TableSourceFHSSRow : TableSourceFHSS, INotifyPropertyChanged
    {
        private TableJamDirect selectedJamDirect = new TableJamDirect { JamDirect = new JamDirect() { } };
        private int selectedASP = -2;

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }

        public int SelectedASP
        {
            get
            {
                if (selectedASP < 0 && ListJamDirect != null && ListJamDirect.Count != 0)
                    selectedASP = ListJamDirect.First().JamDirect.NumberASP;
                return selectedASP;
            }
            set
            {
                if (selectedASP == value) return;
                selectedASP = value;
                OnPropertyChanged();
            }
        }

        public TableJamDirect SelectedJamDirect
        {
            get
            {
                if (!CompareJams(selectedJamDirect, ListJamDirect?.FirstOrDefault(rec => rec.JamDirect.NumberASP == SelectedASP)))
                {
                    //SelectedJamDirect = ListJamDirect.FirstOrDefault(rec => rec.JamDirect.NumberASP == SelectedASP);
                    selectedJamDirect = ListJamDirect.FirstOrDefault(rec => rec.JamDirect.NumberASP == SelectedASP);
                }

                return selectedJamDirect;
            }
            set
            {
                if (value == null) return;
                if (selectedJamDirect == value) return;

                selectedJamDirect.JamDirect.Bearing = value.JamDirect.Bearing;
                selectedJamDirect.JamDirect.DistanceKM = value.JamDirect.DistanceKM;
                selectedJamDirect.JamDirect.IsOwn = value.JamDirect.IsOwn;
                selectedJamDirect.JamDirect.Level = value.JamDirect.Level;
                selectedJamDirect.JamDirect.NumberASP = value.JamDirect.NumberASP;
                selectedJamDirect.JamDirect.Std = value.JamDirect.Std;
                OnPropertyChanged(nameof(SelectedJamDirect));
            }
        }

        private bool CompareJams(TableJamDirect jam1, TableJamDirect jam2)
        {
            if (jam1 == null || jam2 == null)
                return true;
            if (jam1.JamDirect == null || jam2.JamDirect == null)
                return true;
            if (jam1.JamDirect.Bearing != jam2.JamDirect.Bearing ||
                jam1.JamDirect.DistanceKM != jam2.JamDirect.DistanceKM ||
                jam1.JamDirect.IsOwn != jam2.JamDirect.IsOwn ||
                jam1.JamDirect.Level != jam2.JamDirect.Level ||
                jam1.JamDirect.NumberASP != jam2.JamDirect.NumberASP ||
                jam1.JamDirect.Std != jam2.JamDirect.Std)
                return false;
            return true;
        }

        public TableSourceFHSSRow()
        {
            if (ListJamDirect != null && ListJamDirect.Count != 0)
                selectedJamDirect = ListJamDirect.First();
        }

        public TableSourceFHSSRow(TableSourceFHSS tableSourceFHSS)
        {
            Id = tableSourceFHSS.Id;
            IdFHSS = tableSourceFHSS.IdFHSS;
            Coordinates = tableSourceFHSS.Coordinates;
            ListJamDirect = tableSourceFHSS.ListJamDirect;
        }
    }
}
