﻿using ModelsTablesDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using TableEvents;
using TableOperations;
using ValuesCorrectLib;

namespace ReconFHSSControl
{
    /// <summary>
    /// Логика взаимодействия для UserControl1.xaml
    /// </summary>
    public partial class UserControlReconFHSS : UserControl
    {
        #region Events
        // Удалить запись из таблицы
        public event EventHandler<TableEvent> OnDeleteRecord = (object sender, TableEvent data) => { };
        // Удалить все записи в таблице
        public event EventHandler<NameTable> OnClearRecords = (object sender, NameTable data) => { };
        // Добавить запись в таблицу ИИ ППРЧ РП
        public event EventHandler<TableSuppressFHSS> OnAddFHSS_RS_Recon = (object sender, TableSuppressFHSS data) => { };
        public event EventHandler<List<TableFHSSExcludedFreq>> OnAddFHSS_RS_Excluded = (object sender, List <TableFHSSExcludedFreq> data) => { };

        public event EventHandler<TableEvent> OnAddRecord = (object srnder, TableEvent data) => { };

        // Изменить статус выделенной строки (поле IsSelected = true)
        public event EventHandler<TableEvent> OnSelectedRow = (object sender, TableEvent data) => { };

        public event EventHandler<TableEventReport> OnAddTableToReport = (object sender, TableEventReport data) => { };
        #endregion

        #region Properties
        public NameTable NameTableReconFHSS { get; } = NameTable.TableReconFHSS;
        public NameTable NameTableSourceFHSS { get; } = NameTable.TableSourceFHSS;
        public NameTable NameTableFHSSReconExcluded { get; } = NameTable.TableFHSSReconExcluded;
        #endregion


        public UserControlReconFHSS()
        {
            InitializeComponent();

           
         
            dgvReconFHSS.DataContext = new GlobalReconFHSS();

            dgvReconFHSSCurRow.DataContext = new GlobalReconFHSSCurRow();

            dgvSourceFHSS.DataContext = new GlobalSourceFHSS();

            dgvFHSSReconExcluded.DataContext = new GlobalFHSSReconExcluded();
        }

        private void ButtonDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (((GlobalReconFHSSCurRow)dgvReconFHSSCurRow.DataContext).CollectionReconFHSSCurRow != null)
                {
                    if (!IsSelectedRowEmpty())
                        return;

                    TableReconFHSS reconFHSS = new TableReconFHSS 
                    {
                        Id = ((GlobalReconFHSSCurRow)dgvReconFHSSCurRow.DataContext).CollectionReconFHSSCurRow[0].TableReconFHSS.Id
                    };

                    // Событие удаления одной записи
                    OnDeleteRecord(this, new TableEvent(reconFHSS));
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        private void ButtonClear_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // Событие удаления всех записей
                OnClearRecords(this, NameTableReconFHSS);
            }
            catch { }
            //catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        private void ButtonWord_Click(object sender, RoutedEventArgs e)
        {
            string[,] Table = AddReconFHSSToTable();

            OnAddTableToReport(this, new TableEventReport(Table, NameTable.TableReconFHSS));
        }

        private void ButtonPrint_Click(object sender, RoutedEventArgs e)
        {

        }

        private void dgvReconFHSSCurRow_SelectedCellsChanged(object sender, SelectedCellsChangedEventArgs e)
        {
            try
            {
                // Отменяет выделение всех ячеек в элементе управления
                dgvReconFHSSCurRow.UnselectAllCells();
            }
            catch (Exception ex)
            { MessageBox.Show(ex.Message); }
        }

        private void dgvReconFHSSCurRow_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            AddEmptyRows(dgvReconFHSSCurRow);
        }

        private void DgvReconFHSS_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            AddEmptyRows(dgvReconFHSS);
        }

        private void dgvFHSSReconExcluded_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            AddEmptyRowsFHSSReconExcluded();
        }

        private void dgvReconFHSSCurRow_LayoutUpdated(object sender, EventArgs e)
        {
            for (int i = 0; i < dgvReconFHSS.Columns.Count && i < dgvReconFHSSCurRow.Columns.Count; ++i)
                dgvReconFHSS.Columns[i].Width = dgvReconFHSSCurRow.Columns[i].ActualWidth;
        }

        private void dgvSourceFHSS_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            AddEmptyRowsSource();
        }

        private void ButtonAddFHSS_RS_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (((GlobalReconFHSSCurRow)dgvReconFHSSCurRow.DataContext).CollectionReconFHSSCurRow != null)
                {
                    if (!IsSelectedRowEmpty())
                        return;

                    TableSuppressFHSS tableSuppressFHSS = new TableSuppressFHSS
                    {
                        Id = 0,
                        NumberASP = int.Parse(string.Join(string.Empty, (((GlobalReconFHSSCurRow)dgvReconFHSSCurRow.DataContext).CollectionReconFHSSCurRow[0].TableNumASP.SelectedASPRP).Where(c => char.IsDigit(c)))),

                        FreqMinKHz = ((GlobalReconFHSSCurRow)dgvReconFHSSCurRow.DataContext).CollectionReconFHSSCurRow[0].TableReconFHSS.FreqMinKHz,
                        FreqMaxKHz = ((GlobalReconFHSSCurRow)dgvReconFHSSCurRow.DataContext).CollectionReconFHSSCurRow[0].TableReconFHSS.FreqMaxKHz,
                        StepKHz = ((GlobalReconFHSSCurRow)dgvReconFHSSCurRow.DataContext).CollectionReconFHSSCurRow[0].TableReconFHSS.StepKHz,
                        Letters = DefinitionParams.DefineLetters(((GlobalReconFHSSCurRow)dgvReconFHSSCurRow.DataContext).CollectionReconFHSSCurRow[0].TableReconFHSS.FreqMinKHz, ((GlobalReconFHSSCurRow)dgvReconFHSSCurRow.DataContext).CollectionReconFHSSCurRow[0].TableReconFHSS.FreqMaxKHz),

                        Threshold = ConstValues.constThreshold,

                        EPO = DefinitionParams.DefineEPO(((GlobalReconFHSSCurRow)dgvReconFHSSCurRow.DataContext).CollectionReconFHSSCurRow[0].TableReconFHSS.FreqMinKHz, ((GlobalReconFHSSCurRow)dgvReconFHSSCurRow.DataContext).CollectionReconFHSSCurRow[0].TableReconFHSS.FreqMaxKHz),

                        InterferenceParam = new InterferenceParam
                        {
                            Manipulation = 0,
                            Modulation = 1,
                            Deviation = 8,
                            Duration = 1
                        },
                    };

                    List<TableFHSSExcludedFreq> tableFHSSExcludedFreq = new List<TableFHSSExcludedFreq>
                    {

                    };
                    // Событие Отправить ИРИ на РП
                    OnAddFHSS_RS_Recon(this, tableSuppressFHSS);
                }
            }
            catch { }
        }

        private void dgvReconFHSS_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if ((TableReconFHSSAll)dgvReconFHSS.SelectedItem == null) { return; }

            if (PropSelectedIdFHSS.SelectedIdReconFHSS != ((TableReconFHSSAll)dgvReconFHSS.SelectedItem).TableReconFHSS.Id)
            {
                PropSelectedIdFHSS.SelectedIdReconFHSS = ((TableReconFHSSAll)dgvReconFHSS.SelectedItem).TableReconFHSS.Id;

                if (PropSelectedIdFHSS.SelectedIdReconFHSS != -2)
                {
                    if (((TableReconFHSSAll)dgvReconFHSS.SelectedItem).TableNumASP.SelectedASPRP == string.Empty)
                    {
                       if (((TableReconFHSSAll)dgvReconFHSS.SelectedItem).TableNumASP.NumberASP != null)
                            ((TableReconFHSSAll)dgvReconFHSS.SelectedItem).TableNumASP.SelectedASPRP = ((TableReconFHSSAll)dgvReconFHSS.SelectedItem).TableNumASP.NumberASP[0];
                    }
                        
                    TableReconFHSS reconFHSS = SelectedRowReconForSend();

                    OnSelectedRow(this, new TableEvent(reconFHSS));

                    UpdateSourceFHSS(((TableReconFHSSAll)dgvReconFHSS.SelectedItem).TableReconFHSS.Id);
                    UpdateTableFHSSReconExcluded(((TableReconFHSSAll)dgvReconFHSS.SelectedItem).TableReconFHSS.Id);

                    
                    dgvSourceFHSS.SelectedIndex = 0;

                    //if (dgvSourceFHSS.SelectedItem != null)
                    //{
                    //}

                    dgvReconFHSS.UnselectAllCells();
                }
            }
        }

        private void dgvSourceFHSS_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            
            //dgvSourceFHSS.UnselectAllCells();
        }

        private void dgvFHSSReconExcluded_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            dgvFHSSReconExcluded.UnselectAllCells();
        }

        private void dgvSourceFHSS_SelectedCellsChanged(object sender, SelectedCellsChangedEventArgs e)
        {
            try
            {
                if (dgvSourceFHSS.SelectedItem != null)
                {
                    ((TableSourceFHSSRow)dgvSourceFHSS.SelectedItem).SelectedASP = PropNumberASP.SelectedNumASPFHSS;
                    ((TableSourceFHSSRow)dgvSourceFHSS.SelectedItem).SelectedJamDirect = ((TableSourceFHSSRow)dgvSourceFHSS.SelectedItem).ListJamDirect.FirstOrDefault(rec => rec.JamDirect.NumberASP == PropNumberASP.SelectedNumASPFHSS);
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        private void cbAddrASP_RP_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var dgvRowIndex = dgvReconFHSS.SelectedItem;
            try
            {
                if (dgvRowIndex != null)
                {
                    ComboBox cellComboBox = sender as ComboBox;
                    string strNumberASP = Convert.ToString(cellComboBox.SelectedValue);

                    ((TableReconFHSSAll)dgvReconFHSS.SelectedItem).TableNumASP.SelectedASPRP = strNumberASP;

                   
                } 
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        private void cbAddrASP_RR_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var dgvRowIndex = dgvSourceFHSS.SelectedItem;
            try
            {
                if (dgvRowIndex != null)
                {
                    ComboBox cellComboBox = sender as ComboBox;
                    string strNumberASP = Convert.ToString(cellComboBox.SelectedValue);
                    if (strNumberASP.Substring(0, 1) == "*")
                    {
                        strNumberASP = strNumberASP.Remove(0, 1);
                    }

                    int iNumberASP = Convert.ToInt32(strNumberASP);

                    PropNumberASP.SelectedNumASPFHSS = iNumberASP;
                    ((TableSourceFHSSRow)dgvSourceFHSS.SelectedItem).SelectedASP = iNumberASP;
                    ((TableSourceFHSSRow)dgvSourceFHSS.SelectedItem).SelectedJamDirect = ((TableSourceFHSSRow)dgvSourceFHSS.SelectedItem).ListJamDirect.FirstOrDefault(rec => rec.JamDirect.NumberASP == iNumberASP);
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        private void grid_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            try
            {
                if (e.Key == System.Windows.Input.Key.F)
                    ButtonAddFHSS_RS_Click(this, null);
                
            }
            catch { }
        }
    }
}
