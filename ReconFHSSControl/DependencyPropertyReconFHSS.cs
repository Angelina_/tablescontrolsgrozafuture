﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using TableEvents;

namespace ReconFHSSControl
{
    public partial class UserControlReconFHSS : UserControl, INotifyPropertyChanged
    {
        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }
        #endregion

        #region FormatViewCoord

        public static readonly DependencyProperty ViewCoordProperty = DependencyProperty.Register("ViewCoord", typeof(byte), typeof(UserControlReconFHSS),
                                                                       new PropertyMetadata((byte)1, new PropertyChangedCallback(ViewCoordChanged)));

        public byte ViewCoord
        {
            get { return (byte)GetValue(ViewCoordProperty); }
            set { SetValue(ViewCoordProperty, value); }
        }

        private static void ViewCoordChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                UserControlReconFHSS userControlReconFHSS = (UserControlReconFHSS)d;
                userControlReconFHSS.UpdateFormatCoords();
            }
            catch
            { }

        }
        #endregion

        /// <summary>
        /// Обновить вид координат в таблице
        /// </summary>
        private void UpdateFormatCoords()
        {
            try
            {
                if (TempListSourceFHSS == null)
                {
                    return;
                }

                PropViewCoords.ViewCoords = ViewCoord;

                UpdateSourceFHSS(TempListSourceFHSS);
            }
            catch { }
        }
    }
}
