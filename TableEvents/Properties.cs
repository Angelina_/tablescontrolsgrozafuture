﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TableEvents
{
    public class PropLetter
    {
        #region Properties Letters (ToggleButton SuppressFWS)
        public static bool CheckIridium
        {
            get { return сheckIridium; }
            set
            {
                if (сheckIridium != value)
                    сheckIridium = value;
            }
        }
        private static bool сheckIridium = false;

        public static bool CheckInmarsat
        {
            get { return сheckInmarsat; }
            set
            {
                if (сheckInmarsat != value)
                    сheckInmarsat = value;
            }
        }
        private static bool сheckInmarsat = false;

        public static bool Check2_4
        {
            get { return check2_4; }
            set
            {
                if (check2_4 != value)
                    check2_4 = value;
            }
        }
        private static bool check2_4 = false;

        public static bool Check5_8
        {
            get { return check5_8; }
            set
            {
                if (check5_8 != value)
                    check5_8 = value;
            }
        }
        private static bool check5_8 = false;

        public static bool CheckGlobalStar
        {
            get { return checkGlobalStar; }
            set
            {
                if (checkGlobalStar != value)
                    checkGlobalStar = value;
            }
        }
        private static bool checkGlobalStar = false;

        public static bool Check433
        {
            get { return check433; }
            set
            {
                if (check433 != value)
                    check433 = value;
            }
        }
        private static bool check433 = false;

        public static bool Check868
        {
            get { return check868; }
            set
            {
                if (check868 != value)
                    check868 = value;
            }
        }
        private static bool check868 = false;

        public static bool Check920
        {
            get { return check920; }
            set
            {
                if (check920 != value)
                    check920 = value;
            }
        }
        private static bool check920 = false;

        public static bool CheckNAV
        {
            get { return checkNAV; }
            set
            {
                if (checkNAV != value)
                    checkNAV = value;
            }
        }
        private static bool checkNAV = false;

        public static bool CheckBPLA
        {
            get { return checkBPLA; }
            set
            {
                if (checkBPLA != value)
                    checkBPLA = value;
            }
        }
        private static bool checkBPLA = false;
        #endregion
    }

    public class PropNumberASP
    {
        //private static int selectedNumASP = 2;// для отладки
        private static int selectedNumASP = 0;
        public static int SelectedNumASP
        {
            get { return selectedNumASP; }
            set
            {
                if (selectedNumASP == value)
                    return;

                selectedNumASP = value;
            }
        }

        private static int selectedASPforListQ = 0;
        public static int SelectedASPforListQ
        {
            get { return selectedASPforListQ; }
            set
            {
                if (selectedASPforListQ == value)
                    return;

                selectedASPforListQ = value;
            }
        }

        private static int selectedNumASPFHSS = -2;
        public static int SelectedNumASPFHSS
        {
            get { return selectedNumASPFHSS; }
            set
            {
                if (selectedNumASPFHSS == value)
                    return;

                selectedNumASPFHSS = value;
            }
        }

        //private static bool isSelectedRowASP = true; // для отладки
        private static bool isSelectedRowASP = false;
        public static bool IsSelectedRowASP
        {
            get { return isSelectedRowASP; }
            set
            {
                if (isSelectedRowASP == value)
                    return;

                isSelectedRowASP = value;
            }
        }

        private static object[] comboBoxASPSuppr = null;
        public static object[] ComboBoxASPSuppr
        {
            get { return comboBoxASPSuppr; }
            set
            {
                if (comboBoxASPSuppr == value)
                    return;

                comboBoxASPSuppr = value;
            }
        }

    }

    public class PropShowDialog
    {
        private static bool isShowDialogOpen = false;
        public static bool IsShowDialogOpen
        {
            get { return isShowDialogOpen; }
            set
            {
                if (isShowDialogOpen == value)
                    return;

                isShowDialogOpen = value;
            }
        }
    }

    public class PropSelectedIdTempFWS
    {
        private static int selectedIdTempFWS = 0;
        public static int SelectedIdTempFWS
        {
            get { return selectedIdTempFWS; }
            set
            {
                if (selectedIdTempFWS == value)
                    return;

                selectedIdTempFWS = value;
            }
        }
    }

    public class PropChangeJammerParams
    {
        private static bool isChangeJammerParams = false;
        public static bool IsChangeJammerParams
        {
            get { return isChangeJammerParams; }
            set
            {
                if (isChangeJammerParams == value)
                    return;

                isChangeJammerParams = value;
            }
        }
    }

    public class PropSelectedIdFHSS
    {
        private static int selectedIdSupprFHSS = 0;
        public static int SelectedIdSupprFHSS
        {
            get { return selectedIdSupprFHSS; }
            set
            {
                if (selectedIdSupprFHSS == value)
                    return;

                selectedIdSupprFHSS = value;
            }
        }

        private static int selectedIdReconFHSS = 0;
        public static int SelectedIdReconFHSS
        {
            get { return selectedIdReconFHSS; }
            set
            {
                if (selectedIdReconFHSS == value)
                    return;

                selectedIdReconFHSS = value;
            }
        }
    }

    public class PropRS_US
    {
        public static bool CheckRS
        {
            get { return сheckRS; }
            set
            {
                if (сheckRS != value)
                    сheckRS = value;
            }
        }
        private static bool сheckRS = false;

        public static bool CheckUS
        {
            get { return сheckUS; }
            set
            {
                if (сheckUS != value)
                    сheckUS = value;
            }
        }
        private static bool сheckUS = false;
    }

    public class PropNumSuppressFWS
    {
        private static int selectedNumSuppressFWS = 0;
        public static int SelectedNumSuppressFWS
        {
            get { return selectedNumSuppressFWS; }
            set
            {
                if (selectedNumSuppressFWS == value)
                    return;

                selectedNumSuppressFWS = value;
            }
        }

        private static int selectedIdSuppressFWS = 0;
        public static int SelectedIdSuppressFWS
        {
            get { return selectedIdSuppressFWS; }
            set
            {
                if (selectedIdSuppressFWS == value)
                    return;

                selectedIdSuppressFWS = value;
            }
        }
    }

    public class PropGlobalProperty
    {
        private static byte rangeRadioRecon = 0;
        public static byte RangeRadioRecon
        {
            get { return rangeRadioRecon; }
            set
            {
                if (rangeRadioRecon == value)
                    return;

                rangeRadioRecon = value;
            }
        }

        private static byte rangeJamming = 7;
        public static byte RangeJamming
        {
            get { return rangeJamming; }
            set
            {
                if (rangeJamming == value)
                    return;

                rangeJamming = value;
            }
        }

        private static byte amplifiers = 0;
        public static byte Amplifiers
        {
            get { return amplifiers; }
            set
            {
                if (amplifiers == value)
                    return;

                amplifiers = value;
            }
        }
    }

    public class PropNameTable
    {
        private static ModelsTablesDBLib.NameTable sRangesName;
        public static ModelsTablesDBLib.NameTable SRangesName
        {
            get { return sRangesName; }
            set
            {
                if (sRangesName == value)
                    return;

                sRangesName = value;
            }
        }

        private static ModelsTablesDBLib.NameTable specFreqsName;
        public static ModelsTablesDBLib.NameTable SpecFreqsName
        {
            get { return specFreqsName; }
            set
            {
                if (specFreqsName == value)
                    return;

                specFreqsName = value;
            }
        }
    }

    public class PropLocalCRRD
    {
        private static bool crrd1;
        public static bool CRRD1
        {
            get { return crrd1; }
            set
            {
                if (crrd1 == value)
                    return;

                crrd1 = value;
            }
        }

        private static bool crrd2;
        public static bool CRRD2
        {
            get { return crrd2; }
            set
            {
                if (crrd2 == value)
                    return;

                crrd2 = value;
            }
        }
    }

    /// <summary>
    /// Отображение названий F, MHz / F, kHz
    /// </summary>
    public class PropLocalFreqMHz_kHz
    {
        private static byte freqMHz_kHz = 0;
        public static byte FreqMHz_kHz
        {
            get { return freqMHz_kHz; }
            set
            {
                if (freqMHz_kHz == value)
                    return;

                freqMHz_kHz = value;
            }
        }
    }

    public class PropViewCoords
    {
        private static byte viewCoords = 1;

        public static byte ViewCoords
        {
            get { return viewCoords; }
            set
            {
                if (viewCoords == value)
                    return;

                viewCoords = value;
            }
        }
    }

    public class PropIsRecASP
    {
        private static bool isRecAdd = false;
        public static bool IsRecAdd
        {
            get { return isRecAdd; }
            set
            {
                if (isRecAdd == value)
                    return;

                isRecAdd = value;
            }
        }

        private static bool isRecChange = false;
        public static bool IsRecChange
        {
            get { return isRecChange; }
            set
            {
                if (isRecChange == value)
                    return;

                isRecChange = value;
            }
        }
    }

  
}
