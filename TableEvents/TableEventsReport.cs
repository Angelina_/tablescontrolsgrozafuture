﻿using ModelsTablesDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TableEvents
{
    public class TableEventReport : EventArgs
    {
        public string[,] Table { get; }

        public NameTable NameTable { get; private set; }

        public TableEventReport(string[,] table, NameTable nameTable)
        {
            Table = table;
            NameTable = nameTable;
           
        }
    }
}
