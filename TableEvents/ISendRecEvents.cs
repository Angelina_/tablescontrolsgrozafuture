﻿using ModelsTablesDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TableEvents
{
    public interface ISendRecEvents
    {
        event EventHandler<TableSuppressFWS> OnAddFWS_RS;
        event EventHandler<TableReconFWS> OnAddFWS_TD;
        event EventHandler<TempFWS> OnSendFreqCRRD;
        event EventHandler<TempFWS> OnGetExecBear;
    }
}
