﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TableEvents
{
    public enum LatitudeSign : byte
    {
        N,
        S
    }

    public enum LongitudeSign : byte
    {
        E,
        W
    }
}
