﻿using ModelsTablesDBLib;
using System;

namespace TableEvents
{
    public interface ITempFWSEvents
    {
        event EventHandler<TableEvent> OnDeleteRecord;
        event EventHandler<NameTable> OnClearRecords;

        event EventHandler<TableEvent> OnSelectedRow;
    }
}
