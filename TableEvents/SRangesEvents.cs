﻿using ModelsTablesDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TableEvents
{
    //public class SRangesArgs<T> where T : TableSectorsRanges
    //{
    //    public NameTable Name { get; private set; }

    //    public List<T> SRanges { get; private set; }

    //    public SRangesArgs(NameTable nameTable, List<TableSectorsRanges> ranges)
    //    {
    //        Name = nameTable;

    //        SRanges = (from t in ranges let c = t as T select c).ToList();
    //    }
    //}

    public class SRangesArgs
    {
        public NameTable Name { get; private set; }

        public object Ranges { get; private set; }

        public SRangesArgs(NameTable nameTable, List<TableSectorsRanges> ranges)
        {
            Name = nameTable;

            switch (Name)
            {
                case NameTable.TableSectorsRangesRecon:
                    Ranges = (from t in ranges let c = t.ToRangesRecon() select c).ToList(); break;
                case NameTable.TableSectorsRangesSuppr:
                    Ranges = (from t in ranges let c = t.ToRangesSuppr() select c).ToList(); break;
            }
        }
    }
}
