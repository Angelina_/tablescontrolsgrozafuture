﻿using ModelsTablesDBLib;
using System;

namespace TableEvents
{
    public interface ITableReconEvent
    {
        event EventHandler<TableEvent> OnDeleteRecord;
        event EventHandler<NameTable> OnClearRecords;
    }
}
