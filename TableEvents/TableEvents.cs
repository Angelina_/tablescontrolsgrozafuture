﻿using ModelsTablesDBLib;
using System;

namespace TableEvents
{
    public class TableEvent : EventArgs 
    {
        public AbstractCommonTable Record  { get; }

        public NameTable NameTable { get; private set; }

        public TableEvent(AbstractCommonTable rec)
        {
            Record = rec;
            object[] attrs = rec.GetType().GetCustomAttributes(typeof(InfoTableAttribute), false);
            foreach (InfoTableAttribute info in attrs)
            {
                NameTable = info.Name;
            }
        }
    }
   
}
