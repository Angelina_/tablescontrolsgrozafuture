﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace RadarRodnikControl
{
    public class GlobalRodnik : INotifyPropertyChanged
    {
        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }
        #endregion

        public ObservableCollection<RodnikModel> CollectionRodnik { get; set; }

        public GlobalRodnik()
        {
            CollectionRodnik = new ObservableCollection<RodnikModel> { };

            //CollectionRodnik = new ObservableCollection<RodnikModel>
            //{
            //    new RodnikModel
            //    {
            //        IsCheck = false,
            //        Id = 1,
            //        Azimuth = 35.83F,
            //        Range = 125.4F,
            //        Class = "AAA",
            //        Velocity = 500.3F,
            //        Aspect = 24.7F,
            //        Time = DateTime.Now,
            //        Mode = "BBB",
            //        Flag = "CCC"
            //    },
            //    new RodnikModel
            //    {
            //        IsCheck = true,
            //        Id = 2,
            //        Azimuth = 44.43F,
            //        Range = 58.8F,
            //        Class = "MMM",
            //        Velocity = 456.3F,
            //        Aspect = 33.33F,
            //        Time = DateTime.Now,
            //        Mode = "VVV",
            //        Flag = "XXX"
            //    },
            //    new RodnikModel
            //    {
            //        IsCheck = false,
            //        Id = 3,
            //        Azimuth = 54.43F,
            //        Range = 55.8F,
            //        Class = "YYY",
            //        Velocity = 222.2F,
            //        Aspect = 22.23F,
            //        Time = DateTime.Now,
            //        Mode = "PPP",
            //        Flag = "RRR"
            //    }
            //};

        }
    }
}
