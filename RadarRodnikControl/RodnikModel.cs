﻿using System;

namespace RadarRodnikControl
{
    public class RodnikModel
    {
        public bool IsCheck { get; set; }
        public short Id { get; set; }
        public float Azimuth { get; set; }
        public float Range { get; set; }
        public string Class { get; set; }
        public float Velocity { get; set; }
        public float Aspect { get; set; }
        public DateTime Time { get; set; }
        public string Mode { get; set; } = String.Empty;
        public string Flag { get; set; } = String.Empty;
        public Coords Coordinates { get; set; } = new Coords();

    }

    public class Coords
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
}
