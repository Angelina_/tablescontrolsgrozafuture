﻿namespace RadarRodnikControl
{
    public enum Language : byte
    {
        RU = 0,
        EN = 1,
        AZ = 2
    }

}
