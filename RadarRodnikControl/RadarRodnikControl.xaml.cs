﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace RadarRodnikControl
{
    /// <summary>
    /// Логика взаимодействия для UserControl1.xaml
    /// </summary>
    public partial class UserControlRadarRodnik : UserControl
    {
        public event EventHandler<RodnikModel> OnDeleteRecord = (object sender, RodnikModel data) => { };
        public event EventHandler<RodnikModel> OnChangeRecord = (object sender, RodnikModel data) => { };
        public event EventHandler OnClearRecords;
        public event EventHandler<RodnikModel> OnDoubleClick = (object sender, RodnikModel data) => { };
        public event EventHandler<bool> OnCheckAll = (object sender,bool data) => { };

        public UserControlRadarRodnik()
        {
            InitializeComponent();

            DgvRodnik.DataContext = new GlobalRodnik();
        }

        private void ButtonDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if ((RodnikModel)DgvRodnik.SelectedItem != null)
                {
                    if (!IsSelectedRowEmpty())
                        return;

                    RodnikModel radarRodnikModel = new RodnikModel
                    {
                        Id = ((RodnikModel)DgvRodnik.SelectedItem).Id
                    };

                    // Событие удаления одной записи
                    this.OnDeleteRecord(this, radarRodnikModel);
                }
            }
            catch { }
        }

        private void ButtonClear_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // Событие удаления записей
                this.OnClearRecords(this, null);
            }
            catch { }
        }

        private void DgvRadarRodnik_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            this.AddEmptyRows();
        }

        private void DgvRodnik_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            try
            {
                if ((RodnikModel)DgvRodnik.SelectedItem != null)
                {
                    if (!this.IsSelectedRowEmpty())
                        return;

                    // Событие Центрирование карты по выбранному источнику
                   this.OnDoubleClick(this, (RodnikModel)DgvRodnik.SelectedItem);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }
        }

        private void DgvRodnik_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if ((RodnikModel)DgvRodnik.SelectedItem == null) return;

            if (((RodnikModel)DgvRodnik.SelectedItem).Id > -1)
            {
                if (((RodnikModel)DgvRodnik.SelectedItem).Id != PropSelectedIdRodnik.IdRodnik)
                {
                    PropSelectedIdRodnik.IdRodnik = ((RodnikModel)DgvRodnik.SelectedItem).Id;
                }
            }
            else
            {
                PropSelectedIdRodnik.IdRodnik = 0;
            }
        }

        private void CheckBox_Click(object sender, RoutedEventArgs e)
        {
            try
            {
               if (((RodnikModel)DgvRodnik.SelectedItem).Id > 0)
                {
                    RodnikModel model = new RodnikModel();
                    model.Id = ((RodnikModel)DgvRodnik.SelectedItem).Id;
                    model.Azimuth = ((RodnikModel)DgvRodnik.SelectedItem).Azimuth;
                    model.Aspect = ((RodnikModel)DgvRodnik.SelectedItem).Aspect;
                    model.Class = ((RodnikModel)DgvRodnik.SelectedItem).Class;
                    model.Flag = ((RodnikModel)DgvRodnik.SelectedItem).Flag;
                    model.Mode = ((RodnikModel)DgvRodnik.SelectedItem).Mode;
                    model.Range = ((RodnikModel)DgvRodnik.SelectedItem).Range;
                    model.Time = ((RodnikModel)DgvRodnik.SelectedItem).Time;
                    model.Velocity = ((RodnikModel)DgvRodnik.SelectedItem).Velocity;

                    if (((RodnikModel)DgvRodnik.SelectedItem).IsCheck)
                    {
                        model.IsCheck = false;
                    }
                    else
                    {
                        model.IsCheck = true;
                    }

                    // Событие изменения одной записи
                    OnChangeRecord(this, model);
                }
                else
                {
                    CheckBox chbIsChecked = sender as CheckBox;
                    chbIsChecked.IsChecked = false;
                }
            }
            catch { }
        }

        private void bCheckAll_Click(object sender, RoutedEventArgs e)
        {
            if (tbCheckAll.IsChecked.Value)
            {
                tbCheckAll.IsChecked = false;
                this.OnCheckAll(this, true);
            }
            else
            {
                tbCheckAll.IsChecked = true;
                this.OnCheckAll(this, false);
            }
        }
    }
}
