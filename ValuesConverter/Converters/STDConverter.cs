﻿using ModelsTablesDBLib;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace ValuesConverter
{
    [ValueConversion(sourceType: typeof(float), targetType: typeof(string))]
    public class STDConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            float fSTD = -1F;
            try
            {
                if (value is JamDirect)
                {
                    fSTD = System.Convert.ToSingle(((JamDirect)value).Std);
                }

                if (value is TableJamDirect)
                {
                    fSTD = System.Convert.ToSingle(((TableJamDirect)value).JamDirect.Std);
                }

                if (fSTD == -1F)
                {
                    return "-";
                }
                if (fSTD == -2F)
                {
                    return string.Empty;
                }
            }

            catch { }

            return System.Convert.ToString(fSTD.ToString("0.0"), culture);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
