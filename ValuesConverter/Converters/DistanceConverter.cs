﻿using ModelsTablesDBLib;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Windows.Data;
using System.Linq;

namespace ValuesConverter
{
    //[ValueConversion(sourceType: typeof(List<JamDirect>), targetType: typeof(string[]))]
    [ValueConversion(sourceType: typeof(double), targetType: typeof(string))]
    public class DistanceConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            double dDistance = -1F;

            try
            {
                if (value is JamDirect)
                {
                    dDistance = System.Convert.ToDouble(((JamDirect)value).DistanceKM);
                }

                if (value is TableJamDirect)
                {
                    dDistance = System.Convert.ToDouble(((TableJamDirect)value).JamDirect.DistanceKM);
                }

                if (dDistance == -1F)
                {
                    return "-";
                }
                if (dDistance == -2F)
                {
                    return string.Empty;
                }
            }

            catch { }

            return System.Convert.ToString(dDistance.ToString("0.0"), culture);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
