﻿using ModelsTablesDBLib;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Windows.Data;
using System.Linq;
using TableEvents;

namespace ValuesConverter
{
    [ValueConversion(sourceType: typeof(ObservableCollection<JamDirect>), targetType: typeof(string[]))]
    [ValueConversion(sourceType: typeof(ObservableCollection<TableJamDirect>), targetType: typeof(string[]))]
    public class NumberASPConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string[] sNumberASP = new string[0];

            try
            {
                if (value is ObservableCollection<JamDirect>)
                {
                    //sNumberASP = new string[((ObservableCollection<JamDirect>)value).Count];

                    if (((ObservableCollection<JamDirect>)value).Count != 0)
                    {
                        sNumberASP = new string[((ObservableCollection<JamDirect>)value).Count];

                        for (int i = 0; i < ((ObservableCollection<JamDirect>)value).Count; i++)
                        {
                            if (((ObservableCollection<JamDirect>)value)[i].NumberASP == -2)
                            {
                                return string.Empty;
                            }

                            string NumberASP = System.Convert.ToString(((ObservableCollection<JamDirect>)value)[i].NumberASP);

                            if (((ObservableCollection<JamDirect>)value)[i].IsOwn)
                            {
                                sNumberASP[i] = "*".ToString(culture) + NumberASP;
                            }
                            else
                            {
                                sNumberASP[i] = NumberASP;
                            }
                        }
                    }
                }

                if (value is ObservableCollection<TableJamDirect>)
                {
                    if (((ObservableCollection<TableJamDirect>)value).Count != 0)
                    {
                        sNumberASP = new string[((ObservableCollection<TableJamDirect>)value).Count];

                        for (int i = 0; i < ((ObservableCollection<TableJamDirect>)value).Count; i++)
                        {
                            if (((ObservableCollection<TableJamDirect>)value)[i].JamDirect.NumberASP == -2)
                            {
                                return string.Empty;
                            }

                            string NumberASP = System.Convert.ToString(((ObservableCollection<TableJamDirect>)value)[i].JamDirect.NumberASP);

                            if (((ObservableCollection<TableJamDirect>)value)[i].JamDirect.IsOwn)
                            {
                                sNumberASP[i] = "*".ToString(culture) + NumberASP;
                            }
                            else
                            {
                                sNumberASP[i] = NumberASP;
                            }
                        }
                    }
                }
            }

            catch { }

            return sNumberASP;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
            //throw new NotImplementedException();
        }
    }

    //[ValueConversion(sourceType: typeof(ObservableCollection<JamDirect>), targetType: typeof(string))]
    public class NumberASPCurRowConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string sNumberASP = string.Empty;

            //try
            //{

            if (((JamDirect)value).NumberASP > 0)
            {
                if (((JamDirect)value).IsOwn)
                {
                    sNumberASP = "*".ToString(culture) + ((JamDirect)value).NumberASP.ToString();

                    return sNumberASP;
                }
                else
                {
                    sNumberASP = ((JamDirect)value).NumberASP.ToString();

                    return sNumberASP;
                }
            }

            //catch { }

            return sNumberASP;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
            //throw new NotImplementedException();
        }
    }

    [ValueConversion(sourceType: typeof(int), targetType: typeof(string))]
    public class IdASPConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //try
            //{
            if (System.Convert.ToInt32(value) == -1)
            {
                return "-";
            }
            if (System.Convert.ToInt32(value) == -2)
            {
                return string.Empty;
            }
            //}
            //catch { }

            return System.Convert.ToString(value);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
            //throw new NotImplementedException();
        }
    }

    public class IsOwnIdASPConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            string strASP = System.Convert.ToInt32(values[0]).ToString();
            //try
            //{
            if (System.Convert.ToInt32(values[0]) == -1)
            {
                return "-";
            }
            if (System.Convert.ToInt32(values[0]) == -2)
            {
                return string.Empty;
            }
            if (System.Convert.ToBoolean(values[1]))
            {
                strASP = "*" + strASP;
            }
        //}
        //    catch { }

            return strASP;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            return (object[])value;
            //throw new NotImplementedException();
        }

        public static string NumASP(int id, bool IsOwn)
        {
            string strASP = id.ToString();
            
            if (IsOwn)
            {
                strASP = "*" + strASP;
            }
         
            return strASP;
        }
    }

    public class IndexASPRPConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            int index = -1;

            List<int> temp = new List<int>();

            if ((values[0] as ObservableCollection<string>) != null)
            {
                if ((values[0] as ObservableCollection<string>).Count > 0)
                {
                    for (int i = 0; i < (values[0] as ObservableCollection<string>).Count; i++)
                    {
                        if ((values[0] as ObservableCollection<string>)[i] == " ")
                            temp.Add(-2);
                        else
                            temp.Add(int.Parse(string.Join(string.Empty, ((values[0] as ObservableCollection<string>)[i]).Where(c => char.IsDigit(c)))));
                    }
                    index = temp.FindIndex(x => x == (int)values[1]);
                }
            }

            return index;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            try
            {
                object[] ob = new object[2];
                ob[1] = value;
                //ob[0] = PropNumberASP.ComboBoxASPSuppr;

                //List<object> temp = new List<object>();
                //if (PropNumberASP.ComboBoxASPSuppr.Length > 0)
                //{
                //    for (int i = 0; i < PropNumberASP.ComboBoxASPSuppr.Length; i++)
                //    {
                //        if (PropNumberASP.ComboBoxASPSuppr[i].ToString() == string.Empty)
                //            temp.Add(-2);
                //        else
                //            temp.Add(int.Parse(string.Join(string.Empty, (PropNumberASP.ComboBoxASPSuppr[i].ToString()).Where(c => char.IsDigit(c)))));
                //    }
                //}
                //ob[0] = temp;

                ObservableCollection<string> temp = new ObservableCollection<string>();
                if (PropNumberASP.ComboBoxASPSuppr.Length > 0)
                {
                    for (int i = 0; i < PropNumberASP.ComboBoxASPSuppr.Length; i++)
                    {
                        temp.Add(PropNumberASP.ComboBoxASPSuppr[i].ToString());
                    }
                }
                ob[0] = temp;


                return ob;
            }
            catch { return null; }

            //throw new NotImplementedException();
        }
    }

    //public class IndexASPRPConverter : IValueConverter
    //{
    //    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    //    {
    //        int index = -1;

    //        List<int> temp = new List<int>();

    //        if ((PropNumberASP.ComboBoxASPSuppr) != null)
    //        {
    //            if (PropNumberASP.ComboBoxASPSuppr.Length > 0)
    //            {
    //                for (int i = 0; i < PropNumberASP.ComboBoxASPSuppr.Length; i++)
    //                {
    //                    if (PropNumberASP.ComboBoxASPSuppr[i].ToString() == " ")
    //                        temp.Add(-2);
    //                    else
    //                        temp.Add(int.Parse(string.Join(string.Empty, (PropNumberASP.ComboBoxASPSuppr[i].ToString()).Where(c => char.IsDigit(c)))));
    //                }
    //                index = temp.FindIndex(x => x == PropNumberASP.IndASPSuppr);
    //            }
    //        }

    //        return index;
    //    }

    //    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    //    {
    //        throw new NotImplementedException();
    //    }
    //}
}