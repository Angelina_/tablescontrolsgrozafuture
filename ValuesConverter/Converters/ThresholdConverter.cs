﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace ValuesConverter
{
    [ValueConversion(sourceType: typeof(Int16), targetType: typeof(string))]
    public class ThresholdConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //try
            //{
            Int16 iThreshold = System.Convert.ToInt16(value);

            if (iThreshold == 255)
            {
                return string.Empty;
            }

            //if (System.Convert.ToInt16(value) == 255)
            //{
            //    return string.Empty;
            //}
            //}
            //catch { }

            iThreshold = iThreshold > 0 ? (Int16)(iThreshold * -1) : iThreshold;
            return  System.Convert.ToString(iThreshold);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
