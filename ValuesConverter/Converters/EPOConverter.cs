﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace ValuesConverter
{
    [ValueConversion(sourceType: typeof(byte[]), targetType: typeof(string))]
    public class EPOConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //string sEPO = string.Empty;

            //try
            //{
            //if (((byte[])value).Length == 1)
            //{
            //    sEPO = ((byte[])value)[0].ToString();
            //}
            //else if (((byte[])value).Length == 2)
            //{
            //    sEPO = ((byte[])value)[0] + " - " + ((byte[])value)[1];
            //}

            //return sEPO;

            return EPO((byte[])value);
            //}
            //catch { return string.Empty; }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        public static string EPO(byte[] epo)
        {
            string sEPO = string.Empty;

            if (epo.Length == 1)
            {
                sEPO = epo[0].ToString();
            }
            else if (epo.Length == 2)
            {
                sEPO = epo[0] + " - " + epo[1];
            }

            return sEPO;
        }
    }
}
