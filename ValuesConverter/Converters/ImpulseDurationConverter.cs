﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace ValuesConverter
{
    [ValueConversion(sourceType: typeof(UInt16), targetType: typeof(string))]
    public class ImpulseDurationConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //try
            //{
            if (System.Convert.ToUInt16(value) == 65534)
            {
                return "-";
            }
            if (System.Convert.ToUInt16(value) == 65535)
            {
                return string.Empty;
            }
            //}
            //catch { }

            return System.Convert.ToString(value);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
