﻿using ModelsTablesDBLib;
using System;
using System.Globalization;
using System.Windows.Data;

namespace ValuesConverter
{
    [ValueConversion(sourceType: typeof(Coord), targetType: typeof(string))]
    public class AltConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //try
            //{
                if ((System.Convert.ToDouble(((Coord)value).Altitude) == -1))
                {
                    return "-";
                }
                if ((System.Convert.ToDouble(((Coord)value).Altitude) == -2))
                {
                    return string.Empty;
                }
            //}
            //catch { }

            return (System.Convert.ToString(((Coord)value).Altitude.ToString("0.0")));
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
