﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace ValuesConverter
{
    [ValueConversion(sourceType: typeof(byte[]), targetType: typeof(string))]
    public class LettersConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //try
            //{
                if (value != null)
                {
                    string str = string.Empty;
                    byte[] b = (byte[])value;
                    for (int i = 0; i < b.Length; i++)
                    {
                        if (b[i] == 1)
                            str += (i + 1) + ",".ToString(culture);
                    }

                    if (str != string.Empty)
                        str = str.Remove(str.Length - 1, 1);

                    return str;
                }
                else { return string.Empty; }
            //}
            //catch { return string.Empty; }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    [ValueConversion(sourceType: typeof(byte[]), targetType: typeof(string))]
    public class LettersFHSSConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //string sLetters = string.Empty;

            //try
            //{
            //if (((byte[])value).Length == 1)
            //{
            //    sLetters = ((byte[])value)[0].ToString();
            //}
            //else if (((byte[])value).Length == 2)
            //{
            //    sLetters = ((byte[])value)[0] + " - " + ((byte[])value)[1];
            //}

            //return sLetters;

            return LettersFHSS((byte[])value);
            //}
            //catch { return string.Empty; }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        public static string LettersFHSS(byte[] Letters)
        {
            string sLetters = string.Empty;

            if (Letters.Length == 1)
            {
                sLetters = Letters[0].ToString();
            }
            else if (Letters.Length == 2)
            {
                sLetters = Letters[0] + " - " + Letters[1];
            }

            return sLetters;
        }
    }

    [ValueConversion(sourceType: typeof(byte), targetType: typeof(string))]
    public class LetterConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //try
            //{
                if (System.Convert.ToByte(value) == 0)
                {
                    return string.Empty;
                }
            //}
            //catch { }

            return System.Convert.ToString(value);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
