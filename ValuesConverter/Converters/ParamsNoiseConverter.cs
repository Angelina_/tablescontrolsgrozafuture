﻿using ModelsTablesDBLib;
using System;
using System.Collections;
using System.Globalization;
using System.Windows.Data;
using ValuesCorrectLib;

namespace ValuesConverter
{
    [ValueConversion(sourceType: typeof(InterferenceParam), targetType: typeof(string))]
    public class ParamsNoiseConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //try
            //{
                return ParamHindrance(((InterferenceParam)value).Modulation, ((InterferenceParam)value).Deviation, ((InterferenceParam)value).Manipulation, ((InterferenceParam)value).Duration);
            //}
            //catch { return string.Empty; }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        // декодирование параметров помехи из кодов в строку
        public static string ParamHindrance(byte bModulation, byte bDeviation, byte bManipulation, byte bDuration)
        {
            string strModulation = string.Empty;   // модуляция
            string strDeviation = string.Empty;    // девиация
            string strManipulation = string.Empty; // манипуляция
            string strDuration = string.Empty;     // длительность

            string strHindrance = string.Empty;    // все параметры

            switch (bModulation)
            {
                case 1:
                    strModulation = STypeModulation.paramChMSh + " ";// "FMN ";

                    switch (bDeviation)
                    {
                        case 1:
                            strDeviation = "3,5 " + SMeaning.meaningkHz + " ";
                            break;

                        case 2:
                            strDeviation = "5 " + SMeaning.meaningkHz + " ";
                            break;

                        case 3:
                            strDeviation = "7 " + SMeaning.meaningkHz + " ";
                            break;

                        case 4:
                            strDeviation = "8,4 " + SMeaning.meaningkHz + " ";
                            break;

                        case 5:
                            strDeviation = "10 " + SMeaning.meaningkHz + " ";
                            break;

                        case 6:
                            strDeviation = "14 " + SMeaning.meaningkHz + " ";
                            break;

                        case 7:
                            strDeviation = "20 " + SMeaning.meaningkHz + " ";
                            break;

                        case 8:
                            strDeviation = "32 " + SMeaning.meaningkHz + " ";
                            break;

                        case 9:
                            strDeviation = "50 " + SMeaning.meaningkHz + " ";
                            break;

                        case 10:
                            strDeviation = "70 " + SMeaning.meaningkHz + " ";
                            break;

                        case 11:
                            strDeviation = "100 " + SMeaning.meaningkHz + " ";
                            break;

                        case 12:
                            strDeviation = "140 " + SMeaning.meaningkHz + " ";
                            break;

                        case 13:
                            strDeviation = "200 " + SMeaning.meaningkHz + " ";
                            break;

                        case 14:
                            strDeviation = "300 " + SMeaning.meaningkHz + " ";
                            break;

                        case 15:
                            strDeviation = "500 " + SMeaning.meaningkHz + " ";
                            break;

                        case 16:
                            strDeviation = "700 " + SMeaning.meaningkHz + " ";
                            break;

                        case 17:
                            strDeviation = "1 " + SMeaning.meaningMHz + " ";
                            break;

                        case 18:
                            strDeviation = "1,4 " + SMeaning.meaningMHz + " ";
                            break;

                        case 19:
                            strDeviation = "2 " + SMeaning.meaningMHz + " ";
                            break;

                        //case 1:
                        //    strDeviation = "+/- 1,75 kHz ";
                        //    break;

                        //case 2:
                        //    strDeviation = "+/- 2,47 kHz ";
                        //    break;

                        //case 3:
                        //    strDeviation = "+/- 3,5 kHz ";
                        //    break;

                        //case 4:
                        //    strDeviation = "+/- 4,2 kHz ";
                        //    break;

                        //case 5:
                        //    strDeviation = "+/- 5 kHz ";
                        //    break;

                        //case 6:
                        //    strDeviation = "+/- 7,07 kHz ";
                        //    break;

                        //case 7:
                        //    strDeviation = "+/- 10 kHz ";
                        //    break;

                        //case 8:
                        //    strDeviation = "+/- 15,8 kHz ";
                        //    break;

                        //case 9:
                        //    strDeviation = "+/- 25 kHz ";
                        //    break;

                        //case 10:
                        //    strDeviation = "+/- 35,35 kHz ";
                        //    break;

                        //case 11:
                        //    strDeviation = "+/- 50 kHz ";
                        //    break;

                        //case 12:
                        //    strDeviation = "+/- 70 kHz ";
                        //    break;

                        //case 13:
                        //    strDeviation = "+/- 100 kHz ";
                        //    break;

                        //case 14:
                        //    strDeviation = "+/- 150 kHz ";
                        //    break;

                        //case 15:
                        //    strDeviation = "+/- 250 kHz ";
                        //    break;

                        //case 16:
                        //    strDeviation = "+/- 350 kHz ";
                        //    break;

                        //case 17:
                        //    strDeviation = "+/- 500 kHz ";
                        //    break;

                        //case 18:
                        //    strDeviation = "+/- 700 kHz ";
                        //    break;

                        //case 19:
                        //    strDeviation = "+/- 1000 kHz ";
                        //    break;

                        //case 20:
                        //    strDeviation = "+/- 10000 kHz ";
                        //    break;

                        default:
                            break;
                    }
                    break;

                case 2:
                    strModulation = STypeModulation.paramChM2 + " "; //"FM-2 ";

                    switch (bDeviation)
                    {
                        case 1:
                            strDeviation = "+/- 2 " + SMeaning.meaningkHz + " ";
                            break;

                        case 2:
                            strDeviation = "+/- 4 " + SMeaning.meaningkHz + " ";
                            break;

                        case 3:
                            strDeviation = "+/- 6 " + SMeaning.meaningkHz + " ";
                            break;

                        case 4:
                            strDeviation = "+/- 8 " + SMeaning.meaningkHz + " ";
                            break;

                        case 5:
                            strDeviation = "+/- 10 " + SMeaning.meaningkHz + " ";
                            break;

                        case 6:
                            strDeviation = "+/- 12 " + SMeaning.meaningkHz + " ";
                            break;

                        case 7:
                            strDeviation = "+/- 14 " + SMeaning.meaningkHz + " ";
                            break;

                        case 8:
                            strDeviation = "+/- 16 " + SMeaning.meaningkHz + " ";
                            break;

                        case 9:
                            strDeviation = "+/- 18 " + SMeaning.meaningkHz + " ";
                            break;

                        case 10:
                            strDeviation = "+/- 20 " + SMeaning.meaningkHz + " ";
                            break;

                        case 11:
                            strDeviation = "+/- 22 " + SMeaning.meaningkHz + " ";
                            break;

                        case 12:
                            strDeviation = "+/- 24 " + SMeaning.meaningkHz + " ";
                            break;

                        default:
                            break;
                    }

                    switch (bManipulation)
                    {
                        case 1:
                            strManipulation = "62,5 " + SMeaning.meaningmks + " (16 " + SMeaning.meaningkHz + ") ";
                            break;

                        case 2:
                            strManipulation = "125 " + SMeaning.meaningmks + " (8 " + SMeaning.meaningkHz + ") ";
                            break;

                        case 3:
                            strManipulation = "250 " + SMeaning.meaningmks + " (4 " + SMeaning.meaningkHz + ") ";
                            break;

                        case 4:
                            strManipulation = "500 " + SMeaning.meaningmks + " (2 " + SMeaning.meaningkHz + ") ";
                            break;

                        case 5:
                            strManipulation = "1 " + SMeaning.meaningms + " (1 " + SMeaning.meaningkHz + ") ";
                            break;

                        case 6:
                            strManipulation = "2 " + SMeaning.meaningms + " (0,5 " + SMeaning.meaningkHz + ") ";
                            break;

                        default:
                            break;
                    }
                    break;

                case 3:
                    strModulation = STypeModulation.paramChM4 + " ";// "FM-4 ";

                    switch (bDeviation)
                    {
                        case 1:
                            strDeviation = "+/- 2 " + SMeaning.meaningkHz + " ";
                            break;

                        case 2:
                            strDeviation = "+/- 4 " + SMeaning.meaningkHz + " ";
                            break;

                        case 3:
                            strDeviation = "+/- 6 " + SMeaning.meaningkHz + " ";
                            break;

                        case 4:
                            strDeviation = "+/- 8 " + SMeaning.meaningkHz + " ";
                            break;

                        case 5:
                            strDeviation = "+/- 10 " + SMeaning.meaningkHz + " ";
                            break;

                        case 6:
                            strDeviation = "+/- 12 " + SMeaning.meaningkHz + " ";
                            break;

                        case 7:
                            strDeviation = "+/- 14 " + SMeaning.meaningkHz + " ";
                            break;

                        case 8:
                            strDeviation = "+/- 16 " + SMeaning.meaningkHz + " ";
                            break;

                        case 9:
                            strDeviation = "+/- 18 " + SMeaning.meaningkHz + " ";
                            break;

                        case 10:
                            strDeviation = "+/- 20 " + SMeaning.meaningkHz + " ";
                            break;

                        case 11:
                            strDeviation = "+/- 22 " + SMeaning.meaningkHz + " ";
                            break;

                        case 12:
                            strDeviation = "+/- 24 " + SMeaning.meaningkHz + " ";
                            break;

                        default:
                            break;
                    }

                    switch (bManipulation)
                    {
                        case 1:
                            strManipulation = "62,5 " + SMeaning.meaningmks + " (16 " + SMeaning.meaningkHz + ") ";
                            break;

                        case 2:
                            strManipulation = "125 " + SMeaning.meaningmks + " (8 " + SMeaning.meaningkHz + ") ";
                            break;

                        case 3:
                            strManipulation = "250 " + SMeaning.meaningmks + " (4 " + SMeaning.meaningkHz + ") ";
                            break;

                        case 4:
                            strManipulation = "500 " + SMeaning.meaningmks + " (2 " + SMeaning.meaningkHz + ") ";
                            break;

                        case 5:
                            strManipulation = "1 " + SMeaning.meaningms + " (1 " + SMeaning.meaningkHz + ") ";
                            break;

                        case 6:
                            strManipulation = "2 " + SMeaning.meaningms + " (0,5 " + SMeaning.meaningkHz + ") ";
                            break;

                        default:
                            break;
                    }
                    break;

                case 4:
                    strModulation = STypeModulation.paramChM8 + " ";// "FM-8 ";

                    switch (bDeviation)
                    {
                        case 1:
                            strDeviation = "+/- 2 " + SMeaning.meaningkHz + " ";
                            break;

                        case 2:
                            strDeviation = "+/- 4 " + SMeaning.meaningkHz + " ";
                            break;

                        case 3:
                            strDeviation = "+/- 6 " + SMeaning.meaningkHz + " ";
                            break;

                        case 4:
                            strDeviation = "+/- 8 " + SMeaning.meaningkHz + " ";
                            break;

                        case 5:
                            strDeviation = "+/- 10 " + SMeaning.meaningkHz + " ";
                            break;

                        case 6:
                            strDeviation = "+/- 12 " + SMeaning.meaningkHz + " ";
                            break;

                        case 7:
                            strDeviation = "+/- 14 " + SMeaning.meaningkHz + " ";
                            break;

                        case 8:
                            strDeviation = "+/- 16 " + SMeaning.meaningkHz + " ";
                            break;

                        case 9:
                            strDeviation = "+/- 18 " + SMeaning.meaningkHz + " ";
                            break;

                        case 10:
                            strDeviation = "+/- 20 " + SMeaning.meaningkHz + " ";
                            break;

                        case 11:
                            strDeviation = "+/- 22 " + SMeaning.meaningkHz + " ";
                            break;

                        case 12:
                            strDeviation = "+/- 24 " + SMeaning.meaningkHz + " ";
                            break;

                        default:
                            break;
                    }

                    switch (bManipulation)
                    {
                        case 1:
                            strManipulation = "62,5 " + SMeaning.meaningmks + " (16 " + SMeaning.meaningkHz + ") ";
                            break;

                        case 2:
                            strManipulation = "125 " + SMeaning.meaningmks + " (8 " + SMeaning.meaningkHz + ") ";
                            break;

                        case 3:
                            strManipulation = "250 " + SMeaning.meaningmks + " (4 " + SMeaning.meaningkHz + ") ";
                            break;

                        case 4:
                            strManipulation = "500 " + SMeaning.meaningmks + " (2 " + SMeaning.meaningkHz + ") ";
                            break;

                        case 5:
                            strManipulation = "1 " + SMeaning.meaningms + " (1 " + SMeaning.meaningkHz + ") ";
                            break;

                        case 6:
                            strManipulation = "2 " + SMeaning.meaningms + " (0,5 " + SMeaning.meaningkHz + ") ";
                            break;

                        default:
                            break;
                    }
                    break;

                case 5:
                    strModulation = STypeModulation.paramFMn + " ";// "PSK ";

                    switch (bManipulation)
                    {
                        case 10:
                            strManipulation = "0,08 " + SMeaning.meaningmks + " (12,5 " + SMeaning.meaningMHz + ") ";
                            break;

                        case 1:
                            strManipulation = "0,2 " + SMeaning.meaningmks + " (5 " + SMeaning.meaningMHz + ") ";
                            break;

                        case 2:
                            strManipulation = "0,5 " + SMeaning.meaningmks + " (2 " + SMeaning.meaningMHz + ") ";
                            break;

                        case 3:
                            strManipulation = "1 " + SMeaning.meaningmks + " (1 " + SMeaning.meaningMHz + ") ";
                            break;

                        case 4:
                            strManipulation = "5 " + SMeaning.meaningmks + " (200 " + SMeaning.meaningkHz + ") ";
                            break;

                        case 5:
                            strManipulation = "10 " + SMeaning.meaningmks + " (100 " + SMeaning.meaningkHz + ") ";
                            break;

                        case 6:
                            strManipulation = "20 " + SMeaning.meaningmks + " (50 " + SMeaning.meaningkHz + ") ";
                            break;

                        case 7:
                            strManipulation = "40 " + SMeaning.meaningmks + " (25 " + SMeaning.meaningkHz + ") ";
                            break;

                        case 8:
                            strManipulation = "80 " + SMeaning.meaningmks + " (12,5 " + SMeaning.meaningkHz + ") ";
                            break;

                        case 9:
                            strManipulation = "400 " + SMeaning.meaningmks + " (2,5 " + SMeaning.meaningkHz + ") ";
                            break;

                        default:
                            break;
                    }
                    break;

                case 6:
                    strModulation = STypeModulation.paramFMn4 + " ";// "PSK-4 ";

                    switch (bManipulation)
                    {
                        case 10:
                            strManipulation = "0,08 " + SMeaning.meaningmks + " (12,5 " + SMeaning.meaningMHz + ") ";
                            break;

                        case 1:
                            strManipulation = "0,2 " + SMeaning.meaningmks + " (5 " + SMeaning.meaningMHz + ") ";
                            break;

                        case 2:
                            strManipulation = "0,5 " + SMeaning.meaningmks + " (2 " + SMeaning.meaningMHz + ") ";
                            break;

                        case 3:
                            strManipulation = "1 " + SMeaning.meaningmks + " (1 " + SMeaning.meaningMHz + ") ";
                            break;

                        case 4:
                            strManipulation = "5 " + SMeaning.meaningmks + " (200 " + SMeaning.meaningkHz + ") ";
                            break;

                        case 5:
                            strManipulation = "10 " + SMeaning.meaningmks + " (100 " + SMeaning.meaningkHz + ") ";
                            break;

                        case 6:
                            strManipulation = "20 " + SMeaning.meaningmks + " (50 " + SMeaning.meaningkHz + ") ";
                            break;

                        case 7:
                            strManipulation = "40 " + SMeaning.meaningmks + " (25 " + SMeaning.meaningkHz + ") ";
                            break;

                        case 8:
                            strManipulation = "80 " + SMeaning.meaningmks + " (12,5 " + SMeaning.meaningkHz + ") ";
                            break;

                        case 9:
                            strManipulation = "400 " + SMeaning.meaningmks + " (2,5 " + SMeaning.meaningkHz + ") ";
                            break;

                        default:
                            break;
                    }
                    break;

                case 7:
                    strModulation = STypeModulation.paramFMn8 + " ";// "PSK-8 ";

                    switch (bManipulation)
                    {
                        case 10:
                            strManipulation = "0,08 " + SMeaning.meaningmks + " (12,5 " + SMeaning.meaningMHz + ") ";
                            break;

                        case 1:
                            strManipulation = "0,2 " + SMeaning.meaningmks + " (5 " + SMeaning.meaningMHz + ") ";
                            break;

                        case 2:
                            strManipulation = "0,5 " + SMeaning.meaningmks + " (2 " + SMeaning.meaningMHz + ") ";
                            break;

                        case 3:
                            strManipulation = "1 " + SMeaning.meaningmks + " (1 " + SMeaning.meaningMHz + ") ";
                            break;

                        case 4:
                            strManipulation = "5 " + SMeaning.meaningmks + " (200 " + SMeaning.meaningkHz + ") ";
                            break;

                        case 5:
                            strManipulation = "10 " + SMeaning.meaningmks + " (100 " + SMeaning.meaningkHz + ") ";
                            break;

                        case 6:
                            strManipulation = "20 " + SMeaning.meaningmks + " (50 " + SMeaning.meaningkHz + ") ";
                            break;

                        case 7:
                            strManipulation = "40 " + SMeaning.meaningmks + " (25 " + SMeaning.meaningkHz + ") ";
                            break;

                        case 8:
                            strManipulation = "80 " + SMeaning.meaningmks + " (12,5 " + SMeaning.meaningkHz + ") ";
                            break;

                        case 9:
                            strManipulation = "400 " + SMeaning.meaningmks + " (2,5 " + SMeaning.meaningkHz + ") ";
                            break;

                        default:
                            break;
                    }
                    break;

                case 8:
                    strModulation = STypeModulation.paramZSh + " ";// "BN (CPSK) ";

                    switch (bManipulation)
                    {
                        case 1:
                            strManipulation = "0,4 " + SMeaning.meaningmks + " (2,5 " + SMeaning.meaningMHz + ") ";
                            break;

                        case 2:
                            strManipulation = "0,16 " + SMeaning.meaningmks + " (6,25 " + SMeaning.meaningMHz + ") ";
                            break;

                        case 3:
                            strManipulation = "0,08 " + SMeaning.meaningmks + " (12,5 " + SMeaning.meaningMHz + ") ";
                            break;

                        case 4:
                            strManipulation = "0,04 " + SMeaning.meaningmks + " (25 " + SMeaning.meaningMHz + ") ";
                            break;

                        default:
                            break;
                    }
                    break;

                case 11:
                    strModulation = STypeModulation.paramLChM + " ";// "LFM ";

                    BitArray devBA = new BitArray(new byte[] { bDeviation });
                    foreach (bool bit in devBA)
                    {
                        strDeviation += System.Convert.ToByte(bit);
                    }
                    int devBit = System.Convert.ToInt32(strDeviation.Substring(7)); // старший бит 
                    if (devBit == 1)
                    {
                        strDeviation = "+/- " + (System.Convert.ToByte(bDeviation ^ 0x80)).ToString() + " " + SMeaning.meaningMHz + " ";
                    }
                    else
                    {
                        strDeviation = "+/- " + (bDeviation * 10).ToString() + " " + SMeaning.meaningkHz + " ";
                    }

                    BitArray manBA = new BitArray(new byte[] { bManipulation });
                    foreach (bool bit in manBA)
                    {
                        strManipulation += System.Convert.ToByte(bit);
                    }
                    int manBit = System.Convert.ToInt32(strManipulation.Substring(7)); // старший бит 
                    if (manBit == 1)
                    {
                        strManipulation = "+/- " + (System.Convert.ToByte(bManipulation ^ 0x80)).ToString() + " " + SMeaning.meaningkHz + " ";
                    }
                    else
                    {
                        strManipulation = "+/- " + (bManipulation * 10).ToString() + " " + SMeaning.meaningHz + " ";
                    }

                    break;
            }

            switch (bDuration)
            {
                case 1:
                    strDuration = "0.5 " + SMeaning.meaningms + " ";
                    break;

                case 2:
                    strDuration = "1 " + SMeaning.meaningms + " ";
                    break;

                case 3:
                    strDuration = "1.5 " + SMeaning.meaningms + " ";
                    break;

                case 4:
                    strDuration = "2 " + SMeaning.meaningms + " ";
                    break;

                default:
                    strDuration = "";
                    break;
            }

            strHindrance = strModulation + " " + strDeviation + " " + strManipulation + " " + strDuration;
          
            return strHindrance;
        }
    }

    [ValueConversion(sourceType: typeof(InterferenceParam), targetType: typeof(string))]
    public class Modulation : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //try
            //{
                if (((InterferenceParam)value).Modulation == 255) { return string.Empty; }
                else { return ((InterferenceParam)value).Modulation.ToString(); }
            //}
            //catch { return string.Empty; }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    [ValueConversion(sourceType: typeof(InterferenceParam), targetType: typeof(string))]
    public class Manipulation : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //try
            //{
                if (((InterferenceParam)value).Manipulation == 255) { return string.Empty; }
                else { return ((InterferenceParam)value).Manipulation.ToString(); }
            //}
            //catch { return string.Empty; }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    [ValueConversion(sourceType: typeof(InterferenceParam), targetType: typeof(string))]
    public class Duration : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //try
            //{
                if (((InterferenceParam)value).Duration == 255) { return string.Empty; }
                else { return ((InterferenceParam)value).Duration.ToString(); }
            //}
            //catch { return string.Empty; }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    [ValueConversion(sourceType: typeof(InterferenceParam), targetType: typeof(string))]
    public class Deviation : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //try
            //{
                if (((InterferenceParam)value).Deviation == 255) { return string.Empty; }
                else { return ((InterferenceParam)value).Deviation.ToString(); }
            //}
            //catch { return string.Empty; }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
