﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace ValuesConverter
{
    [ValueConversion(sourceType: typeof(byte), targetType: typeof(string))]
    public class ByteConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //try
            //{
            if (System.Convert.ToByte(value) == 254)
            {
                return "-";
            }
            if (System.Convert.ToByte(value) == 255)
            {
                return string.Empty;
            }
            //}
            //catch { }

            return (System.Convert.ToString(value));
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
