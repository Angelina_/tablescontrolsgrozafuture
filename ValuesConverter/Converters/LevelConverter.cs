﻿using ModelsTablesDBLib;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace ValuesConverter
{
    [ValueConversion(sourceType: typeof(Int16), targetType: typeof(string))]
    public class LevelConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Int16 iLevel = -1;

            try
            {
                if (value is JamDirect)
                {
                    iLevel = System.Convert.ToInt16(((JamDirect)value).Level);
                }

                if (value is TableJamDirect)
                {
                    iLevel = System.Convert.ToInt16(((TableJamDirect)value).JamDirect.Level);
                }

                if (iLevel == -1)
                {
                    return "-";
                }
                if (iLevel == -2)
                {
                    return string.Empty;
                }
            }

            catch { }

            iLevel = iLevel > 0 ? (Int16)(iLevel * -1) : iLevel;
            return System.Convert.ToString(iLevel);
            //return System.Convert.ToString(iLevel);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
