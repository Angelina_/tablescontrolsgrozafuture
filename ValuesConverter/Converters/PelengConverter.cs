﻿using ModelsTablesDBLib;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace ValuesConverter
{
    //[ValueConversion(sourceType: typeof(List<JamDirect>), targetType: typeof(string[]))]
    [ValueConversion(sourceType: typeof(float), targetType: typeof(string))]
    public class PelengConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string sBearing = string.Empty;
            float fBearing = -1F;

            try
            {
                if (value is JamDirect)
                {
                    fBearing = System.Convert.ToSingle(((JamDirect)value).Bearing);
                }

                if (value is TableJamDirect)
                {
                    fBearing = System.Convert.ToSingle(((TableJamDirect)value).JamDirect.Bearing);
                }

                if (fBearing == -1F)
                {
                    return "-";
                }
                if (fBearing == -2F)
                {
                    return string.Empty;
                }

                bool bTemp = value.ToString().Contains(",");
                int iBearing = 0;

                if (bTemp)
                {
                    iBearing = (int)((((JamDirect)value).Bearing) * 10d);
                }
                else
                {
                    iBearing = System.Convert.ToInt32(fBearing * 10);
                }

                string strBearing = System.Convert.ToString(iBearing);

                if (strBearing.Length == 1)
                {
                    sBearing = strBearing + ",0".ToString(culture); ;
                }
                else
                {
                    sBearing = strBearing.Substring(0, strBearing.Length - 1) + ",".ToString(culture) + strBearing.Substring(strBearing.Length - 1, 1);
                }
            }

            catch { }

            return sBearing;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    [ValueConversion(sourceType: typeof(float), targetType: typeof(string))]
    public class BearingConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string sBearing = string.Empty;

            //try
            //{
                if ((float)value == -1F)
                {
                    return "-";
                }
                if ((float)value == -2F)
                {
                    return string.Empty;
                }

                bool bTemp = value.ToString().Contains(",");
                int iBearing = 0;

                if (bTemp) { iBearing = (int)((float)(value) * 10d); }
                else iBearing = System.Convert.ToInt32((float)value * 10);
                string strBearing = System.Convert.ToString(iBearing);

                if (strBearing.Length == 1)
                {
                    sBearing = strBearing + ",0".ToString(culture); ;
                }
                else
                {
                    sBearing = strBearing.Substring(0, strBearing.Length - 1) + ",".ToString(culture) + strBearing.Substring(strBearing.Length - 1, 1);
                }
            //}

            //catch { }

            return sBearing;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
