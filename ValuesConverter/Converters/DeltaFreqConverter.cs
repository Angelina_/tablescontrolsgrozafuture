﻿using ModelsTablesDBLib;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace ValuesConverter
{
    [ValueConversion(sourceType: typeof(float), targetType: typeof(string[]))]
    public class DeltaFreqConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string sDeltaFreq = string.Empty;

            //try
            //{
                if ((float)value == -1F)
                {
                    return "-";
                }
                if ((float)value == -2F)
                {
                    return string.Empty;
                }

                bool bTemp = value.ToString().Contains(",");
                int DFreq = 0;

                if (bTemp) { DFreq = (int)((float)(value) * 10d); }
                else DFreq = System.Convert.ToInt32((float)value * 10);
                string strDFreq = System.Convert.ToString(DFreq);

                if (strDFreq.Length == 1)
                {
                    sDeltaFreq = strDFreq + ",0".ToString(culture); ;
                }
                else
                {
                    if (strDFreq.Length == 8)
                    {
                        sDeltaFreq = strDFreq.Substring(0, 1) + " " + strDFreq.Substring(1, 3) + " " + strDFreq.Substring(strDFreq.Length - 4, 3) + ",".ToString(culture) + strDFreq.Substring(strDFreq.Length - 1, 1);
                    }
                    else
                    {
                        if (strDFreq.Length < 5)
                        {
                            sDeltaFreq = strDFreq.Substring(0, strDFreq.Length - 1) + ",".ToString(culture) + strDFreq.Substring(strDFreq.Length - 1, 1);
                        }
                        else
                        {
                            sDeltaFreq = strDFreq.Substring(0, strDFreq.Length - 4) + " " + strDFreq.Substring(strDFreq.Length - 4, 3) + ",".ToString(culture) + strDFreq.Substring(strDFreq.Length - 1, 1);
                        }
                    }
                    //sDeltaFreq = strDFreq.Substring(0, strDFreq.Length - 1) + ",".ToString(culture) + strDFreq.Substring(strDFreq.Length - 1, 1);
                }

           
            //}

            //catch { }

            return sDeltaFreq;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
