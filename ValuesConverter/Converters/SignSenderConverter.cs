﻿using ModelsTablesDBLib;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using ValuesCorrectLib;

namespace ValuesConverter
{
    [ValueConversion(sourceType: typeof(byte), targetType: typeof(string))]
    public class SignSenderConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //try
            //{
                string sSignSender = string.Empty;
                switch (value)
                {
                    case SignSender.PC:
                    case SignSender.RadioRecevier:
                    case SignSender.OtherTable:
                    case SignSender.Panorama:
                    sSignSender = SMeaning.meaningSignA; // "A";
                        break;

                    case SignSender.IR:
                    case SignSender.IN:
                    case SignSender.GS:
                    case SignSender.UAV_433:
                    case SignSender.UAV_860:
                    case SignSender.UAV_920:
                    case SignSender.UAV_2400:
                    case SignSender.UAV_5200:
                    case SignSender.UAV_5800:
                    case SignSender.NAV_L1:
                    case SignSender.NAV_L2:
                    sSignSender = SMeaning.meaningSignB; // "B";
                        break;

                    case SignSender.Cicada:
                    sSignSender = SMeaning.meaningSignC; // "C";
                        break;

                    default:
                        break;
                }

                return sSignSender;
            //}
            //catch { return null; }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    [ValueConversion(sourceType: typeof(byte), targetType: typeof(string))]
    public class SignSenderToolTipConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //try
            //{
            string sSignSender = string.Empty;
            switch (value)
            {
                case SignSender.PC:
                    sSignSender = SMeaning.meaningSignPC; // "Control post";// "ПУ";
                    break;

                case SignSender.RadioRecevier:
                    sSignSender = SMeaning.meaningSignRadioRecevier; // "Reference radio receiver";// "КРПУ";
                    break;

                case SignSender.OtherTable:
                    sSignSender = SMeaning.meaningSignOtherTable; // "Table";// "Таблица";
                    break;

                case SignSender.Panorama:
                    sSignSender = SMeaning.meaningSignPanorama; // "Panorama";// "Панорама";
                    break;

                case SignSender.IR:
                case SignSender.IN:
                case SignSender.GS:
                case SignSender.UAV_433:
                case SignSender.UAV_860:
                case SignSender.UAV_920:
                case SignSender.UAV_2400:
                case SignSender.UAV_5200:
                case SignSender.UAV_5800:
                case SignSender.NAV_L1:
                case SignSender.NAV_L2:
                    sSignSender = SMeaning.meaningSignButton; // "Button";// "Кнопка";
                    break;

                case SignSender.Cicada:
                    sSignSender = SMeaning.meaningSignCicada; // "Cicada";// "Цикада";
                    break;

                default:
                    break;
            }

            return sSignSender;
            //}
            //catch { return null; }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
