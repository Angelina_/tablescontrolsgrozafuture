﻿using System;
using System.Globalization;
using System.Windows.Data;
using ValuesCorrectLib;

namespace ValuesConverter
{
    [ValueConversion(sourceType: typeof(byte), targetType: typeof(string))]
    public class ModulationConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string sModulation = "";

            //try
            //{
                // Вид модуляции (декодирование из кодов в строку)
                switch ((byte)value)
                {
                    case 0:
                        sModulation = SMeaning.meaningShum; //"Noise";// "Шум"; //"ZeroImpulse";
                        break;

                    case 1:
                        sModulation = SMeaning.meaningNes; //"CF";// "Несущая"; //"Am2";
                        break;

                    case 2:
                        sModulation = SMeaning.meaningAMn; // "AM";// "АМн"; //"Am101";
                        break;

                    case 3:
                        sModulation = SMeaning.meaningFMn; // "PSK";// "ФМн"; //"Fm2";
                        break;

                    case 4:
                        sModulation = SMeaning.meaningChMn2;// "FSK-2";// "ЧМн2"; //"Fsk2"; 
                        break;

                    case 5:
                        sModulation = SMeaning.meaningAMChM; // "AM FM";// "АМ ЧМ"; //"AmFm";
                        break;

                    case 6:
                        sModulation = SMeaning.meaningChMn4; // "FSK-4";// "ЧМн4"; //"Fsk4"; 
                        break;

                    case 7:
                        sModulation = SMeaning.meaningChMn8; // "FSK-8";//"ЧМн8"; //"Fsk8"; 
                        break;

                    case 8:
                        sModulation = SMeaning.meaningChM; // "FM";// "ЧМ"; //"Fm"; 
                        break;

                    case 9:
                        sModulation = SMeaning.meaningChMn; // "FSK";//"ЧМн"; //"Fsk"; 
                        break;

                    case 10:
                        sModulation = SMeaning.meaningShPS; // "SSM";// "ШПС"; //"ShPS"; 
                        break;

                    case 33:
                        sModulation = string.Empty; // при добавлении пустых строк в dgv
                        break;

                    default:
                        sModulation = SMeaning.meaningUnkn; // "Unknown"; //;"Неизв."
                        break;
                }
            //}
            //catch { }

            return sModulation;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
      
    }
}
