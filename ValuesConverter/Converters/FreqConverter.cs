﻿using System;
using System.Globalization;
using System.Windows.Data;
using TableEvents;

namespace ValuesConverter
{
    [ValueConversion(sourceType: typeof(double), targetType: typeof(string))]
    public class FreqConverter : IValueConverter
    {
        /// <summary>
        /// Преобразовать значение double частоты в строку string 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if ((double)value == -2F)
            {
                return string.Empty;
            }

            bool bTemp = value.ToString().Contains(",");
            int Freq = 0;
            string strFreqResult = string.Empty;
            string strFreq = string.Empty;
            int iLength;

            switch (PropLocalFreqMHz_kHz.FreqMHz_kHz)
            {
                case 0:

                    if (bTemp) { Freq = (int)((double)(value) * 10d); }
                    else Freq = System.Convert.ToInt32((double)value * 10);
                    
                    strFreq = System.Convert.ToString(Freq);
                    iLength = strFreq.Length;

                    try
                    {
                        if (strFreq == "0")
                            return strFreq;// string.Empty;

                        if (iLength == 8)
                        {
                            strFreqResult = strFreq.Substring(0, 1) + " " + strFreq.Substring(1, 3) + " " + strFreq.Substring(iLength - 4, 3) + ",".ToString(culture) + strFreq.Substring(iLength - 1, 1);
                        }
                        else
                        {
                            strFreqResult = strFreq.Substring(0, iLength - 4) + " " + strFreq.Substring(iLength - 4, 3) + ",".ToString(culture) + strFreq.Substring(iLength - 1, 1);
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message.ToString());
                    }
                    break;

                case 1:

                    if (bTemp) { Freq = (int)((double)(value) / 1000 * 10d); }
                    else Freq = System.Convert.ToInt32((double)value / 1000 * 10);

                    strFreqResult = "";
                    strFreq = System.Convert.ToString(Freq);
                    iLength = strFreq.Length;

                    try
                    {
                        if (strFreq == "0")
                            return strFreq; //string.Empty;

                        if (iLength == 5)
                        {
                            strFreqResult = strFreq.Substring(0, 1) + " " + strFreq.Substring(1, 3) + ",".ToString(culture) + strFreq.Substring(iLength - 1, 1);
                        }
                        else
                        {
                            strFreqResult = strFreq.Substring(0, iLength - 1) + ",".ToString(culture) + strFreq.Substring(iLength - 1, 1);
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message.ToString());
                    }
                    break;

                default:
                    break;
            }

            return strFreqResult;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //double result;

            //if (Double.TryParse(value.ToString(), NumberStyles.Any, culture, out result))
            //{
            //    return result;
            //}

            Int32 result;

            if (Int32.TryParse(value.ToString(), NumberStyles.Any, culture, out result))
            {
                return result;
            }
            return value;
        }

        /// <summary>
        /// представление частоты строкой с разделением на разряды
        /// </summary>
        /// <param name="iFreq"> частота, kHz </param>
        /// <returns></returns>
        public string FreqToStr(int iFreq)
        {
            string strFreqResult = "";
            string strFreq = System.Convert.ToString(iFreq);
            int iLength = strFreq.Length;

            if (strFreq == "0")
            {
                return "";
            }

            if (iLength == 8)
            {
                strFreqResult = strFreq.Substring(0, 1) + " " + strFreq.Substring(1, 3) + " " + strFreq.Substring(iLength - 4, 3) + "," + strFreq.Substring(iLength - 1, 1);
            }
            else
            {
                strFreqResult = strFreq.Substring(0, iLength - 4) + " " + strFreq.Substring(iLength - 4, 3) + "," + strFreq.Substring(iLength - 1, 1);
            }

            return strFreqResult;
        }
    }

    public class FreqCountConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            //string sFcount = string.Empty;
            //try
            //{
            //int Fmin = System.Convert.ToInt32(values[0]);
            //int Fmax = System.Convert.ToInt32(values[1]);
            //Int16 Step = System.Convert.ToInt16(values[2]);

            //if (Fmin == 0 || Fmax == 0) { return sFcount; }

            //sFcount = ((Fmax - Fmin) / Step).ToString();
            //} 

            //catch { }

            //return sFcount;

            return FreqCount(System.Convert.ToInt32(values[0]), System.Convert.ToInt32(values[1]), System.Convert.ToInt16(values[2]));
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        public static string FreqCount(int Fmin, int Fmax, Int16 Step)
        {
            string sFcount = string.Empty;

            if (Fmin == -2F || Fmax == -2F) { return string.Empty; }

            if (Fmin == 0 || Fmax == 0) { return sFcount; }

            sFcount = ((Fmax - Fmin) / Step).ToString();

            return sFcount;

        }
    }
}