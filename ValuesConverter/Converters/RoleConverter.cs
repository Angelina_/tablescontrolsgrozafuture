﻿using ModelsTablesDBLib;
using System;
using System.ComponentModel;
using System.Globalization;
using System.Windows.Data;
using ValuesCorrectLib;

namespace ValuesConverter
{
    public class RoleConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            //string sRole = string.Empty;

            //try
            //{
            if ((int)values[0] == -2)
                return string.Empty;

            //if ((int)values[0] == -2)
            //        return sRole;

            //switch (values[1])
            //{
            //    case RoleStation.Master:
            //    sRole = SMeaning.meaningOwn;// "Master";//"Ведущая" ;
            //    break;

            //    case RoleStation.Slave:
            //    sRole = SMeaning.meaningLinked;// "Slave"; // "Ведомая";
            //        break;

            //    case RoleStation.Autonomic:
            //    sRole = SMeaning.meaningAutonomous;// "Autonomous";// "Автономная";
            //        break;

            //    default:
            //        sRole = string.Empty;
            //        break;
            //}
            //}
            //catch { }

            return RoleStationParam(values[1]);
            //return sRole;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        public static string RoleStationParam(object role)
        {
            string sRole = string.Empty;

            switch (role)
            {
                case RoleStation.Master:
                    sRole = SMeaning.meaningOwn;// "Master";//"Ведущая" ;
                    break;

                case RoleStation.Slave:
                    sRole = SMeaning.meaningLinked;// "Slave"; // "Ведомая";
                    break;

                case RoleStation.Autonomic:
                    sRole = SMeaning.meaningAutonomous;// "Autonomous";// "Автономная";
                    break;

                default:
                    sRole = string.Empty;
                    break;
            }

            return sRole;
        }
    }

    //public class RoleStationConverter : IValueConverter
    //{
    //    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    //    {
    //        return (RoleStation)value;
    //    }

    //    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    //    {
    //        return (byte)value;
    //    }
    //}

    public class RolePropGridConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            byte bRole = (byte)value;

            return (byte)value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (RoleStation)System.Convert.ToByte(value);
        }
    }

   
}
