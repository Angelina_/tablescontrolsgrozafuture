﻿using ModelsTablesDBLib;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace ValuesConverter
{
    [ValueConversion(sourceType: typeof(ModulationKondor), targetType: typeof(string))]
    public class ModulationKondorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string sModulation = string.Empty;

            //try
            //{
            // Вид модуляции
            switch (value)
            {
                case ModulationKondor.AM:
                    sModulation = "AM";
                    break;

                case ModulationKondor.APCO_P25:
                    sModulation = "APCO P25";
                    break;

                case ModulationKondor.DMR:
                    sModulation = "DMR";
                    break;

                case ModulationKondor.FM:
                    sModulation = "FM";
                    break;

                case ModulationKondor.LSB:
                    sModulation = "LSB";
                    break;

                case ModulationKondor.TETRA:
                    sModulation = "TETRA";
                    break;

                case ModulationKondor.USB:
                    sModulation = "USB";
                    break;

                case ModulationKondor.WFM:
                    sModulation = "WFM";
                    break;

                case ModulationKondor.None:
                    sModulation = string.Empty;
                    break;

                default:
                    sModulation = string.Empty;
                    break;
            }
            //}
            //catch { }

            return sModulation;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
