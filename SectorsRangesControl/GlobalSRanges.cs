﻿using ModelsTablesDBLib;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace SectorsRangesControl
{
    public class GlobalSRanges : INotifyPropertyChanged
    {
        public ObservableCollection<TableSectorsRanges> CollectionSRanges { get; set; }

        public GlobalSRanges()
        {
            CollectionSRanges = new ObservableCollection<TableSectorsRanges> { };

            //CollectionSRanges = new ObservableCollection<TableSectorsRanges>
            //{
            //    new TableSectorsRanges()
            //    {
            //        NumberASP = 222,
            //        IsCheck = false,
            //        FreqMinKHz = 788999.879,
            //        FreqMaxKHz = 888888.787870,
            //        AngleMin = 35,
            //        AngleMax = 70,
            //        Note = "ASDDSSkl"
            //    },
            //    new TableSectorsRanges()
            //    {
            //        NumberASP = 32,
            //        IsCheck = true,
            //        FreqMinKHz = 559999.7878,
            //        FreqMaxKHz = 998880.6765,
            //        AngleMin = 0,
            //        AngleMax = 360,
            //        Note = "JNJHJGJHY"
            //    }
            //};
        }

        public TableSectorsRanges SelectedSRanges
        {
            get { return selectedSRanges; }
            set
            {
                selectedSRanges = value;
                OnPropertyChanged("SelectedSRanges");
            }
        }
        private TableSectorsRanges selectedSRanges;

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
    }                    
}
