﻿using ModelsTablesDBLib;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using TableEvents;
using TableOperations;
using ValuesCorrectLib;

namespace SectorsRangesControl
{
    /// <summary>
    /// Логика взаимодействия для UserControl1.xaml
    /// </summary>
    public partial class UserControlSectorsRanges : UserControl, ITableReconEvent//, ISRangesEvents
    {
        public SectorsRangesProperty sectorsRangesWindow;

        #region Events
        public event EventHandler<TableEvent> OnAddRecord = (object sender, TableEvent data) => { };
        public event EventHandler<TableEvent> OnChangeRecord = (object sender, TableEvent data) => { };
        public event EventHandler<TableEvent> OnDeleteRecord = (object sender, TableEvent data) => { };
        public event EventHandler<NameTable> OnClearRecords = (object sender, NameTable data) => { };

        //public event EventHandler<SRangesArgs> OnLoadDefaultSRanges = (object sender, SRangesArgs data) => { };
        public event EventHandler<NameTable> OnLoadDefaultSRanges = (object sender, NameTable data) => { };

        // Открылось окно с PropertyGrid
        public event EventHandler<SectorsRangesProperty> OnIsWindowPropertyOpen = (object sender, SectorsRangesProperty data) => { };

        public event EventHandler<TableEventReport> OnAddTableToReport = (object sender, TableEventReport data) => { };
        #endregion

        #region Properties
        public SRanges NameTable { get; set; } = SRanges.Recon;
        #endregion

        public UserControlSectorsRanges()
        {
            InitializeComponent();

            dgvSRanges.DataContext = new GlobalSRanges();

            // TEST -------------------------------------------
            //TranslatorTables.LoadDictionary(Languages.Rus);
            //PropLocalFreqMHz_kHz.FreqMHz_kHz = 1;
            //if (PropLocalFreqMHz_kHz.FreqMHz_kHz == 1)
            //    FreqHeaderMHz();
            // ------------------------------------------- TEST 
        }

        /// <summary>
        /// Удалить одну запись
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!IsSelectedRowEmpty())
                    return;

                TableSectorsRanges sr = new TableSectorsRanges
                {
                    Id = ((GlobalSRanges)dgvSRanges.DataContext).SelectedSRanges.Id,// 2;
                    IsCheck = ((GlobalSRanges)dgvSRanges.DataContext).SelectedSRanges.IsCheck,
                    FreqMinKHz = ((GlobalSRanges)dgvSRanges.DataContext).SelectedSRanges.FreqMinKHz,// 54000;
                    FreqMaxKHz = ((GlobalSRanges)dgvSRanges.DataContext).SelectedSRanges.FreqMaxKHz,// 65000;
                    AngleMin = ((GlobalSRanges)dgvSRanges.DataContext).SelectedSRanges.AngleMin,// 10;
                    AngleMax = ((GlobalSRanges)dgvSRanges.DataContext).SelectedSRanges.AngleMax,// 70;
                    NumberASP = ((GlobalSRanges)dgvSRanges.DataContext).SelectedSRanges.NumberASP,// 34;
                    Note = ((GlobalSRanges)dgvSRanges.DataContext).SelectedSRanges.Note
                };

                // Событие удаления одной записи
                OnDeleteRecord(this, new TableEvent(FindTypeSRanges(sr)));
            }
            catch { }
        }
        
        /// <summary>
        /// Удалить все записи
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonClear_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // Событие удаления записей
                OnClearRecords(this, FindNameTableSRanges());
            }
            catch { }
        }

        
        /// <summary>
        /// Добавить запись
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonAdd_Click(object sender, RoutedEventArgs e)
        {

            if (PropNumberASP.SelectedNumASP != 0 && PropNumberASP.IsSelectedRowASP)
            {
                sectorsRangesWindow = new SectorsRangesProperty(((GlobalSRanges)dgvSRanges.DataContext).CollectionSRanges);
                sectorsRangesWindow.sr.NumberASP = PropNumberASP.SelectedNumASP;

                PropNameTable.SRangesName = FindNameTableSRanges();

                OnIsWindowPropertyOpen(this, sectorsRangesWindow);

                if (sectorsRangesWindow.ShowDialog() == true)
                {
                    // Событие добавления одной записи
                    OnAddRecord(this, new TableEvent(FindTypeSRanges(sectorsRangesWindow.sr)));
                }
            }
        }

        /// <summary>
        /// Изменить запись
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonChange_Click(object sender, RoutedEventArgs e)
        {
            if ((dgvSRanges.DataContext as GlobalSRanges).SelectedSRanges != null)
            {
                if ((dgvSRanges.DataContext as GlobalSRanges).SelectedSRanges.Id > 0)
                {
                    var selected = (dgvSRanges.DataContext as GlobalSRanges).SelectedSRanges;

                    sectorsRangesWindow = new SectorsRangesProperty(((GlobalSRanges)dgvSRanges.DataContext).CollectionSRanges, selected.Clone());

                    PropNameTable.SRangesName = FindNameTableSRanges();

                    OnIsWindowPropertyOpen(this, sectorsRangesWindow);

                    if (sectorsRangesWindow.ShowDialog() == true)
                    {
                        TableSectorsRanges srReplace = new TableSectorsRanges();
                        srReplace = sectorsRangesWindow.sr;
                       
                        // Событие изменения одной записи
                        OnChangeRecord(this, new TableEvent(FindTypeSRanges(srReplace)));
                    }
                }
            }
        }

        private void ButtonDefault_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // Событие добавления секторов и диапазонов по умолчанию
                OnClearRecords(this, FindNameTableSRanges());

                NameTable nameTable = FindNameTableSRanges();

                OnLoadDefaultSRanges(this, nameTable);
            }
            catch { }
        }

        private void DataGridSectorsRanges_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            AddEmptyRows();
        }

        private void ButtonWord_Click(object sender, RoutedEventArgs e)
        {
            string[,] Table = AddSRangesToTable();

            OnAddTableToReport(this, new TableEventReport(Table, FindNameTableSRanges()));
        }

        private void ButtonPrint_Click(object sender, RoutedEventArgs e)
        {
            //
        }

        private void CheckBox_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if ((dgvSRanges.DataContext as GlobalSRanges).SelectedSRanges.Id > 0)
                {
                    if ((dgvSRanges.DataContext as GlobalSRanges).SelectedSRanges.IsCheck)
                    {
                        (dgvSRanges.DataContext as GlobalSRanges).SelectedSRanges.IsCheck = false;
                    }
                    else
                    {
                        (dgvSRanges.DataContext as GlobalSRanges).SelectedSRanges.IsCheck = true;
                    }

                    // Событие изменения одной записи
                    OnChangeRecord(this, new TableEvent(FindTypeSRanges((dgvSRanges.DataContext as GlobalSRanges).SelectedSRanges)));
                }
                else
                {
                    CheckBox chbIsChecked = sender as CheckBox;
                    chbIsChecked.IsChecked = false;
                }
            }
            catch { }
        }

        
    }
}
