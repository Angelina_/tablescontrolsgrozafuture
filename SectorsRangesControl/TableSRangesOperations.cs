﻿using Microsoft.Office.Interop.Word;
using ModelsTablesDBLib;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Windows;
using System.Windows.Controls;
using TableEvents;
using ValuesCorrectLib;

namespace SectorsRangesControl
{
    public partial class UserControlSectorsRanges : UserControl
    {
        /// <summary>
        /// Добавить несколько записей в контрол
        /// </summary>
        /// <param name="listSRanges"></param>
        public void UpdateSRanges(List<TableSectorsRanges> listSRanges)
        {
            try
            {
                if (listSRanges == null)
                    return;

                IEnumerable<TableSectorsRanges> sortFreqMinKHz = listSRanges.OrderBy(x => x.FreqMinKHz); // сортировка по частоте FreqMinKHz
                ((GlobalSRanges)dgvSRanges.DataContext).CollectionSRanges.Clear();

                for (int i = 0; i < sortFreqMinKHz.ToList().Count; i++)
                {
                    ((GlobalSRanges)dgvSRanges.DataContext).CollectionSRanges.Add(sortFreqMinKHz.ToList()[i]);
                }

                AddEmptyRows();
                
            }
            catch { }
        }

        /// <summary>
        /// Добавить несколько записей в контрол
        /// </summary>
        /// <param name="listSRanges"></param>
        public void AddSRanges(List<TableSectorsRanges> listSRanges)
        {
            try
            {
                DeleteEmptyRows();

                for (int i = 0; i < listSRanges.Count; i++)
                {
                    ((GlobalSRanges)dgvSRanges.DataContext).CollectionSRanges.Add(listSRanges[i]);
                }

                AddEmptyRows();
            }
            catch { }
        }

        /// <summary>
        /// Добавить одну запись в контрол
        /// </summary>
        /// <param name="listSectorsRanges"></param>
        public void AddSRange(TableSectorsRanges tableSRanges)
        {
            try
            {
                DeleteEmptyRows();

                ((GlobalSRanges)dgvSRanges.DataContext).CollectionSRanges.Add(tableSRanges);

                AddEmptyRows();
            }
            catch { }

        }

        /// <summary>
        /// Изменить одну запись в контроле
        /// </summary>
        /// <param name="ind"> индекс записи, которую надо заменить </param>
        /// <param name="srReplace"> запись, на которую надо заменить </param>
        public void ChangeSRange(int id, TableSectorsRanges srReplace)
        {
            try
            {
                int index = ((GlobalSRanges)dgvSRanges.DataContext).CollectionSRanges.ToList().FindIndex(x => x.Id == id);

                if (index != -1)
                {
                    ((GlobalSRanges)dgvSRanges.DataContext).CollectionSRanges.RemoveAt(index);
                    ((GlobalSRanges)dgvSRanges.DataContext).CollectionSRanges.Insert(index, srReplace);

                    AddEmptyRows();
                }
            }
            catch { }
        }

        /// <summary>
        /// Удалить одну запись из контрола
        /// </summary>
        public void DeleteSRange(TableSectorsRanges tableSRanges)
        {
            try
            {
                int index = ((GlobalSRanges)dgvSRanges.DataContext).CollectionSRanges.ToList().FindIndex(x => x.Id == tableSRanges.Id);

                if (index != -1)
                {
                    ((GlobalSRanges)dgvSRanges.DataContext).CollectionSRanges.RemoveAt(index);

                    AddEmptyRows();
                }
            }
            catch { }
        }

        /// <summary>
        /// Удалить все записи из контрола
        /// </summary>
        public void ClearSRanges()
        {
            try
            {
                ((GlobalSRanges)dgvSRanges.DataContext).CollectionSRanges.Clear();

                AddEmptyRows();
            }
            catch { }
        }

        /// <summary>
        /// Добавить в таблицу пустые строки
        /// </summary>
        private void AddEmptyRows()
        {
            try
            {
                int сountRowsAll = dgvSRanges.Items.Count; // количество всех строк в таблице
                double hs = 23; // высота строки
                double ah = dgvSRanges.ActualHeight; // визуализированная высота dataGrid
                double chh = dgvSRanges.ColumnHeaderHeight; // высота заголовка

                int countRows = Convert.ToInt32(Math.Floor(Convert.ToDouble(ah) - Convert.ToDouble(chh)) / hs); // количество строк, которое помещается в область видимости = (высота таблицы - заголовок таблицы) / высота строки

                int count = сountRowsAll - countRows; // сколько пустых строк нужно удалить
                int index = -1;
                for (int i = 0; i < count; i++)
                {
                    // Удалить пустые строки в dgv
                    index = ((GlobalSRanges)dgvSRanges.DataContext).CollectionSRanges.ToList().FindIndex(x => x.Id < 0);
                    if (index != -1)
                    {
                        ((GlobalSRanges)dgvSRanges.DataContext).CollectionSRanges.RemoveAt(index);
                    }
                }

                List<TableSectorsRanges> list = new List<TableSectorsRanges>();
                for (int i = 0; i < countRows - сountRowsAll; i++)
                {
                    TableSectorsRanges strSR = new TableSectorsRanges
                    {
                        Id = -2,
                        FreqMinKHz = -2F,
                        FreqMaxKHz = -2F,
                        AngleMin = -2,
                        AngleMax = -2
                    };
                    list.Add(strSR);
                }

                for (int i = 0; i < list.Count; i++)
                {
                    ((GlobalSRanges)dgvSRanges.DataContext).CollectionSRanges.Add(list[i]);
                }
            }
            catch { }
        }

        /// <summary>
        /// Удалить пустые строки из таблицы
        /// </summary>
        private void DeleteEmptyRows()
        {
            try
            {
                int countEmptyRows = ((GlobalSRanges)dgvSRanges.DataContext).CollectionSRanges.Count(s => s.Id < 0);
                int countAllRows = ((GlobalSRanges)dgvSRanges.DataContext).CollectionSRanges.Count;
                int iCount = countAllRows - countEmptyRows;
                for (int i = iCount; i < countAllRows; i++)
                {
                    ((GlobalSRanges)dgvSRanges.DataContext).CollectionSRanges.RemoveAt(iCount);
                }
            }
            catch { }
        }

        /// <summary>
        /// Определить тип таблицы
        /// </summary>
        /// <param name="sr"></param>
        /// <returns></returns>
        private AbstractCommonTable FindTypeSRanges(TableSectorsRanges sr)
        {
            switch (NameTable)
            {
                case SRanges.Recon:
                    return sr.ToRangesRecon();
                case SRanges.Suppr:
                    return sr.ToRangesSuppr();
                default:
                    break;
            }
            return null;
        }

        /// <summary>
        /// Определить имя таблицы
        /// </summary>
        /// <returns></returns>
        private NameTable FindNameTableSRanges()
        {
            switch (NameTable)
            {
                case SRanges.Recon:
                    return ModelsTablesDBLib.NameTable.TableSectorsRangesRecon;
                case SRanges.Suppr:
                    return ModelsTablesDBLib.NameTable.TableSectorsRangesSuppr;
                default:
                    break;
            }
            return ModelsTablesDBLib.NameTable.TableSectorsRangesRecon;
        }

        /// <summary>
        /// Загрузить Сектора и диапазоны РР по умолчанию из файла defaultSRangesRecon.json
        /// </summary>
        /// <returns></returns>
        public List<TableSectorsRanges> LoadDefaultSRangesRecon(ModelsTablesDBLib.Languages languages)
        {
            DataContractJsonSerializer jsonFormat = new DataContractJsonSerializer(typeof(TableSectorsRanges[]));
            try
            {
                TableSectorsRanges[] defaultSRanges = new TableSectorsRanges[] { };
                switch (languages)
                {
                    case ModelsTablesDBLib.Languages.Rus:

                        using (FileStream stream = new FileStream(Path.GetFullPath(@"DefaultTableSRanges\defaultSRangesReconRUS.json"), FileMode.Open))
                        {
                            defaultSRanges = (TableSectorsRanges[])jsonFormat.ReadObject(stream);
                        }
                        break;
                       
                    case ModelsTablesDBLib.Languages.Eng:

                        using (FileStream stream = new FileStream(Path.GetFullPath(@"DefaultTableSRanges\defaultSRangesReconENG.json"), FileMode.Open))
                        {
                            defaultSRanges = (TableSectorsRanges[])jsonFormat.ReadObject(stream);
                        }
                        break;

                    default:
                        using (FileStream stream = new FileStream(Path.GetFullPath(@"DefaultTableSRanges\defaultSRangesReconENG.json"), FileMode.Open))
                        {
                            defaultSRanges = (TableSectorsRanges[])jsonFormat.ReadObject(stream);
                        }
                        break;
                }

                return defaultSRanges.ToList();
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Загрузить Сектора и диапазоны РП по умолчанию из файла defaultSRangesSuppr.json
        /// </summary>
        /// <returns></returns>
        public List<TableSectorsRanges> LoadDefaultSRangesSuppr(ModelsTablesDBLib.Languages languages)
        {
            DataContractJsonSerializer jsonFormat = new DataContractJsonSerializer(typeof(TableSectorsRanges[]));
            try
            {
                TableSectorsRanges[] defaultSRanges = new TableSectorsRanges[] { };
                switch (languages)
                {
                    case ModelsTablesDBLib.Languages.Rus:

                        switch (PropGlobalProperty.Amplifiers)
                        {
                            case 0:
                                using (FileStream stream = new FileStream(Path.GetFullPath(@"DefaultTableSRanges\defaultSRangesSupprRUS.json"), FileMode.Open))
                                {
                                    defaultSRanges = (TableSectorsRanges[])jsonFormat.ReadObject(stream);
                                }
                                break;
                              
                            case 1:
                                using (FileStream stream = new FileStream(Path.GetFullPath(@"DefaultTableSRanges\defaultSRangesSupprModifRUS.json"), FileMode.Open))
                                {
                                    defaultSRanges = (TableSectorsRanges[])jsonFormat.ReadObject(stream);
                                }
                                break;
                        }
                        break;

                    case ModelsTablesDBLib.Languages.Eng:

                        switch (PropGlobalProperty.Amplifiers)
                        {
                            case 0:
                                using (FileStream stream = new FileStream(Path.GetFullPath(@"DefaultTableSRanges\defaultSRangesSupprENG.json"), FileMode.Open))
                                {
                                    defaultSRanges = (TableSectorsRanges[])jsonFormat.ReadObject(stream);
                                }
                                break;

                            case 1:
                                using (FileStream stream = new FileStream(Path.GetFullPath(@"DefaultTableSRanges\defaultSRangesSupprModifENG.json"), FileMode.Open))
                                {
                                    defaultSRanges = (TableSectorsRanges[])jsonFormat.ReadObject(stream);
                                }
                                break;
                        }
                        break;

                    default:
                        using (FileStream stream = new FileStream(Path.GetFullPath(@"DefaultTableSRanges\defaultSRangesSupprReconENG.json"), FileMode.Open))
                        {
                            defaultSRanges = (TableSectorsRanges[])jsonFormat.ReadObject(stream);
                        }
                        break;
                }

                return defaultSRanges.ToList();
            }
            catch
            {
                return null;
            }
        }

        private bool IsSelectedRowEmpty()
        {
            try
            {
                if (((GlobalSRanges)dgvSRanges.DataContext).SelectedSRanges != null)
                {
                    if (((GlobalSRanges)dgvSRanges.DataContext).SelectedSRanges.Id == -2)
                        return false;
                }
            }
            catch { }

            return true;
        }

        /// <summary>
        /// Добавить таблицу для отчета
        /// </summary>
        /// <returns></returns>
        private string[,] AddSRangesToTable()
        {
            int Columns = 6;// dgvSRanges.Columns.Count;
            int Rows = ((GlobalSRanges)dgvSRanges.DataContext).CollectionSRanges.Count(x => x.Id > 0) + 1;
            string[,] Table = new string[Rows, Columns];

            try
            {
                Table[0, 0] = SHeaders.headerNumAJS;
                Table[0, 1] = SHeaders.headerFreqMin;
                Table[0, 2] = SHeaders.headerFreqMax;
                Table[0, 3] = SHeaders.headerAngleMin;
                Table[0, 4] = SHeaders.headerAngleMax;
                Table[0, 5] = SHeaders.headerNote;
                for (int i = 1; i < Rows; i++)
                {
                    Table[i, 0] = ((GlobalSRanges)dgvSRanges.DataContext).CollectionSRanges[i - 1].NumberASP.ToString();
                    Table[i, 1] = ((GlobalSRanges)dgvSRanges.DataContext).CollectionSRanges[i - 1].FreqMinKHz.ToString();
                    Table[i, 2] = ((GlobalSRanges)dgvSRanges.DataContext).CollectionSRanges[i - 1].FreqMaxKHz.ToString();
                    Table[i, 3] = ((GlobalSRanges)dgvSRanges.DataContext).CollectionSRanges[i - 1].AngleMin.ToString();
                    Table[i, 4] = ((GlobalSRanges)dgvSRanges.DataContext).CollectionSRanges[i - 1].AngleMax.ToString();
                    Table[i, 5] = ((GlobalSRanges)dgvSRanges.DataContext).CollectionSRanges[i - 1].Note;
                }
            }
            catch { }

            return Table;
        }

        public void ChangeFreqHeaderMHz_kHz()
        {
            switch (PropLocalFreqMHz_kHz.FreqMHz_kHz)
            {
                case 0: // kHz
                    lHeaderFreqMin.Content = SMeaning.meaningFminkHz;
                    lHeaderFreqMax.Content = SMeaning.meaningFmaxkHz;
                    break;

                case 1: // MHz
                    lHeaderFreqMin.Content = SMeaning.meaningFminMHz;
                    lHeaderFreqMax.Content = SMeaning.meaningFmaxMHz;
                    break;
            }

        }
    }
}
