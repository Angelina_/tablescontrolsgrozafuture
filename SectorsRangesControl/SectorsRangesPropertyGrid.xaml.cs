﻿using ModelsTablesDBLib;
using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Media.Imaging;

namespace SectorsRangesControl
{
    /// <summary>
    /// Логика взаимодействия для SectorsRangesPropertyGrid.xaml
    /// </summary>
    public partial class SectorsRangesPropertyGrid : Window
    {
        private ObservableCollection<TableSectorsRanges> collectionTemp;
        public TableSectorsRanges sr { get; private set; }

        public SectorsRangesPropertyGrid(ObservableCollection<TableSectorsRanges> collectionSectorsRanges)
        {
            InitializeComponent();

            //collectionTemp = collectionSectorsRanges;
            //sr = new TableSectorsRanges();
            //propertyGrid.SelectedObject = sr;

            //Title = "Добавить запись";
            //Icon = new BitmapImage(new Uri(@"pack://application:,,,/"
            //                    + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
            //                    + ";component/"
            //                    + "Resources/AddRec.ico", UriKind.Absolute));
        }

        public SectorsRangesPropertyGrid(ObservableCollection<TableSectorsRanges> collectionSectorsRanges, TableSectorsRanges range)
        {
            InitializeComponent();

            //collectionTemp = collectionSectorsRanges;
            //sr = range;
            //propertyGrid.SelectedObject = sr;

            //Title = "Изменить запись";
            //Icon = new BitmapImage(new Uri(@"pack://application:,,,/"
            //                    + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
            //                    + ";component/"
            //                    + "Resources/ChangeRec.ico", UriKind.Absolute));
        }

        private void ButtonApple_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void ButtonNoApple_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}
