﻿using ModelsTablesDBLib;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls.WpfPropertyGrid;
using System.Windows.Forms;
using System.Windows.Media.Imaging;
using TableEvents;
using TableOperations;
using ValuesCorrectLib;

namespace SectorsRangesControl
{
    /// <summary>
    /// Логика взаимодействия для SectorsRangesProperty.xaml
    /// </summary>
    public partial class SectorsRangesProperty : Window
    {
        private ObservableCollection<TableSectorsRanges> collectionTemp;
        public TableSectorsRanges sr { get; private set; }

        public SectorsRangesProperty(ObservableCollection<TableSectorsRanges> collectionSectorsRanges)
        {
            try
            {
                InitializeComponent();

                
                InitEditors();

                collectionTemp = collectionSectorsRanges;
                sr = new TableSectorsRanges();
                sr.AngleMin = 0;
                sr.AngleMax = 360;
                propertyGrid.SelectedObject = sr;

               
                //Title = "Add record";
                Title = SMeaning.meaningAddRecord;
                Icon = new BitmapImage(new Uri(@"pack://application:,,,/"
                                    + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                                    + ";component/"
                                    + "Resources/AddRec.ico", UriKind.Absolute));
                InitProperty();

                //ChangeCategories();
            }
            catch { }
        }

        public SectorsRangesProperty(ObservableCollection<TableSectorsRanges> collectionSectorsRanges, TableSectorsRanges range)
        {
            try
            {
                InitializeComponent();

                InitEditors();

                collectionTemp = collectionSectorsRanges;
                sr = range;
                switch(PropLocalFreqMHz_kHz.FreqMHz_kHz)
                {
                    case 0: // kHz
                        sr.FreqMinKHz = Math.Round(sr.FreqMinKHz, 1);
                        sr.FreqMaxKHz = Math.Round(sr.FreqMaxKHz, 1);
                        break;

                    case 1: // MHz
                        sr.FreqMinKHz = Math.Round(sr.FreqMinKHz / 1000, 1);
                        sr.FreqMaxKHz = Math.Round(sr.FreqMaxKHz / 1000, 1);
                        break;

                    default:
                        sr.FreqMinKHz = Math.Round(sr.FreqMinKHz, 1);
                        sr.FreqMaxKHz = Math.Round(sr.FreqMaxKHz, 1);
                        break;
                }
                propertyGrid.SelectedObject = sr;

                //Title = "Change record";
                Title = SMeaning.meaningChangeRecord;
                Icon = new BitmapImage(new Uri(@"pack://application:,,,/"
                                    + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                                    + ";component/"
                                    + "Resources/ChangeRec.ico", UriKind.Absolute));

                //ChangeCategories();
                InitProperty();
            }
            catch { }
        }

        public SectorsRangesProperty()
        {
            try
            {
                InitializeComponent();

                InitEditors();
                //ChangeCategories();
                InitProperty();
            }
            catch { }
        }

        private void ButtonApply_Click(object sender, RoutedEventArgs e)
        {
            if (IsAddClick((TableSectorsRanges)propertyGrid.SelectedObject) != null)
            {
                DialogResult = true;
            }
        }

        public TableSectorsRanges IsAddClick(TableSectorsRanges SRangesWindow)
        {
            if (PropLocalFreqMHz_kHz.FreqMHz_kHz == 1)
            {
                SRangesWindow.FreqMinKHz = SRangesWindow.FreqMinKHz * 1000;
                SRangesWindow.FreqMaxKHz = SRangesWindow.FreqMaxKHz * 1000;
            }

            CorrectMinMax.IsCorrectMinMax(SRangesWindow, PropNameTable.SRangesName);
            if (CorrectMinMax.IsCorrectRangeMinMax(SRangesWindow))
            {
                SRangesWindow.IsCheck = true;
                return SRangesWindow;
            }
           
            return null;
        }

        private void ButtonNoApply_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void InitProperty()
        {
            foreach (var property in propertyGrid.Properties)
            {
                try
                {
                    if (property.IsBrowsable == false) { continue; }

                    if (property.PropertyValue.SubProperties.Count != 0)
                    {
                        foreach (var subProperty in property.PropertyValue.SubProperties)
                            subProperty.PropertyValue.PropertyValueException += PropertyGridSubException;
                        continue;
                    }
                    property.PropertyValue.PropertyValueException += PropertyGridException;
                }
                catch (Exception ex)
                { System.Windows.MessageBox.Show(ex.Message); }
            }
        }

        private void PropertyGridSubException(object sender, ValueExceptionEventArgs e)
        {
            propertyGrid.Properties[(sender as PropertyItemValue).ParentProperty.ParentValue.ParentProperty.Name].SetValue((sender as PropertyItemValue).Value);
        }

        private void PropertyGridException(object sender, ValueExceptionEventArgs e)
        {
            propertyGrid.Properties[(sender as PropertyItemValue).ParentProperty.Name].SetValue((sender as PropertyItemValue).Value);
        }

        private void ChangeCategories()
        {
            propertyGrid.Categories.FirstOrDefault(t => t.Name == "Сектор").HeaderCategoryName = "Sector";
            propertyGrid.Categories.FirstOrDefault(t => t.Name == "Диапазон").HeaderCategoryName = "Band";
            propertyGrid.Categories.FirstOrDefault(t => t.Name == "Прочее").HeaderCategoryName = "Other";

            propertyGrid.Properties[nameof(TableSectorsRanges.NumberASP)].DisplayName = "№ AJS";
            propertyGrid.Properties[nameof(TableSectorsRanges.FreqMinKHz)].DisplayName = "F min, MHz";
            propertyGrid.Properties[nameof(TableSectorsRanges.FreqMaxKHz)].DisplayName = "F max, MHz";
            propertyGrid.Properties[nameof(TableSectorsRanges.AngleMin)].DisplayName = "θ min, °";
            propertyGrid.Properties[nameof(TableSectorsRanges.AngleMax)].DisplayName = "θ max, °";
            propertyGrid.Properties[nameof(TableSectorsRanges.Note)].DisplayName = "Note";
        }

        public void SetLanguagePropertyGrid(Languages language)
        {
            TranslatorTables.ChangeLanguagePropertyGrid(language, propertyGrid);

            SetDisplayNameFreqs();
        }

        private void SetDisplayNameFreqs()
        {
            switch(PropLocalFreqMHz_kHz.FreqMHz_kHz)
            {
                case 0: // kHz
                    propertyGrid.Properties[nameof(TableFreqSpec.FreqMinKHz)].DisplayName = SMeaning.meaningFminkHz;
                    propertyGrid.Properties[nameof(TableFreqSpec.FreqMaxKHz)].DisplayName = SMeaning.meaningFmaxkHz;
                    break;

                case 1: // MHz
                    propertyGrid.Properties[nameof(TableSectorsRanges.FreqMinKHz)].DisplayName = SMeaning.meaningFminMHz;
                    propertyGrid.Properties[nameof(TableSectorsRanges.FreqMaxKHz)].DisplayName = SMeaning.meaningFmaxMHz;
                    break;

                default:
                    break;
            }
        }

        private void InitEditors()
        {
            propertyGrid.Editors.Add(new GeneralSRangesNoteEditor(nameof(sr.Note), typeof(TableSectorsRanges)));
        }

        private void gridProperty_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Enter)
            {
                if (IsAddClick((TableSectorsRanges)propertyGrid.SelectedObject) != null)
                {
                    DialogResult = true;
                }
            }
        }
       
    }
}

