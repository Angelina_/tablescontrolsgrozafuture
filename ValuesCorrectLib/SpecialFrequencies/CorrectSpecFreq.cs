﻿using ModelsTablesDBLib;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace ValuesCorrectLib
{
    public class CorrectSpecFreq
    {
        /// <summary>
        /// Проверка на возможность добавления записей в коллекцию
        /// </summary>
        /// <param name="collectionTemp"> текущий список записей, в который добавить </param>
        /// <param name="collectionFrom"> что добавить </param>
        /// <returns> true - успешно, false - нет </returns>
        public static bool IsAddRecordsToCollection(ObservableCollection<TableFreqSpec> collectionTemp, ObservableCollection<TableFreqSpec> collectionFrom)
        {
            bool bAdd = false;

            try
            {
                List<TableFreqSpec> listTemp = new List<TableFreqSpec>();
                listTemp.AddRange(collectionTemp);

                List<TableFreqSpec> listFrom = new List<TableFreqSpec>();
                listFrom.AddRange(collectionFrom);

                for (int i = 0; i < listFrom.Count; i++)
                {
                    if (IsCorrectFreqMinMax(listFrom[i].FreqMinKHz, listFrom[i].FreqMaxKHz))
                    {
                        if (IsCorrectJoinRanges(listTemp, listFrom[i]))
                        {
                            bAdd = true;
                        }
                    }
                }
            }
            catch { bAdd = false; }

            return bAdd;
        }

        /// <summary>
        /// Проверка на возможность добавления записи в коллекцию
        /// </summary>
        /// <param name="collectionTemp"> текущий список записей, в который добавить </param>
        /// <param name="addRec"> что добавить </param>
        /// <returns> true - успешно, false - нет </returns>
        public static bool IsAddRecordToCollection(ObservableCollection<TableFreqSpec> collectionTemp, TableFreqSpec addRec)
        {
            bool bAdd = false;

            try
            {
                List<TableFreqSpec> listTemp = new List<TableFreqSpec>();
                listTemp.AddRange(collectionTemp);

                if (IsCorrectFreqMinMax(addRec.FreqMinKHz, addRec.FreqMaxKHz))
                {
                    if (IsCorrectJoinRanges(listTemp, addRec))
                    {
                        bAdd = true;
                    }
                }
            }
            catch { bAdd = false; }

            return bAdd;
        }

        /// <summary>
        /// Проверка на возможность изменения записи в коллекции
        /// </summary>
        /// <param name="collectionTemp"> список записей </param>
        /// <param name="record"> запись в списке, которую необходимо изменить </param>
        /// <param name="recordReplace"> запись, на которую необходимо изменить </param>
        /// <returns></returns>
        public static bool IsChangeRecordToCollection(ObservableCollection<TableFreqSpec> collectionTemp, TableFreqSpec record, TableFreqSpec recordReplace)
        {
            bool bChange = false;
            int ind = -1;
            try
            {
                List<TableFreqSpec> listTemp = new List<TableFreqSpec>();
                listTemp.AddRange(collectionTemp);

                ind = listTemp.FindIndex(x => x.FreqMinKHz == record.FreqMinKHz && x.FreqMaxKHz == record.FreqMaxKHz);

                if (ind != -1)
                {
                    if (AmountRecordsList(listTemp) > 1)
                    {
                        if (IsCorrectFreqMinMax(recordReplace.FreqMinKHz, recordReplace.FreqMaxKHz))
                        {
                            if (IsCorrectJoinRanges(listTemp, recordReplace))
                            {
                                bChange = true;
                            }
                        }
                    }
                    else
                    {
                        listTemp.Add(record);
                        if (IsCorrectFreqMinMax(recordReplace.FreqMinKHz, recordReplace.FreqMaxKHz))
                        {
                            if (IsCorrectJoinRanges(listTemp, recordReplace))
                            {
                                bChange = true;
                            }
                        }
                    }
                }
            }
            catch { bChange = false; }

            return bChange;
        }

        /// <summary>
        /// Проверка на возможность объединения диапазонов Специальных частот
        /// </summary>
        /// <param name="listSpecFreq"> текущие значения </param>
        /// <param name="specFreq"> значения, которые надо проверить </param>
        /// <returns> true - успешно, false - нет </returns>
        public static bool IsCorrectJoinRanges(List<TableFreqSpec> listSpecFreq, TableFreqSpec specFreq)
        {
            if (specFreq.FreqMinKHz == specFreq.FreqMaxKHz)
                return true;

            for (int i = 0; i < listSpecFreq.Count; i++)
            {
                int cmp1 = listSpecFreq[i].FreqMinKHz.CompareTo(specFreq.FreqMinKHz);
                int cmp2 = listSpecFreq[i].FreqMaxKHz.CompareTo(specFreq.FreqMinKHz);
                int cmp3 = listSpecFreq[i].FreqMinKHz.CompareTo(specFreq.FreqMaxKHz);
                int cmp4 = listSpecFreq[i].FreqMaxKHz.CompareTo(specFreq.FreqMaxKHz);

                if (((cmp1 < 0) || (cmp1 == 0)) && (cmp3 < 0) && (cmp2 > 0) && ((cmp4 < 0) || (cmp4 == 0)))
                {
                    MessageCrossingRange(listSpecFreq[i].FreqMinKHz, listSpecFreq[i].FreqMaxKHz);
                    return false;
                }

                else if (((cmp1 > 0) || (cmp1 == 0)) && (cmp3 < 0) && (cmp2 > 0) && ((cmp4 > 0) || (cmp4 == 0)))
                {
                    MessageCrossingRange(listSpecFreq[i].FreqMinKHz, listSpecFreq[i].FreqMaxKHz);
                    return false;
                }

                else if (((cmp1 < 0) || (cmp1 == 0)) && (cmp3 < 0) && (cmp2 > 0) && ((cmp4 > 0) || (cmp4 == 0)))
                {
                    MessageCrossingRange(listSpecFreq[i].FreqMinKHz, listSpecFreq[i].FreqMaxKHz);
                    return false;
                }

                else if (((cmp1 > 0) || (cmp1 == 0)) && (cmp3 < 0) && (cmp2 > 0) && ((cmp4 < 0) || (cmp4 == 0)))
                {
                    MessageCrossingRange(listSpecFreq[i].FreqMinKHz, listSpecFreq[i].FreqMaxKHz);
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Сообщение о пересечении диапазона с заданным
        /// </summary>
        /// <param name="freqMin"> начальное значение частоты </param>
        /// <param name="freqMax"> конечное значение частоты</param>
        //private static void MessageCrossingRange(int freqMin, int freqMax)
        private static void MessageCrossingRange(double dFreqMin, double dFreqMax)
        {
            MessageBox.Show(SMessages.mesRange + dFreqMin.ToString() + " - " + dFreqMax.ToString() +
                 SMessages.mesCrossingRange, SMessages.mesMessage, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        /// <summary>
        /// Проверка на корректность ввода значений частоты
        /// </summary>
        /// <param name="iFreqMin"> начальное значение частоты </param>
        /// <param name="iFreqMax"> конечное значение частоты </param>
        /// <returns> true - успешно, false - нет </returns>
        // private static bool IsCorrectFreqMinMax(int iFreqMin, int iFreqMax)
        private static bool IsCorrectFreqMinMax(double dFreqMin, double dFreqMax)
        {
            bool bCorrect = true;

            if (dFreqMin >= dFreqMax)
            {
                MessageBox.Show(SMessages.mesValuesMaxMin, SMessages.mesMessage, MessageBoxButtons.OK, MessageBoxIcon.Information);

                bCorrect = false;
            }

            return bCorrect;
        }

        /// <summary>
        /// Количество записей в листе
        /// </summary>
        /// <param name="listSpecFreq"></param>
        /// <returns> количество записей </returns>
        public static int AmountRecordsList(List<TableFreqSpec> listSpecFreq)
        {
            try { return listSpecFreq.Count; }
            catch { return -1; }
        }
    }
}
