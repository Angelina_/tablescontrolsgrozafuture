﻿using ModelsTablesDBLib;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Forms;

namespace ValuesCorrectLib
{
    public class CorrectSRanges
    {
        public ObservableCollection<TableSectorsRanges> CollectionSectorsRanges { get; set; }

        public CorrectSRanges()
        {
            CollectionSectorsRanges = new ObservableCollection<TableSectorsRanges> { };
        }

        /// <summary>
        /// Проверка на возможность добавления записей в коллекцию
        /// </summary>
        /// <param name="collectionTemp"> текущий список записей, в который добавить </param>
        /// <param name="collectionFrom"> что добавить </param>
        /// <returns> true - успешно, false - нет </returns>
        public static bool IsAddRecordsToCollection(ObservableCollection<TableSectorsRanges> collectionTemp, ObservableCollection<TableSectorsRanges> collectionFrom)
        {
            bool bAdd = false;

            try
            {
                List<TableSectorsRanges> listTemp = new List<TableSectorsRanges>();
                listTemp.AddRange(collectionTemp);

                List<TableSectorsRanges> listFrom = new List<TableSectorsRanges>();
                listFrom.AddRange(collectionFrom);

                for (int i = 0; i < listFrom.Count; i++)
                {
                    if (IsCorrectFreqMinMax(listFrom[i].FreqMinKHz, listFrom[i].FreqMaxKHz))
                    {
                        if (IsCorrectJoinRanges(listTemp, listFrom[i]))
                        {
                            bAdd = true;
                        }
                    }
                }
            }
            catch { bAdd = false; }

            return bAdd;
        }

        /// <summary>
        /// Проверка на возможность добавления записи в коллекцию
        /// </summary>
        /// <param name="collectionTemp"> текущий список записей, в который добавить </param>
        /// <param name="addRec"> что добавить </param>
        /// <returns> true - успешно, false - нет </returns>
        public static bool IsAddRecordToCollection(ObservableCollection<TableSectorsRanges> collectionTemp, TableSectorsRanges addRec)
        {
            bool bAdd = false;

            try
            {
                List<TableSectorsRanges> listTemp = new List<TableSectorsRanges>();
                listTemp.AddRange(collectionTemp);

                if (IsCorrectFreqMinMax(addRec.FreqMinKHz, addRec.FreqMaxKHz))
                {
                    if (IsCorrectJoinRanges(listTemp, addRec))
                    {
                        bAdd = true;
                    }
                }
            }
            catch { bAdd = false; }

            return bAdd;
        }

        /// <summary>
        /// Проверка на возможность изменения записи в коллекции
        /// </summary>
        /// <param name="collectionTemp"> список записей </param>
        /// <param name="record"> запись в списке, которую необходимо изменить </param>
        /// <param name="recordReplace"> запись, на которую необходимо изменить </param>
        /// <returns></returns>
        public static bool IsChangeRecordToCollection(ObservableCollection<TableSectorsRanges> collectionTemp, TableSectorsRanges record, TableSectorsRanges recordReplace)
        {
            bool bChange = false;
            int ind = -1;
            try
            {
                List<TableSectorsRanges> listTemp = new List<TableSectorsRanges>();
                listTemp.AddRange(collectionTemp);

                ind = listTemp.FindIndex(x => x.FreqMinKHz == record.FreqMinKHz && x.FreqMaxKHz == record.FreqMaxKHz &&
                    x.AngleMin == record.AngleMin && x.AngleMax == record.AngleMax);

                if (ind != -1)
                {
                    if (AmountRecordsList(listTemp) > 1)
                    {
                        if (IsCorrectFreqMinMax(recordReplace.FreqMinKHz, recordReplace.FreqMaxKHz))
                        {
                            if (IsCorrectJoinRanges(listTemp, recordReplace))
                            {
                                bChange = true;
                            }
                        }
                    }
                    else
                    {
                        listTemp.Add(record);
                        if (IsCorrectFreqMinMax(recordReplace.FreqMinKHz, recordReplace.FreqMaxKHz))
                        {
                            if (IsCorrectJoinRanges(listTemp, recordReplace))
                            {
                                bChange = true;
                            }
                        }
                    }
                }
            }
            catch { bChange = false; }

            return bChange;
        }

        /// <summary>
        /// Количество записей в листе
        /// </summary>
        /// <param name="listTemp"> список записей </param>
        /// <returns></returns>
        public static int AmountRecordsList(List<TableSectorsRanges> listSectorsRanges)
        {
            try { return listSectorsRanges.Count; }
            catch { return -1; }
        }

        /// <summary>
        /// Проверка на корректность ввода значений частоты
        /// </summary>
        /// <param name="iFreqMin"> начальное значение частоты </param>
        /// <param name="iFreqMax"> конечное значение частоты </param>
        /// <returns> true - успешно, false - нет </returns>
        //private static bool IsCorrectFreqMinMax(int iFreqMin, int iFreqMax)
        private static bool IsCorrectFreqMinMax(double dFreqMin, double dFreqMax)
        {
            bool bCorrect = true;

            if (dFreqMin >= dFreqMax)
            {
                MessageBox.Show(SMessages.mesValuesMaxMin, SMessages.mesMessage, MessageBoxButtons.OK, MessageBoxIcon.Information);

                bCorrect = false;
            }

            return bCorrect;
        }

        /// <summary>
        /// Проверка на возможность объединения диапазонов РР и РП 
        /// при добавлении или изменении записи
        /// </summary>
        /// <param name="listSectorsRanges"> текущие значения </param>
        /// <param name="sectorsRanges"> значения, которые надо проверить </param>
        /// <returns> true - успешно, false - нет </returns>
        private static bool IsCorrectJoinRanges(List<TableSectorsRanges> listSectorsRanges, TableSectorsRanges sectorsRanges)
        {
            for (int i = 0; i < listSectorsRanges.Count; i++)
            {
                int cmp1 = listSectorsRanges[i].FreqMinKHz.CompareTo(sectorsRanges.FreqMinKHz);
                int cmp2 = listSectorsRanges[i].FreqMaxKHz.CompareTo(sectorsRanges.FreqMinKHz);
                int cmp3 = listSectorsRanges[i].FreqMinKHz.CompareTo(sectorsRanges.FreqMaxKHz);
                int cmp4 = listSectorsRanges[i].FreqMaxKHz.CompareTo(sectorsRanges.FreqMaxKHz);

                // с = a.CompareTo(b), если результат сравнения
                // < 0 --> a < b,
                // = 0 --> a = b,
                // > 0 --> a > b.
                int cmp5 = listSectorsRanges[i].AngleMin.CompareTo(sectorsRanges.AngleMin);
                int cmp6 = listSectorsRanges[i].AngleMax.CompareTo(sectorsRanges.AngleMin);
                int cmp7 = listSectorsRanges[i].AngleMin.CompareTo(sectorsRanges.AngleMax);
                int cmp8 = listSectorsRanges[i].AngleMax.CompareTo(sectorsRanges.AngleMax);

                if (((cmp1 < 0) || (cmp1 == 0)) && (cmp3 < 0) && (cmp2 > 0) && ((cmp4 < 0) || (cmp4 == 0)))
                {
                    for (int j = 0; j < listSectorsRanges.Count; j++)
                    {
                        if ((listSectorsRanges[i].AngleMin == sectorsRanges.AngleMin) &&
                                (listSectorsRanges[i].AngleMax == sectorsRanges.AngleMax) &&
                                ((cmp1 == 0) && (cmp4 == 0)))
                        {
                            MessageBox.Show(SMessages.mesSector + listSectorsRanges[i].AngleMin.ToString() + " - " +
                                listSectorsRanges[i].AngleMax.ToString() + SMessages.mesAndRange +
                                listSectorsRanges[i].FreqMinKHz.ToString() + " - " +
                                listSectorsRanges[i].FreqMaxKHz.ToString() + SMessages.mesContainSetSector,
                                SMessages.mesMessage, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return false;
                        }
                    }

                    // если значение угла мин. < значения угла макс. 
                    if (((cmp1 == 0) && (cmp4 == 0)) && (sectorsRanges.AngleMin < sectorsRanges.AngleMax))
                    {
                        for (int j = 0; j < listSectorsRanges.Count; j++)
                        {
                            if (((cmp5 < 0) || (cmp5 == 0)) && ((cmp8 > 0) || (cmp8 == 0)))
                            {
                                MessageBox.Show(SMessages.mesSector + listSectorsRanges[i].AngleMin.ToString() + " - " +
                                    listSectorsRanges[i].AngleMax.ToString() + SMessages.mesAndRange +
                                    listSectorsRanges[i].FreqMinKHz.ToString() + " - " +
                                    listSectorsRanges[i].FreqMaxKHz.ToString() + SMessages.mesContainSetSector,
                                    SMessages.mesMessage, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                return false;
                            }
                        }
                    }
                    // если значение угла мин. < значения угла макс. 
                    else if (((cmp1 == 0) && (cmp4 == 0)) && (sectorsRanges.AngleMin > sectorsRanges.AngleMax))
                    {
                        return true;
                    }

                    else
                    {
                        MessageBox.Show(SMessages.mesRange + listSectorsRanges[i].FreqMinKHz.ToString() + " - " + listSectorsRanges[i].FreqMaxKHz.ToString() +
                           SMessages.mesCrossingRange, SMessages.mesMessage, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }
                }

                else if (((cmp1 > 0) || (cmp1 == 0)) && (cmp3 < 0) && (cmp2 > 0) && ((cmp4 > 0) || (cmp4 == 0)))
                {
                    MessageBox.Show(SMessages.mesRange + listSectorsRanges[i].FreqMinKHz.ToString() + " - " + listSectorsRanges[i].FreqMaxKHz.ToString() +
                         SMessages.mesCrossingRange, SMessages.mesMessage, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return false;
                }

                else if (((cmp1 < 0) || (cmp1 == 0)) && (cmp3 < 0) && (cmp2 > 0) && ((cmp4 > 0) || (cmp4 == 0)))
                {
                    MessageBox.Show(SMessages.mesRange + listSectorsRanges[i].FreqMinKHz.ToString() + " - " + listSectorsRanges[i].FreqMaxKHz.ToString() +
                         SMessages.mesCrossingRange, SMessages.mesMessage, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return false;
                }

                else if (((cmp1 > 0) || (cmp1 == 0)) && (cmp3 < 0) && (cmp2 > 0) && ((cmp4 < 0) || (cmp4 == 0)))
                {
                    MessageBox.Show(SMessages.mesRange + listSectorsRanges[i].FreqMinKHz.ToString() + " - " + listSectorsRanges[i].FreqMaxKHz.ToString() +
                         SMessages.mesCrossingRange, SMessages.mesMessage, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return false;
                }
            }
            return true;
        }
    }                    
}
