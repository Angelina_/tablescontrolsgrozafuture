﻿using System;

namespace ValuesCorrectLib
{
    public class ConstValues
    {
        #region Const Freqs for ToggleButton (SuppressFWS)
        public const double dFreqIridium = 1621250.0F;
        public const double dFreqInmarsat = 1542000.0F;
        public const double dFreq2_4 = 2442500.0F;
        public const double dFreq5_2 = 5185000.0F;
        public const double dFreq5_8 = 5800000.0F;
        public const double dFreqGlobalStar = 2491750.0F;
        public const double dFreq433 = 434500.0F;
        public const double dFreq868 = 866500.0F;
        public const double dFreq920 = 915000.0F;
        public const double dFreqNAV_L1 = 1568000.0F; //1575420.0F;
        public const double dFreqNAV_L2 = 1602000.0F;

        //public const double dFreqNAV_L1 = 1575420.0F;
        //public const double dFreqNAV_L2 = 1227600.0F;
        //public const double dFreqNAV_L3 = 1176450.0F;
        #endregion

        #region Const Level
        public const Int16 constLevel = -130;
        public const Int16 constThreshold = -80;
        #endregion

        #region Ranges RadioRecon Jamming
        public const double Range_0 = 0.0F;
        public const double Range_30000 = 30000.0F;
        public const double Range_1215000 = 1215000.0F;
        public const double Range_3000000 = 3000000.0F;
        public const double Range_6000000 = 6000000.0F;
        #endregion

        #region Bands
        public const int Band_30000 = 30000;
        public const int Band_80000 = 80000;
        #endregion
    }


}
