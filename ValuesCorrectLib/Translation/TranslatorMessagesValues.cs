﻿using System.Collections.Generic;

namespace ValuesCorrectLib
{
    public class FunctionsTranslate
    {
        /// <summary>
        /// Переименование сообщений при смене языка
        /// </summary>
        /// <param name="Dictionary"></param>
        public static void RenameMessages(Dictionary<string, string> TranslateDic)
        {
            if (TranslateDic.ContainsKey("mesMessage"))
                SMessages.mesMessage = TranslateDic["mesMessage"];

            if (TranslateDic.ContainsKey("mesValuesMaxMin"))
                SMessages.mesValuesMaxMin = TranslateDic["mesValuesMaxMin"];

            if (TranslateDic.ContainsKey("mesSector"))
                SMessages.mesSector = TranslateDic["mesSector"];

            if (TranslateDic.ContainsKey("mesAndRange"))
                SMessages.mesAndRange = TranslateDic["mesAndRange"];

            if (TranslateDic.ContainsKey("mesContainSetSector"))
                SMessages.mesContainSetSector = TranslateDic["mesContainSetSector"];

            if (TranslateDic.ContainsKey("mesRange"))
                SMessages.mesRange = TranslateDic["mesRange"];

            if (TranslateDic.ContainsKey("mesCrossingRange"))
                SMessages.mesCrossingRange = TranslateDic["mesCrossingRange"];

            if (TranslateDic.ContainsKey("mesLetter8IRIN"))
                SMessages.mesLetter8IRIN = TranslateDic["mesLetter8IRIN"];

            if (TranslateDic.ContainsKey("mesChangeValDeviationMin"))
                SMessages.mesChangeValDeviationMin = TranslateDic["mesChangeValDeviationMin"];

            if (TranslateDic.ContainsKey("mesNavigationBroken"))
                SMessages.mesNavigationBroken = TranslateDic["mesNavigationBroken"];

            if (TranslateDic.ContainsKey("mesInBandLetterRESJamming"))
                SMessages.mesInBandLetterRESJamming = TranslateDic["mesInBandLetterRESJamming"];

            if (TranslateDic.ContainsKey("mesCountChannelsMax"))
                SMessages.mesCountChannelsMax = TranslateDic["mesCountChannelsMax"];

            if (TranslateDic.ContainsKey("mesUnableAddRecord"))
                SMessages.mesUnableAddRecord = TranslateDic["mesUnableAddRecord"];

            if (TranslateDic.ContainsKey("mesFreqBelongBand"))
                SMessages.mesFreqBelongBand = TranslateDic["mesFreqBelongBand"];

            if (TranslateDic.ContainsKey("mesFreqRestricted"))
                SMessages.mesFreqRestricted = TranslateDic["mesFreqRestricted"];

            if (TranslateDic.ContainsKey("mesBandBelongBand"))
                SMessages.mesBandBelongBand = TranslateDic["mesBandBelongBand"];

            if (TranslateDic.ContainsKey("mesBandRestricted"))
                SMessages.mesBandRestricted = TranslateDic["mesBandRestricted"];

            if (TranslateDic.ContainsKey("mesMaxRESInLetter"))
                SMessages.mesMaxRESInLetter = TranslateDic["mesMaxRESInLetter"];

            if (TranslateDic.ContainsKey("mesIncorrectFreq"))
                SMessageError.mesIncorrectFreq = TranslateDic["mesIncorrectFreq"];

            if (TranslateDic.ContainsKey("mesErr"))
                SMessageError.mesErr = TranslateDic["mesErr"];
            
            if (TranslateDic.ContainsKey("mesErrFreqWithoutRange"))
                SMessageError.mesErrFreqWithoutRange = TranslateDic["mesErrFreqWithoutRange"];

            if (TranslateDic.ContainsKey("mesErrIncorrSetPel"))
                SMessageError.mesErrIncorrSetPel = TranslateDic["mesErrIncorrSetPel"];

            if (TranslateDic.ContainsKey("mesErrValBandwidth"))
                SMessageError.mesErrValBandwidth = TranslateDic["mesErrValBandwidth"];

            if (TranslateDic.ContainsKey("mesErrValStep"))
                SMessageError.mesErrValStep = TranslateDic["mesErrValStep"];

            if (TranslateDic.ContainsKey("mesErrFreqOutNetwork"))
                SMessageError.mesErrFreqOutNetwork = TranslateDic["mesErrFreqOutNetwork"];

            if (TranslateDic.ContainsKey("mesErrIncorrSetManip"))
                SMessageError.mesErrIncorrSetManip = TranslateDic["mesErrIncorrSetManip"];

            if (TranslateDic.ContainsKey("mesErrIncorrSetManip6_25"))
                SMessageError.mesErrIncorrSetManip6_25 = TranslateDic["mesErrIncorrSetManip6_25"];

            if (TranslateDic.ContainsKey("mesErrIncorrSetManip12_5"))
                SMessageError.mesErrIncorrSetManip12_5 = TranslateDic["mesErrIncorrSetManip12_5"];

            if (TranslateDic.ContainsKey("mesErrEnterValues"))
                SMessageError.mesErrEnterValues = TranslateDic["mesErrEnterValues"];
        }

        /// <summary>
        /// Переименование значений при смене языка
        /// </summary>
        /// <param name="TranslateDic"></param>
        public static void RenameMeaning(Dictionary<string, string> TranslateDic)
        {
            if (TranslateDic.ContainsKey("meaningAddRecord"))
                SMeaning.meaningAddRecord = TranslateDic["meaningAddRecord"];

            if (TranslateDic.ContainsKey("meaningChangeRecord"))
                SMeaning.meaningChangeRecord = TranslateDic["meaningChangeRecord"];

            if (TranslateDic.ContainsKey("meaningOwn"))
                SMeaning.meaningOwn = TranslateDic["meaningOwn"];

            if (TranslateDic.ContainsKey("meaningLinked"))
                SMeaning.meaningLinked = TranslateDic["meaningLinked"];

            if (TranslateDic.ContainsKey("meaningAutonomous"))
                SMeaning.meaningAutonomous = TranslateDic["meaningAutonomous"];

             if (TranslateDic.ContainsKey("meaningHz"))
                SMeaning.meaningHz = TranslateDic["meaningHz"];
 
            if (TranslateDic.ContainsKey("meaningkHz"))
                SMeaning.meaningkHz = TranslateDic["meaningkHz"];

            if (TranslateDic.ContainsKey("meaningMHz"))
                SMeaning.meaningMHz = TranslateDic["meaningMHz"];

            if (TranslateDic.ContainsKey("meaningmks"))
                SMeaning.meaningmks = TranslateDic["meaningmks"];

            if (TranslateDic.ContainsKey("meaningms"))
                SMeaning.meaningms = TranslateDic["meaningms"];
            
            if (TranslateDic.ContainsKey("meaningFMHz"))
                SMeaning.meaningFMHz = TranslateDic["meaningFMHz"];

            if (TranslateDic.ContainsKey("meaningFminMHz"))
                SMeaning.meaningFminMHz = TranslateDic["meaningFminMHz"];

            if (TranslateDic.ContainsKey("meaningFmaxMHz"))
                SMeaning.meaningFmaxMHz = TranslateDic["meaningFmaxMHz"];

            if (TranslateDic.ContainsKey("meaningFkHz"))
                SMeaning.meaningFkHz = TranslateDic["meaningFkHz"];

            if (TranslateDic.ContainsKey("meaningFminkHz"))
                SMeaning.meaningFminkHz = TranslateDic["meaningFminkHz"];

            if (TranslateDic.ContainsKey("meaningFmaxkHz"))
                SMeaning.meaningFmaxkHz = TranslateDic["meaningFmaxkHz"];

            if (TranslateDic.ContainsKey("meaningShum"))
                SMeaning.meaningShum = TranslateDic["meaningShum"];

            if (TranslateDic.ContainsKey("meaningNes"))
                SMeaning.meaningNes = TranslateDic["meaningNes"];

            if (TranslateDic.ContainsKey("meaningAMn"))
                SMeaning.meaningAMn = TranslateDic["meaningAMn"];

            if (TranslateDic.ContainsKey("meaningFMn"))
                SMeaning.meaningFMn = TranslateDic["meaningFMn"];

            if (TranslateDic.ContainsKey("meaningChMn2"))
                SMeaning.meaningChMn2 = TranslateDic["meaningChMn2"];

            if (TranslateDic.ContainsKey("meaningAMChM"))
                SMeaning.meaningAMChM = TranslateDic["meaningAMChM"];

            if (TranslateDic.ContainsKey("meaningChMn4"))
                SMeaning.meaningChMn4 = TranslateDic["meaningChMn4"];

            if (TranslateDic.ContainsKey("meaningChMn8"))
                SMeaning.meaningChMn8 = TranslateDic["meaningChMn8"];

            if (TranslateDic.ContainsKey("meaningChM"))
                SMeaning.meaningChM = TranslateDic["meaningChM"];

            if (TranslateDic.ContainsKey("meaningChMn"))
                SMeaning.meaningChMn = TranslateDic["meaningChMn"];

            if (TranslateDic.ContainsKey("meaningShPS"))
                SMeaning.meaningShPS = TranslateDic["meaningShPS"];

            if (TranslateDic.ContainsKey("meaningUnkn"))
                SMeaning.meaningUnkn = TranslateDic["meaningUnkn"];

            if (TranslateDic.ContainsKey("meaningCoord"))
                SMeaning.meaningCoord = TranslateDic["meaningCoord"];

            if (TranslateDic.ContainsKey("meaningInterferenceParam"))
                SMeaning.meaningInterferenceParam = TranslateDic["meaningInterferenceParam"];

            if (TranslateDic.ContainsKey("meaningInterferenceParam"))
                SMeaning.meaningInterferenceParam = TranslateDic["meaningInterferenceParam"];

            if (TranslateDic.ContainsKey("meaningNAV"))
                SMeaning.meaningNAV = TranslateDic["meaningNAV"];

            if (TranslateDic.ContainsKey("meaningSPUF"))
                SMeaning.meaningSPUF = TranslateDic["meaningSPUF"];

            if (TranslateDic.ContainsKey("meaningSignA"))
                SMeaning.meaningSignA = TranslateDic["meaningSignA"];

            if (TranslateDic.ContainsKey("meaningSignB"))
                SMeaning.meaningSignB = TranslateDic["meaningSignB"];

            if (TranslateDic.ContainsKey("meaningSignC"))
                SMeaning.meaningSignC = TranslateDic["meaningSignC"];

            if (TranslateDic.ContainsKey("meaningSignPC"))
                SMeaning.meaningSignPC = TranslateDic["meaningSignPC"];

            if (TranslateDic.ContainsKey("meaningSignRadioRecevier"))
                SMeaning.meaningSignRadioRecevier = TranslateDic["meaningSignRadioRecevier"];

            if (TranslateDic.ContainsKey("meaningSignOtherTable"))
                SMeaning.meaningSignOtherTable = TranslateDic["meaningSignOtherTable"];

            if (TranslateDic.ContainsKey("meaningSignButton"))
                SMeaning.meaningSignButton = TranslateDic["meaningSignButton"];

            if (TranslateDic.ContainsKey("meaningSignPanorama"))
                SMeaning.meaningSignPanorama = TranslateDic["meaningSignPanorama"];

            if (TranslateDic.ContainsKey("meaningSignCicada"))
                SMeaning.meaningSignCicada = TranslateDic["meaningSignCicada"];
        }

        /// <summary>
        /// Переименование видов модуляции при смене языка
        /// </summary>
        /// <param name="TranslateDic"></param>
        public static void RenameParams(Dictionary<string, string> TranslateDic)
        {
            if (TranslateDic.ContainsKey("paramChMSh"))
                STypeModulation.paramChMSh = TranslateDic["paramChMSh"];

            if (TranslateDic.ContainsKey("paramChM2"))
                STypeModulation.paramChM2 = TranslateDic["paramChM2"];

            if (TranslateDic.ContainsKey("paramChM4"))
                STypeModulation.paramChM4 = TranslateDic["paramChM4"];

            if (TranslateDic.ContainsKey("paramChM8"))
                STypeModulation.paramChM8 = TranslateDic["paramChM8"];

            if (TranslateDic.ContainsKey("paramFMn"))
                STypeModulation.paramFMn = TranslateDic["paramFMn"];

            if (TranslateDic.ContainsKey("paramFMn4"))
                STypeModulation.paramFMn4 = TranslateDic["paramFMn4"];

            if (TranslateDic.ContainsKey("paramFMn8"))
                STypeModulation.paramFMn8 = TranslateDic["paramFMn8"];

            if (TranslateDic.ContainsKey("paramZSh"))
                STypeModulation.paramZSh = TranslateDic["paramZSh"];

            if (TranslateDic.ContainsKey("paramLChM"))
                STypeModulation.paramLChM = TranslateDic["paramLChM"];
        }

        /// <summary>
        /// Переименование заголовков для вывода в текстовый документ при смене языка
        /// </summary>
        /// <param name="TranslateDic"></param>
        public static void RenameHeaders(Dictionary<string, string> TranslateDic)
        {
            if (TranslateDic.ContainsKey("headerNumAJS"))
                SHeaders.headerNumAJS = TranslateDic["headerNumAJS"];

            if (TranslateDic.ContainsKey("headerNumSituation"))
                SHeaders.headerNumSituation = TranslateDic["headerNumSituation"];

            if (TranslateDic.ContainsKey("headerConnect"))
                SHeaders.headerConnect = TranslateDic["headerConnect"];

            if (TranslateDic.ContainsKey("headerIPaddress"))
                SHeaders.headerIPaddress = TranslateDic["headerIPaddress"];

            if (TranslateDic.ContainsKey("headerPort"))
                SHeaders.headerPort = TranslateDic["headerPort"];

            if (TranslateDic.ContainsKey("headerMode"))
                SHeaders.headerMode = TranslateDic["headerMode"];

            if (TranslateDic.ContainsKey("headerCallSign"))
                SHeaders.headerCallSign = TranslateDic["headerCallSign"];

            if (TranslateDic.ContainsKey("headerRole"))
                SHeaders.headerRole = TranslateDic["headerRole"];

            if (TranslateDic.ContainsKey("headerType"))
                SHeaders.headerType = TranslateDic["headerType"];

            if (TranslateDic.ContainsKey("headerAntennaJ"))
                SHeaders.headerAntennaJ = TranslateDic["headerAntennaJ"];

            if (TranslateDic.ContainsKey("headerAntennaRR"))
                SHeaders.headerAntennaRR = TranslateDic["headerAntennaRR"];

            if (TranslateDic.ContainsKey("headerLatLon"))
                SHeaders.headerLatLon = TranslateDic["headerLatLon"];

            if (TranslateDic.ContainsKey("headerAlt"))
                SHeaders.headerAlt = TranslateDic["headerAlt"];

            if (TranslateDic.ContainsKey("headerRRC1"))
                SHeaders.headerRRC1 = TranslateDic["headerRRC1"];

            if (TranslateDic.ContainsKey("headerRRC2"))
                SHeaders.headerRRC2 = TranslateDic["headerRRC2"];

            if (TranslateDic.ContainsKey("headerLPA1_3"))
                SHeaders.headerLPA1_3 = TranslateDic["headerLPA1_3"];

            if (TranslateDic.ContainsKey("headerLPA2_4"))
                SHeaders.headerLPA2_4 = TranslateDic["headerLPA2_4"];

            if (TranslateDic.ContainsKey("headerLPA5_10"))
                SHeaders.headerLPA5_10 = TranslateDic["headerLPA5_10"];

            if (TranslateDic.ContainsKey("headerLPA5_7"))
                SHeaders.headerLPA5_7 = TranslateDic["headerLPA5_7"];
            
            if (TranslateDic.ContainsKey("headerLPA5_9"))
                SHeaders.headerLPA5_9 = TranslateDic["headerLPA5_9"];
            
            if (TranslateDic.ContainsKey("headerLPA10"))
                SHeaders.headerLPA10 = TranslateDic["headerLPA10"];
            
            if (TranslateDic.ContainsKey("headerBPSS"))
                SHeaders.headerBPSS = TranslateDic["headerBPSS"];

            if (TranslateDic.ContainsKey("headerSignature"))
                SHeaders.headerSignature = TranslateDic["headerSignature"];

            if (TranslateDic.ContainsKey("headerSign"))
                SHeaders.headerSign = TranslateDic["headerSign"];

            if (TranslateDic.ContainsKey("headerFreqMin"))
                SHeaders.headerFreqMin = TranslateDic["headerFreqMin"];

            if (TranslateDic.ContainsKey("headerFreqMax"))
                SHeaders.headerFreqMax = TranslateDic["headerFreqMax"];

            if (TranslateDic.ContainsKey("headerAngleMin"))
                SHeaders.headerAngleMin = TranslateDic["headerAngleMin"];

            if (TranslateDic.ContainsKey("headerAngleMax"))
                SHeaders.headerAngleMax = TranslateDic["headerAngleMax"];

            if (TranslateDic.ContainsKey("headerNote"))
                SHeaders.headerNote = TranslateDic["headerNote"];

            if (TranslateDic.ContainsKey("headerDeltaF"))
                SHeaders.headerDeltaF = TranslateDic["headerDeltaF"];

            if (TranslateDic.ContainsKey("headerTime"))
                SHeaders.headerTime = TranslateDic["headerTime"];

            if (TranslateDic.ContainsKey("headerStep"))
                SHeaders.headerStep = TranslateDic["headerStep"];

            if (TranslateDic.ContainsKey("headerPulseWidth"))
                SHeaders.headerPulseWidth = TranslateDic["headerPulseWidth"];

            if (TranslateDic.ContainsKey("headerNumFreq"))
                SHeaders.headerNumFreq = TranslateDic["headerNumFreq"];

            if (TranslateDic.ContainsKey("headerJ_AJS"))
                SHeaders.headerJ_AJS = TranslateDic["headerJ_AJS"];

            if (TranslateDic.ContainsKey("headerBearing"))
                SHeaders.headerBearing = TranslateDic["headerBearing"];

            if (TranslateDic.ContainsKey("headerNum"))
                SHeaders.headerNum = TranslateDic["headerNum"];

            if (TranslateDic.ContainsKey("headerRMSE"))
                SHeaders.headerRMSE = TranslateDic["headerRMSE"];

            if (TranslateDic.ContainsKey("headerLevel"))
                SHeaders.headerLevel = TranslateDic["headerLevel"];

            if (TranslateDic.ContainsKey("headerDistance"))
                SHeaders.headerDistance = TranslateDic["headerDistance"];

            if (TranslateDic.ContainsKey("headerRI_AJS"))
                SHeaders.headerRI_AJS = TranslateDic["headerRI_AJS"];

            if (TranslateDic.ContainsKey("headerFreq"))
                SHeaders.headerFreq = TranslateDic["headerFreq"];

            if (TranslateDic.ContainsKey("headerCount"))
                SHeaders.headerCount = TranslateDic["headerCount"];

            if (TranslateDic.ContainsKey("headerJammerParams"))
                SHeaders.headerJammerParams = TranslateDic["headerJammerParams"];

            if (TranslateDic.ContainsKey("headerSSR"))
                SHeaders.headerSSR = TranslateDic["headerSSR"];

            if (TranslateDic.ContainsKey("headerCtrl"))
                SHeaders.headerCtrl = TranslateDic["headerCtrl"];

            if (TranslateDic.ContainsKey("headerJam"))
                SHeaders.headerJam = TranslateDic["headerJam"];

            if (TranslateDic.ContainsKey("headerRad"))
                SHeaders.headerRad = TranslateDic["headerRad"];

            if (TranslateDic.ContainsKey("headerLetters"))
                SHeaders.headerLetters = TranslateDic["headerLetters"];

            if (TranslateDic.ContainsKey("headerAJS"))
                SHeaders.headerAJS = TranslateDic["headerAJS"];

            if (TranslateDic.ContainsKey("headerPriority"))
                SHeaders.headerPriority = TranslateDic["headerPriority"];
        }
    }
}
