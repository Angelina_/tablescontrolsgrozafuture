﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ValuesCorrectLib
{
    #region Сообщения об ошибках
    public struct SMessageError
    {
        public static string mesErr;
        public static string mesIncorrectFreq;
        public static string mesErrFreqWithoutRange;
        public static string mesErrIncorrSetPel;
        public static string mesErrValBandwidth;
        public static string mesErrValStep;
        public static string mesErrFreqOutNetwork;
        public static string mesErrIncorrSetManip;
        public static string mesErrIncorrSetManip6_25;
        public static string mesErrIncorrSetManip12_5;
        public static string mesErrEnterValues;

        public static void InitSMessageError()
        {
            mesErr = "Ошибка!";
            mesIncorrectFreq = "Неверно задана чатота!";
            mesErrFreqWithoutRange = "Частота вне диапазона работы станции!";
            mesErrIncorrSetPel = "Неверно задан пеленг!";
            mesErrValBandwidth = "Значение ширины полосы задается от 0 до 1000!";
            mesErrValStep = "Значение шага сетки задается от 1 до 30000!";
            mesErrFreqOutNetwork = "Частота не принадлежит выбранной сети!";
            mesErrIncorrSetManip = "Для выбранной девиации значение манипуляции должно быть больше!";
            mesErrIncorrSetManip6_25 = "Для заданной частоты значение манипуляции не должно превышать 6,25 МГц!";
            mesErrIncorrSetManip12_5 = "Для заданной частоты значение манипуляции не должно превышать 12,5 МГц!";
            mesErrEnterValues = "Введите значения!";
        }
    }
    #endregion

    #region Сообщения
    public struct SMessages
    {
        public static string mesMessage;
        public static string mesValuesMaxMin;
        public static string mesSector;
        public static string mesAndRange;
        public static string mesContainSetSector;
        public static string mesRange;
        public static string mesCrossingRange;
        public static string mesLetter8IRIN;
        public static string mesChangeValDeviationMin;
        public static string mesNavigationBroken;
        public static string mesInBandLetterRESJamming;
        public static string mesCountChannelsMax;
        public static string mesUnableAddRecord;
        public static string mesFreqBelongBand;
        public static string mesFreqRestricted;
        public static string mesBandBelongBand;
        public static string mesBandRestricted;
        public static string mesMaxRESInLetter;

        public static void InitSMessages()
        {
            mesMessage = "Сообщение!";
            mesValuesMaxMin = "Значение поля 'F мин.' должно быть меньше значения поля 'F макс.'!";
            mesSector = "Сектор ";
            mesAndRange = " и диапазон ";
            mesContainSetSector = " содержит значения заданного сектора!";
            mesRange = "Диапазон ";
            mesCrossingRange = " пересекается с заданным диапазоном! Объедините диапазоны или разбейте их на несколько!";
            mesLetter8IRIN = "В диапазоне литеры 8 осуществляется подавление IRIDIUM (INMARSAT)!";
            mesChangeValDeviationMin = "Значение девиации изменено до минимального возможного значения! Девиация (+/-), кГц = ";
            mesNavigationBroken = "Будет нарушена навигация в радиусе 70 км! Выполнить команду?";
            mesInBandLetterRESJamming = "ИРИ для подавления уже имеются в диапазоне литеры  ";
            mesCountChannelsMax = "Суммарное количество каналов превышает допустимое!";
            mesUnableAddRecord = "Невозможно добавить запись! Количество записей превысит допустимое!";
            mesFreqBelongBand = "Частота не принадлежит диапазону РП!";
            mesFreqRestricted = "Частота не может быть добавлена, так как запрещенная!";
            mesBandBelongBand = "Диапазон не принадлежит диапазону РП!";
            mesBandRestricted = "Диапазон не может быть добавлен, так как запрещенный!";
            mesMaxRESInLetter = "Количество ИРИ в данной литере равно максимальному значению!";
        }
    }
    #endregion

    #region Значения
    public struct SMeaning
    {
        public static string meaningAddRecord;
        public static string meaningChangeRecord;

        public static string meaningOwn;
        public static string meaningLinked;
        public static string meaningAutonomous;

        public static string meaningHz;
        public static string meaningkHz;
        public static string meaningMHz;

        public static string meaningmks;
        public static string meaningms;

        public static string meaningFMHz;
        public static string meaningFminMHz;
        public static string meaningFmaxMHz;
        public static string meaningFkHz;
        public static string meaningFminkHz;
        public static string meaningFmaxkHz;


        public static string meaningShum;
        public static string meaningNes;
        public static string meaningAMn;
        public static string meaningFMn;
        public static string meaningChMn2;
        public static string meaningAMChM;
        public static string meaningChMn4;
        public static string meaningChMn8;
        public static string meaningChM;
        public static string meaningChMn;
        public static string meaningShPS; 
        public static string meaningUnkn;

        public static string meaningCoord;
        public static string meaningInterferenceParam;

        public static string meaningNAV;
        public static string meaningSPUF;

        public static string meaningSignA;
        public static string meaningSignB;
        public static string meaningSignC;
        public static string meaningSignPC;
        public static string meaningSignRadioRecevier;
        public static string meaningSignOtherTable;
        public static string meaningSignPanorama;
        public static string meaningSignButton;
        public static string meaningSignCicada;

        public static void InitSMeaning()
        {
            meaningAddRecord = "Добавить запись";
            meaningChangeRecord = "Изменить запись";

            meaningOwn = "Ведущая";
            meaningLinked = "Ведомая";
            meaningAutonomous = "Автономная";

            meaningHz = "Гц";
            meaningkHz = "кГц";
            meaningMHz = "МГц";

            meaningmks = "мкс";
            meaningms = "мс";

            meaningFMHz = "F, МГц";
            meaningFminMHz = "F мин., МГц";
            meaningFmaxMHz = "F макс., МГц";
            meaningFkHz = "F, кГц";
            meaningFminkHz = "F мин., кГц";
            meaningFmaxkHz = "F макс., кГц";

            meaningShum = "Шум";
            meaningNes = "Несущая";
            meaningAMn = "АМн";
            meaningFMn = "ФМн";
            meaningChMn2 = "ЧМн2";
            meaningAMChM = "АМ ЧМ";
            meaningChMn4 = "ЧМн4";
            meaningChMn8 = "ЧМн8";
            meaningChM = "ЧМ";
            meaningChMn = "ЧМн";
            meaningShPS = "ШПС";
            meaningUnkn = "Неизв.";

            meaningCoord = "Координаты";
            meaningInterferenceParam = "Параметры помехи";

            meaningNAV = "Навигация";
            meaningSPUF = "Спуфинг";

            meaningSignA = "A";
            meaningSignB = "К";
            meaningSignC = "Ц";
            meaningSignPC = "Пункт управления";
            meaningSignRadioRecevier = "Контрольное радиоприемное устройство";
            meaningSignOtherTable = "Таблица";
            meaningSignPanorama = "Панорама";
            meaningSignButton = "Кнопка";
            meaningSignCicada = "Цикада";
        }
    }
    #endregion

    #region Параметры
    public struct STypeModulation
    {
        public static string paramChMSh;
        public static string paramChM2;
        public static string paramChM4;
        public static string paramChM8;
        public static string paramFMn;
        public static string paramFMn4;
        public static string paramFMn8;
        public static string paramZSh;
        public static string paramLChM;

        public static void InitSTypeModulation()
        {
            paramChMSh = "ЧМШ";
            paramChM2 = "ЧМ-2";
            paramChM4 = "ЧМ-4";
            paramChM8 = "ЧМ-8";
            paramFMn = "ФМн";
            paramFMn4 = "ФМн-4";
            paramFMn8 = "ФМн-8";
            paramZSh = "ЗШ (КФМ)";
            paramLChM = "ЛЧМ";
        }
    }
    #endregion

    #region Заголовки таблиц для отчетов (текстовых файлов .doc, .xls, .txt)
    public struct SHeaders
    {
        public static string headerNumAJS;
        public static string headerNumSituation;
        public static string headerConnect;
        public static string headerIPaddress;
        public static string headerPort;
        public static string headerMode;
        public static string headerCallSign;
        public static string headerRole;
        public static string headerType;
        public static string headerAntennaJ;
        public static string headerAntennaRR;
        public static string headerLatLon;
        public static string headerAlt;
        public static string headerRRC1;
        public static string headerRRC2;
        public static string headerLPA1_3;
        public static string headerLPA2_4;
        public static string headerBPSS;
        public static string headerLPA5_7;
        public static string headerLPA5_9;
        public static string headerLPA10;
        public static string headerLPA5_10;
        public static string headerSignature;
        public static string headerSign;
        public static string headerFreqMin;
        public static string headerFreqMax;
        public static string headerAngleMin;
        public static string headerAngleMax;
        public static string headerNote;
        public static string headerDeltaF;
        public static string headerTime;
        public static string headerStep;
        public static string headerPulseWidth;
        public static string headerNumFreq;
        public static string headerJ_AJS;
        public static string headerBearing;
        public static string headerNum;
        public static string headerRMSE;
        public static string headerLevel;
        public static string headerDistance;
        public static string headerRI_AJS;
        public static string headerFreq;
        public static string headerCount;
        public static string headerJammerParams;
        public static string headerSSR;
        public static string headerCtrl;
        public static string headerJam;
        public static string headerRad;
        public static string headerLetters;
        public static string headerAJS;
        public static string headerPriority;

        public static void InitSHeader()
        {
            headerNumAJS = "№ АСП";
            headerNumSituation = "№ Обст.";
            headerConnect = "Подкл.";
            headerIPaddress = "Адрес IP";
            headerPort = "Порт";
            headerMode = "Режим";
            headerCallSign = "Позывной";
            headerRole = "Роль";
            headerType = "Тип";
            headerAntennaJ = "Ант. РП, м";
            headerAntennaRR = "Ант. РР, м";
            headerLatLon = "Шир.,°  Долг.,°";
            headerAlt = "Выс., м";
            headerRRC1 = "РРС 1,°";
            headerRRC2 = "РРС 2,°";
            headerLPA1_3 = "ЛПА 1,3,°";
            headerLPA2_4 = "ЛПА 2,4,°";
            headerBPSS = "БПСС,°";
            headerLPA5_7 = "ЛПА 5-7,°";
            headerLPA5_9 = "ЛПА 5-9,°";
            headerLPA10 = "ЛПА 10,°";
            headerLPA5_10 = "ЛПА 5-10,°";
            headerSignature = "Подпись";
            headerSign = "Знак";
            headerFreqMin = "F мин., кГц";
            headerFreqMax = "F макс., кГц";
            headerAngleMin = "θ мин., °";
            headerAngleMax = "θ макс., °";
            headerNote = "Примечание";
            headerDeltaF = "Δf, кГц";
            headerTime = "Время";
            headerStep = "Шаг, кГц";
            headerPulseWidth = "Длит. имп., мс";
            headerNumFreq = "Кол. частот";
            headerJ_AJS = "АСП РП";
            headerBearing = "θ, °";
            headerNum = "№";
            headerRMSE = "СКО, °";
            headerLevel = "U, дБ";
            headerDistance = "S, м";
            headerRI_AJS = "АСП РР";
            headerFreq = "F, кГц";
            headerCount = "Кол-во";
            headerJammerParams = "Параметры помехи";
            headerSSR = "ЕПО";
            headerCtrl = "КР";
            headerJam = "РП";
            headerRad = "Изл.";
            headerLetters = "Л";
            headerAJS = "АСП";
            headerPriority = "Пр.";
        }
    }
    #endregion
}
