﻿using ModelsTablesDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TableEvents;

namespace ValuesCorrectLib
{
    public class CorrectMinMax
    {
        /// <summary>
        /// Проверка на правильность ввода значений (Сектора и диапазоны):  
        /// если Fmin меньше 25000 или больше 6000000, Fmin = 25000;
        /// если Fmax меньше 25000 или больше 6000000, Fmax = 6000000;
        /// если Аmin меньше 0 или больше 360, Аmin = 0;
        /// если Аmax меньше 0 или больше 360, Аmax = 0
        /// </summary>
        /// <param name="sr">Fmin, Fmax, Amin, Amax</param>
        public static void IsCorrectMinMax(TableSectorsRanges sr, NameTable nameTable)
        {
            if (sr.AngleMin < 0 || sr.AngleMin > 360) { sr.AngleMin = 0; }
            if (sr.AngleMax < 0 || sr.AngleMax > 360) { sr.AngleMax = 360; }

            switch(nameTable)
            {
                case NameTable.TableSectorsRangesRecon:
                    
                    switch (PropGlobalProperty.RangeRadioRecon)
                    {
                        case 0:

                            if (sr.FreqMinKHz < ConstValues.Range_30000 || sr.FreqMinKHz > ConstValues.Range_3000000) { sr.FreqMinKHz = ConstValues.Range_30000; }
                            if (sr.FreqMaxKHz < ConstValues.Range_30000 || sr.FreqMaxKHz > ConstValues.Range_3000000) { sr.FreqMaxKHz = ConstValues.Range_3000000; }
                            break;

                        case 1:

                            if (sr.FreqMinKHz < ConstValues.Range_30000 || sr.FreqMinKHz > ConstValues.Range_6000000) { sr.FreqMinKHz = ConstValues.Range_30000; }
                            if (sr.FreqMaxKHz < ConstValues.Range_30000 || sr.FreqMaxKHz > ConstValues.Range_6000000) { sr.FreqMaxKHz = ConstValues.Range_6000000; }
                            break;

                        case 2:

                            if (sr.FreqMinKHz < ConstValues.Range_0 || sr.FreqMinKHz > ConstValues.Range_6000000) { sr.FreqMinKHz = ConstValues.Range_0; }
                            if (sr.FreqMaxKHz < ConstValues.Range_0 || sr.FreqMaxKHz > ConstValues.Range_6000000) { sr.FreqMaxKHz = ConstValues.Range_6000000; }
                            break;

                        default:

                            if (sr.FreqMinKHz < ConstValues.Range_30000 || sr.FreqMinKHz > ConstValues.Range_6000000) { sr.FreqMinKHz = ConstValues.Range_30000; }
                            if (sr.FreqMaxKHz < ConstValues.Range_30000 || sr.FreqMaxKHz > ConstValues.Range_6000000) { sr.FreqMaxKHz = ConstValues.Range_6000000; }
                            break;
                    }
                    break;


                case NameTable.TableSectorsRangesSuppr:

                    switch (PropGlobalProperty.RangeJamming)
                    {
                        case 7:

                            if (sr.FreqMinKHz < ConstValues.Range_30000 || sr.FreqMinKHz > ConstValues.Range_1215000) { sr.FreqMinKHz = ConstValues.Range_30000; }
                            if (sr.FreqMaxKHz < ConstValues.Range_30000 || sr.FreqMaxKHz > ConstValues.Range_1215000) { sr.FreqMaxKHz = ConstValues.Range_1215000; }
                            break;
         
                        case 9:

                            if (sr.FreqMinKHz < ConstValues.Range_30000 || sr.FreqMinKHz > ConstValues.Range_3000000) { sr.FreqMinKHz = ConstValues.Range_30000; }
                            if (sr.FreqMaxKHz < ConstValues.Range_30000 || sr.FreqMaxKHz > ConstValues.Range_3000000) { sr.FreqMaxKHz = ConstValues.Range_3000000; }
                            break;

                        case 10:

                            if (sr.FreqMinKHz < ConstValues.Range_30000 || sr.FreqMinKHz > ConstValues.Range_6000000) { sr.FreqMinKHz = ConstValues.Range_30000; }
                            if (sr.FreqMaxKHz < ConstValues.Range_30000 || sr.FreqMaxKHz > ConstValues.Range_6000000) { sr.FreqMaxKHz = ConstValues.Range_6000000; }
                            break;

                        case 11:

                            if (sr.FreqMinKHz < ConstValues.Range_0 || sr.FreqMinKHz > ConstValues.Range_6000000) { sr.FreqMinKHz = ConstValues.Range_0; }
                            if (sr.FreqMaxKHz < ConstValues.Range_0 || sr.FreqMaxKHz > ConstValues.Range_6000000) { sr.FreqMaxKHz = ConstValues.Range_6000000; }
                            break;

                        default:

                            if (sr.FreqMinKHz < ConstValues.Range_30000 || sr.FreqMinKHz > ConstValues.Range_6000000) { sr.FreqMinKHz = ConstValues.Range_30000; }
                            if (sr.FreqMaxKHz < ConstValues.Range_30000 || sr.FreqMaxKHz > ConstValues.Range_6000000) { sr.FreqMaxKHz = ConstValues.Range_6000000; }
                            break;
                    }
                    break;

                default:

                    if (sr.FreqMinKHz < ConstValues.Range_30000 || sr.FreqMinKHz > ConstValues.Range_6000000) { sr.FreqMinKHz = ConstValues.Range_30000; }
                    if (sr.FreqMaxKHz < ConstValues.Range_30000 || sr.FreqMaxKHz > ConstValues.Range_6000000) { sr.FreqMaxKHz = ConstValues.Range_6000000; }
                    break;
            }
        }

        /// <summary>
        /// Проверка на правильность ввода значений (Специальные частоты):  
        /// если Fmin меньше 25000 или больше 6000000, Fmin = 25000;
        /// если Fmax меньше 25000 или больше 6000000, Fmax = 6000000;
        /// </summary>
        /// <param name="specFreq">Fmin, Fmax</param>
        public static void IsCorrectMinMax(TableFreqSpec specFreq, NameTable nameTable)
        {
            switch(nameTable)
            {
                case NameTable.TableFreqForbidden:

                    switch (PropGlobalProperty.RangeJamming)
                    {
                        case 7:

                            if (specFreq.FreqMinKHz < ConstValues.Range_30000 || specFreq.FreqMinKHz > ConstValues.Range_1215000) { specFreq.FreqMinKHz = ConstValues.Range_30000; }
                            if (specFreq.FreqMaxKHz < ConstValues.Range_30000 || specFreq.FreqMaxKHz > ConstValues.Range_1215000) { specFreq.FreqMaxKHz = ConstValues.Range_1215000; }
                            break;

                        case 9:

                            if (specFreq.FreqMinKHz < ConstValues.Range_30000 || specFreq.FreqMinKHz > ConstValues.Range_3000000) { specFreq.FreqMinKHz = ConstValues.Range_30000; }
                            if (specFreq.FreqMaxKHz < ConstValues.Range_30000 || specFreq.FreqMaxKHz > ConstValues.Range_3000000) { specFreq.FreqMaxKHz = ConstValues.Range_3000000; }
                            break;

                        case 10:

                            if (specFreq.FreqMinKHz < ConstValues.Range_30000 || specFreq.FreqMinKHz > ConstValues.Range_6000000) { specFreq.FreqMinKHz = ConstValues.Range_30000; }
                            if (specFreq.FreqMaxKHz < ConstValues.Range_30000 || specFreq.FreqMaxKHz > ConstValues.Range_6000000) { specFreq.FreqMaxKHz = ConstValues.Range_6000000; }
                            break;

                        case 11:

                            if (specFreq.FreqMinKHz < ConstValues.Range_0 || specFreq.FreqMinKHz > ConstValues.Range_6000000) { specFreq.FreqMinKHz = ConstValues.Range_0; }
                            if (specFreq.FreqMaxKHz < ConstValues.Range_0 || specFreq.FreqMaxKHz > ConstValues.Range_6000000) { specFreq.FreqMaxKHz = ConstValues.Range_6000000; }
                            break;

                        default:

                            if (specFreq.FreqMinKHz < ConstValues.Range_30000 || specFreq.FreqMinKHz > ConstValues.Range_6000000) { specFreq.FreqMinKHz = ConstValues.Range_30000; }
                            if (specFreq.FreqMaxKHz < ConstValues.Range_30000 || specFreq.FreqMaxKHz > ConstValues.Range_6000000) { specFreq.FreqMaxKHz = ConstValues.Range_6000000; }
                            break;
                    }
                    break;

                case NameTable.TableFreqImportant:
                case NameTable.TableFreqKnown:

                    switch (PropGlobalProperty.RangeRadioRecon)
                    {
                        case 0:

                            if (specFreq.FreqMinKHz < ConstValues.Range_30000 || specFreq.FreqMinKHz > ConstValues.Range_3000000) { specFreq.FreqMinKHz = ConstValues.Range_30000; }
                            if (specFreq.FreqMaxKHz < ConstValues.Range_30000 || specFreq.FreqMaxKHz > ConstValues.Range_3000000) { specFreq.FreqMaxKHz = ConstValues.Range_3000000; }
                            break;

                        case 1:

                            if (specFreq.FreqMinKHz < ConstValues.Range_30000 || specFreq.FreqMinKHz > ConstValues.Range_6000000) { specFreq.FreqMinKHz = ConstValues.Range_30000; }
                            if (specFreq.FreqMaxKHz < ConstValues.Range_30000 || specFreq.FreqMaxKHz > ConstValues.Range_6000000) { specFreq.FreqMaxKHz = ConstValues.Range_6000000; }
                            break;

                        case 2:

                            if (specFreq.FreqMinKHz < ConstValues.Range_0 || specFreq.FreqMinKHz > ConstValues.Range_6000000) { specFreq.FreqMinKHz = ConstValues.Range_0; }
                            if (specFreq.FreqMaxKHz < ConstValues.Range_0 || specFreq.FreqMaxKHz > ConstValues.Range_6000000) { specFreq.FreqMaxKHz = ConstValues.Range_6000000; }
                            break;

                        default:

                            if (specFreq.FreqMinKHz < ConstValues.Range_30000 || specFreq.FreqMinKHz > ConstValues.Range_6000000) { specFreq.FreqMinKHz = ConstValues.Range_30000; }
                            if (specFreq.FreqMaxKHz < ConstValues.Range_30000 || specFreq.FreqMaxKHz > ConstValues.Range_6000000) { specFreq.FreqMaxKHz = ConstValues.Range_6000000; }
                            break;
                    }

                    break;

                default:

                    if (specFreq.FreqMinKHz < ConstValues.Range_30000 || specFreq.FreqMinKHz > ConstValues.Range_6000000) { specFreq.FreqMinKHz = ConstValues.Range_30000; }
                    if (specFreq.FreqMaxKHz < ConstValues.Range_30000 || specFreq.FreqMaxKHz > ConstValues.Range_6000000) { specFreq.FreqMaxKHz = ConstValues.Range_6000000; }
                    break;
            }
           
        }

        /// <summary>
        /// Проверка на правильность ввода значений (ИРИ ППРЧ РП):  
        /// если Fmin меньше 25000 или больше 6000000, Fmin = 25000;
        /// если Fmax меньше 25000 или больше 6000000, Fmax = 6000000;
        /// </summary>
        /// <param name="suppressFHSS">Fmin, Fmax</param>
        public static void IsCorrectMinMax(TableSuppressFHSS suppressFHSS, double dFreqMin, double dFreqMax)
        {
            if(PropLocalFreqMHz_kHz.FreqMHz_kHz == 1)
            {
                suppressFHSS.FreqMinKHz = dFreqMin;
                suppressFHSS.FreqMaxKHz = dFreqMax;
            }

            switch (PropGlobalProperty.RangeJamming)
            {
                case 7:

                    if (suppressFHSS.FreqMinKHz < ConstValues.Range_30000 || suppressFHSS.FreqMinKHz > ConstValues.Range_1215000) { suppressFHSS.FreqMinKHz = ConstValues.Range_30000; }
                    if (suppressFHSS.FreqMaxKHz < ConstValues.Range_30000 || suppressFHSS.FreqMaxKHz > ConstValues.Range_1215000) { suppressFHSS.FreqMaxKHz = ConstValues.Range_1215000; }
                    break;

                case 9:

                    if (suppressFHSS.FreqMinKHz < ConstValues.Range_30000 || suppressFHSS.FreqMinKHz > ConstValues.Range_3000000) { suppressFHSS.FreqMinKHz = ConstValues.Range_30000; }
                    if (suppressFHSS.FreqMaxKHz < ConstValues.Range_30000 || suppressFHSS.FreqMaxKHz > ConstValues.Range_3000000) { suppressFHSS.FreqMaxKHz = ConstValues.Range_3000000; }
                    break;

                case 10:

                    if (suppressFHSS.FreqMinKHz < ConstValues.Range_30000 || suppressFHSS.FreqMinKHz > ConstValues.Range_6000000) { suppressFHSS.FreqMinKHz = ConstValues.Range_30000; }
                    if (suppressFHSS.FreqMaxKHz < ConstValues.Range_30000 || suppressFHSS.FreqMaxKHz > ConstValues.Range_6000000) { suppressFHSS.FreqMaxKHz = ConstValues.Range_6000000; }
                    break;

                case 11:

                    if (suppressFHSS.FreqMinKHz < ConstValues.Range_0 || suppressFHSS.FreqMinKHz > ConstValues.Range_6000000) { suppressFHSS.FreqMinKHz = ConstValues.Range_0; }
                    if (suppressFHSS.FreqMaxKHz < ConstValues.Range_0 || suppressFHSS.FreqMaxKHz > ConstValues.Range_6000000) { suppressFHSS.FreqMaxKHz = ConstValues.Range_6000000; }
                    break;

                default:

                    if (suppressFHSS.FreqMinKHz < ConstValues.Range_30000 || suppressFHSS.FreqMinKHz > ConstValues.Range_6000000) { suppressFHSS.FreqMinKHz = ConstValues.Range_30000; }
                    if (suppressFHSS.FreqMaxKHz < ConstValues.Range_30000 || suppressFHSS.FreqMaxKHz > ConstValues.Range_6000000) { suppressFHSS.FreqMaxKHz = ConstValues.Range_6000000; }
                    break;
            }

            if (PropLocalFreqMHz_kHz.FreqMHz_kHz == 1)
            {
                suppressFHSS.FreqMinKHz = suppressFHSS.FreqMinKHz / 1000;
                suppressFHSS.FreqMaxKHz = suppressFHSS.FreqMaxKHz / 1000;
            }
        }

        public static bool IsCorrectFreqMHz_kHz(TableSuppressFHSS suppressFHSS)
        {
            if (PropGlobalProperty.RangeJamming == 11) return true;

            if (suppressFHSS.FreqMinKHz == 0 || suppressFHSS.FreqMaxKHz == 0)
            {
                MessageBox.Show(SMessageError.mesIncorrectFreq, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                return false;
            }

            switch (PropLocalFreqMHz_kHz.FreqMHz_kHz)
            {
                case 0: // kHz

                    if (suppressFHSS.FreqMinKHz < 30000 || suppressFHSS.FreqMaxKHz < 30000)
                    {
                        MessageBox.Show(SMessageError.mesIncorrectFreq, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                        return false;
                    }
                    else { return true; }

                case 1: // MHz

                    switch (PropGlobalProperty.RangeJamming)
                    {
                        case 7:

                            if (suppressFHSS.FreqMinKHz > 1215 || suppressFHSS.FreqMaxKHz > 1215)
                            {
                                MessageBox.Show(SMessageError.mesIncorrectFreq, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                                return false;
                            }
                            else { return true; }

                        case 9:

                            if(suppressFHSS.FreqMinKHz > 3000 || suppressFHSS.FreqMaxKHz > 3000)
                            {
                                MessageBox.Show(SMessageError.mesIncorrectFreq, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                                return false;
                            }
                            else { return true; }

                        case 10:
                        case 11:

                            if (suppressFHSS.FreqMinKHz > 6000 || suppressFHSS.FreqMaxKHz > 6000)
                            {
                                MessageBox.Show(SMessageError.mesIncorrectFreq, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                                return false;
                            }
                            else { return true; }
                    }
                    break;
            }
            return true;
        }

        public static bool IsCorrectRangeMinMax(double dFreqMin, double dFreqMax)
        {
            if (dFreqMin >= dFreqMax)
            {
                MessageBox.Show(SMessages.mesValuesMaxMin, SMessages.mesMessage, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }

            return true;
        }

        //public static bool IsCorrectRangeMinMax(TableSuppressFHSS suppressFHSS)
        //{
        //    if (suppressFHSS.FreqMinKHz >= suppressFHSS.FreqMaxKHz)
        //    {
        //        MessageBox.Show(SMessages.mesValuesMaxMin, SMessages.mesMessage, MessageBoxButtons.OK, MessageBoxIcon.Information);
        //        return false;
        //    }

        //    return true;
        //}

        public static bool IsCorrectRangeMinMax(TableSectorsRanges sRanges)
        {
            if (sRanges.FreqMinKHz >= sRanges.FreqMaxKHz)
            {
                MessageBox.Show(SMessages.mesValuesMaxMin, SMessages.mesMessage, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }

            return true;
        }

        public static bool IsCorrectRangeMinMax(TableFreqSpec freqSpec)
        {
            if (freqSpec.FreqMinKHz >= freqSpec.FreqMaxKHz)
            {
                MessageBox.Show(SMessages.mesValuesMaxMin, SMessages.mesMessage, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }

            return true;
        }
    }
}
