﻿using ModelsTablesDBLib;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ValuesCorrectLib
{
    public class CorrectJammerParams
    {
        // декодирование параметров помехи из кодов индекс или значение для PropertyGrid
        public static void PropertyGridJammerParams(InterferenceParam interferenceParam)
        {
            switch (interferenceParam.Modulation)
            {
                case 1:
                    ComboBoxEditor.IndComboBoxModulation = 0;

                    switch (interferenceParam.Deviation)
                    {
                        case 1:
                            ComboBoxEditor.IndComboBoxDeviation = 0;
                            break;

                        case 2:
                            ComboBoxEditor.IndComboBoxDeviation = 1;
                            break;

                        case 3:
                            ComboBoxEditor.IndComboBoxDeviation = 2;
                            break;

                        case 4:
                            ComboBoxEditor.IndComboBoxDeviation = 3;
                            break;

                        case 5:
                            ComboBoxEditor.IndComboBoxDeviation = 4;
                            break;

                        case 6:
                            ComboBoxEditor.IndComboBoxDeviation = 5;
                            break;

                        case 7:
                            ComboBoxEditor.IndComboBoxDeviation = 6;
                            break;

                        case 8:
                            ComboBoxEditor.IndComboBoxDeviation = 7;
                            break;

                        case 9:
                            ComboBoxEditor.IndComboBoxDeviation = 8;
                            break;

                        case 10:
                            ComboBoxEditor.IndComboBoxDeviation = 9;
                            break;

                        case 11:
                            ComboBoxEditor.IndComboBoxDeviation = 10;
                            break;

                        case 12:
                            ComboBoxEditor.IndComboBoxDeviation = 11;
                            break;

                        case 13:
                            ComboBoxEditor.IndComboBoxDeviation = 12;
                            break;

                        case 14:
                            ComboBoxEditor.IndComboBoxDeviation = 13;
                            break;

                        case 15:
                            ComboBoxEditor.IndComboBoxDeviation = 14;
                            break;

                        case 16:
                            ComboBoxEditor.IndComboBoxDeviation = 15;
                            break;

                        case 17:
                            ComboBoxEditor.IndComboBoxDeviation = 16;
                            break;

                        case 18:
                            ComboBoxEditor.IndComboBoxDeviation = 17;
                            break;

                        case 19:
                            ComboBoxEditor.IndComboBoxDeviation = 18;
                            break;

                        default:
                            break;
                    }
                    break;

                case 2:
                    ComboBoxEditor.IndComboBoxModulation = 1;

                    switch (interferenceParam.Deviation)
                    {
                        case 1:
                            ComboBoxEditor.IndComboBoxDeviation = 0;
                            break;

                        case 2:
                            ComboBoxEditor.IndComboBoxDeviation = 1;
                            break;

                        case 3:
                            ComboBoxEditor.IndComboBoxDeviation = 2;
                            break;

                        case 4:
                            ComboBoxEditor.IndComboBoxDeviation = 3;
                            break;

                        case 5:
                            ComboBoxEditor.IndComboBoxDeviation = 4;
                            break;

                        case 6:
                            ComboBoxEditor.IndComboBoxDeviation = 5;
                            break;

                        case 7:
                            ComboBoxEditor.IndComboBoxDeviation = 6;
                            break;

                        case 8:
                            ComboBoxEditor.IndComboBoxDeviation = 7;
                            break;

                        case 9:
                            ComboBoxEditor.IndComboBoxDeviation = 8;
                            break;

                        case 10:
                            ComboBoxEditor.IndComboBoxDeviation = 9;
                            break;

                        case 11:
                            ComboBoxEditor.IndComboBoxDeviation = 10;
                            break;

                        case 12:
                            ComboBoxEditor.IndComboBoxDeviation = 11;
                            break;

                        default:
                            break;
                    }

                    switch (interferenceParam.Manipulation)
                    {
                        case 1:
                            ComboBoxEditor.IndComboBoxManipulation = 0;
                            break;

                        case 2:
                            ComboBoxEditor.IndComboBoxManipulation = 1;
                            break;

                        case 3:
                            ComboBoxEditor.IndComboBoxManipulation = 2;
                            break;

                        case 4:
                            ComboBoxEditor.IndComboBoxManipulation = 3;
                            break;

                        case 5:
                            ComboBoxEditor.IndComboBoxManipulation = 4;
                            break;

                        case 6:
                            ComboBoxEditor.IndComboBoxManipulation = 5;
                            break;

                        default:
                            break;
                    }
                    break;
                case 3:
                    ComboBoxEditor.IndComboBoxModulation = 2;

                    switch (interferenceParam.Deviation)
                    {
                        case 1:
                            ComboBoxEditor.IndComboBoxDeviation = 0;
                            break;

                        case 2:
                            ComboBoxEditor.IndComboBoxDeviation = 1;
                            break;

                        case 3:
                            ComboBoxEditor.IndComboBoxDeviation = 2;
                            break;

                        case 4:
                            ComboBoxEditor.IndComboBoxDeviation = 3;
                            break;

                        case 5:
                            ComboBoxEditor.IndComboBoxDeviation = 4;
                            break;

                        case 6:
                            ComboBoxEditor.IndComboBoxDeviation = 5;
                            break;

                        case 7:
                            ComboBoxEditor.IndComboBoxDeviation = 6;
                            break;

                        case 8:
                            ComboBoxEditor.IndComboBoxDeviation = 7;
                            break;

                        case 9:
                            ComboBoxEditor.IndComboBoxDeviation = 8;
                            break;

                        case 10:
                            ComboBoxEditor.IndComboBoxDeviation = 9;
                            break;

                        case 11:
                            ComboBoxEditor.IndComboBoxDeviation = 10;
                            break;

                        case 12:
                            ComboBoxEditor.IndComboBoxDeviation = 11;
                            break;

                        default:
                            break;
                    }

                    switch (interferenceParam.Manipulation)
                    {
                        case 1:
                            ComboBoxEditor.IndComboBoxManipulation = 0;
                            break;

                        case 2:
                            ComboBoxEditor.IndComboBoxManipulation = 1;
                            break;

                        case 3:
                            ComboBoxEditor.IndComboBoxManipulation = 2;
                            break;

                        case 4:
                            ComboBoxEditor.IndComboBoxManipulation = 3;
                            break;

                        case 5:
                            ComboBoxEditor.IndComboBoxManipulation = 4;
                            break;

                        case 6:
                            ComboBoxEditor.IndComboBoxManipulation = 5;
                            break;

                        default:
                            break;
                    }
                    break;
                case 4:
                    ComboBoxEditor.IndComboBoxModulation = 3;

                    switch (interferenceParam.Deviation)
                    {
                        case 1:
                            ComboBoxEditor.IndComboBoxDeviation = 0;
                            break;

                        case 2:
                            ComboBoxEditor.IndComboBoxDeviation = 1;
                            break;

                        case 3:
                            ComboBoxEditor.IndComboBoxDeviation = 2;
                            break;

                        case 4:
                            ComboBoxEditor.IndComboBoxDeviation = 3;
                            break;

                        case 5:
                            ComboBoxEditor.IndComboBoxDeviation = 4;
                            break;

                        case 6:
                            ComboBoxEditor.IndComboBoxDeviation = 5;
                            break;

                        case 7:
                            ComboBoxEditor.IndComboBoxDeviation = 6;
                            break;

                        case 8:
                            ComboBoxEditor.IndComboBoxDeviation = 7;
                            break;

                        case 9:
                            ComboBoxEditor.IndComboBoxDeviation = 8;
                            break;

                        case 10:
                            ComboBoxEditor.IndComboBoxDeviation = 9;
                            break;

                        case 11:
                            ComboBoxEditor.IndComboBoxDeviation = 10;
                            break;

                        case 12:
                            ComboBoxEditor.IndComboBoxDeviation = 11;
                            break;

                        default:
                            break;
                    }

                    switch (interferenceParam.Manipulation)
                    {
                        case 1:
                            ComboBoxEditor.IndComboBoxManipulation = 0;
                            break;

                        case 2:
                            ComboBoxEditor.IndComboBoxManipulation = 1;
                            break;

                        case 3:
                            ComboBoxEditor.IndComboBoxManipulation = 2;
                            break;

                        case 4:
                            ComboBoxEditor.IndComboBoxManipulation = 3;
                            break;

                        case 5:
                            ComboBoxEditor.IndComboBoxManipulation = 4;
                            break;

                        case 6:
                            ComboBoxEditor.IndComboBoxManipulation = 5;
                            break;

                        default:
                            break;
                    }
                    break;

                case 5:
                    ComboBoxEditor.IndComboBoxModulation = 4;

                    switch (interferenceParam.Manipulation)
                    {
                        case 1:
                            ComboBoxEditor.IndComboBoxManipulation = 1;
                            break;

                        case 2:
                            ComboBoxEditor.IndComboBoxManipulation = 2;
                            break;

                        case 3:
                            ComboBoxEditor.IndComboBoxManipulation = 3;
                            break;

                        case 4:
                            ComboBoxEditor.IndComboBoxManipulation = 4;
                            break;

                        case 5:
                            ComboBoxEditor.IndComboBoxManipulation = 5;
                            break;

                        case 6:
                            ComboBoxEditor.IndComboBoxManipulation = 6;
                            break;

                        case 7:
                            ComboBoxEditor.IndComboBoxManipulation = 7;
                            break;

                        case 8:
                            ComboBoxEditor.IndComboBoxManipulation = 8;
                            break;

                        case 9:
                            ComboBoxEditor.IndComboBoxManipulation = 9;
                            break;

                        case 10:
                            ComboBoxEditor.IndComboBoxManipulation = 10;
                            break;
                      
                        default:
                            break;
                    }
                    break;
                case 6:
                    ComboBoxEditor.IndComboBoxModulation = 5;

                    switch (interferenceParam.Manipulation)
                    {
                        case 1:
                            ComboBoxEditor.IndComboBoxManipulation = 1;
                            break;

                        case 2:
                            ComboBoxEditor.IndComboBoxManipulation = 2;
                            break;

                        case 3:
                            ComboBoxEditor.IndComboBoxManipulation = 3;
                            break;

                        case 4:
                            ComboBoxEditor.IndComboBoxManipulation = 4;
                            break;

                        case 5:
                            ComboBoxEditor.IndComboBoxManipulation = 5;
                            break;

                        case 6:
                            ComboBoxEditor.IndComboBoxManipulation = 6;
                            break;

                        case 7:
                            ComboBoxEditor.IndComboBoxManipulation = 7;
                            break;

                        case 8:
                            ComboBoxEditor.IndComboBoxManipulation = 8;
                            break;

                        case 9:
                            ComboBoxEditor.IndComboBoxManipulation = 9;
                            break;

                        case 10:
                            ComboBoxEditor.IndComboBoxManipulation = 0;
                            break;

                        default:
                            break;
                    }
                    break;
                case 7:
                    ComboBoxEditor.IndComboBoxModulation = 6;

                    switch (interferenceParam.Manipulation)
                    {
                        case 1:
                            ComboBoxEditor.IndComboBoxManipulation = 1;
                            break;

                        case 2:
                            ComboBoxEditor.IndComboBoxManipulation = 2;
                            break;

                        case 3:
                            ComboBoxEditor.IndComboBoxManipulation = 3;
                            break;

                        case 4:
                            ComboBoxEditor.IndComboBoxManipulation = 4;
                            break;

                        case 5:
                            ComboBoxEditor.IndComboBoxManipulation = 5;
                            break;

                        case 6:
                            ComboBoxEditor.IndComboBoxManipulation = 6;
                            break;

                        case 7:
                            ComboBoxEditor.IndComboBoxManipulation = 7;
                            break;

                        case 8:
                            ComboBoxEditor.IndComboBoxManipulation = 8;
                            break;

                        case 9:
                            ComboBoxEditor.IndComboBoxManipulation = 9;
                            break;

                        case 10:
                            ComboBoxEditor.IndComboBoxManipulation = 0;
                            break;

                        default:
                            break;
                    }
                    break;

                case 8:
                    ComboBoxEditor.IndComboBoxModulation = 7;

                    switch (interferenceParam.Manipulation)
                    {
                        case 1:
                            ComboBoxEditor.IndComboBoxManipulation = 0;
                            break;

                        case 2:
                            ComboBoxEditor.IndComboBoxManipulation = 1;
                            break;

                        case 3:
                            ComboBoxEditor.IndComboBoxManipulation = 2;
                            break;

                        case 4:
                            ComboBoxEditor.IndComboBoxManipulation = 3;
                            break;

                        default:
                            break;
                    }
                    break;

                case 11:
                    ComboBoxEditor.IndComboBoxModulation = 8;

                    BitArray devBA = new BitArray(new byte[] { interferenceParam.Deviation });
                    string strDeviation = string.Empty;
                    foreach (bool bit in devBA)
                    {
                        strDeviation += Convert.ToByte(bit);
                    }
                    int devBit = Convert.ToInt32(strDeviation.Substring(7)); // старший бит 
                    if (devBit == 1)
                    {
                        ComboBoxEditor.ValueDecUpDownDeviation = (Convert.ToByte(interferenceParam.Deviation ^ 0x80)) * 1000;
                    }
                    else
                    {
                        ComboBoxEditor.ValueDecUpDownDeviation = interferenceParam.Deviation * 10;
                    }

                    BitArray manBA = new BitArray(new byte[] { interferenceParam.Manipulation });
                    string strManipulation = string.Empty;
                    foreach (bool bit in manBA)
                    {
                        strManipulation += Convert.ToByte(bit);
                    }
                    int manBit = Convert.ToInt32(strManipulation.Substring(7)); // старший бит 
                    if (manBit == 1)
                    {
                        ComboBoxEditor.ValueDecUpDownManipulation = (Convert.ToByte(interferenceParam.Manipulation ^ 0x80)) * 1000;
                    }
                    else
                    {
                        ComboBoxEditor.ValueDecUpDownManipulation = interferenceParam.Manipulation * 10;
                    }

                    break;
            }

            switch (interferenceParam.Duration)
            {
                case 1:
                    ComboBoxEditor.IndComboBoxDuration = 0;
                    break;

                case 2:
                    ComboBoxEditor.IndComboBoxDuration = 1;
                    break;

                case 3:
                    ComboBoxEditor.IndComboBoxDuration = 2;
                    break;

                case 4:
                    ComboBoxEditor.IndComboBoxDuration = 3;
                    break;

                default:
                    ComboBoxEditor.IndComboBoxDuration = -1;
                    break;
            }
        }
    }
}
