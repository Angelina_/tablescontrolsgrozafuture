﻿using ModelsTablesDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ValuesCorrectLib
{
    public class CorrectReconFWS
    {
        public static InterferenceParam GetParamsForModulationKondor(ModulationKondor modulationKondor)
        {
            InterferenceParam interferenceParam = new InterferenceParam();

            switch (modulationKondor)
            {
                case ModulationKondor.AM:
                case ModulationKondor.FM:
                case ModulationKondor.WFM:
                case ModulationKondor.USB:
                case ModulationKondor.LSB:

                    interferenceParam.Modulation = 2;
                    interferenceParam.Manipulation = 4;
                    interferenceParam.Deviation = 1;
                    interferenceParam.Duration = 2;
                    break;

                case ModulationKondor.APCO_P25:

                    interferenceParam.Modulation = 2;
                    interferenceParam.Manipulation = 2;
                    interferenceParam.Deviation = 2;
                    interferenceParam.Duration = 1;
                    break;

                case ModulationKondor.DMR:

                    interferenceParam.Modulation = 5;
                    interferenceParam.Manipulation = 9;
                    interferenceParam.Deviation = 1;
                    interferenceParam.Duration = 2;
                    break;

                case ModulationKondor.TETRA:

                    interferenceParam.Modulation = 6;
                    interferenceParam.Manipulation = 7;
                    interferenceParam.Deviation = 0;
                    interferenceParam.Duration = 2;
                    break;

                case ModulationKondor.None:

                    interferenceParam.Modulation = 1;
                    interferenceParam.Manipulation = 0;
                    interferenceParam.Deviation = 8;
                    interferenceParam.Duration = 4;
                    break;

                default:
                    break;
            }

            return interferenceParam;
        }
    }
}
