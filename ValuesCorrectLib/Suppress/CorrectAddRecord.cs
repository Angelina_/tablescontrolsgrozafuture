﻿using System;
using System.Windows.Controls.Primitives;
using System.Windows.Forms;
using TableEvents;

namespace ValuesCorrectLib
{
    public class CorrectAddRecord
    {
        public static byte TypeStation = 2;

        /// <summary>
        /// проверить частоту на принадлежность частотному диапазону 
        /// </summary>
        /// <param name="dFreq"></param>
        /// <returns></returns>
        public static bool IsCorrectFreqRanges(double dFreq)
        {
            switch (PropGlobalProperty.RangeJamming)
            {
                case 7:

                    if (dFreq < RangesLetters.FREQ_START_LETTER_1 | dFreq > RangesLetters.FREQ_START_LETTER_8)
                    {
                        MessageBox.Show(SMessageError.mesErrFreqWithoutRange, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                        return false;
                    }
                    break;

                case 9:

                    switch(PropGlobalProperty.Amplifiers)
                    {
                        case 0:
                            if (dFreq < RangesLetters.FREQ_START_LETTER_1 | dFreq > RangesLetters.FREQ_START_LETTER_10)
                            {
                                MessageBox.Show(SMessageError.mesErrFreqWithoutRange, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                                return false;
                            }
                            break;

                        case 1:
                            if (dFreq < RangesLetters.FREQ_START_LETTER_1 | dFreq > RangesLetters.FREQ_START_LETTER_10_MODIF)
                            {
                                MessageBox.Show(SMessageError.mesErrFreqWithoutRange, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                                return false;
                            }
                            break;
                    }
                    break;

                case 10:

                    if (dFreq < RangesLetters.FREQ_START_LETTER_1 | dFreq > RangesLetters.FREQ_STOP_LETTER_10)
                    {
                        MessageBox.Show(SMessageError.mesErrFreqWithoutRange, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                        return false;
                    }
                    break;

                default:

                    if (dFreq < RangesLetters.FREQ_START_LETTER_1 | dFreq > RangesLetters.FREQ_STOP_LETTER_10)
                    {
                        MessageBox.Show(SMessageError.mesErrFreqWithoutRange, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                        return false;
                    }
                    break;
            }

            return true;
        }

        /// <summary>
        /// проверить частоту на принадлежность литере 8
        /// </summary>
        /// <param name="dFreq"></param>
        /// <param name="tbIridium"></param>
        /// <param name="tbInmarsat"></param>
        /// <returns></returns>
        public static bool IsCorrectFreqLetter(double dFreq, ToggleButton tbIridium, ToggleButton tbInmarsat)
        {
            if (tbInmarsat.IsChecked == true || tbIridium.IsChecked == true)
            {
                if (dFreq > RangesLetters.FREQ_START_LETTER_8 && dFreq < RangesLetters.FREQ_START_LETTER_9)
                {
                    MessageBox.Show(SMessages.mesLetter8IRIN, SMessages.mesMessage, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return false;
                }
                else { return true; }

            }
            else { return true; }
        }

        /// <summary>
        /// проверить пеленг на принадлежность диапазону 
        /// </summary>
        /// <param name="fBearing"></param>
        /// <returns></returns>
        public static bool IsCorrectPeleng(float fBearing)
        {
            if (fBearing < -1 | fBearing > 360)
            {
                MessageBox.Show(SMessageError.mesErrIncorrSetPel, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                return false;
            }
            else { return true; }
        }

        /// <summary>
        /// проверить порог на принадлежность диапазону 
        /// </summary>
        /// <param name="threshold"></param>
        /// <returns></returns>
        public static short IsCorrectThreshold(short threshold)
        {
            if (threshold < -130 | threshold > 0)
            {
                threshold = -110;
            }
            return threshold; 
        }

        /// <summary>
        /// проверить значение ширины полосы
        /// </summary>
        /// <param name="iBearing"></param>
        /// <returns></returns>
        public static bool IsCorrectDeviation(float fDeviation)
        {
            if (fDeviation < 0 | fDeviation > 1000)
            {
                MessageBox.Show(SMessageError.mesErrValBandwidth, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                return false;
            }
            else { return true; }
        }

        /// <summary>
        /// проверить шаг сетки ППРЧ
        /// </summary>
        /// <param name="iBearing"></param>
        /// <returns></returns>
        public static bool IsCorrectStep(Int16 iStep)
        {
            if (iStep < 1 | iStep > 30000)
            {
                MessageBox.Show(SMessageError.mesErrValStep, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                return false;
            }
            else { return true; }
        }

        public static bool IsCorrectRangeForExcludedFreq(double dFreqExcluded, double dFreqMin, double dFreqMax)
        {
            if ((dFreqExcluded < dFreqMin) || (dFreqExcluded > dFreqMax))
            {
                MessageBox.Show(SMessageError.mesErrFreqOutNetwork, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                return false;
            }
            else { return true; }
        }
    }
}
