﻿using ModelsTablesDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using TableEvents;

namespace ValuesCorrectLib
{
    public class CorrectSuppress
    {
        /// <summary>
        /// Принадлежность частоты заданным диапазонам РП
        /// </summary>
        /// <param name="dFreq"> частота </param>
        /// <returns> сообщение об ошибке (пустая строка - успешно) </returns>
        public static string IsCheckRangesSuppress(double dFreq, List<TableSectorsRanges> tableSRangesSuppr)
        {
            string sMessage = string.Empty;

            sMessage = SMessages.mesFreqBelongBand;

            int len = tableSRangesSuppr.Count;
            for (int i = 0; i < len; i++)
            {
                if (dFreq >= tableSRangesSuppr[i].FreqMinKHz && dFreq <= tableSRangesSuppr[i].FreqMaxKHz && tableSRangesSuppr[i].IsCheck)
                    sMessage = "";
            }                     

            return sMessage;
        }

        /// <summary>
        /// Принадлежность частоты Запрещенным частотам
        /// </summary>
        /// <param name="dFreq"> частота </param>
        /// <returns> сообщение об ошибке (пустая строка - успешно) </returns>
        public static string IsCheckForbiddenFreq(double dFreq, List<TableFreqSpec> tableFreqForbidden)
        {
            string sMessage = string.Empty;
         
            int len = tableFreqForbidden.Count;
            for (int i = 0; i < len; i++)
            {
                if (dFreq >= tableFreqForbidden[i].FreqMinKHz && dFreq <= tableFreqForbidden[i].FreqMaxKHz)
                    sMessage = SMessages.mesFreqRestricted;
            }

            return sMessage;
        }

        /// <summary>
        /// Новый список РП
        /// </summary>
        /// <param name="tableSRangesSuppr"></param>
        /// <returns></returns>
        private static List<TableSectorsRanges> NewRangeSuppress(List<TableSectorsRanges> tableSRangesSuppr)
        {
            List<TableSectorsRanges> SortFreqMinKHz = tableSRangesSuppr.OrderBy(x => x.FreqMinKHz).ToList();

            List<TableSectorsRanges> newListSRangesSuppr = new List<TableSectorsRanges>();
            
            int len = SortFreqMinKHz.Count;
            for (int i = 0; i < len; i++)
            {
                if (i + 1 == len) { return newListSRangesSuppr; }

                TableSectorsRanges newTable = new TableSectorsRanges();

                if (SortFreqMinKHz[i].FreqMaxKHz != SortFreqMinKHz[i + 1].FreqMinKHz)
                {
                    newTable.FreqMinKHz = SortFreqMinKHz[i].FreqMaxKHz;
                    newTable.FreqMaxKHz = SortFreqMinKHz[i + 1].FreqMinKHz;

                    newListSRangesSuppr.Add(newTable);
                }
            }

            return newListSRangesSuppr;
        }

        /// <summary>
        /// Принадлежность частот заданным диапазонам РП
        /// </summary>
        /// <param name="dFreqMin"> частота min </param>
        /// <param name="dFreqMax"> частота max </param>
        /// <returns> сообщение об ошибке (пустая строка - успешно) </returns>
        public static string IsCheckRangesSuppress(double dFreqMin, double dFreqMax, List<TableSectorsRanges> tableSRangesSuppr)
        {
            List<TableSectorsRanges> newTableSRangesSuppr = NewRangeSuppress(tableSRangesSuppr);

            string sMessage = string.Empty;

            if (newTableSRangesSuppr.Count == 0) return sMessage;

            sMessage = SMessages.mesBandBelongBand;

            int len = newTableSRangesSuppr.Count;
            for (int i = 0; i < len; i++)
            {
                if ((dFreqMin > newTableSRangesSuppr[i].FreqMinKHz && dFreqMin < newTableSRangesSuppr[i].FreqMaxKHz) ||
                    (dFreqMax > newTableSRangesSuppr[i].FreqMinKHz && dFreqMax < newTableSRangesSuppr[i].FreqMaxKHz))
                    return sMessage;  
            }

            return string.Empty;


            //string sMessage = string.Empty;

            //sMessage = SMessages.mesBandBelongBand;

            //int len = tableSRangesSuppr.Count;
            //for (int i = 0; i < len; i++)
            //{
            //    if ((dFreqMin >= tableSRangesSuppr[i].FreqMinKHz && dFreqMin <= tableSRangesSuppr[i].FreqMaxKHz) &&
            //        (dFreqMax >= tableSRangesSuppr[i].FreqMinKHz && dFreqMax <= tableSRangesSuppr[i].FreqMaxKHz))
            //        sMessage = "";
            //}

            //return sMessage;
        }

        /// <summary>
        /// Принадлежность частоты Запрещенным частотам
        /// </summary>
        /// <param name="dFreq"> частота </param>
        /// <returns> сообщение об ошибке (пустая строка - успешно) </returns>
        public static string IsCheckForbiddenRange(double dFreqMin, double dFreqMax, List<TableFreqSpec> tableFreqForbidden)
        {
            string sMessage = string.Empty;

            int len = tableFreqForbidden.Count;
            for (int i = 0; i < len; i++)
            {
                if ((dFreqMin >= tableFreqForbidden[i].FreqMinKHz && dFreqMin <= tableFreqForbidden[i].FreqMaxKHz) || 
                    (dFreqMax >= tableFreqForbidden[i].FreqMinKHz && dFreqMax <= tableFreqForbidden[i].FreqMaxKHz))
                    sMessage = SMessages.mesBandRestricted;
            }

            return sMessage;
        }

        /// <summary>
        /// Количество ИРИ в данной литере
        /// </summary>
        /// <param name="iLetter"> литера </param>
        /// <returns> сообщение об ошибке (пустая строка - успешно) </returns>
        public static string CountFreqLetter(byte bLetter, int countFreq, List<TableSuppressFWS> tableSuppressFWS)
        {
            string sMessage = string.Empty;

            int iCount = tableSuppressFWS.Count(x => x.Letter == bLetter);

            if (iCount == countFreq) // Количество ИРИ в данной литере
            {
                sMessage = SMessages.mesMaxRESInLetter;
            }

            return sMessage;
        }

        /// <summary>
        /// Проверка количества каналов ППРЧ
        /// </summary>
        /// <param name="lSuppressFHSS"></param>
        /// <param name="SuppressFHSS"></param>
        /// <returns></returns>
        public static bool IsCheckCountChannel(List<TableSuppressFHSS> lSuppressFHSS, TableSuppressFHSS SuppressFHSS)
        {
            bool fCheckCount = false;

            int iCountChannel = 0;

            try
            {
                List<TableSuppressFHSS> tempListSuppressFHSS = new List<TableSuppressFHSS>();
                tempListSuppressFHSS.AddRange(lSuppressFHSS);
                tempListSuppressFHSS.Add(SuppressFHSS);

                int band = PropGlobalProperty.RangeJamming == 11 ? ConstValues.Band_80000 : ConstValues.Band_30000;
             
                for (int i = 0; i < tempListSuppressFHSS.Count; i++)
                {
                    if (Convert.ToInt32(tempListSuppressFHSS[i].FreqMaxKHz - tempListSuppressFHSS[i].FreqMinKHz) % band == 0)
                    {
                        iCountChannel += Convert.ToInt32(tempListSuppressFHSS[i].FreqMaxKHz - tempListSuppressFHSS[i].FreqMinKHz) / band;
                    }
                    else
                    {
                        iCountChannel += Convert.ToInt32(tempListSuppressFHSS[i].FreqMaxKHz - tempListSuppressFHSS[i].FreqMinKHz) / band + 1;
                    }
                }

                if (iCountChannel < 5)
                {
                    lSuppressFHSS = tempListSuppressFHSS;
                    fCheckCount = true;
                }
                else
                {
                    fCheckCount = false;
                }
            }
            catch { }

            return fCheckCount;
        }
        //public static bool IsCheckCountChannel(List<TableSuppressFHSS> lSuppressFHSS, TableSuppressFHSS SuppressFHSS)
        //{
        //    bool fCheckCount = false;

        //    int iCountChannel = 0;

        //    try
        //    {
        //        List<TableSuppressFHSS> tempListSuppressFHSS = new List<TableSuppressFHSS>();
        //        tempListSuppressFHSS.AddRange(lSuppressFHSS);
        //        tempListSuppressFHSS.Add(SuppressFHSS);

        //        for (int i = 0; i < tempListSuppressFHSS.Count; i++)
        //        {
        //            if (Convert.ToInt32(tempListSuppressFHSS[i].FreqMaxKHz - tempListSuppressFHSS[i].FreqMinKHz) % 30000 == 0)
        //            {
        //                iCountChannel += Convert.ToInt32(tempListSuppressFHSS[i].FreqMaxKHz - tempListSuppressFHSS[i].FreqMinKHz) / 30000;
        //            }
        //            else
        //            {
        //                iCountChannel += Convert.ToInt32(tempListSuppressFHSS[i].FreqMaxKHz - tempListSuppressFHSS[i].FreqMinKHz) / 30000 + 1;
        //            }
        //        }

        //        if (iCountChannel < 5)
        //        {
        //            lSuppressFHSS = tempListSuppressFHSS;
        //            fCheckCount = true;
        //        }
        //        else
        //        {
        //            fCheckCount = false;
        //        }
        //    }
        //    catch { }

        //    return fCheckCount;
        //}
    }
}
