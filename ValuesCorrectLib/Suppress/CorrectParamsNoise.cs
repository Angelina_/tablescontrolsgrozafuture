﻿using ModelsTablesDBLib;
using System;
using System.Windows.Forms;
using TableEvents;

namespace ValuesCorrectLib
{
    public class CorrectParamsNoise
    {
        /// <summary>
        /// Получить параметры помехи
        /// </summary>
        /// <param name="interferenceParam"></param>
        /// <param name="dFreq"> Для рассчета девиации ЛЧМ </param>
        /// <param name="bLetter"> Для рассчета девиации ЛЧМ </param>
        /// <returns></returns>
        public static bool GetParamsNoise(InterferenceParam interferenceParam, double dFreq, byte bLetter)
        {
            try
            {
                double dDev = 0;

                switch (ComboBoxEditor.IndComboBoxModulation)
                {
                    // ЧМШ
                    case 0:
                        interferenceParam.Modulation = 0x01;
                        interferenceParam.Deviation = Convert.ToByte(ComboBoxEditor.IndComboBoxDeviation + 1);
                        interferenceParam.Manipulation = 0;
                        break;

                    // ЧМ-2
                    case 1:
                        interferenceParam.Modulation = 0x02;

                        dDev = Convert.ToDouble(ComboBoxEditor.TextComboBoxDeviation);
                        dDev = (1 / (2000 * dDev)) * 1000000;

                        if (Convert.ToDouble(ComboBoxEditor.TextComboBoxManipulation) < dDev)
                        {
                            System.Windows.Forms.MessageBox.Show(SMessageError.mesErrIncorrSetManip, SMessageError.mesErr, System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                            return false;
                        }

                        interferenceParam.Deviation = Convert.ToByte(ComboBoxEditor.IndComboBoxDeviation + 1);
                        interferenceParam.Manipulation = Convert.ToByte(ComboBoxEditor.IndComboBoxManipulation + 1);
                        break;

                    // ЧМ-4
                    case 2:
                        interferenceParam.Modulation = 0x03;

                        dDev = Convert.ToDouble(ComboBoxEditor.TextComboBoxDeviation);
                        dDev = (3 / (2000 * dDev)) * 1000000;

                        if (Convert.ToDouble(ComboBoxEditor.TextComboBoxManipulation) < dDev)
                        {
                            System.Windows.Forms.MessageBox.Show(SMessageError.mesErrIncorrSetManip, SMessageError.mesErr, System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                            return false;
                        }

                        interferenceParam.Deviation = Convert.ToByte(ComboBoxEditor.IndComboBoxDeviation + 1);
                        interferenceParam.Manipulation = Convert.ToByte(ComboBoxEditor.IndComboBoxManipulation + 1);
                        break;

                    // ЧМ-8
                    case 3:
                        interferenceParam.Modulation = 0x04;

                        dDev = Convert.ToDouble(ComboBoxEditor.TextComboBoxDeviation);
                        dDev = (7 / (2000 * dDev)) * 1000000;

                        if (Convert.ToDouble(ComboBoxEditor.TextComboBoxManipulation) < dDev)
                        {
                            System.Windows.Forms.MessageBox.Show(SMessageError.mesErrIncorrSetManip, SMessageError.mesErr, System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                            return false;
                        }

                        interferenceParam.Deviation = Convert.ToByte(ComboBoxEditor.IndComboBoxDeviation + 1);
                        interferenceParam.Manipulation = Convert.ToByte(ComboBoxEditor.IndComboBoxManipulation + 1);
                        break;

                    // ФМн
                    case 4:
                        interferenceParam.Modulation = 0x05;
                        // в байт девиации значение манипуляции
                        interferenceParam.Deviation = 0;
                        if (ComboBoxEditor.IndComboBoxManipulation == 0) { interferenceParam.Manipulation = 0x0A; }
                        else { interferenceParam.Manipulation = Convert.ToByte(ComboBoxEditor.IndComboBoxManipulation); }
                        break;

                    // ФМн-4
                    case 5:
                        interferenceParam.Modulation = 0x06;
                        // в байт девиации значение манипуляции
                        interferenceParam.Deviation = 0;
                        if (ComboBoxEditor.IndComboBoxManipulation == 0) { interferenceParam.Manipulation = 0x0A; }
                        else { interferenceParam.Manipulation = Convert.ToByte(ComboBoxEditor.IndComboBoxManipulation); }
                        break;

                    // ФМн-8
                    case 6:
                        interferenceParam.Modulation = 0x07;
                        // в байт девиации значение манипуляции
                        interferenceParam.Deviation = 0;
                        if (ComboBoxEditor.IndComboBoxManipulation == 0) { interferenceParam.Manipulation = 0x0A; }
                        else { interferenceParam.Manipulation = Convert.ToByte(ComboBoxEditor.IndComboBoxManipulation); }
                        break;

                    // ЗШ (КФМ)
                    case 7:
                        interferenceParam.Modulation = 0x08;
                        interferenceParam.Deviation = 0;

                        if (bLetter > 0)
                        {
                            switch (bLetter)
                            {
                                case 1:
                                case 2:
                                    if (Convert.ToByte(ComboBoxEditor.IndComboBoxManipulation + 1) > 2)
                                    {
                                        System.Windows.Forms.MessageBox.Show(SMessageError.mesErrIncorrSetManip6_25, SMessageError.mesErr, System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                                        return false;
                                    }
                                    break;

                                case 3:
                                case 4:
                                    if (Convert.ToByte(ComboBoxEditor.IndComboBoxManipulation + 1) > 3)
                                    {
                                        System.Windows.Forms.MessageBox.Show(SMessageError.mesErrIncorrSetManip12_5, SMessageError.mesErr, System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                                        return false;
                                    }
                                    break;

                                default:
                                    break;
                            }
                        }

                        interferenceParam.Manipulation = Convert.ToByte(ComboBoxEditor.IndComboBoxManipulation + 1);

                        break;

                    // ЛЧМ
                    case 8:
                        interferenceParam.Modulation = 0x0B;

                        int iDev = ComboBoxEditor.ValueDecUpDownDeviation;

                        double Freq1 = 0;
                        double Freq2 = 0;

                        if (bLetter > 0)
                        {
                            switch (bLetter)
                            {
                                case 1:
                                    Freq1 = Math.Abs(RangesLetters.FREQ_START_LETTER_1 - dFreq);
                                    Freq2 = Math.Abs(RangesLetters.FREQ_START_LETTER_2 - dFreq);
                                    break;

                                case 2:
                                    Freq1 = Math.Abs(RangesLetters.FREQ_START_LETTER_2 - dFreq);
                                    Freq2 = Math.Abs(RangesLetters.FREQ_START_LETTER_3 - dFreq);
                                    break;

                                case 3:
                                    Freq1 = Math.Abs(RangesLetters.FREQ_START_LETTER_3 - dFreq);
                                    Freq2 = Math.Abs(RangesLetters.FREQ_START_LETTER_4 - dFreq);
                                    break;

                                case 4:
                                    Freq1 = Math.Abs(RangesLetters.FREQ_START_LETTER_4 - dFreq);
                                    Freq2 = Math.Abs(RangesLetters.FREQ_START_LETTER_5 - dFreq);
                                    break;

                                case 5:
                                    Freq1 = Math.Abs(RangesLetters.FREQ_START_LETTER_5 - dFreq);
                                    Freq2 = Math.Abs(RangesLetters.FREQ_START_LETTER_6 - dFreq);
                                    break;

                                case 6:
                                    Freq1 = Math.Abs(RangesLetters.FREQ_START_LETTER_6 - dFreq);
                                    Freq2 = Math.Abs(RangesLetters.FREQ_START_LETTER_7 - dFreq);
                                    break;

                                case 7:
                                    Freq1 = Math.Abs(RangesLetters.FREQ_START_LETTER_7 - dFreq);
                                    Freq2 = Math.Abs(RangesLetters.FREQ_START_LETTER_8 - dFreq);
                                    break;

                                case 8:
                                    Freq1 = Math.Abs(RangesLetters.FREQ_START_LETTER_8 - dFreq);
                                    Freq2 = Math.Abs(RangesLetters.FREQ_START_LETTER_9 - dFreq);
                                    break;

                                case 9:
                                    Freq1 = Math.Abs(RangesLetters.FREQ_START_LETTER_9 - dFreq);
                                    switch(PropGlobalProperty.Amplifiers)
                                    {
                                        case 0:
                                            Freq2 = Math.Abs(RangesLetters.FREQ_START_LETTER_10 - dFreq);
                                            break;

                                        case 1:
                                            Freq2 = Math.Abs(RangesLetters.FREQ_START_LETTER_10_MODIF - dFreq);
                                            break;
                                    }
                                    break;

                                case 10:
                                    switch (PropGlobalProperty.Amplifiers)
                                    {
                                        case 0:
                                            Freq1 = Math.Abs(RangesLetters.FREQ_START_LETTER_10 - dFreq);
                                            break;

                                        case 1:
                                            Freq1 = Math.Abs(RangesLetters.FREQ_START_LETTER_10_MODIF - dFreq);
                                            break;
                                    }
                                    Freq2 = Math.Abs(RangesLetters.FREQ_STOP_LETTER_10 - dFreq);
                                    break;

                                default:
                                    break;
                            }

                            if (iDev > Math.Min(Freq1, Freq2))
                            {
                                iDev = Convert.ToInt32(Math.Min(Freq1, Freq2));
                                System.Windows.MessageBox.Show(SMessages.mesChangeValDeviationMin + iDev, SMessages.mesMessage, System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Information);
                            }
                        }

                        byte bDev = 0;
                        if (iDev <= 1270)
                        {
                            bDev = Convert.ToByte(Math.Floor(Convert.ToDecimal(iDev)) / 10);
                        }
                        else
                        {
                            bDev = Convert.ToByte(Math.Floor(Convert.ToDecimal(iDev)) / 1000);
                            bDev |= (1 << 7); // Записать единицу в старший бит
                        }
                        interferenceParam.Deviation = bDev;

                        int iMan = ComboBoxEditor.ValueDecUpDownManipulation;
                        byte bMan = 0;
                        if (iMan <= 1270)
                        {
                            bMan = Convert.ToByte(Math.Floor(Convert.ToDecimal(iMan)) / 10);
                        }
                        else
                        {
                            bMan = Convert.ToByte(Math.Floor(Convert.ToDecimal(iMan)) / 1000);
                            bMan |= (1 << 7); // Записать единицу в старший бит
                        }
                        interferenceParam.Manipulation = bMan;

                        break;

                    default:
                        interferenceParam.Modulation = 0x06;
                        interferenceParam.Deviation = 0;
                        interferenceParam.Manipulation = 1;
                        break;
                }

                if (ComboBoxEditor.IndComboBoxModulation == 8) { interferenceParam.Duration = 0; }
                else { interferenceParam.Duration = Convert.ToByte(ComboBoxEditor.IndComboBoxDuration + 1); }
            }
            catch { }
            
            return true;
        }

    }
}
