﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;

namespace ValuesCorrectLib
{
    public static class TextBoxEditor
    {
        #region TextBox Correct FreqKHz
        public static readonly DependencyProperty TextBoxDoubleFreqKHzChanged = 
            DependencyProperty.RegisterAttached("TextBoxDoubleFreqKHz", typeof(bool), typeof(TextBoxEditor), 
            new FrameworkPropertyMetadata(false, OnTextBoxDoubleFreqKHzChanged));

        private static void OnTextBoxDoubleFreqKHzChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var textBox = sender as TextBox;
            if (textBox == null) return;

            var wasBound = (bool)(e.OldValue);
            var needToBind = (bool)(e.NewValue);
            if (wasBound)
            {
                textBox.PreviewTextInput -= TextBoxDoubleFreqKHzPreviewInput;
            }
            if (needToBind)
            {
                textBox.PreviewTextInput += TextBoxDoubleFreqKHzPreviewInput;
            }
        }

        private static void TextBoxDoubleFreqKHzPreviewInput(object sender, TextCompositionEventArgs e)
        {
            try
            {
                char[] tbText = (sender as TextBox).Text.ToCharArray();

                List<string> s = new List<string>();
                for (int i = 0; i < tbText.Length; i++)
                {
                    s.Add(tbText[i].ToString());
                }

                if (e.Text == "," || e.Text == ".")
                {
                    if ((s.IndexOf(",") != -1) || s.IndexOf(".") != -1)
                    {
                        e.Handled = true;
                    }
                    // ,
                    return;
                }

                if ((Convert.ToChar(e.Text) >= '0') && (Convert.ToChar(e.Text) <= '9'))
                {
                    // цифра
                    return;
                }

                // остальные символы запрещены
                e.Handled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public static void SetTextBoxDoubleFreqKHzPreviewInput(TextBox target, bool value)
        {
            target.SetValue(TextBoxDoubleFreqKHzChanged, value);
        }

        public static bool GetTextBoxDoublePreviewInput(TextBox target)
        {
            return (bool)target.GetValue(TextBoxDoubleFreqKHzChanged);
        }
        #endregion

        #region TextBox Correct Double Value
        public static readonly DependencyProperty TextBoxDoubleValuesChanged =
            DependencyProperty.RegisterAttached("TextBoxDoubleValues", typeof(bool), typeof(TextBoxEditor),
            new FrameworkPropertyMetadata(false, OnTextBoxDoubleValuesChanged));

        private static void OnTextBoxDoubleValuesChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var textBox = sender as TextBox;
            if (textBox == null) return;

            var wasBound = (bool)(e.OldValue);
            var needToBind = (bool)(e.NewValue);
            if (wasBound)
            {
                textBox.PreviewTextInput += TextBoxDoubleValuesPreviewInput;
                textBox.KeyUp -= TextBoxDoubleValuesKeyUp;
            }
            if (needToBind)
            {
                textBox.PreviewTextInput += TextBoxDoubleValuesPreviewInput;
                textBox.KeyUp += TextBoxDoubleValuesKeyUp;
            }
        }

        private static void TextBoxDoubleValuesPreviewInput(object sender, TextCompositionEventArgs e)
        {
            try
            {
                char[] tbText = (sender as TextBox).Text.ToCharArray();

                List<string> s = new List<string>();
                for (int i = 0; i < tbText.Length; i++)
                {
                    s.Add(tbText[i].ToString());
                }

                if (e.Text == ".")
                {
                    if (s.IndexOf(".") != -1)
                    {
                        e.Handled = true;
                    }
                    // .
                    return;
                }

                //if (e.Text == "," || e.Text == ".")
                //{
                //    if ((s.IndexOf(",") != -1) || s.IndexOf(".") != -1)
                //    {
                //        e.Handled = true;
                //    }
                //    // ,
                //    return;
                //}

                if ((Convert.ToChar(e.Text) >= '0') && (Convert.ToChar(e.Text) <= '9'))
                {
                    // цифра
                    return;
                }

                // остальные символы запрещены
                e.Handled = true;

                //e.Handled = "0123456789.".IndexOf(e.Text) < 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private static void TextBoxDoubleValuesKeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                var textbox = sender as TextBox;
                if (textbox == null) return;
                if (e.Key == Key.OemMinus || e.Key == Key.Subtract || e.Key == Key.Back || e.Key == Key.Escape) //e.Key==Key.OemComma||
                    return;

                var eS = textbox.Text[textbox.Text.Length - 1];
                if (eS.ToString() == "," || eS.ToString() == ".")
                {
                    string text = textbox.Text.Remove(textbox.Text.Length - 1, 1);

                    //Char separator = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator[0];
                    //String Source = textbox.Text.Replace(',', separator);
                    //Source = Source.Replace('.', separator);
                    //textbox.Text = Source;
                    textbox.SelectionStart = textbox.Text.Length;
                    textbox.SelectionLength = 0;
                    if (!(text.Contains(",") || text.Contains(".")))
                        return;
                }

                if (textbox.Text.Contains(",") || textbox.Text.Contains(".") || textbox.Text.Contains("-"))
                {
                    if (e.Key == Key.D0 || e.Key == Key.NumPad0)
                        return;
                }

                BindingExpression expression = textbox.GetBindingExpression(TextBox.TextProperty);
                if (expression != null) expression.UpdateSource();
                e.Handled = true;
                //char[] tbText = (sender as TextBox).Text.ToCharArray();
                //var textbox = sender as TextBox;
                //BindingExpression expression;
                //List<string> s = new List<string>();
                //for (int i = 0; i < tbText.Length; i++)
                //{
                //    s.Add(tbText[i].ToString());
                //}

                //if (e.Key == Key.OemComma || e.Key == Key.OemPeriod)
                //{
                //    if (s.Count == 1 && (s[0] == "," || s[0] == "."))
                //    {
                //        String Source = (-1).ToString();
                //        textbox.Text = Source;
                //        expression = textbox.GetBindingExpression(TextBox.TextProperty);
                //        if (expression != null) expression.UpdateSource();
                //    return;
                //    }
                //    else if (s.IndexOf(",") != -1 || s.IndexOf(".") != -1)
                //    {
                //        string text = textbox.Text.Remove(textbox.Text.Length - 1, 1);

                //        if (!(text.Contains(",") || text.Contains(".")))
                //            return;



                //    }

                //}

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public static void SetTextBoxDoubleValues(TextBox target, bool value)
        {
            target.SetValue(TextBoxDoubleValuesChanged, value);
        }

        public static bool GetTextBoxDoubleValues(TextBox target)
        {
            return (bool)target.GetValue(TextBoxDoubleValuesChanged);
        }

        #endregion

        #region TextBox Correct Int Value
        public static readonly DependencyProperty TextBoxIntValuesChanged =
            DependencyProperty.RegisterAttached("TextBoxIntValues", typeof(bool), typeof(TextBoxEditor),
            new FrameworkPropertyMetadata(false, OnTextBoxIntValuesChanged));

        private static void OnTextBoxIntValuesChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var textBox = sender as TextBox;
            if (textBox == null) return;

            var wasBound = (bool)(e.OldValue);
            var needToBind = (bool)(e.NewValue);
            if (wasBound)
            {
                textBox.PreviewTextInput += TextBoxIntValuesPreviewInput;
                textBox.KeyUp -= TextBoxIntValuesKeyUp;
            }
            if (needToBind)
            {
                textBox.PreviewTextInput += TextBoxIntValuesPreviewInput;
                textBox.KeyUp += TextBoxIntValuesKeyUp;
            }
        }

        private static void TextBoxIntValuesPreviewInput(object sender, TextCompositionEventArgs e)
        {
            try
            {
                // остальные символы запрещены
                e.Handled = "0123456789".IndexOf(e.Text) < 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private static void TextBoxIntValuesKeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                var textbox = sender as TextBox;
                if (textbox == null) return;
                if (e.Key == Key.OemMinus || e.Key == Key.Subtract || e.Key == Key.Back || e.Key == Key.Escape) //e.Key==Key.OemComma||
                    return;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public static void SetTextBoxIntValues(TextBox target, bool value)
        {
            target.SetValue(TextBoxIntValuesChanged, value);
        }

        public static bool GetTextBoxIntValues(TextBox target)
        {
            return (bool)target.GetValue(TextBoxIntValuesChanged);
        }

        #endregion

        #region CommitOnTyping
        public static readonly DependencyProperty CommitOnTypingProperty = 
            DependencyProperty.RegisterAttached("CommitOnTyping", typeof(bool), typeof(TextBoxEditor), 
            new FrameworkPropertyMetadata(false, OnCommitOnTypingChanged));

        private static void OnCommitOnTypingChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var textbox = sender as TextBox;
            if (textbox == null) return;

            var wasBound = (bool)(e.OldValue);
            var needToBind = (bool)(e.NewValue);

            if (wasBound)
                textbox.KeyUp -= TextBoxCommitValueWhileTyping;

            if (needToBind)
                textbox.KeyUp += TextBoxCommitValueWhileTyping;
        }

        static void TextBoxCommitValueWhileTyping(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.OemMinus || e.Key == Key.Escape || e.Key == Key.OemPeriod || e.Key == Key.OemComma || e.Key == Key.Decimal)
                return;

            var textbox = sender as TextBox;
            if (textbox == null) return;
            BindingExpression expression = textbox.GetBindingExpression(TextBox.TextProperty);
            if (expression != null)
            {
                expression.UpdateSource();
            }
            e.Handled = true;
        }

        public static void SetCommitOnTyping(TextBox target, bool value)
        {
            target.SetValue(CommitOnTypingProperty, value);
        }

        public static bool GetCommitOnTyping(TextBox target)
        {
            return (bool)target.GetValue(CommitOnTypingProperty);
        }
        #endregion

        #region OnTyping Float

        public static readonly DependencyProperty CommitOnFloatTypingProperty = DependencyProperty.RegisterAttached("CommitOnFloatTyping", typeof(bool), typeof(TextBoxEditor), new FrameworkPropertyMetadata(false, OnCommitOnFloatTypingChanged));

        private static void OnCommitOnFloatTypingChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var textbox = sender as TextBox;
            if (textbox == null) return;

            var wasBound = (bool)(e.OldValue);
            var needToBind = (bool)(e.NewValue);

            if (wasBound)
            {
                textbox.PreviewTextInput -= TextboxFloatCommitPreviewTextInput;
                textbox.KeyUp -= TextBoxFloatCommitValueWhileTyping;
                //textbox.PreviewKeyDown -= Textbox_PreviewKeyDown;
            }

            if (needToBind)
            {
                textbox.PreviewTextInput += TextboxFloatCommitPreviewTextInput;
                textbox.KeyUp += TextBoxFloatCommitValueWhileTyping;
                //textbox.PreviewKeyDown += Textbox_PreviewKeyDown;
            }
        }

        static void TextBoxFloatCommitValueWhileTyping(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.OemMinus || e.Key == Key.Escape)
                return;

            var textbox = sender as TextBox;

            if (textbox == null) return;

            if (textbox.Text == "") return;

            string text = textbox.Text.Remove(textbox.Text.Length - 1, 1);
            var charView = GetCharFromKey(e.Key).ToString();

            if ((charView == "." || charView == ",") && !(text.Contains(",") || text.Contains("."))) return;

            if (charView != "." && charView != "," && !int.TryParse(charView, out var result) && e.Key != Key.Back)
                return;

            if (e.Key == Key.Back && textbox.Text == "-")
                return;

            if (textbox.Text.Contains(",") || textbox.Text.Contains("."))
            {

                textbox.Text = SubstituteSeparator(textbox.Text);

                textbox.SelectionStart = textbox.Text.Length;
                textbox.SelectionLength = 0;

                if (e.Key == Key.D0 || e.Key == Key.NumPad0)
                    return;
            }

            BindingExpression expression = textbox.GetBindingExpression(TextBox.TextProperty);
            if (expression != null)
            {
                expression.UpdateSource();
                expression.UpdateTarget();
            }
            e.Handled = true;
        }

        private static string SubstituteSeparator(string text)
        {
            char separator = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator[0];
            string Source = text.Replace(',', separator);
            Source = Source.Replace('.', separator);
            Source = RemoveAdditionalSepparators(Source, separator);
            return Source;
        }

        private static string RemoveAdditionalSepparators(string text, char separator)
        {
            int index = text.IndexOf(separator);
            text = text.Trim(separator);
            if (!text.Contains(separator))
                text = text.Insert(index, separator.ToString());
            return text;
        }


        private static void TextboxFloatCommitPreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = "-0123456789.,".IndexOf(e.Text) < 0;
        }


        public static void SetCommitOnFloatTyping(TextBox target, bool value)
        {
            target.SetValue(CommitOnFloatTypingProperty, value);
        }

        public static bool GetCommitOnFloatTyping(TextBox target)
        {
            return (bool)target.GetValue(CommitOnFloatTypingProperty);
        }
        #endregion

        #region
        public enum MapType : uint
        {
            MAPVK_VK_TO_VSC = 0x0,
            MAPVK_VSC_TO_VK = 0x1,
            MAPVK_VK_TO_CHAR = 0x2,
            MAPVK_VSC_TO_VK_EX = 0x3,
        }

        [DllImport("user32.dll")]
        public static extern int ToUnicode(
            uint wVirtKey,
            uint wScanCode,
            byte[] lpKeyState,
            [Out, MarshalAs(UnmanagedType.LPWStr, SizeParamIndex = 4)]
            StringBuilder pwszBuff,
            int cchBuff,
            uint wFlags);

        [DllImport("user32.dll")]
        public static extern bool GetKeyboardState(byte[] lpKeyState);

        [DllImport("user32.dll")]
        public static extern uint MapVirtualKey(uint uCode, MapType uMapType);

        public static char GetCharFromKey(Key key)
        {
            char ch = ' ';

            int virtualKey = KeyInterop.VirtualKeyFromKey(key);
            byte[] keyboardState = new byte[256];
            GetKeyboardState(keyboardState);

            uint scanCode = MapVirtualKey((uint)virtualKey, MapType.MAPVK_VK_TO_VSC);
            StringBuilder stringBuilder = new StringBuilder(2);

            int result = ToUnicode((uint)virtualKey, scanCode, keyboardState, stringBuilder, stringBuilder.Capacity, 0);
            switch (result)
            {
                case -1:
                    break;
                case 0:
                    break;
                case 1:
                    {
                        ch = stringBuilder[0];
                        break;
                    }
                default:
                    {
                        ch = stringBuilder[0];
                        break;
                    }
            }
            return ch;
        }
        #endregion
    }
}
