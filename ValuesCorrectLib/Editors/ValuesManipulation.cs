﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ValuesCorrectLib
{
    public class ValuesManipulation : ObservableCollection<string> 
    {
        public ValuesManipulation()
        {
            InitManipulation(10);
        }

        public void InitManipulation(int countItems)
        {
            Clear();

            switch (countItems)
            {
                case 1:
                    Add("0,2");

                    break;

                case 4:
                    Add("0,4 (2,5 " + SMeaning.meaningMHz + ")");
                    Add("0,16 (6,25 " + SMeaning.meaningMHz + ")");
                    Add("0,08 (12,5 " + SMeaning.meaningMHz + ")");
                    Add("0,04 (25 " + SMeaning.meaningMHz + ")");

                    break;

                case 6:
                    Add("62,5 (16 " + SMeaning.meaningkHz + ")");
                    Add("125 (8 " + SMeaning.meaningkHz + ")");
                    Add("250 (4 " + SMeaning.meaningkHz + ")");
                    Add("500 (2 " + SMeaning.meaningkHz + ")");
                    Add("1000 (1 " + SMeaning.meaningkHz + ")");
                    Add("2000 (0,5 " + SMeaning.meaningkHz + ")");

                    break;

                //case 9:
                //    Add("0,2");
                //    Add("0,5");
                //    Add("1");
                //    Add("5");
                //    Add("10");
                //    Add("20");
                //    Add("40");
                //    Add("80");
                //    Add("400");

                //    break;

                case 10:
                    Add("0,08 (12,5 " + SMeaning.meaningMHz + ")");
                    Add("0,2 (5 " + SMeaning.meaningMHz + ")");
                    Add("0,5 (2 " + SMeaning.meaningMHz + ")");
                    Add("1 (1 " + SMeaning.meaningMHz + ")");
                    Add("5 (200 " + SMeaning.meaningkHz + ")");
                    Add("10 (100 " + SMeaning.meaningkHz + ")");
                    Add("20 (50 " + SMeaning.meaningkHz + ")");
                    Add("40 (25 " + SMeaning.meaningkHz + ")");
                    Add("80 (12,5 " + SMeaning.meaningkHz + ")");
                    Add("400 (2,5 " + SMeaning.meaningkHz + ")");

                    break;

                default:
                    break;
            }
        }

        /// <summary>
        /// отобразить виды манипуляции (в зависимости от модуляции)
        /// </summary>
        /// <param name="iTypeModulation"> тип модуляции </param>
        /// <param name="iInd"> индекс </param>
        public bool SetManipulation(int iTypeModulation, bool bEnabled)
        {
            switch (iTypeModulation)
            {
                case 1: // ЧМ-2
                case 2: // ЧМ-4
                case 3: // ЧМ-8
                    InitManipulation(6);
                    break;
                
                case 4: // ФМн
                case 5: // ФМн-4
                case 6: // ФМн-8
                    InitManipulation(10);
                    break;
                
                case 7: // КФМ
                    InitManipulation(4);
                    break;

                //case 8: // Речеподобная ЧМ 
                //    //InitManipulation(4);
                //    break;

                //case 9: // ЛЧМ 
                //    //InitManipulation(4);
                //    break;

                default:
                    bEnabled = false;
                    Clear();
                    break;
            }
            return bEnabled;
        }
    }
}
