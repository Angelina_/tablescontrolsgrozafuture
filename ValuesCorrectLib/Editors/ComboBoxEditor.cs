﻿using System;
using System.Collections;
using System.Windows;
using System.Windows.Controls;
using TableEvents;
using Xceed.Wpf.Toolkit;

namespace ValuesCorrectLib
{
    public static class ComboBoxEditor
    {
        #region Params
        private static int indComboBoxModulation = 4;
        private static int indComboBoxManipulation = 8;
        private static int indComboBoxDeviation = 0;
        private static int indComboBoxDuration = 0;

        private static int countComboBoxManipulation = -1;

        private static string textComboBoxDeviation = string.Empty;
        private static string textComboBoxManipulation = string.Empty;

        private static int valueDecUpDownDeviation = 10;
        private static int valueDecUpDownManipulation = 10;

        private static bool isEnabledDeviation = true;
        private static bool isEnabledManipulation = true;

        public static bool isSelected_LChM = false;

        public static int IndComboBoxModulation
        {
            get { return indComboBoxModulation; }
            set
            {
                if (indComboBoxModulation == value)
                    return;
                indComboBoxModulation = value;
            }
        }

        public static int IndComboBoxManipulation
        {
            get { return indComboBoxManipulation; }
            set
            {
                if (indComboBoxManipulation == value)
                    return;
                indComboBoxManipulation = value;
            }
        }

        public static int IndComboBoxDeviation
        {
            get { return indComboBoxDeviation; }
            set
            {
                if (indComboBoxDeviation == value)
                    return;
                indComboBoxDeviation = value;
            }
        }

        public static int IndComboBoxDuration
        {
            get { return indComboBoxDuration; }
            set
            {
                if (indComboBoxDuration == value)
                    return;
                indComboBoxDuration = value;
            }
        }

        public static int CountComboBoxManipulation
        {
            get { return countComboBoxManipulation; }
            set
            {
                if (countComboBoxManipulation == value)
                    return;
                countComboBoxManipulation = value;
            }
        }

        public static string TextComboBoxDeviation
        {
            get
            {
                return textComboBoxDeviation;
            }
            set
            {
                if (textComboBoxDeviation == value)
                    return;
                textComboBoxDeviation = value;
            }
        }

        public static string TextComboBoxManipulation
        {
            get
            {
                string[] strMan = textComboBoxManipulation.Split('(');
                textComboBoxManipulation = strMan[0];
                return textComboBoxManipulation;
            }
            set
            {
                if (textComboBoxManipulation == value)
                    return;
                textComboBoxManipulation = value;
            }
        }

        public static int ValueDecUpDownDeviation
        {
            get { return valueDecUpDownDeviation; }
            set
            {
                if (valueDecUpDownDeviation == value)
                    return;
                valueDecUpDownDeviation = value;
            }
        }

        public static int ValueDecUpDownManipulation
        {
            get { return valueDecUpDownManipulation; }
            set
            {
                if (valueDecUpDownManipulation == value)
                    return;
                valueDecUpDownManipulation = value;
            }
        }

        public static bool IsEnabledDeviation
        {
            get { return isEnabledDeviation; }
            set
            {
                if (isEnabledDeviation == value)
                    return;
                isEnabledDeviation = value;
            }
        }

        public static bool IsEnabledManipulation
        {
            get { return isEnabledManipulation; }
            set
            {
                if (isEnabledManipulation == value)
                    return;
                isEnabledManipulation = value;
            }
        }

        public static bool IsSelected_LChM
        {
            get { return isSelected_LChM; }
            set
            {
                if (isSelected_LChM == value)
                    return;
                isSelected_LChM = value;
            }
        }
        #endregion

        public static byte TypeStation = 2;

        #region ComboBox Modulation
        public static readonly DependencyProperty ComboBoxModulationChanged = DependencyProperty.RegisterAttached("ComboBoxModulation", typeof(bool), typeof(ComboBoxEditor), new FrameworkPropertyMetadata(false, OnComboBoxModulationChanged));
      
        private static void OnComboBoxModulationChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var comboBox = sender as ComboBox;
            if (comboBox == null) return;
            
            var wasBound = (bool)(e.OldValue);
            var needToBind = (bool)(e.NewValue);
            if (wasBound) { comboBox.SelectionChanged -= ComboBoxModulation_SelectionChanged; }
            if (needToBind) { comboBox.SelectionChanged += ComboBoxModulation_SelectionChanged; }
        }

        public static void ComboBoxModulation_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                var parent = (sender as ComboBox).Parent;
                if (parent is Grid)
                {
                    foreach (var child in (parent as Grid).Children)
                    {
                        if (child is ComboBox)
                        {
                            switch ((child as ComboBox).Name)
                            {
                                case "cbModulation":
                                    IndComboBoxModulation = (child as ComboBox).SelectedIndex;

                                    if (IndComboBoxModulation == 8)
                                    {
                                        IsSelected_LChM = true;
                                    }
                                    else
                                    {
                                        IsSelected_LChM = false;
                                    }

                                    break;

                                case "cbManipulation":
                                    if (IndComboBoxModulation < 4)
                                    {
                                        IsEnabledManipulation = ((child as ComboBox).ItemsSource as ValuesManipulation).SetManipulation(IndComboBoxModulation, true);
                                        if(!IsEnabledManipulation) (child as ComboBox).IsEnabled = IsEnabledManipulation;
                                        else (child as ComboBox).SelectedIndex = 0;
                                    }
                                    if (IndComboBoxModulation > 3 & IndComboBoxModulation < 7)
                                    {
                                        IsEnabledManipulation = ((child as ComboBox).ItemsSource as ValuesManipulation).SetManipulation(IndComboBoxModulation, true);
                                        if (!IsEnabledManipulation) (child as ComboBox).IsEnabled = IsEnabledManipulation;
                                        else (child as ComboBox).SelectedIndex = 8;
                                    }
                                    if (IndComboBoxModulation == 7)
                                    {
                                        IsEnabledManipulation = ((child as ComboBox).ItemsSource as ValuesManipulation).SetManipulation(IndComboBoxModulation, true);
                                        if (!IsEnabledManipulation) (child as ComboBox).IsEnabled = IsEnabledManipulation;
                                        else (child as ComboBox).SelectedIndex = 0;
                                    }
                                    if (IndComboBoxModulation == 8)
                                    {
                                        
                                        //IsEnabledManipulation = ((child as ComboBox).ItemsSource as ValuesManipulation).SetManipulation(IndComboBoxModulation, true);
                                        //if (!IsEnabledManipulation) (child as ComboBox).IsEnabled = IsEnabledManipulation;
                                        //else (child as ComboBox).SelectedIndex = 0;
                                    }
                                    //if (IndComboBoxModulation == 9)
                                    //{
                                    //    IsEnabledManipulation = ((child as ComboBox).ItemsSource as ValuesManipulation).SetManipulation(IndComboBoxModulation, true);
                                    //    if (!IsEnabledManipulation) (child as ComboBox).IsEnabled = IsEnabledManipulation;
                                    //    else (child as ComboBox).SelectedIndex = 0;
                                    //}
                                    ////////////////
                                    if (((child as ComboBox).ItemsSource as ValuesManipulation).Count != 0)
                                    {
                                        IndComboBoxManipulation = (child as ComboBox).SelectedIndex;
                                        CountComboBoxManipulation = (child as ComboBox).Items.Count;
                                        TextComboBoxManipulation = (child as ComboBox).SelectedValue.ToString();
                                        (child as ComboBox).IsEnabled = IsEnabledManipulation;
                                    }
                                    ///////////////////
                                    break;

                                case "cbDeviation":

                                    if (IsSelected_LChM)
                                    {
                                        (child as ComboBox).Visibility = Visibility.Hidden;
                                    }
                                    else
                                    {
                                        (child as ComboBox).Visibility = Visibility.Visible;
                                    }

                                    IsEnabledDeviation = ((child as ComboBox).ItemsSource as ValuesDeviation).SetDeviation(IndComboBoxModulation, true);
                                    if (!IsEnabledDeviation)
                                    {
                                        (child as ComboBox).IsEnabled = IsEnabledDeviation;
                                    }
                                    else
                                    {
                                        (child as ComboBox).SelectedIndex = 0;
                                    }

                                    //////////////////////////////////
                                    if (((child as ComboBox).ItemsSource as ValuesDeviation).Count != 0)
                                    {
                                        IndComboBoxDeviation = (child as ComboBox).SelectedIndex;
                                        TextComboBoxDeviation = (child as ComboBox).SelectedValue.ToString();
                                        (child as ComboBox).IsEnabled = IsEnabledDeviation;
                                    }
                                    //////////////////////////////////////

                                    break;

                                case "cbDuration":
                                    ///////////////////////////////
                                    IndComboBoxDuration = (child as ComboBox).SelectedIndex;
                                    //////////////////////////////////

                                    (child as ComboBox).SelectedIndex = 0;
                                    break;

                                default:
                                    break;
                            }
                        }

                        if (child is DecimalUpDown)
                        {
                            if ((child as DecimalUpDown).Name == "dudDeviationLChM")
                            {
                                if (IsSelected_LChM) { (child as DecimalUpDown).Visibility = Visibility.Visible; }
                                else { (child as DecimalUpDown).Visibility = Visibility.Hidden; }
                            }
                            if ((child as DecimalUpDown).Name == "dudManipulationLChM")
                            {
                                if (IsSelected_LChM) { (child as DecimalUpDown).Visibility = Visibility.Visible; }
                                else { (child as DecimalUpDown).Visibility = Visibility.Hidden; }
                            }
                        }

                        if (child is Label)
                        {
                            if ((child as Label).Name == "lManipulationLChM")
                            {
                                if (IsSelected_LChM) { (child as Label).Visibility = Visibility.Visible; }
                                else { (child as Label).Visibility = Visibility.Hidden; }
                            }
                            if ((child as Label).Name == "lManipulation")
                            {
                                if (IsSelected_LChM) { (child as Label).Visibility = Visibility.Hidden; }
                                else { (child as Label).Visibility = Visibility.Visible; }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message);
            }
        }

        public static void SetComboBoxModulationSelected(ComboBox target, bool value)
        {
            if(PropChangeJammerParams.IsChangeJammerParams)
            {
                 target.SelectedIndex = IndComboBoxModulation;
            }
           
            target.SetValue(ComboBoxModulationChanged, value);
        }

        public static bool GetComboBoxModulationSelected(ComboBox target)
        {
            return (bool)target.GetValue(ComboBoxModulationChanged);
        }
        #endregion

        #region ComboBox Manipulation
        public static readonly DependencyProperty ComboBoxManipulationChanged = DependencyProperty.RegisterAttached("ComboBoxManipulation", typeof(bool), typeof(ComboBoxEditor), new FrameworkPropertyMetadata(false, OnComboBoxManipulationChanged));

        private static void OnComboBoxManipulationChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var comboBox = sender as ComboBox;
            if (comboBox == null) return;

            var wasBound = (bool)(e.OldValue);
            var needToBind = (bool)(e.NewValue);
            if (wasBound) { comboBox.SelectionChanged -= ComboBoxManipulation_SelectionChanged; }
            if (needToBind) { comboBox.SelectionChanged += ComboBoxManipulation_SelectionChanged; }
        }

        private static void ComboBoxManipulation_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var parent = (sender as ComboBox).Parent;
            if (parent is Grid)
            {
                foreach (var child in (parent as Grid).Children)
                {
                    if (child is ComboBox)
                    {
                        switch ((child as ComboBox).Name)
                        {
                            case "cbManipulation":
                                if (((child as ComboBox).ItemsSource as ValuesManipulation).Count != 0)
                                {
                                    IndComboBoxManipulation = (child as ComboBox).SelectedIndex;
                                    CountComboBoxManipulation = (child as ComboBox).Items.Count;
                                    TextComboBoxManipulation = (child as ComboBox).SelectedValue.ToString();
                                }
                                break;

                            default:
                                break;
                        }
                    }
                }
            }
        }

        public static void SetComboBoxManipulationSelected(ComboBox target, bool value)
        {
            if (PropChangeJammerParams.IsChangeJammerParams)
            {
               
                if ((target.ItemsSource as ValuesManipulation).SetManipulation(IndComboBoxModulation, true))
                {
                    target.SelectedIndex = IndComboBoxManipulation;
                    target.IsEnabled = true;
                }
                else
                {
                    target.SelectedIndex = IndComboBoxManipulation;
                    target.IsEnabled = false;
                }

                //(target.ItemsSource as ValuesManipulation).SetManipulation(IndComboBoxModulation, true)
                //target.SelectedIndex = IndComboBoxManipulation;

            }

            target.SetValue(ComboBoxManipulationChanged, value);
        }

        public static bool GetComboBoxManipulationSelected(ComboBox target)
        {
            return (bool)target.GetValue(ComboBoxManipulationChanged);
        }
        #endregion

        #region ComboBox Deviation
        public static readonly DependencyProperty ComboBoxDeviationChanged = DependencyProperty.RegisterAttached("ComboBoxDeviation", typeof(bool), typeof(ComboBoxEditor), new FrameworkPropertyMetadata(false, OnComboBoxDeviationChanged));

        private static void OnComboBoxDeviationChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var comboBox = sender as ComboBox;
            if (comboBox == null) return;

            var wasBound = (bool)(e.OldValue);
            var needToBind = (bool)(e.NewValue);
            if (wasBound) { comboBox.SelectionChanged -= ComboBoxDeviation_SelectionChanged; }
            if (needToBind) { comboBox.SelectionChanged += ComboBoxDeviation_SelectionChanged; }
        }

        private static void ComboBoxDeviation_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var parent = (sender as ComboBox).Parent;
            if (parent is Grid)
            {
                foreach (var child in (parent as Grid).Children)
                {
                    if (child is ComboBox)
                    {
                        switch ((child as ComboBox).Name)
                        {
                            case "cbDeviation":
                                if (((child as ComboBox).ItemsSource as ValuesDeviation).Count != 0)
                                {
                                    IndComboBoxDeviation = (child as ComboBox).SelectedIndex;
                                    TextComboBoxDeviation = (child as ComboBox).SelectedValue.ToString();
                                    IsEnabledDeviation = true;
                                }
                                break;

                            default:
                                break;
                        }
                    }
                }
            }
        }

        public static void SetComboBoxDeviationSelected(ComboBox target, bool value)
        {
            if (PropChangeJammerParams.IsChangeJammerParams)
            {
                if ((target.ItemsSource as ValuesDeviation).SetDeviation(IndComboBoxModulation, true))
                {
                    target.SelectedIndex = IndComboBoxDeviation;
                    target.IsEnabled = true;
                }
                else
                {
                    target.SelectedIndex = IndComboBoxDeviation;
                    target.IsEnabled = false;
                }

                //(target.ItemsSource as ValuesDeviation).SetDeviation(IndComboBoxModulation, true);
                //target.SelectedIndex = IndComboBoxDeviation;
            }

            target.SetValue(ComboBoxDeviationChanged, value);
        }

        public static bool GetComboBoxDeviationSelected(ComboBox target)
        {
            return (bool)target.GetValue(ComboBoxDeviationChanged);
        }
        #endregion

        #region ComboBox Duration
        public static readonly DependencyProperty ComboBoxDurationChanged = DependencyProperty.RegisterAttached("ComboBoxDuration", typeof(bool), typeof(ComboBoxEditor), new FrameworkPropertyMetadata(false, OnComboBoxDurationChanged));

        private static void OnComboBoxDurationChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var comboBox = sender as ComboBox;
            if (comboBox == null) return;

            var wasBound = (bool)(e.OldValue);
            var needToBind = (bool)(e.NewValue);
            if (wasBound) { comboBox.SelectionChanged -= ComboBoxDuration_SelectionChanged; }
            if (needToBind) { comboBox.SelectionChanged += ComboBoxDuration_SelectionChanged; }
        }

        private static void ComboBoxDuration_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var parent = (sender as ComboBox).Parent;
            if (parent is Grid)
            {
                foreach (var child in (parent as Grid).Children)
                {
                    if (child is ComboBox)
                    {
                        switch ((child as ComboBox).Name)
                        {
                            case "cbDuration":
                                IndComboBoxDuration = (child as ComboBox).SelectedIndex;
                                break;

                            default:
                                break;
                        }
                    }
                }
            }
        }

        public static void SetComboBoxDurationSelected(ComboBox target, bool value)
        {
            if (PropChangeJammerParams.IsChangeJammerParams)
            {
                target.SelectedIndex = IndComboBoxDuration;
            }

            target.SetValue(ComboBoxDurationChanged, value);
        }

        public static bool GetComboBoxDurationSelected(ComboBox target)
        {
            return (bool)target.GetValue(ComboBoxDurationChanged);
        }
        #endregion

        #region DecimalUpDown Deviation
        public static readonly DependencyProperty DecimalUpDownDeviationChanged = DependencyProperty.RegisterAttached("DecimalUpDownDeviation", typeof(bool), typeof(ComboBoxEditor), new FrameworkPropertyMetadata(false, OnDecimalUpDownDeviationChanged));

        private static void OnDecimalUpDownDeviationChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var decimalUpDown = sender as DecimalUpDown;
            if (decimalUpDown == null) return;

            var wasBound = (bool)(e.OldValue);
            var needToBind = (bool)(e.NewValue);
            if (wasBound) { decimalUpDown.ValueChanged -= DecimalUpDownDeviation_ValueChanged; }
            if (needToBind) { decimalUpDown.ValueChanged += DecimalUpDownDeviation_ValueChanged; }
        }

        private static void DecimalUpDownDeviation_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            var parent = (sender as DecimalUpDown).Parent;
            if (parent is Grid)
            {
                foreach (var child in (parent as Grid).Children)
                {
                    if (child is DecimalUpDown)
                    {
                        if ((child as DecimalUpDown).Name == "dudDeviationLChM")
                        {
                            ValueDecUpDownDeviation = Convert.ToInt32((child as DecimalUpDown).Value);
                        }
                    }
                }
            }
        }

        public static void SetDecimalUpDownDeviationValue(DecimalUpDown target, bool value)
        {
            if (PropChangeJammerParams.IsChangeJammerParams)
            {
                if (IndComboBoxModulation == 8)
                {
                    target.Visibility = Visibility.Visible;
                    target.Value = ValueDecUpDownDeviation;
                }
            }

            target.SetValue(DecimalUpDownDeviationChanged, value);
        }

        public static bool GetDecimalUpDownDeviationValue(DecimalUpDown target)
        {
            return (bool)target.GetValue(DecimalUpDownDeviationChanged);
        }
        #endregion

        #region DecimalUpDown Manipulation
        public static readonly DependencyProperty DecimalUpDownManipulationChanged = DependencyProperty.RegisterAttached("DecimalUpDownManipulation", typeof(bool), typeof(ComboBoxEditor), new FrameworkPropertyMetadata(false, OnDecimalUpDownManipulationChanged));

        private static void OnDecimalUpDownManipulationChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var decimalUpDown = sender as DecimalUpDown;
            if (decimalUpDown == null) return;

            var wasBound = (bool)(e.OldValue);
            var needToBind = (bool)(e.NewValue);
            if (wasBound) { decimalUpDown.ValueChanged -= DecimalUpDownManipulation_ValueChanged; }
            if (needToBind) { decimalUpDown.ValueChanged += DecimalUpDownManipulation_ValueChanged; }
        }

        private static void DecimalUpDownManipulation_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            var parent = (sender as DecimalUpDown).Parent;
            if (parent is Grid)
            {
                foreach (var child in (parent as Grid).Children)
                {
                    if (child is DecimalUpDown)
                    {
                        if ((child as DecimalUpDown).Name == "dudManipulationLChM")
                        {
                            ValueDecUpDownManipulation = Convert.ToInt32((child as DecimalUpDown).Value);
                        }
                    }
                }
            }
        }

        public static void SetDecimalUpDownManipulationValue(DecimalUpDown target, bool value)
        {
            if (PropChangeJammerParams.IsChangeJammerParams)
            {
                if (IndComboBoxModulation == 8)
                {
                    target.Visibility = Visibility.Visible;
                    target.Value = ValueDecUpDownManipulation;
                }
            }

            target.SetValue(DecimalUpDownManipulationChanged, value);
        }

        public static bool GetDecimalUpDownManipulationValue(DecimalUpDown target)
        {
            return (bool)target.GetValue(DecimalUpDownManipulationChanged);
        }
        #endregion

        #region Label Manipulation
        public static readonly DependencyProperty LabelManipulationChanged = DependencyProperty.RegisterAttached("LabelManipulation", typeof(bool), typeof(ComboBoxEditor), new FrameworkPropertyMetadata(false, OnLabelManipulationChanged));

        private static void OnLabelManipulationChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var label = sender as Label;
            if (label == null) return;


        }

        //public static void LabelManipulation_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    try
        //    {
        //        var parent = (sender as Label).Parent;
        //        if (parent is Grid)
        //        {
        //            foreach (var child in (parent as Grid).Children)
        //            {
        //                if (child is Label)
        //                {
        //                    if ((child as Label).Name == "lManipulationLChM")
        //                    {
        //                        if (IsSelected_LChM) { (child as Label).Visibility = Visibility.Visible; }
        //                        else { (child as Label).Visibility = Visibility.Hidden; }
        //                    }
        //                    if ((child as Label).Name == "lManipulation")
        //                    {
        //                        if (IsSelected_LChM) { (child as Label).Visibility = Visibility.Hidden; }
        //                        else { (child as Label).Visibility = Visibility.Visible; }
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        System.Windows.MessageBox.Show(ex.Message);
        //    }
        //}

        public static void SetLabelManipulationSelected(Label target, bool value)
        {
            if (PropChangeJammerParams.IsChangeJammerParams)
            {
                if (target.Name == "lManipulationLChM")
                {
                    if (IndComboBoxModulation == 8) { target.Visibility = Visibility.Visible; }
                    else { target.Visibility = Visibility.Hidden; }
                }
                if (target.Name == "lManipulation")
                {
                    if (IndComboBoxModulation == 8) { target.Visibility = Visibility.Hidden; }
                    else { target.Visibility = Visibility.Visible; }
                }
            }

            target.SetValue(LabelManipulationChanged, value);
        }

        public static bool GetLabelManipulationSelected(Label target)
        {
            return (bool)target.GetValue(LabelManipulationChanged);
        }
        #endregion
    }
}
