﻿using System.Collections.ObjectModel;

namespace ValuesCorrectLib
{
    public class ValuesModulation : ObservableCollection<string>
    {
        public ValuesModulation()
        {
            Add(STypeModulation.paramChMSh);
            Add(STypeModulation.paramChM2);
            Add(STypeModulation.paramChM4);
            Add(STypeModulation.paramChM8);
            Add(STypeModulation.paramFMn);
            Add(STypeModulation.paramFMn4);
            Add(STypeModulation.paramFMn8);
            Add(STypeModulation.paramZSh);
            Add(STypeModulation.paramLChM);

            //Add("FMN");
            //Add("FM-2");
            //Add("FM-4");
            //Add("FM-8");
            //Add("PSK");
            //Add("PSK-4");
            //Add("PSK-8");
            //Add("BN (CPSK)");
            //Add("LFM");

            //Add("ЧМШ");
            //Add("ЧМ-2");
            //Add("ЧМ-4");
            //Add("ЧМ-8");
            //Add("ФМн");
            //Add("ФМн-4");
            //Add("ФМн-8");
            //Add("ЗШ (КФМ)");
            //Add("ЛЧМ");
        }
    }
}
