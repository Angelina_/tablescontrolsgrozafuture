﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ValuesCorrectLib
{
    public class ValuesDuration : ObservableCollection<string>
    {
        public ValuesDuration()
        {
            InitDuration();
        }

        public void InitDuration()
        {
            Add("0.5");
            Add("1");
            Add("1.5");
            Add("2");
        }

    }
}
