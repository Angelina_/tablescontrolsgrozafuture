﻿using System;
using System.Windows;
using System.Windows.Controls.WpfPropertyGrid;

namespace ValuesCorrectLib
{
    public class InterferenceParamFHSSEditor : PropertyEditor
    {
        public InterferenceParamFHSSEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/SuppressFHSSControl;component/PGEditors/PropertyGridEditorFHSS.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource["InterferenceParamFHSSEditorKey"];

            
        }
       
    }
}
