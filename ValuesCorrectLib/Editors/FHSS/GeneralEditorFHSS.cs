﻿using System;
using System.Windows;
using System.Windows.Controls.WpfPropertyGrid;

namespace ValuesCorrectLib
{
    public class GeneralFreqMinEditor : PropertyEditor
    {
        public GeneralFreqMinEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/SuppressFHSSControl;component/PGEditors/PropertyGridEditorFHSS.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource["GeneralFreqMinEditorKey"];
        }
    }

    public class GeneralFreqMaxEditor : PropertyEditor
    {
        public GeneralFreqMaxEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/SuppressFHSSControl;component/PGEditors/PropertyGridEditorFHSS.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource["GeneralFreqMaxEditorKey"];
        }
    }

    public class GeneralThresholdFHSSEditor : PropertyEditor
    {
        public GeneralThresholdFHSSEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/SuppressFHSSControl;component/PGEditors/PropertyGridEditorFHSS.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource["GeneralThresholdFHSSEditorKey"];
        }
    }

    public class GeneralStepEditor : PropertyEditor
    {
        public GeneralStepEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/SuppressFHSSControl;component/PGEditors/PropertyGridEditorFHSS.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource["GeneralStepEditorKey"];
        }
    }
}
