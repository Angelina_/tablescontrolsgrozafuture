﻿using System.Collections.ObjectModel;

namespace ValuesCorrectLib
{
    public class ValuesDeviation : ObservableCollection<string>
    {
        public ValuesDeviation()
        {
        }

        public void InitDeviation(int countItems)
        {
            Clear();

            switch (countItems)
            {
                //case 1:
                //    Add("2500");

                //    break;

                case 4:
                    Add("3");
                    Add("6");
                    Add("12");
                    Add("100");

                    break;

                case 12:
                    Add("2");
                    Add("4");
                    Add("6");
                    Add("8");
                    Add("10");
                    Add("12");
                    Add("14");
                    Add("16");
                    Add("18");
                    Add("20");
                    Add("22");
                    Add("24");

                    break;

                case 19:
                    Add("3,5");
                    Add("5");
                    Add("7");
                    Add("8,4");
                    Add("10");
                    Add("14");
                    Add("20");
                    Add("32");
                    Add("50");
                    Add("70");
                    Add("100");
                    Add("140");
                    Add("200");
                    Add("300");
                    Add("500");
                    Add("700");
                    Add("1000");
                    Add("1400");
                    Add("2000");

                    //Add("1,75");
                    //Add("2,47");
                    //Add("3,5");
                    //Add("4,2");
                    //Add("5,0");
                    //Add("7,07");
                    //Add("10");
                    //Add("15,8");
                    //Add("25");
                    //Add("35,35");
                    //Add("50");
                    //Add("70");
                    //Add("100");
                    //Add("150");
                    //Add("250");
                    //Add("350");
                    //Add("500");
                    //Add("700");
                    //Add("1000");

                    break;

                default:
                    Clear();
                    break;
            }
        }

        public bool SetDeviation(int iTypeModulation, bool bEnabled)
        {
            switch (iTypeModulation)
            {
                case 0: // ЧМШ
                    InitDeviation(19);
                    break;

                case 1: // ЧМ-2
                case 2: // ЧМ-4
                case 3: // ЧМ-8
                    InitDeviation(12);
                    break;

                case 9: // Речеподобная ЧМ
                    InitDeviation(4);
                    break;

                default:
                    Clear();
                    bEnabled = false;
                    break;
            }

            return bEnabled;
        }

    }

    public class ValuesDeviationNUD
    {
        public ValuesDeviationNUD()
        {

        }



    }
}
