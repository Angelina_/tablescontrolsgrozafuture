﻿using System;
using System.Windows;
using System.Windows.Controls.WpfPropertyGrid;

namespace ValuesCorrectLib
{
    public class GeneralSpecFreqNoteEditor : PropertyEditor
    {
        public GeneralSpecFreqNoteEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/SpecialFrequenciesControl;component/PGEditors/PropertyGridEditorSpecFreq.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource["GeneralSpecFreqNoteEditorKey"];
        }
    }
}
