﻿using System;
using System.Windows;
using System.Windows.Controls.WpfPropertyGrid;

namespace ValuesCorrectLib
{
    #region Задание координат в градусах (DD)
    public class CoordinatesFWSDDEditor : PropertyEditor
    {
        public CoordinatesFWSDDEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/SuppressFWSControl;component/PGEditors/PropertyGridEditorFWSDD.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            this.InlineTemplate = resource["CoordinatesFWSDDEditorKey"];
        }
    }
    public class FreqFWSDDEditor : PropertyEditor
    {
        public FreqFWSDDEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/SuppressFWSControl;component/PGEditors/PropertyGridEditorFWSDD.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource["FreqFWSDDEditorKey"];
        }
    }
    public class ThresholdFWSDDEditor : PropertyEditor
    {
        public ThresholdFWSDDEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/SuppressFWSControl;component/PGEditors/PropertyGridEditorFWSDD.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource["ThresholdFWSDDEditorKey"];
        }
    }
    public class BearingFWSDDEditor : PropertyEditor
    {
        public BearingFWSDDEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/SuppressFWSControl;component/PGEditors/PropertyGridEditorFWSDD.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource["BearingFWSDDEditorKey"];
        }
    }
    public class PriorityFWSDDEditor : PropertyEditor
    {
        public PriorityFWSDDEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/SuppressFWSControl;component/PGEditors/PropertyGridEditorFWSDD.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource["PriorityFWSDDEditorKey"];
        }
    }
    public class InterferenceParamFWSDDEditor : PropertyEditor
    {
        public InterferenceParamFWSDDEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/SuppressFWSControl;component/PGEditors/PropertyGridEditorFWSDD.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource["InterferenceParamFWSDDEditorKey"];
        }
    }
    #endregion

    #region Задание координат в градусах (DDMM)
    public class CoordinatesFWSDDMMEditor : PropertyEditor
    {
        public CoordinatesFWSDDMMEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/SuppressFWSControl;component/PGEditors/PropertyGridEditorFWSDDMM.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            this.InlineTemplate = resource["CoordinatesFWSDDMMEditorKey"];
        }
    }
    public class FreqFWSDDMMEditor : PropertyEditor
    {
        public FreqFWSDDMMEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/SuppressFWSControl;component/PGEditors/PropertyGridEditorFWSDDMM.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource["FreqFWSDDMMEditorKey"];
        }
    }
    public class ThresholdFWSDDMMEditor : PropertyEditor
    {
        public ThresholdFWSDDMMEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/SuppressFWSControl;component/PGEditors/PropertyGridEditorFWSDDMM.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource["ThresholdFWSDDMMEditorKey"];
        }
    }
    public class BearingFWSDDMMEditor : PropertyEditor
    {
        public BearingFWSDDMMEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/SuppressFWSControl;component/PGEditors/PropertyGridEditorFWSDDMM.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource["BearingFWSDDMMEditorKey"];
        }
    }
    public class PriorityFWSDDMMEditor : PropertyEditor
    {
        public PriorityFWSDDMMEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/SuppressFWSControl;component/PGEditors/PropertyGridEditorFWSDDMM.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource["PriorityFWSDDMMEditorKey"];
        }
    }
    public class InterferenceParamFWSDDMMEditor : PropertyEditor
    {
        public InterferenceParamFWSDDMMEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/SuppressFWSControl;component/PGEditors/PropertyGridEditorFWSDDMM.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource["InterferenceParamFWSDDMMEditorKey"];
        }
    }
    #endregion

    #region Задание координат в градусах (DDMMSS)
    public class CoordinatesFWSDDMMSSEditor : PropertyEditor
    {
        public CoordinatesFWSDDMMSSEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/SuppressFWSControl;component/PGEditors/PropertyGridEditorFWSDDMMSS.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            this.InlineTemplate = resource["CoordinatesFWSDDMMSSEditorKey"];
        }
    }
    public class FreqFWSDDMMSSEditor : PropertyEditor
    {
        public FreqFWSDDMMSSEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/SuppressFWSControl;component/PGEditors/PropertyGridEditorFWSDDMMSS.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource["FreqFWSDDMMSSEditorKey"];
        }
    }
    public class ThresholdFWSDDMMSSEditor : PropertyEditor
    {
        public ThresholdFWSDDMMSSEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/SuppressFWSControl;component/PGEditors/PropertyGridEditorFWSDDMMSS.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource["ThresholdFWSDDMMSSEditorKey"];
        }
    }
    public class BearingFWSDDMMSSEditor : PropertyEditor
    {
        public BearingFWSDDMMSSEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/SuppressFWSControl;component/PGEditors/PropertyGridEditorFWSDDMMSS.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource["BearingFWSDDMMSSEditorKey"];
        }
    }
    public class PriorityFWSDDMMSSEditor : PropertyEditor
    {
        public PriorityFWSDDMMSSEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/SuppressFWSControl;component/PGEditors/PropertyGridEditorFWSDDMMSS.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource["PriorityFWSDDMMSSEditorKey"];
        }
    }
    public class InterferenceParamFWSDDMMSSEditor : PropertyEditor
    {
        public InterferenceParamFWSDDMMSSEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/SuppressFWSControl;component/PGEditors/PropertyGridEditorFWSDDMMSS.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource["InterferenceParamFWSDDMMSSEditorKey"];
        }
    }
    #endregion
}
