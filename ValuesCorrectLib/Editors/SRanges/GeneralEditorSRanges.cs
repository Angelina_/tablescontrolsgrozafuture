﻿using System;
using System.Windows;
using System.Windows.Controls.WpfPropertyGrid;

namespace ValuesCorrectLib
{
    public class GeneralSRangesNoteEditor : PropertyEditor
    {
        public GeneralSRangesNoteEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/SectorsRangesControl;component/PGEditors/PropertyGridEditorSRanges.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource["GeneralSRangesNoteEditorKey"];
        }
    }
}
