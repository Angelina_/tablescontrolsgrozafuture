﻿using System;
using System.Windows;
using System.Windows.Controls.WpfPropertyGrid;

namespace ValuesCorrectLib
{
    #region Задание координат в градусах (DD)
    public class CoordinatesDDASPEditor : PropertyEditor
    {
        public CoordinatesDDASPEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/ASPControl;component/PGEditors/PropertyGridEditorASPDD.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource["CoordinatesDDASPEditorKey"];
        }
    }

    public class RoleDDASPEditor : PropertyEditor
    {
        public RoleDDASPEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/ASPControl;component/PGEditors/PropertyGridEditorASPDD.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            this.InlineTemplate = resource["RoleDDASPEditorKey"];
        }
    }
    #endregion

    #region Задание координат в градусах (DDMM)
    public class CoordinatesDDMMASPEditor : PropertyEditor
    {
        public CoordinatesDDMMASPEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/ASPControl;component/PGEditors/PropertyGridEditorASPDDMM.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource["CoordinatesDDMMASPEditorKey"];
        }
    }

    public class RoleDDMMASPEditor : PropertyEditor
    {
        public RoleDDMMASPEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/ASPControl;component/PGEditors/PropertyGridEditorASPDDMM.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            this.InlineTemplate = resource["RoleDDMMASPEditorKey"];
        }
    }
    #endregion

    #region Задание координат в градусах (DDMMSS)
    public class CoordinatesDDMMSSASPEditor : PropertyEditor
    {
        public CoordinatesDDMMSSASPEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/ASPControl;component/PGEditors/PropertyGridEditorASPDDMMSS.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource["CoordinatesDDMMSSASPEditorKey"];
        }
    }

    public class RoleDDMMSSASPEditor : PropertyEditor
    {
        public RoleDDMMSSASPEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/ASPControl;component/PGEditors/PropertyGridEditorASPDDMMSS.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            this.InlineTemplate = resource["RoleDDMMSSASPEditorKey"];
        }
    }
    #endregion
}
