﻿using ModelsTablesDBLib;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using TableEvents;
using ValuesCorrectLib;
using TableOperations;

namespace SpecFreqControl
{
    /// <summary>
    /// Логика взаимодействия для UserControl1.xaml
    /// </summary>
    public partial class UserControlSpecFreq : UserControl, ITableEvent
    {
        #region Events
        public event EventHandler<TableEvent> OnAddRecord = (object srnder, TableEvent data) => { };
        public event EventHandler<TableEvent> OnChangeRecord = (object srnder, TableEvent data) => { };
        public event EventHandler<TableEvent> OnDeleteRecord = (object srnder, TableEvent data) => { };
        public event EventHandler<NameTable> OnClearRecords = (object srnder, NameTable data) => { };

        // Открылось окно с PropertyGrid
        public event EventHandler<SpecFreqProperty> OnIsWindowPropertyOpen = (object sender, SpecFreqProperty data) => { };

        public event EventHandler<TableEventReport> OnAddTableToReport = (object sender, TableEventReport data) => { };
        #endregion

        #region Properties
        public SpecFreq NameTable { get; set; } = SpecFreq.Forbidden;
        #endregion

        public UserControlSpecFreq()
        {
            InitializeComponent();

            dgvSpecFreq.DataContext = new GlobalSpecFreq();
        }

        private void ButtonAdd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (PropNumberASP.SelectedNumASP != 0 && PropNumberASP.IsSelectedRowASP)
                {
                    var specFreqWindow = new SpecFreqProperty(((GlobalSpecFreq)dgvSpecFreq.DataContext).CollectionSpecFreq);
                    specFreqWindow.specFreq.NumberASP = PropNumberASP.SelectedNumASP;

                    PropNameTable.SpecFreqsName = FindNameTableSpecFreq();

                    OnIsWindowPropertyOpen(this, specFreqWindow);

                    if (specFreqWindow.ShowDialog() == true)
                    {
                        //CorrectMinMax.IsCorrectMinMax(specFreqWindow.specFreq);
                        //if (CorrectMinMax.IsCorrectRangeMinMax(specFreqWindow.specFreq))
                        //{
                        //var collection = new ObservableCollection<TableFreqSpec> { specFreqWindow.specFreq };

                            //if (CorrectSpecFreq.IsAddRecordsToCollection(((GlobalSpecFreq)dgvSpecFreq.DataContext).CollectionSpecFreq, collection))
                            //{
                                // Событие добавления одной записи
                                OnAddRecord(this, new TableEvent(FindTypeSpecFreq(specFreqWindow.specFreq)));
                            //}
                        //}
                    }
                }
            }
            catch(Exception ex) { ex.Message.ToString(); }
        }

        private void ButtonChange_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if ((dgvSpecFreq.DataContext as GlobalSpecFreq).SelectedSpecFreq != null)
                {
                    if ((dgvSpecFreq.DataContext as GlobalSpecFreq).SelectedSpecFreq.Id > 0)
                    {
                        var selected = (dgvSpecFreq.DataContext as GlobalSpecFreq).SelectedSpecFreq;
                        var specFreqWindow = new SpecFreqProperty(((GlobalSpecFreq)dgvSpecFreq.DataContext).CollectionSpecFreq, selected.Clone());

                        PropNameTable.SpecFreqsName = FindNameTableSpecFreq();

                        OnIsWindowPropertyOpen(this, specFreqWindow);

                        if (specFreqWindow.ShowDialog() == true)
                        {
                            TableFreqSpec srReplace = new TableFreqSpec();
                            srReplace = specFreqWindow.specFreq;

                            //CorrectMinMax.IsCorrectMinMax(srReplace);
                            //if (CorrectMinMax.IsCorrectRangeMinMax(specFreqWindow.specFreq))
                            //{
                                //if (CorrectSpecFreq.IsChangeRecordToCollection(((GlobalSpecFreq)dgvSpecFreq.DataContext).CollectionSpecFreq, (TableFreqSpec)dgvSpecFreq.SelectedItem, srReplace))
                                //{
                                    // Событие изменения одной записи
                                    OnChangeRecord(this, new TableEvent(FindTypeSpecFreq(srReplace)));
                                    //OnChangeSpecFreq?.Invoke(srReplace);

                                    //int ind = dataGridSectorsRanges.SelectedIndex;
                                    //ChangeSectorRangeToControl(ind, srReplace);
                                //}
                            //}
                        }
                    }
                }
            }
            catch { }
        }

        /// <summary>
        /// Удалить одну запись
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!IsSelectedRowEmpty())
                    return;

                TableFreqSpec specFrec = new TableFreqSpec
                {
                    Id = ((GlobalSpecFreq)dgvSpecFreq.DataContext).SelectedSpecFreq.Id,// 2;
                    FreqMinKHz = ((GlobalSpecFreq)dgvSpecFreq.DataContext).SelectedSpecFreq.FreqMinKHz,// 54000;
                    FreqMaxKHz = ((GlobalSpecFreq)dgvSpecFreq.DataContext).SelectedSpecFreq.FreqMaxKHz,// 65000;
                    NumberASP = ((GlobalSpecFreq)dgvSpecFreq.DataContext).SelectedSpecFreq.NumberASP,// 34;
                    Note = ((GlobalSpecFreq)dgvSpecFreq.DataContext).SelectedSpecFreq.Note
                };

                // Событие удаления одной записи
                OnDeleteRecord(this, new TableEvent(FindTypeSpecFreq(specFrec)));
            }
            catch { }
        }

        /// <summary>
        /// Удалить все записи
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonClear_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // Событие удаления одной записи
                OnClearRecords(this, FindNameTableSpecFreq());
                //OnClearSpecFreqs?.Invoke();

                //ClearSectorsRangesFromControl();
            }
            catch { }
        }

        private void dgvSpecFreq_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            AddEmptyRows();
        }

        private void ButtonWord_Click(object sender, RoutedEventArgs e)
        {
            string[,] Table = AddSpecFreqToTable();

            OnAddTableToReport(this, new TableEventReport(Table, FindNameTableSpecFreq()));
        }

        private void ButtonPrint_Click(object sender, RoutedEventArgs e)
        {
            //
        }
    }
}
