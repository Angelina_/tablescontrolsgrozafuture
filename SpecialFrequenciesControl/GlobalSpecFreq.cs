﻿using ModelsTablesDBLib;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace SpecFreqControl
{
    public class GlobalSpecFreq : INotifyPropertyChanged
    {
        public ObservableCollection<TableFreqSpec> CollectionSpecFreq { get; set; }

        public GlobalSpecFreq()
        {
            CollectionSpecFreq = new ObservableCollection<TableFreqSpec> { };

            //CollectionSpecFreq = new ObservableCollection<TableFreqSpec>
            //{
            //    new TableFreqSpec {
            //        Id = 1,
            //        FreqMinKHz = 340007,
            //        FreqMaxKHz = 450007,
            //        NumberASP = 12
            //    },
            //    new TableFreqSpec {
            //        Id = 2,
            //        FreqMinKHz = 540007,
            //        FreqMaxKHz = 650007,
            //        NumberASP = 34
            //    },
            //    new TableFreqSpec {
            //        Id = 3,
            //        FreqMinKHz = 1340007,
            //        FreqMaxKHz = 1450004,
            //        NumberASP = 33
            //    },

            //};
        }

        public TableFreqSpec SelectedSpecFreq
        {
            get { return selectedSpecFreq; }
            set
            {
                selectedSpecFreq = value;
                OnPropertyChanged("SelectedSpecFreq");
            }
        }
        private TableFreqSpec selectedSpecFreq;

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
    }
}
