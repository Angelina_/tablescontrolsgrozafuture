﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.WpfPropertyGrid;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ModelsTablesDBLib;
using TableEvents;
using TableOperations;
using ValuesCorrectLib;

namespace SpecFreqControl
{
    /// <summary>
    /// Логика взаимодействия для SpecialFrequenciesProperty.xaml
    /// </summary>
    public partial class SpecFreqProperty : Window
    {
        private ObservableCollection<TableFreqSpec> collectionTemp;
        public TableFreqSpec specFreq { get; private set; }

        public SpecFreqProperty(ObservableCollection<TableFreqSpec> collectionSpecFreq)
        {
            try
            {
                InitializeComponent();

                InitEditors();

                collectionTemp = collectionSpecFreq;
                specFreq = new TableFreqSpec();
                propertyGrid.SelectedObject = specFreq;

                //Title = "Add record";
                Title = SMeaning.meaningAddRecord;
                Icon = new BitmapImage(new Uri(@"pack://application:,,,/"
                                    + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                                    + ";component/"
                                    + "Resources/AddRec.ico", UriKind.Absolute));

                InitProperty();
                //ChangeCategories();
            }
            catch { }
        }

        public SpecFreqProperty(ObservableCollection<TableFreqSpec> collectionSpecFreq, TableFreqSpec range)
        {
            try
            {
                InitializeComponent();

                InitEditors();

                collectionTemp = collectionSpecFreq;
                specFreq = range;
                switch (PropLocalFreqMHz_kHz.FreqMHz_kHz)
                {
                    case 0: // kHz
                        specFreq.FreqMinKHz = Math.Round(specFreq.FreqMinKHz, 1);
                        specFreq.FreqMaxKHz = Math.Round(specFreq.FreqMaxKHz, 1);
                        break;

                    case 1: // MHz
                        specFreq.FreqMinKHz = Math.Round(specFreq.FreqMinKHz / 1000, 1);
                        specFreq.FreqMaxKHz = Math.Round(specFreq.FreqMaxKHz / 1000, 1);
                        break;

                    default:
                        specFreq.FreqMinKHz = Math.Round(specFreq.FreqMinKHz, 1);
                        specFreq.FreqMaxKHz = Math.Round(specFreq.FreqMaxKHz, 1);
                        break;
                }
                propertyGrid.SelectedObject = specFreq;

                //Title = "Change record";
                Title = SMeaning.meaningChangeRecord;
                Icon = new BitmapImage(new Uri(@"pack://application:,,,/"
                                    + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                                    + ";component/"
                                    + "Resources/ChangeRec.ico", UriKind.Absolute));

                //ChangeCategories();
                InitProperty();
            }
            catch { }
        }

        public SpecFreqProperty()
        {
            try
            {
                InitializeComponent();

                InitEditors();
                //ChangeCategories();
                InitProperty();
            }
            catch { }
        }

        private void ButtonApply_Click(object sender, RoutedEventArgs e)
        {
            if (IsAddClick((TableFreqSpec)propertyGrid.SelectedObject) != null)
            {
                DialogResult = true;
            }
        }

        public TableFreqSpec IsAddClick(TableFreqSpec specFreqWindow)
        {
            if (PropLocalFreqMHz_kHz.FreqMHz_kHz == 1)
            {
                specFreqWindow.FreqMinKHz = specFreqWindow.FreqMinKHz * 1000;
                specFreqWindow.FreqMaxKHz = specFreqWindow.FreqMaxKHz * 1000;
            }

            CorrectMinMax.IsCorrectMinMax(specFreqWindow, PropNameTable.SpecFreqsName);
            if (CorrectMinMax.IsCorrectRangeMinMax(specFreqWindow))
            {
                return specFreqWindow;
            }

            return null;
        }

        private void ButtonNoApply_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void InitProperty()
        {
            foreach (var property in propertyGrid.Properties)
            {
                try
                {
                    if (property.IsBrowsable == false)
                        continue;
                    if (property.PropertyValue.SubProperties.Count != 0)
                    {
                        foreach (var subProperty in property.PropertyValue.SubProperties)
                            subProperty.PropertyValue.PropertyValueException += PropertyGridSubException;
                        continue;
                    }
                    property.PropertyValue.PropertyValueException += PropertyGridException;
                }
                catch (Exception ex)
                { MessageBox.Show(ex.Message); }
            }
        }

        private void PropertyGridSubException(object sender, ValueExceptionEventArgs e)
        {
            propertyGrid.Properties[(sender as PropertyItemValue).ParentProperty.ParentValue.ParentProperty.Name].SetValue((sender as PropertyItemValue).Value);
        }

        private void PropertyGridException(object sender, ValueExceptionEventArgs e)
        {
            propertyGrid.Properties[(sender as PropertyItemValue).ParentProperty.Name].SetValue((sender as PropertyItemValue).Value);
        }

        private void ChangeCategories()
        {
            propertyGrid.Categories.FirstOrDefault(t => t.Name == "Диапазон").HeaderCategoryName = "Band";
            propertyGrid.Categories.FirstOrDefault(t => t.Name == "Прочее").HeaderCategoryName = "Other";

            propertyGrid.Properties[nameof(TableFreqSpec.NumberASP)].DisplayName = "№ AJS";
            propertyGrid.Properties[nameof(TableFreqSpec.FreqMinKHz)].DisplayName = "F min, kHz";
            propertyGrid.Properties[nameof(TableFreqSpec.FreqMaxKHz)].DisplayName = "F max, kHz";
            propertyGrid.Properties[nameof(TableFreqSpec.Note)].DisplayName = "Note";
        }

        public void SetLanguagePropertyGrid(Languages language)
        {
            TranslatorTables.ChangeLanguagePropertyGrid(language, propertyGrid);

            SetDisplayNameFreqs();
        }

        private void SetDisplayNameFreqs()
        {
            switch (PropLocalFreqMHz_kHz.FreqMHz_kHz)
            {
                case 0: // kHz
                    propertyGrid.Properties[nameof(TableFreqSpec.FreqMinKHz)].DisplayName = SMeaning.meaningFminkHz;
                    propertyGrid.Properties[nameof(TableFreqSpec.FreqMaxKHz)].DisplayName = SMeaning.meaningFmaxkHz;
                    break;

                case 1: // MHz
                    propertyGrid.Properties[nameof(TableFreqSpec.FreqMinKHz)].DisplayName = SMeaning.meaningFminMHz;
                    propertyGrid.Properties[nameof(TableFreqSpec.FreqMaxKHz)].DisplayName = SMeaning.meaningFmaxMHz;
                    break;

                default:
                    break;
            }
        }

        private void InitEditors()
        {
            propertyGrid.Editors.Add(new GeneralSpecFreqNoteEditor(nameof(specFreq.Note), typeof(TableFreqSpec)));
        }

        private void gridProperty_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Enter)
            {
                if (IsAddClick((TableFreqSpec)propertyGrid.SelectedObject) != null)
                {
                    DialogResult = true;
                }
            }
        }
    }
}
