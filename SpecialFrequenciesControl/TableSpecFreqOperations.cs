﻿using ModelsTablesDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using TableEvents;
using ValuesCorrectLib;

namespace SpecFreqControl
{
    public partial class UserControlSpecFreq : UserControl
    {
        /// <summary>
        /// Обновить записи в контроле
        /// </summary>
        /// <param name="listSectorsRanges"></param>
        public void UpdateSpecFreqs(List<TableFreqSpec> listSpecFreq)
        {
            try
            {
                ((GlobalSpecFreq)dgvSpecFreq.DataContext).CollectionSpecFreq.Clear();

                for (int i = 0; i < listSpecFreq.Count; i++)
                {
                    ((GlobalSpecFreq)dgvSpecFreq.DataContext).CollectionSpecFreq.Add(listSpecFreq[i]);
                }

                AddEmptyRows();
            }
            catch { }
        }

        /// <summary>
        /// Добавить несколько записей в контрол
        /// </summary>
        /// <param name="listSectorsRanges"></param>
        public void AddSpecFreqs(List<TableFreqSpec> listSpecFreq)
        {
            try
            {
                DeleteEmptyRows();

                for (int i = 0; i < listSpecFreq.Count; i++)
                {
                    ((GlobalSpecFreq)dgvSpecFreq.DataContext).CollectionSpecFreq.Add(listSpecFreq[i]);
                }

                AddEmptyRows();
            }
            catch { }
        }

        /// <summary>
        /// Добавить одну запись в контрол
        /// </summary>
        /// <param name="specFreq"></param>
        public void AddSpecFreq(TableFreqSpec specFreq)
        {
            try
            {
                DeleteEmptyRows();

                ((GlobalSpecFreq)dgvSpecFreq.DataContext).CollectionSpecFreq.Add(specFreq);

                AddEmptyRows();
            }
            catch { }

        }

        /// <summary>
        /// Изменить одну запись в контроле
        /// </summary>
        /// <param name="ind"> индекс записи, которую надо заменить </param>
        /// <param name="sfReplace"> запись, на которую надо заменить </param>
        public void ChangeSpecFreq(int id, TableFreqSpec sfReplace)
        {
            try
            {
                int index = ((GlobalSpecFreq)dgvSpecFreq.DataContext).CollectionSpecFreq.ToList().FindIndex(x => x.Id == id);

                if (index != -1)
                {
                    ((GlobalSpecFreq)dgvSpecFreq.DataContext).CollectionSpecFreq.RemoveAt(index);
                    ((GlobalSpecFreq)dgvSpecFreq.DataContext).CollectionSpecFreq.Insert(index, sfReplace);

                    AddEmptyRows();
                }
            }
            catch { }
        }

        /// <summary>
        /// Удалить одну запись из контрола
        /// </summary>
        /// <param name="specFreq"></param>
        public void DeleteSpecFreq(TableFreqSpec specFreq)
        {
            try
            {
                int index = ((GlobalSpecFreq)dgvSpecFreq.DataContext).CollectionSpecFreq.ToList().FindIndex(x => x.Id == specFreq.Id);

                if (index != -1)
                {
                    ((GlobalSpecFreq)dgvSpecFreq.DataContext).CollectionSpecFreq.RemoveAt(index);

                    AddEmptyRows();
                }
            }
            catch { }
        }

        /// <summary>
        /// Удалить все записи из контрола
        /// </summary>
        public void ClearSpecFreq()
        {
            try
            {
                ((GlobalSpecFreq)dgvSpecFreq.DataContext).CollectionSpecFreq.Clear();

                AddEmptyRows();
            }
            catch { }
        }

        /// <summary>
        /// Добавить пустые строки в таблицу
        /// </summary>
        private void AddEmptyRows()
        {
            int сountRowsAll = dgvSpecFreq.Items.Count; // количество всех строк в таблице
            double hs = 23; // высота строки
            double ah = dgvSpecFreq.ActualHeight; // визуализированная высота dataGrid
            double chh = dgvSpecFreq.ColumnHeaderHeight; // высота заголовка

            int countRows = Convert.ToInt32(Math.Floor(Convert.ToDouble(ah) - Convert.ToDouble(chh)) / hs); // количество строк, которое помещается в область видимости = (высота таблицы - заголовок таблицы) / высота строки

            int count = сountRowsAll - countRows; // сколько пустых строк нужно удалить
            int index = -1;
            for (int i = 0; i < count; i++)
            {
                // Удалить пустые строки в dgv
                index = ((GlobalSpecFreq)dgvSpecFreq.DataContext).CollectionSpecFreq.ToList().FindIndex(x => x.FreqMinKHz == -1);
                if (index != -1)
                {
                    ((GlobalSpecFreq)dgvSpecFreq.DataContext).CollectionSpecFreq.RemoveAt(index);
                }
            }

            List<TableFreqSpec> list = new List<TableFreqSpec>();
            for (int i = 0; i < countRows - сountRowsAll; i++)
            {
                TableFreqSpec strSpecFreq = new TableFreqSpec
                {
                    Id = -2,
                    FreqMinKHz = -2F,
                    FreqMaxKHz = -2F
                };
                
                list.Add(strSpecFreq);
            }

            for (int i = 0; i < list.Count; i++)
            {
                ((GlobalSpecFreq)dgvSpecFreq.DataContext).CollectionSpecFreq.Add(list[i]);
            }
        }

        /// <summary>
        /// Удалить пустые строки из таблицы
        /// </summary>
        private void DeleteEmptyRows()
        {
            int countEmptyRows = ((GlobalSpecFreq)dgvSpecFreq.DataContext).CollectionSpecFreq.Count(s => s.Id < 0);
            int countAllRows = ((GlobalSpecFreq)dgvSpecFreq.DataContext).CollectionSpecFreq.Count;
            int iCount = countAllRows - countEmptyRows;
            for (int i = iCount; i < countAllRows; i++)
            {
                ((GlobalSpecFreq)dgvSpecFreq.DataContext).CollectionSpecFreq.RemoveAt(iCount);
            }
        }

        /// <summary>
        /// Определить тип таблицы
        /// </summary>
        /// <param name="specFreq"></param>
        /// <returns></returns>
        private AbstractCommonTable FindTypeSpecFreq(TableFreqSpec specFreq)
        {
            switch (NameTable)
            {
                case SpecFreq.Forbidden:
                    return specFreq.ToFreqForbidden();
                case SpecFreq.Important:
                    return specFreq.ToFreqImportant();
                case SpecFreq.Known:
                    return specFreq.ToFreqKnown();
                default:
                    break;
            }
            return null;
        }

        /// <summary>
        /// Определить имя таблицы
        /// </summary>
        /// <returns></returns>
        private NameTable FindNameTableSpecFreq()
        {
            switch (NameTable)
            {
                case SpecFreq.Forbidden:
                    return ModelsTablesDBLib.NameTable.TableFreqForbidden;
                case SpecFreq.Important:
                    return ModelsTablesDBLib.NameTable.TableFreqImportant;
                case SpecFreq.Known:
                    return ModelsTablesDBLib.NameTable.TableFreqKnown;
                default:
                    break;
            }
            return ModelsTablesDBLib.NameTable.TableFreqForbidden;
        }

        private bool IsSelectedRowEmpty()
        {
            try
            {
                if (((GlobalSpecFreq)dgvSpecFreq.DataContext).SelectedSpecFreq != null)
                {
                    if (((GlobalSpecFreq)dgvSpecFreq.DataContext).SelectedSpecFreq.Id == -2)
                        return false;
                }
            }
            catch { }

            return true;
        }

        /// <summary>
        /// Добавить таблицу для отчета
        /// </summary>
        /// <returns></returns>
        private string[,] AddSpecFreqToTable()
        {
            int Columns = 4;// dgvSRanges.Columns.Count;
            int Rows = ((GlobalSpecFreq)dgvSpecFreq.DataContext).CollectionSpecFreq.Count(x => x.Id > 0) + 1;
            string[,] Table = new string[Rows, Columns];

            try
            {
                Table[0, 0] = SHeaders.headerNumAJS;
                Table[0, 1] = SHeaders.headerFreqMin;
                Table[0, 2] = SHeaders.headerFreqMax;
                Table[0, 3] = SHeaders.headerNote;
                for (int i = 1; i < Rows; i++)
                {
                    Table[i, 0] = ((GlobalSpecFreq)dgvSpecFreq.DataContext).CollectionSpecFreq[i - 1].NumberASP.ToString();
                    Table[i, 1] = ((GlobalSpecFreq)dgvSpecFreq.DataContext).CollectionSpecFreq[i - 1].FreqMinKHz.ToString();
                    Table[i, 2] = ((GlobalSpecFreq)dgvSpecFreq.DataContext).CollectionSpecFreq[i - 1].FreqMaxKHz.ToString();
                    Table[i, 3] = ((GlobalSpecFreq)dgvSpecFreq.DataContext).CollectionSpecFreq[i - 1].Note.ToString();
                }
            }
            catch { }

            return Table;
        }

        public void ChangeFreqHeaderMHz_kHz()
        {
            switch (PropLocalFreqMHz_kHz.FreqMHz_kHz)
            {
                case 0: // kHz
                    lHeaderFreqMin.Content = SMeaning.meaningFminkHz;
                    lHeaderFreqMax.Content = SMeaning.meaningFmaxkHz;
                    break;

                case 1: // MHz
                    lHeaderFreqMin.Content = SMeaning.meaningFminMHz;
                    lHeaderFreqMax.Content = SMeaning.meaningFmaxMHz;
                    break;
            }
                    
        }
    }
}
