﻿using ModelsTablesDBLib;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using TableEvents;
using ValuesCorrectLib;

namespace TempFWSControl
{
    public partial class UserControlTempFWS : UserControl
    {
        public List<TempFWS> TempListTempFWS { get; set; }
        public List<TableFreqSpec> TempListFreqImportant { get; set; }

        /// <summary>
        /// Обновить записи в контроле
        /// </summary>
        public void UpdateTempFWS(List<TempFWS> listTempFWS, List<TableFreqSpec> listFreqImportant)
        {
            try
            {
                TempListTempFWS = listTempFWS;
                TempListFreqImportant = listFreqImportant;

                if (listTempFWS.Count == 0)
                {
                    ((GlobalTempFWSCurRow)dgvTempFWSCurRow.DataContext).CollectionTempFWSCurRow.Clear();
                    ((GlobalTempFWS)dgvTempFWS.DataContext).CollectionTempFWS.Clear();

                    //------------------------------------------------------------------------
                    // Отображение в StatusBar
                    ((PropertiesModel)DataContext).CountFreqAll = ((GlobalTempFWS)dgvTempFWS.DataContext).CollectionTempFWS.Count;
                    ((PropertiesModel)DataContext).CountFreqActive = ((GlobalTempFWS)dgvTempFWS.DataContext).CollectionTempFWS.Count(ctrlG => ctrlG.Control == Led.Green);
                    //------------------------------------------------------------------------
                }
                else
                {
                    // -----------------------------------------
                    DeleteEmptyRows();

                    for (int i = 0; i < listTempFWS.Count; i++)
                    {
                        listTempFWS[i].IsSelected = false;
                    }
                    int ind = ((GlobalTempFWS)dgvTempFWS.DataContext).CollectionTempFWS.ToList().FindIndex(x => x.IsSelected == true);
                    if (ind != -1)
                    {
                        int indId = listTempFWS.FindIndex(x => x.Id == ((GlobalTempFWS)dgvTempFWS.DataContext).CollectionTempFWS[ind].Id);
                        if (indId != -1)
                        {
                            listTempFWS[indId].IsSelected = true;
                        }
                    }
                    //------------------------------
                    ((GlobalTempFWSCurRow)dgvTempFWSCurRow.DataContext).CollectionTempFWSCurRow.Clear();
                    ((GlobalTempFWS)dgvTempFWS.DataContext).CollectionTempFWS.Clear();

                    //------------------------------------------------------------------------
                    // Отображение в StatusBar
                    ((PropertiesModel)DataContext).CountFreqAll = ((GlobalTempFWS)dgvTempFWS.DataContext).CollectionTempFWS.Count;
                    ((PropertiesModel)DataContext).CountFreqActive = ((GlobalTempFWS)dgvTempFWS.DataContext).CollectionTempFWS.Count(ctrlG => ctrlG.Control == Led.Green);
                    //------------------------------------------------------------------------

                    if (bSortActive.IsChecked.Value)
                    {
                        //Сортировка по частоте(сначала с Control == Led.Green, потом с Control == Led.Empty)
                        var listAll = ((GlobalTempFWS)dgvTempFWS.DataContext).CollectionTempFWS.ToList();

                        var listControlGreen = listAll.Where(ctrlG => ctrlG.Control == Led.Green); // записали в лист значения Control == Led.Green
                        IEnumerable<TempFWSRow> SortFreqGreen = listControlGreen.OrderBy(x => x.FreqKHz); // сортировка по частоте

                        var listControlEmpty = listAll.Where(ctrlE => ctrlE.Control == Led.Empty); // записали в лист значения Control == Led.Empty
                        IEnumerable<TempFWSRow> SortFreqEmpty = listControlEmpty.OrderBy(x => x.FreqKHz); // сортировка по частоте

                        List<TempFWSRow> list = new List<TempFWSRow>();
                        list.AddRange(SortFreqGreen.ToList());
                        list.AddRange(SortFreqEmpty.ToList());

                        for (int i = 0; i < list.ToList().Count; i++)
                        {
                            ((GlobalTempFWS)dgvTempFWS.DataContext).CollectionTempFWS[i] = list[i];
                        }
                    }
                    else
                    {
                        var listSort = ((GlobalTempFWS)dgvTempFWS.DataContext).CollectionTempFWS.ToList();
                        IEnumerable<TempFWSRow> SortFreq = listSort.OrderBy(x => x.FreqKHz); // сортировка по частоте

                        // Важные сверху ---------------------------------------------------------------------------
                        if (listFreqImportant.Count > 0)
                        {
                            List<TempFWSRow> tempFreqImportant = new List<TempFWSRow>();
                            List<TempFWSRow> tempFreq = new List<TempFWSRow>();
                            List<TempFWSRow> listFirstFreqImp = new List<TempFWSRow>();
                            bool f = false;

                            for (int i = 0; i < SortFreq.ToList().Count; i++)
                            {
                                for (int j = 0; j < listFreqImportant.Count; j++)
                                {
                                    if (SortFreq.ToList()[i].FreqKHz >= listFreqImportant[j].FreqMinKHz && SortFreq.ToList()[i].FreqKHz <= listFreqImportant[j].FreqMaxKHz)
                                    {
                                        f = true;
                                    }
                                }
                                if (f)
                                {
                                    tempFreqImportant.Add(SortFreq.ToList()[i]);
                                    f = false;
                                }
                                else
                                {
                                    tempFreq.Add(SortFreq.ToList()[i]);
                                }
                            }

                            listFirstFreqImp.AddRange(tempFreqImportant);
                            listFirstFreqImp.AddRange(tempFreq);

                            for (int i = 0; i < ((GlobalTempFWS)dgvTempFWS.DataContext).CollectionTempFWS.Count; i++)
                            {
                                ((GlobalTempFWS)dgvTempFWS.DataContext).CollectionTempFWS[i] = listFirstFreqImp[i];
                            }
                        }
                        else
                        {
                            for (int i = 0; i < ((GlobalTempFWS)dgvTempFWS.DataContext).CollectionTempFWS.Count; i++)
                            {
                                ((GlobalTempFWS)dgvTempFWS.DataContext).CollectionTempFWS[i] = SortFreq.ToList()[i];
                            }
                        }
                    }

                    int indIsSelectedRow = ((GlobalTempFWS)dgvTempFWS.DataContext).CollectionTempFWS.ToList().FindIndex(x => (x.IsSelected.HasValue && x.IsSelected.Value));
                    if (indIsSelectedRow != -1)
                    {
                        if (((GlobalTempFWSCurRow)dgvTempFWSCurRow.DataContext).CollectionTempFWSCurRow.Count == 0)
                        {
                            AddEmptyRows(dgvTempFWSCurRow);
                            ((GlobalTempFWSCurRow)dgvTempFWSCurRow.DataContext).CollectionTempFWSCurRow[0] = ((GlobalTempFWS)dgvTempFWS.DataContext).CollectionTempFWS[indIsSelectedRow];
                        }
                        else
                        {
                            ((GlobalTempFWSCurRow)dgvTempFWSCurRow.DataContext).CollectionTempFWSCurRow[0] = ((GlobalTempFWS)dgvTempFWS.DataContext).CollectionTempFWS[indIsSelectedRow];
                        }
                    }
                }

                GC.Collect(1, GCCollectionMode.Optimized);

                AddEmptyRows(dgvTempFWS);
                AddEmptyRows(dgvTempFWSCurRow);
            }
            catch { }
        }

        /// <summary>
        /// Добавить записи в контрол
        /// </summary>
        public void AddTempFWSs(List<TempFWS> listTempFWS, List<TableFreqSpec> listFreqImportant)
        {
            try
            {
                DeleteEmptyRows();

                for (int i = 0; i < listTempFWS.Count; i++)
                {
                    int ind = ((GlobalTempFWS)dgvTempFWS.DataContext).CollectionTempFWS.ToList().FindIndex(x => x.Id == listTempFWS[i].Id);
                    if (ind != -1)
                    {
                        /////
                        if (!((GlobalTempFWS)dgvTempFWS.DataContext).CollectionTempFWS[ind].IsSelected.HasValue)
                            ((GlobalTempFWS)dgvTempFWS.DataContext).CollectionTempFWS[ind].IsSelected = false;
                        /////

                        var isSelected = ((GlobalTempFWS)dgvTempFWS.DataContext).CollectionTempFWS[ind].IsSelected;
                        var newRec = new TempFWSRow(listTempFWS[i]);

                        newRec.SelectedASP = ((GlobalTempFWS)dgvTempFWS.DataContext).CollectionTempFWS[ind].SelectedASP;
                        newRec.IsSelected = isSelected;
                        ((GlobalTempFWS)dgvTempFWS.DataContext).CollectionTempFWS[ind] = newRec;
                    }
                    else
                    {
                        ((GlobalTempFWS)dgvTempFWS.DataContext).CollectionTempFWS.Add(new TempFWSRow(listTempFWS[i]));
                    }
                }

                //------------------------------------------------------------------------
                // Отображение в StatusBar
                ((PropertiesModel)DataContext).CountFreqAll = ((GlobalTempFWS)dgvTempFWS.DataContext).CollectionTempFWS.Count;
                ((PropertiesModel)DataContext).CountFreqActive = ((GlobalTempFWS)dgvTempFWS.DataContext).CollectionTempFWS.Count(ctrlG => ctrlG.Control == Led.Green);

                //------------------------------------------------------------------------

                if (bSortActive.IsChecked.Value)
                {
                    //Сортировка по частоте(сначала с Control == Led.Green, потом с Control == Led.Empty)
                    var listAll = ((GlobalTempFWS)dgvTempFWS.DataContext).CollectionTempFWS.ToList();

                    var listControlGreen = listAll.Where(ctrlG => ctrlG.Control == Led.Green); // записали в лист значения Control == Led.Green
                    IEnumerable<TempFWSRow> SortFreqGreen = listControlGreen.OrderBy(x => x.FreqKHz); // сортировка по частоте

                    var listControlEmpty = listAll.Where(ctrlE => ctrlE.Control == Led.Empty); // записали в лист значения Control == Led.Empty
                    IEnumerable<TempFWSRow> SortFreqEmpty = listControlEmpty.OrderBy(x => x.FreqKHz); // сортировка по частоте

                    List<TempFWSRow> list = new List<TempFWSRow>();
                    list.AddRange(SortFreqGreen.ToList());
                    list.AddRange(SortFreqEmpty.ToList());

                    for (int i = 0; i < list.ToList().Count; i++)
                    {
                        ((GlobalTempFWS)dgvTempFWS.DataContext).CollectionTempFWS[i] = list[i];
                    }
                }
                else
                {
                    var listSort = ((GlobalTempFWS)dgvTempFWS.DataContext).CollectionTempFWS.ToList();
                    IEnumerable<TempFWSRow> SortFreq = listSort.OrderBy(x => x.FreqKHz); // сортировка по частоте

                    // Важные сверху ---------------------------------------------------------------------------
                    if (listFreqImportant.Count > 0)
                    {
                        List<TempFWSRow> tempFreqImportant = new List<TempFWSRow>();
                        List<TempFWSRow> tempFreq = new List<TempFWSRow>();
                        List<TempFWSRow> listFirstFreqImp = new List<TempFWSRow>();
                        bool f = false;

                        for (int i = 0; i < SortFreq.ToList().Count; i++)
                        {
                            for (int j = 0; j < listFreqImportant.Count; j++)
                            {
                                if (SortFreq.ToList()[i].FreqKHz >= listFreqImportant[j].FreqMinKHz && SortFreq.ToList()[i].FreqKHz <= listFreqImportant[j].FreqMaxKHz)
                                {
                                    f = true;
                                }
                            }
                            if (f)
                            {
                                tempFreqImportant.Add(SortFreq.ToList()[i]);
                                f = false;
                            }
                            else
                            {
                                tempFreq.Add(SortFreq.ToList()[i]);
                            }
                        }

                        listFirstFreqImp.AddRange(tempFreqImportant);
                        listFirstFreqImp.AddRange(tempFreq);

                        for (int i = 0; i < ((GlobalTempFWS)dgvTempFWS.DataContext).CollectionTempFWS.Count; i++)
                        {
                            ((GlobalTempFWS)dgvTempFWS.DataContext).CollectionTempFWS[i] = listFirstFreqImp[i];
                        }
                    }
                    else
                    {
                        for (int i = 0; i < ((GlobalTempFWS)dgvTempFWS.DataContext).CollectionTempFWS.Count; i++)
                        {
                            ((GlobalTempFWS)dgvTempFWS.DataContext).CollectionTempFWS[i] = SortFreq.ToList()[i];
                        }
                    }
                }

                int indIsSelectedRow = ((GlobalTempFWS)dgvTempFWS.DataContext).CollectionTempFWS.ToList().FindIndex(x => (x.IsSelected.HasValue && x.IsSelected.Value));
                if (indIsSelectedRow != -1)
                {
                    if (((GlobalTempFWSCurRow)dgvTempFWSCurRow.DataContext).CollectionTempFWSCurRow.Count == 0)
                    {
                        AddEmptyRows(dgvTempFWSCurRow);
                        ((GlobalTempFWSCurRow)dgvTempFWSCurRow.DataContext).CollectionTempFWSCurRow[0] = ((GlobalTempFWS)dgvTempFWS.DataContext).CollectionTempFWS[indIsSelectedRow];
                    }
                    else
                    {
                        ((GlobalTempFWSCurRow)dgvTempFWSCurRow.DataContext).CollectionTempFWSCurRow[0] = ((GlobalTempFWS)dgvTempFWS.DataContext).CollectionTempFWS[indIsSelectedRow];
                    }
                }

                GC.Collect(1, GCCollectionMode.Optimized);

                AddEmptyRows(dgvTempFWS);
            }
            catch { }
        }

        /// <summary>
        /// Изменить одну запись в контроле
        /// </summary>
        /// <param name="ind"> индекс записи, которую надо заменить </param>
        /// <param name="replaceTempFWS"> запись, на которую надо заменить </param>
        public void ChangeTempFWS(int id, TempFWS replaceTempFWS)
        {
            try
            {
                int ind = ((GlobalTempFWS)dgvTempFWS.DataContext).CollectionTempFWS.ToList().FindIndex(x => x.Id == replaceTempFWS.Id);

                if (ind != -1)
                {
                    int IdSelectedRecord = ((TempFWSRow)dgvTempFWS.SelectedItem)?.Id ?? -2;
                    ((GlobalTempFWSCurRow)dgvTempFWSCurRow.DataContext).CollectionTempFWSCurRow.Clear();

                    ((GlobalTempFWS)dgvTempFWS.DataContext).CollectionTempFWS.RemoveAt(ind);
                    ((GlobalTempFWS)dgvTempFWS.DataContext).CollectionTempFWS.Insert(ind, new TempFWSRow(replaceTempFWS));

                    //------------------------------------------------------------------------
                    // Отображение в StatusBar
                    ((PropertiesModel)DataContext).CountFreqAll = ((GlobalTempFWS)dgvTempFWS.DataContext).CollectionTempFWS.Count;
                    ((PropertiesModel)DataContext).CountFreqActive = ((GlobalTempFWS)dgvTempFWS.DataContext).CollectionTempFWS.Count(ctrlG => ctrlG.Control == Led.Green);

                    //------------------------------------------------------------------------

                    if (IdSelectedRecord > 0)
                    {
                        var selectedObj = (((GlobalTempFWS)dgvTempFWS.DataContext).CollectionTempFWS.FirstOrDefault(rec => rec.Id == IdSelectedRecord));
                        dgvTempFWS.SelectedItem = selectedObj;
                    }
                }
            }
            catch { }
        }

        /// <summary>
        /// Удалить одну запись из контрола
        /// </summary>
        public void DeleteTempFWS(TempFWS tempFWS)
        {
            try
            {
                int index = ((GlobalTempFWS)dgvTempFWS.DataContext).CollectionTempFWS.ToList().FindIndex(x => x.Id == tempFWS.Id);
                ((GlobalTempFWS)dgvTempFWS.DataContext).CollectionTempFWS.RemoveAt(index);

                ((GlobalTempFWSCurRow)dgvTempFWSCurRow.DataContext).CollectionTempFWSCurRow.RemoveAt(0);
            }
            catch { }
        }

        /// <summary>
        /// Удалить все записи из контрола
        /// </summary>
        public void ClearTempFWS()
        {
            try
            {
                ((GlobalTempFWS)dgvTempFWS.DataContext).CollectionTempFWS.Clear();
                AddEmptyRows(dgvTempFWS);

                ((GlobalTempFWSCurRow)dgvTempFWSCurRow.DataContext).CollectionTempFWSCurRow.Clear();
                AddEmptyRows(dgvTempFWSCurRow);
            }
            catch { }
        }

        /// <summary>
        /// Добавить / удалить пустую строку в dgvTempFWSCurRow или dgvTempFWS
        /// </summary>
        private void AddEmptyRows(DataGrid dataGrid)
        {
            try
            {
                int сountRowsAll = dataGrid.Items.Count; // количество всех строк в таблице
                double hs = 23; // высота строки
                double ah = dataGrid.ActualHeight; // визуализированная высота dataGrid
                double chh = dataGrid.ColumnHeaderHeight; // высота заголовка
                int countRows = -1;
                int index = -1;
                int count = -1;

                List<TempFWSRow> list;

                switch (dataGrid.Name)
                {
                    case "dgvTempFWS":

                        countRows = Convert.ToInt32(Math.Floor(Convert.ToDouble(ah) - Convert.ToDouble(chh)) / hs); // количество строк, которое помещается в область видимости = (высота таблицы - заголовок таблицы) / высота строки
                        count = сountRowsAll - countRows; // сколько пустых строк нужно удалить

                        for (int i = 0; i < count; i++)
                        {
                            // Удалить пустые строки в dgv
                            index = ((GlobalTempFWS)dgvTempFWS.DataContext).CollectionTempFWS.ToList().FindIndex(x => x.Id < 0);
                            if (index != -1)
                            {
                                ((GlobalTempFWS)dgvTempFWS.DataContext).CollectionTempFWS.RemoveAt(index);
                            }
                        }

                        list = new List<TempFWSRow>();

                        for (int i = 0; i < (countRows - сountRowsAll) + 1; i++)
                        {
                            TempFWSRow strTempFWS = new TempFWSRow
                            {
                                Id = -2,
                                FreqKHz = -2F,
                                Control = Led.Empty,
                                Deviation = -2F,
                                Coordinates = new Coord
                                {
                                    Latitude = -2,
                                    Longitude = -2,
                                    Altitude = -2
                                },
                                ListQ = new System.Collections.ObjectModel.ObservableCollection<JamDirect>
                                {
                                    new JamDirect
                                    {
                                        NumberASP = -2,
                                        Bearing = -2F,
                                        Level = -2,
                                        Std = -2,
                                        DistanceKM = -2
                                    }
                                },
                                Type = 33 // 33 - пустое значение 
                            };

                            list.Add(strTempFWS);
                        }

                        for (int i = 0; i < list.Count; i++)
                        {
                            ((GlobalTempFWS)dgvTempFWS.DataContext).CollectionTempFWS.Add(list[i]);
                        }
                        break;

                    case "dgvTempFWSCurRow":

                        countRows = Convert.ToInt32(Math.Floor(Convert.ToDouble(ah) - Convert.ToDouble(chh)) / hs); // количество строк, которое помещается в область видимости = (высота таблицы - заголовок таблицы) / высота строки

                        list = new List<TempFWSRow>();
                        for (int i = 0; i < countRows; i++)
                        {
                            TempFWSRow strTempFWS = new TempFWSRow
                            {
                                Id = -2,
                                FreqKHz = -2F,
                                Control = Led.Empty,
                                Deviation = -2F,
                                Coordinates = new Coord
                                {
                                    Latitude = -2,
                                    Longitude = -2,
                                    Altitude = -2
                                },
                                ListQ = new System.Collections.ObjectModel.ObservableCollection<JamDirect>
                                {
                                    new JamDirect
                                    {
                                        NumberASP = -2,
                                        Bearing = -2F,
                                        Level = -2,
                                        Std = -2,
                                        DistanceKM = -2
                                    }
                                },
                                Type = 33 // 33 - пустое значение
                            };

                            list.Add(strTempFWS);
                        }

                        for (int i = 0; i < list.Count; i++)
                        {
                            ((GlobalTempFWSCurRow)dgvTempFWSCurRow.DataContext).CollectionTempFWSCurRow.Add(list[i]);
                        }
                        break;

                    default:
                        break;
                }
            }
            catch { }
        }

        /// <summary>
        /// Удалить пустые строки из таблицы
        /// </summary>
        private void DeleteEmptyRows()
        {
            try
            {
                int countEmptyRows = ((GlobalTempFWS)dgvTempFWS.DataContext).CollectionTempFWS.Count(s => s.Id < 0);
                int countAllRows = ((GlobalTempFWS)dgvTempFWS.DataContext).CollectionTempFWS.Count;
                int iCount = countAllRows - countEmptyRows;
                for (int i = iCount; i < countAllRows; i++)
                {
                    ((GlobalTempFWS)dgvTempFWS.DataContext).CollectionTempFWS.RemoveAt(iCount);
                }

                int countEmptyRowsCur = ((GlobalTempFWSCurRow)dgvTempFWSCurRow.DataContext).CollectionTempFWSCurRow.Count(s => s.Id < 0);
                int countAllRowsCur = ((GlobalTempFWSCurRow)dgvTempFWSCurRow.DataContext).CollectionTempFWSCurRow.Count;
                int iCountCur = countAllRowsCur - countEmptyRowsCur;
                for (int i = iCountCur; i < countAllRowsCur; i++)
                {
                    ((GlobalTempFWSCurRow)dgvTempFWSCurRow.DataContext).CollectionTempFWSCurRow.RemoveAt(iCountCur);
                }
            }
            catch { }
        }

        private bool IsSelectedRowEmpty()
        {
            try
            {
                //if (((TempFWSRow)dgvTempFWS.SelectedItem).Id == -2)
                if (((GlobalTempFWSCurRow)dgvTempFWSCurRow.DataContext).CollectionTempFWSCurRow != null)
                {
                    if (((GlobalTempFWSCurRow)dgvTempFWSCurRow.DataContext).CollectionTempFWSCurRow[0].Id == -2)
                        return false;
                }
            }
            catch { }

            return true;
        }

        /// <summary>
        /// Записать в TempFWS выделенную строку 
        /// </summary>
        /// <returns></returns>
        private TempFWS SelectedRowForSend()
        {
            int index = ((GlobalTempFWS)dgvTempFWS.DataContext).CollectionTempFWS.ToList().FindIndex(x => x.FreqKHz == ((TempFWSRow)dgvTempFWS.SelectedItem).FreqKHz);

            ObservableCollection<JamDirect> listJamDirect = new ObservableCollection<JamDirect>();
            for (int i = 0; i < ((GlobalTempFWS)dgvTempFWS.DataContext).CollectionTempFWS[index].ListQ.Count; i++)
            {
                JamDirect jamDirect = new JamDirect
                {
                    NumberASP = ((GlobalTempFWS)dgvTempFWS.DataContext).CollectionTempFWS[index].ListQ[i].NumberASP,
                    Bearing = ((GlobalTempFWS)dgvTempFWS.DataContext).CollectionTempFWS[index].ListQ[i].Bearing,
                    Level = ((GlobalTempFWS)dgvTempFWS.DataContext).CollectionTempFWS[index].ListQ[i].Level,
                    Std = ((GlobalTempFWS)dgvTempFWS.DataContext).CollectionTempFWS[index].ListQ[i].Std,
                    DistanceKM = ((GlobalTempFWS)dgvTempFWS.DataContext).CollectionTempFWS[index].ListQ[i].DistanceKM,
                    IsOwn = ((GlobalTempFWS)dgvTempFWS.DataContext).CollectionTempFWS[index].ListQ[i].IsOwn
                };

                listJamDirect.Add(jamDirect);
            }

            TempFWS tempFWS = new TempFWS
            {
                Id = ((TempFWSRow)dgvTempFWS.SelectedItem).Id,
                FreqKHz = ((TempFWSRow)dgvTempFWS.SelectedItem).FreqKHz,
                Deviation = ((TempFWSRow)dgvTempFWS.SelectedItem).Deviation,
                Coordinates = new Coord
                {
                    Latitude = ((TempFWSRow)dgvTempFWS.SelectedItem).Coordinates.Latitude,
                    Longitude = ((TempFWSRow)dgvTempFWS.SelectedItem).Coordinates.Longitude
                },
                Time = ((TempFWSRow)dgvTempFWS.SelectedItem).Time,
                Type = ((TempFWSRow)dgvTempFWS.SelectedItem).Type,
                IsSelected = true,
                ListQ = listJamDirect
            };

            return tempFWS;
        }

        /// <summary>
        /// Записать в TempFWS выделенную строку 
        /// </summary>
        /// <returns></returns>
        private TempFWS SelectedCurRowForSend()
        {
            ObservableCollection<JamDirect> listJamDirect = new ObservableCollection<JamDirect>();
            for (int i = 0; i < ((GlobalTempFWSCurRow)dgvTempFWSCurRow.DataContext).CollectionTempFWSCurRow[0].ListQ.Count; i++)
            {
                JamDirect jamDirect = new JamDirect
                {
                    NumberASP = ((GlobalTempFWSCurRow)dgvTempFWSCurRow.DataContext).CollectionTempFWSCurRow[0].ListQ[i].NumberASP,
                    Bearing = ((GlobalTempFWSCurRow)dgvTempFWSCurRow.DataContext).CollectionTempFWSCurRow[0].ListQ[i].Bearing,
                    Level = ((GlobalTempFWSCurRow)dgvTempFWSCurRow.DataContext).CollectionTempFWSCurRow[0].ListQ[i].Level,
                    Std = ((GlobalTempFWSCurRow)dgvTempFWSCurRow.DataContext).CollectionTempFWSCurRow[0].ListQ[i].Std,
                    DistanceKM = ((GlobalTempFWSCurRow)dgvTempFWSCurRow.DataContext).CollectionTempFWSCurRow[0].ListQ[i].DistanceKM,
                    IsOwn = ((GlobalTempFWSCurRow)dgvTempFWSCurRow.DataContext).CollectionTempFWSCurRow[0].ListQ[i].IsOwn
                };

                listJamDirect.Add(jamDirect);
            }

            TempFWS tempFWS = new TempFWS
            {
                Id = ((GlobalTempFWSCurRow)dgvTempFWSCurRow.DataContext).CollectionTempFWSCurRow[0].Id,
                FreqKHz = ((GlobalTempFWSCurRow)dgvTempFWSCurRow.DataContext).CollectionTempFWSCurRow[0].FreqKHz,
                Deviation = ((GlobalTempFWSCurRow)dgvTempFWSCurRow.DataContext).CollectionTempFWSCurRow[0].Deviation,
                Coordinates = new Coord
                {
                    Latitude = ((GlobalTempFWSCurRow)dgvTempFWSCurRow.DataContext).CollectionTempFWSCurRow[0].Coordinates.Latitude,
                    Longitude = ((GlobalTempFWSCurRow)dgvTempFWSCurRow.DataContext).CollectionTempFWSCurRow[0].Coordinates.Longitude,
                    Altitude = ((GlobalTempFWSCurRow)dgvTempFWSCurRow.DataContext).CollectionTempFWSCurRow[0].Coordinates.Altitude
                },
                Time = ((GlobalTempFWSCurRow)dgvTempFWSCurRow.DataContext).CollectionTempFWSCurRow[0].Time,
                Type = ((GlobalTempFWSCurRow)dgvTempFWSCurRow.DataContext).CollectionTempFWSCurRow[0].Type,
                IsSelected = true,
                ListQ = listJamDirect
            };

            return tempFWS;
          
        }

        SolidColorBrush solidBrushRed = new SolidColorBrush(Colors.Red);
        SolidColorBrush solidBrushKhaki = new SolidColorBrush(Colors.Khaki);
        SolidColorBrush solidBrushWhite = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FFD9F9E3"));

        /// <summary>
        /// Обозначить цветом Важные (хаки) и Запрещенные (красным) частоты
        /// </summary>
        /// <param name="listFreqImportant"></param>
        /// <param name="listFreqForbidden"></param>
        public void ColorFreqImportantForbidden(List<TableFreqSpec> listFreqImportant, List<TableFreqSpec> listFreqForbidden)
        {
            try
            {
                //Color colorRed = new Color();
                //colorRed = Color.FromArgb(255, 255, 0, 0); // Red
                //SolidColorBrush solidBrushRed = new SolidColorBrush(colorRed);  

                //Color colorBlue = new Color();
                //colorBlue = Color.FromArgb(255, 0, 0, 255); // Blue
                //SolidColorBrush solidBrushRed = new SolidColorBrush(colorBlue);  

                for (int row = 0; row < dgvTempFWS.Items.Count; row++)
                {
                    var dataGridRow = dgvTempFWS.ItemContainerGenerator.ContainerFromItem(dgvTempFWS.Items[row]) as DataGridRow;

                    if (dataGridRow != null)
                    {
                        dataGridRow.Foreground = solidBrushWhite;
                    }
                }

                for (int row = 0; row < dgvTempFWS.Items.Count; row++)
                {
                    var dataGridRow = dgvTempFWS.ItemContainerGenerator.ContainerFromItem(dgvTempFWS.Items[row]) as DataGridRow;

                    if (dataGridRow != null)
                    {
                        var freqRec = (dataGridRow.Item as TempFWSRow).FreqKHz;
                        for (int i = 0; i < listFreqImportant.Count; i++)
                        {
                            if (freqRec >= listFreqImportant[i].FreqMinKHz && freqRec <= listFreqImportant[i].FreqMaxKHz)
                            {
                                dataGridRow.Foreground = solidBrushKhaki;
                            }
                        }

                        //for (int i = 0; i < listFreqForbidden.Count; i++)
                        //{
                        //    if (freqRec >= listFreqForbidden[i].FreqMinKHz && freqRec <= listFreqForbidden[i].FreqMaxKHz)
                        //    {
                        //        dataGridRow.Foreground = solidBrushRed;
                        //    }
                        //}
                    }
                }
            }
            catch { }
        }

        /// <summary>
        /// Добавить таблицу для отчета
        /// </summary>
        /// <returns></returns>
        private string[,] AddTempFWSToTable()
        {
            int Columns = 10;
            int Rows = (((GlobalTempFWS)dgvTempFWS.DataContext).CollectionTempFWS.Count(x => x.Id > 0)) + 1;
            string[,] Table = new string[Rows, Columns];

            try
            {
                Table[0, 0] = SHeaders.headerFreq;
                Table[0, 1] = SHeaders.headerBearing;
                Table[0, 2] = SHeaders.headerDeltaF;
                Table[0, 3] = SHeaders.headerRMSE;
                Table[0, 4] = SHeaders.headerLevel;
                Table[0, 5] = SHeaders.headerTime;
                Table[0, 6] = SHeaders.headerLatLon;
                Table[0, 7] = SHeaders.headerAlt;
                Table[0, 8] = SHeaders.headerDistance;
                Table[0, 9] = SHeaders.headerAJS;

                for (int i = 1; i < Rows; i++)
                {
                    Table[i, 0] = ((GlobalTempFWS)dgvTempFWS.DataContext).CollectionTempFWS[i - 1].FreqKHz.ToString();
                    Table[i, 1] = ((GlobalTempFWS)dgvTempFWS.DataContext).CollectionTempFWS[i - 1].SelectedJamDirect.Bearing.ToString();
                    Table[i, 2] = ((GlobalTempFWS)dgvTempFWS.DataContext).CollectionTempFWS[i - 1].Deviation.ToString();
                    Table[i, 3] = ((GlobalTempFWS)dgvTempFWS.DataContext).CollectionTempFWS[i - 1].SelectedJamDirect.Std.ToString();
                    Table[i, 4] = ((GlobalTempFWS)dgvTempFWS.DataContext).CollectionTempFWS[i - 1].SelectedJamDirect.Level.ToString();
                    Table[i, 5] = ((GlobalTempFWS)dgvTempFWS.DataContext).CollectionTempFWS[i - 1].Time.ToString();
                    Table[i, 6] = ((GlobalTempFWS)dgvTempFWS.DataContext).CollectionTempFWS[i - 1].Coordinates.Latitude.ToString() + "   " + ((GlobalTempFWS)dgvTempFWS.DataContext).CollectionTempFWS[i - 1].Coordinates.Longitude.ToString();
                    Table[i, 7] = ((GlobalTempFWS)dgvTempFWS.DataContext).CollectionTempFWS[i - 1].Coordinates.Altitude.ToString();
                    Table[i, 8] = ((GlobalTempFWS)dgvTempFWS.DataContext).CollectionTempFWS[i - 1].SelectedJamDirect.DistanceKM.ToString();
                    Table[i, 9] = ((GlobalTempFWS)dgvTempFWS.DataContext).CollectionTempFWS[i - 1].SelectedASP.ToString();
                }

            }
            catch { }

            return Table;
        }

        /// <summary>
        /// Отобразить в StatusBar количество важных частот
        /// </summary>
        /// <param name="countFreqImportant"></param>
        public void AddToStatusBarCountFI(int countFreqImportant)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                ((PropertiesModel)DataContext).CountFreqImportant = countFreqImportant;
            });
        }

        /// <summary>
        /// Видимость кнопок в зависимости от заданного состояния КРПУ
        /// </summary>
        public void ButtonsCRRDVisible()
        {
            // КРПУ1
            if (PropLocalCRRD.CRRD1)
            {
                gridButtons.ColumnDefinitions[10].Width = new GridLength(30);
            }
            else
            {
                gridButtons.ColumnDefinitions[10].Width = new GridLength(0);
            }

            // КРПУ2
            if (PropLocalCRRD.CRRD2)
            {
                gridButtons.ColumnDefinitions[11].Width = new GridLength(30);
            }
            else
            {
                gridButtons.ColumnDefinitions[11].Width = new GridLength(0);
            }
        }
        public void ChangeFreqHeaderMHz_kHz()
        {
            switch (PropLocalFreqMHz_kHz.FreqMHz_kHz)
            {
                case 0: // kHz
                    lHeaderFreq.Content = SMeaning.meaningFkHz;
                    break;

                case 1: // MHz
                    lHeaderFreq.Content = SMeaning.meaningFMHz;
                    break;
            }

        }
    }
}
