﻿using ModelsTablesDBLib;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using TableEvents;
using TableOperations;
using ValuesCorrectLib;

namespace TempFWSControl
{
    /// <summary>
    /// Логика взаимодействия для UserControl1.xaml
    /// </summary>
    public partial class UserControlTempFWS : UserControl, ITableReconEvent
    {
        //const Int16 constLevel = -130;

        #region Events
        // Удалить запись из таблицы
        public event EventHandler<TableEvent> OnDeleteRecord = (object sender, TableEvent data) => { };
        // Удалить все записи в таблице
        public event EventHandler<NameTable> OnClearRecords = (object sender, NameTable data) => { };
        // Добавить запись в таблицу ИИ ФРЧ РП
        public event EventHandler<TableSuppressFWS> OnAddFWS_RS = (object sender, TableSuppressFWS data) => { };
        // Добавить запись в таблицу ИИ ФРЧ ЦР
        public event EventHandler<TableReconFWS> OnAddFWS_TD = (object sender, TableReconFWS data) => { };
        // Отправить частоту на КРПУ
        public event EventHandler<TempFWS> OnSendFreqCRRD = (object sender, TempFWS data) => { };
        // Отправить частоту на КРПУ2
        public event EventHandler<TempFWS> OnSendFreqCRRD2 = (object sender, TempFWS data) => { };
        // Отправить запрос на исполнительное пеленгование
        public event EventHandler<TempFWS> OnGetExecBear = (object sender, TempFWS data) => { };
        // Отправить запрос на квазиодновременное пеленгование
        public event EventHandler<TempFWS> OnGetKvBear = (object sender, TempFWS data) => { };

        public event EventHandler<TableEvent> OnAddRecord = (object srnder, TableEvent data) => { };

        // Изменить статус выделенной строки (поле IsSelected = true)
        public event EventHandler<TableEvent> OnSelectedRow = (object sender, TableEvent data) => { };

        public event EventHandler<TableEventReport> OnAddTableToReport = (object sender, TableEventReport data) => { };
        #endregion

        #region Properties
        public NameTable NameTable { get; } = NameTable.TempFWS;
        #endregion

        public UserControlTempFWS()
        {
            InitializeComponent();

            dgvTempFWS.DataContext = new GlobalTempFWS();
            dgvTempFWSCurRow.DataContext = new GlobalTempFWSCurRow();

            DataContext = new PropertiesModel();
    }

        private void ButtonDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (((GlobalTempFWSCurRow)dgvTempFWSCurRow.DataContext).CollectionTempFWSCurRow != null)
                {
                    if (!IsSelectedRowEmpty())
                        return;

                    TempFWS tempFWS = new TempFWS
                    {
                        Id = ((GlobalTempFWSCurRow)dgvTempFWSCurRow.DataContext).CollectionTempFWSCurRow[0].Id,
                        Coordinates = new Coord { },
                        ListQ = new ObservableCollection<JamDirect> { }
                    };

                    // Событие удаления одной записи
                    OnDeleteRecord(this, new TableEvent(tempFWS));
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        private void ButtonClear_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // Событие удаления всех записей
                OnClearRecords(this, NameTable);
            }
            catch { }
            //catch(Exception ex) { MessageBox.Show(ex.Message); }
        }

        private void DgvTempFWS_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            AddEmptyRows(dgvTempFWS);
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //int dgvRowIndex = dgvTempFWS.SelectedIndex;
            var dgvRowIndex = dgvTempFWS.SelectedItem;
            try
            {
                //if (dgvRowIndex != -1)
                if (dgvRowIndex != null)
                {
                    ComboBox cellComboBox = sender as ComboBox;
                    string strNumberASP = Convert.ToString(cellComboBox.SelectedValue);
                    if (strNumberASP.Substring(0, 1) == "*")
                    {
                        strNumberASP = strNumberASP.Remove(0, 1);
                    }

                    int iNumberASP = Convert.ToInt32(strNumberASP);

                    //((TempFWSRow)dgvTempFWS.SelectedItem).IsSelected = true;
                    ((TempFWSRow)dgvTempFWS.SelectedItem).SelectedASP = iNumberASP;
                    ((TempFWSRow)dgvTempFWS.SelectedItem).SelectedJamDirect = ((TempFWSRow)dgvTempFWS.SelectedItem).ListQ.FirstOrDefault(rec => rec.NumberASP == iNumberASP);
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        private void dgvTempFWSCurRow_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            AddEmptyRows(dgvTempFWSCurRow);
        }

        private void dgvTempFWS_SelectedCellsChanged(object sender, SelectedCellsChangedEventArgs e)
        {
            try
            {
                if (dgvTempFWS.SelectedItem != null)
                {
                    TempFWSRow temp = new TempFWSRow()
                    {
                        Id = ((TempFWSRow)dgvTempFWS.SelectedItem).Id,
                        FreqKHz = ((TempFWSRow)dgvTempFWS.SelectedItem).FreqKHz,
                        Deviation = ((TempFWSRow)dgvTempFWS.SelectedItem).Deviation,
                        Coordinates = new Coord
                        {
                            Latitude = ((TempFWSRow)dgvTempFWS.SelectedItem).Coordinates.Latitude,
                            Longitude = ((TempFWSRow)dgvTempFWS.SelectedItem).Coordinates.Longitude
                        },
                        Time = ((TempFWSRow)dgvTempFWS.SelectedItem).Time,
                        Type = ((TempFWSRow)dgvTempFWS.SelectedItem).Type,
                        SelectedJamDirect = ((TempFWSRow)dgvTempFWS.SelectedItem).ListQ.FirstOrDefault(rec => 
                        rec.NumberASP == ((TempFWSRow)dgvTempFWS.SelectedItem).SelectedASP),
                        //SelectedASP = ((TempFWSRow)dgvTempFWS.SelectedItem).SelectedASP
                        ListQ = ((TempFWSRow)dgvTempFWS.SelectedItem).ListQ
                    };

                    //((GlobalTempFWSCurRow)dgvTempFWSCurRow.DataContext).CollectionTempFWSCurRow.Clear();
                    //((GlobalTempFWSCurRow)dgvTempFWSCurRow.DataContext).CollectionTempFWSCurRow.Add(temp);
                    //dgvTempFWSCurRow.Items.Refresh();

                    if (PropSelectedIdTempFWS.SelectedIdTempFWS != ((TempFWSRow)dgvTempFWS.SelectedItem).Id)
                    {
                        PropSelectedIdTempFWS.SelectedIdTempFWS = ((TempFWSRow)dgvTempFWS.SelectedItem).Id;

                        if (PropSelectedIdTempFWS.SelectedIdTempFWS != -2)
                        {
                            //--------------------------------------------------------------------------------------
                            //((GlobalTempFWSCurRow)dgvTempFWSCurRow.DataContext).CollectionTempFWSCurRow.Clear();
                            //((GlobalTempFWSCurRow)dgvTempFWSCurRow.DataContext).CollectionTempFWSCurRow.Add((TempFWSRow)dgvTempFWS.SelectedItem);
                            //dgvTempFWSCurRow.Items.Refresh();

                            for (int i = 0; i < ((GlobalTempFWS)dgvTempFWS.DataContext).CollectionTempFWS.Count; i++)
                            {
                                ((GlobalTempFWS)dgvTempFWS.DataContext).CollectionTempFWS[i].IsSelected = false;
                            }
                            int indIsSelectedRow = ((GlobalTempFWS)dgvTempFWS.DataContext).CollectionTempFWS.ToList().FindIndex(x => x.Id == ((TempFWSRow)dgvTempFWS.SelectedItem).Id);
                            if (indIsSelectedRow != -1)
                            {
                                ((GlobalTempFWS)dgvTempFWS.DataContext).CollectionTempFWS[indIsSelectedRow].IsSelected = true;
                            }
                            //--------------------------------------------------------------------------------------
                           
                            TempFWS tempFWS = SelectedRowForSend();

                            OnSelectedRow(this, new TableEvent(tempFWS));

                            dgvTempFWS.UnselectAllCells();
                        }
                    }
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        private void dgvTempFWSCurRow_SelectedCellsChanged(object sender, SelectedCellsChangedEventArgs e)
        {
            try
            {
                // Отменяет выделение всех ячеек в элементе управления
                dgvTempFWSCurRow.UnselectAllCells();
            }
            catch (Exception ex)
            { MessageBox.Show(ex.Message); }
        }

        private void ButtonAddFWS_TD_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (((GlobalTempFWSCurRow)dgvTempFWSCurRow.DataContext).CollectionTempFWSCurRow != null)
                {
                    if (!IsSelectedRowEmpty())
                        return;

                    ObservableCollection<TableJamDirect> listJamDirect = new ObservableCollection<TableJamDirect>();
                    for (int i = 0; i < ((GlobalTempFWSCurRow)dgvTempFWSCurRow.DataContext).CollectionTempFWSCurRow[0].ListQ.Count; i++)
                    {
                        TableJamDirect tableJamDirect = new TableJamDirect
                        {
                            JamDirect = new JamDirect
                            {
                                NumberASP = ((GlobalTempFWSCurRow)dgvTempFWSCurRow.DataContext).CollectionTempFWSCurRow[0].ListQ[i].NumberASP,
                                Bearing = ((GlobalTempFWSCurRow)dgvTempFWSCurRow.DataContext).CollectionTempFWSCurRow[0].ListQ[i].Bearing,
                                Level = ((GlobalTempFWSCurRow)dgvTempFWSCurRow.DataContext).CollectionTempFWSCurRow[0].ListQ[i].Level,
                                Std = ((GlobalTempFWSCurRow)dgvTempFWSCurRow.DataContext).CollectionTempFWSCurRow[0].ListQ[i].Std,
                                DistanceKM = ((GlobalTempFWSCurRow)dgvTempFWSCurRow.DataContext).CollectionTempFWSCurRow[0].ListQ[i].DistanceKM,
                                IsOwn = ((GlobalTempFWSCurRow)dgvTempFWSCurRow.DataContext).CollectionTempFWSCurRow[0].ListQ[i].IsOwn
                            }
                        };

                        listJamDirect.Add(tableJamDirect);
                    }

                    TableReconFWS tableReconFWS = new TableReconFWS
                    {
                        Id = ((GlobalTempFWSCurRow)dgvTempFWSCurRow.DataContext).CollectionTempFWSCurRow[0].Id,
                        FreqKHz = ((GlobalTempFWSCurRow)dgvTempFWSCurRow.DataContext).CollectionTempFWSCurRow[0].FreqKHz,
                        Time = DateTime.Now.ToLocalTime(),
                        Deviation = ((GlobalTempFWSCurRow)dgvTempFWSCurRow.DataContext).CollectionTempFWSCurRow[0].Deviation,
                        Type = ((GlobalTempFWSCurRow)dgvTempFWSCurRow.DataContext).CollectionTempFWSCurRow[0].Type,
                        Coordinates = new Coord
                        {
                            Latitude = ((GlobalTempFWSCurRow)dgvTempFWSCurRow.DataContext).CollectionTempFWSCurRow[0].Coordinates.Latitude,
                            Longitude = ((GlobalTempFWSCurRow)dgvTempFWSCurRow.DataContext).CollectionTempFWSCurRow[0].Coordinates.Longitude,
                            Altitude = ((GlobalTempFWSCurRow)dgvTempFWSCurRow.DataContext).CollectionTempFWSCurRow[0].Coordinates.Altitude
                        },
                        ListJamDirect = listJamDirect
                    };

                    // Событие Отправить ИРИ на ЦР
                    OnAddFWS_TD(this, tableReconFWS);
                }
            }
            catch { }
        }

        private void ButtonAddFWS_RS_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (((GlobalTempFWSCurRow)dgvTempFWSCurRow.DataContext).CollectionTempFWSCurRow != null)
                {
                    if (!IsSelectedRowEmpty())
                        return;

                    TableSuppressFWS tableSuppressFWS = new TableSuppressFWS
                    {
                        //Id = 0,
                        Id = ((GlobalTempFWSCurRow)dgvTempFWSCurRow.DataContext).CollectionTempFWSCurRow[0].Id,
                        Sender = SignSender.OtherTable,
                        NumberASP = ((GlobalTempFWSCurRow)dgvTempFWSCurRow.DataContext).CollectionTempFWSCurRow[0].SelectedJamDirect.NumberASP,
                        FreqKHz = ((GlobalTempFWSCurRow)dgvTempFWSCurRow.DataContext).CollectionTempFWSCurRow[0].FreqKHz,
                        Bearing = ((GlobalTempFWSCurRow)dgvTempFWSCurRow.DataContext).CollectionTempFWSCurRow[0].SelectedJamDirect.Bearing,
                        Letter = DefinitionParams.DefineLetter(((GlobalTempFWSCurRow)dgvTempFWSCurRow.DataContext).CollectionTempFWSCurRow[0].FreqKHz),
                        Threshold = (short)(((GlobalTempFWSCurRow)dgvTempFWSCurRow.DataContext).CollectionTempFWSCurRow[0].SelectedJamDirect.Level - 10),
                        Priority = 2,
                        Coordinates = new Coord
                        {
                            Latitude = ((GlobalTempFWSCurRow)dgvTempFWSCurRow.DataContext).CollectionTempFWSCurRow[0].Coordinates.Latitude,
                            Longitude = ((GlobalTempFWSCurRow)dgvTempFWSCurRow.DataContext).CollectionTempFWSCurRow[0].Coordinates.Longitude,
                            Altitude = ((GlobalTempFWSCurRow)dgvTempFWSCurRow.DataContext).CollectionTempFWSCurRow[0].Coordinates.Altitude
                        },
                        InterferenceParam = new InterferenceParam
                        {
                            Manipulation = 0,
                            Modulation = 1,
                            Deviation = 8,
                            Duration = 4
                        },
                    };

                    if (tableSuppressFWS.Threshold < ConstValues.constLevel) { tableSuppressFWS.Threshold = ConstValues.constLevel; }

                    // Событие Отправить ИРИ на РП
                    OnAddFWS_RS(this, tableSuppressFWS);
                }
            }
            catch { }
        }

        private void ButtonAddFWS_CRRD_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //TempFWS tempFWS = SelectedRowForSend();
                if (((GlobalTempFWSCurRow)dgvTempFWSCurRow.DataContext).CollectionTempFWSCurRow != null)
                {
                    if (!IsSelectedRowEmpty())
                        return;

                    TempFWS tempFWS = new TempFWS
                    {
                        FreqKHz = ((GlobalTempFWSCurRow)dgvTempFWSCurRow.DataContext).CollectionTempFWSCurRow[0].FreqKHz
                    };

                    // Событие На КРПУ
                    OnSendFreqCRRD(this, tempFWS);
                }
            }
            catch { }
        }

        private void ButtonAddFWS_CRRD2_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //TempFWS tempFWS = SelectedRowForSend();
                if (((GlobalTempFWSCurRow)dgvTempFWSCurRow.DataContext).CollectionTempFWSCurRow != null)
                {
                    if (!IsSelectedRowEmpty())
                        return;

                    TempFWS tempFWS = new TempFWS
                    {
                        FreqKHz = ((GlobalTempFWSCurRow)dgvTempFWSCurRow.DataContext).CollectionTempFWSCurRow[0].FreqKHz
                    };

                    // Событие На КРПУ2
                    OnSendFreqCRRD2(this, tempFWS);
                }
            }
            catch { }
        }

        private void ButtonGetExecBear_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (((GlobalTempFWSCurRow)dgvTempFWSCurRow.DataContext).CollectionTempFWSCurRow != null)
                {
                    if (!IsSelectedRowEmpty())
                        return;

                    TempFWS tempFWS = SelectedCurRowForSend();

                    PropNumberASP.SelectedASPforListQ = ((GlobalTempFWSCurRow)dgvTempFWSCurRow.DataContext).CollectionTempFWSCurRow[0].SelectedASP;
                   
                    // Событие Запрос на исполнительное пеленгование
                    OnGetExecBear(this, tempFWS);
                }
            }
            catch { }
        }

        private void ButtonGetKvBear_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (((GlobalTempFWSCurRow)dgvTempFWSCurRow.DataContext).CollectionTempFWSCurRow != null)
                {
                    if (!IsSelectedRowEmpty())
                        return;

                    TempFWS tempFWS = SelectedCurRowForSend();
                   
                    PropNumberASP.SelectedASPforListQ = ((GlobalTempFWSCurRow)dgvTempFWSCurRow.DataContext).CollectionTempFWSCurRow[0].SelectedASP;

                    // Событие Запрос на квазиодновременное пеленгование
                    OnGetKvBear(this, tempFWS);
                }
            }
            catch { }
        }

        private void ButtonWord_Click(object sender, RoutedEventArgs e)
        {
            string[,] Table = AddTempFWSToTable();

            OnAddTableToReport(this, new TableEventReport(Table, NameTable.TempFWS));
        }

        private void ButtonPrint_Click(object sender, RoutedEventArgs e)
        {
            //
        }

        /// <summary>
        /// Для синхронного растягивания колонок в двух таблицах
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvTempFWSCurRow_LayoutUpdated(object sender, EventArgs e)
        {
            for (int i = 0; i < dgvTempFWS.Columns.Count && i < dgvTempFWSCurRow.Columns.Count; ++i)
                dgvTempFWS.Columns[i].Width = dgvTempFWSCurRow.Columns[i].ActualWidth;
           
        }

        private void grid_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            try
            {
                if (e.Key == System.Windows.Input.Key.Space)
                    ButtonAddFWS_CRRD_Click(this, null);

                if (e.Key == System.Windows.Input.Key.Q)
                    ButtonGetKvBear_Click(this, null);

                if (e.Key == System.Windows.Input.Key.E)
                    ButtonGetExecBear_Click(this, null);

                if (e.Key == System.Windows.Input.Key.R)
                    ButtonAddFWS_RS_Click(this, null);

                if (e.Key == System.Windows.Input.Key.T)
                    ButtonAddFWS_TD_Click(this, null);
            }
            catch { }
        }

        private void dgvTempFWS_MouseRightButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            foreach (MenuItem i in cmTempFWS.Items)
            {
                switch(i.Name)
                {
                    case "miCRRDTempFWS":

                        if(!PropLocalCRRD.CRRD1)
                            i.Visibility = Visibility.Collapsed;
                        else
                            i.Visibility = Visibility.Visible;

                        break;

                    case "miCRRD2TempFWS":

                        if (!PropLocalCRRD.CRRD2)
                            i.Visibility = Visibility.Collapsed;
                        else
                            i.Visibility = Visibility.Visible;

                        break;
                }
            }
        }

        private void dgvTempFWSCurRow_MouseRightButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            foreach (MenuItem i in cmTempFWSCurRow.Items)
            {
                switch (i.Name)
                {
                    case "miCRRDTempFWSCurRow":

                        if (!PropLocalCRRD.CRRD1)
                            i.Visibility = Visibility.Collapsed;
                        else
                            i.Visibility = Visibility.Visible;

                        break;

                    case "miCRRD2TempFWSCurRow":

                        if (!PropLocalCRRD.CRRD2)
                            i.Visibility = Visibility.Collapsed;
                        else
                            i.Visibility = Visibility.Visible;

                        break;
                }
            }
        }
    }
}
