﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace TempFWSControl
{
    public class PropertiesModel : INotifyPropertyChanged
    {
        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }
        #endregion

        private int countFreqAll;
        public int CountFreqAll
        {
            get => countFreqAll;
            set
            {
                countFreqAll = value;
                OnPropertyChanged();
            }
        }
        private int countFreqActive;
        public int CountFreqActive
        {
            get => countFreqActive;
            set
            {
                countFreqActive = value;
                OnPropertyChanged();
            }
        }
        private int countFreqImportant;
        public int CountFreqImportant
        {
            get => countFreqImportant;
            set
            {
                countFreqImportant = value;
                OnPropertyChanged();
            }
        }
    }
}
