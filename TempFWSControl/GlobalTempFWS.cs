﻿using ModelsTablesDBLib;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Linq;

namespace TempFWSControl
{
    public class GlobalTempFWS 
    {
        public ObservableCollection<TempFWSRow> CollectionTempFWS { get; set; }

        public GlobalTempFWS()
        {
            CollectionTempFWS = new ObservableCollection<TempFWSRow> { };

            //CollectionTempFWS = new ObservableCollection<TempFWSRow>
            //{
            //    new TempFWSRow
            //    {
            //        Id = 3,
            //        Control = Led.Green,
            //        FreqKHz = 1566667.67777,
            //        Deviation = 100.4F,
            //        Coordinates = new Coord
            //        {
            //            Latitude = 53.1234,
            //            Longitude = 27.4569,
            //            Altitude = 100
            //        },
            //        Time = DateTime.Now.ToLocalTime(),
            //        Type = 1,
            //        ListQ = new ObservableCollection<JamDirect>
            //        {
            //            new JamDirect
            //            {
            //                NumberASP = 210,
            //                Bearing = 166,
            //                Level = -90,
            //                Std = 123.88F,
            //                DistanceKM = 500,
            //                IsOwn = true

            //            },
            //            new JamDirect
            //            {
            //                NumberASP = 211,
            //                Bearing = 170,
            //                Level = -80,
            //                Std = 34.88F,
            //                DistanceKM = 1500,
            //                IsOwn = false
            //            },
            //            new JamDirect
            //            {
            //                NumberASP = 212,
            //                Bearing = 10,
            //                Level = -94,
            //                Std = 53.88F,
            //                DistanceKM = 5600,
            //                IsOwn = false
            //            },
            //            new JamDirect
            //            {
            //                NumberASP = 213,
            //                Bearing = 30,
            //                Level = -90,
            //                Std = 13.88F,
            //                DistanceKM = 7500,
            //                IsOwn = false
            //            }
            //        }
            //    },
            //    new TempFWSRow
            //    {
            //        Id = 4,
            //        Control = Led.Gray,
            //        FreqKHz = 1464467,
            //        Deviation = 50F,
            //        Coordinates = new Coord
            //        {
            //            Latitude = 54.4444,
            //            Longitude = 28.6767,
            //            Altitude = 300
            //        },
            //        Time = DateTime.Now.ToLocalTime(),
            //        Type = 4,
            //        ListQ = new ObservableCollection<JamDirect>
            //        {
            //            new JamDirect
            //            {
            //                NumberASP = 213,
            //                Bearing = 160,
            //                Level = -90,
            //                Std = 167.88F,
            //                DistanceKM = 780,
            //                IsOwn = true
            //            },
            //            new JamDirect
            //            {
            //                NumberASP = 234,
            //                Bearing = 170,
            //                Level = -80,
            //                Std = 34.88F,
            //                DistanceKM = 6666,
            //                IsOwn = false
            //            },
            //            new JamDirect
            //            {
            //                NumberASP = 222,
            //                Bearing = 100,
            //                Level = -94,
            //                Std = 53.88F,
            //                DistanceKM = 5888,
            //                IsOwn = false
            //            },
            //            new JamDirect
            //            {
            //                NumberASP = 212,
            //                Bearing = 200,
            //                Level = -40,
            //                Std = 234.88F,
            //                DistanceKM = 566,
            //                IsOwn = false
            //            }
            //        }
            //    },
            //    new TempFWSRow
            //    {
            //        Id = 7,
            //        Control = Led.Green,
            //        FreqKHz = 2226662,
            //        Deviation = 203.33F,
            //        Coordinates = new Coord
            //        {
            //            Latitude = 52.222,
            //            Longitude = 25.8888,
            //            Altitude = 45
            //        },
            //        Time = DateTime.Now.ToLocalTime(),
            //        Type = 3,
            //        ListQ = new ObservableCollection<JamDirect>
            //        {
            //            new JamDirect
            //            {
            //                NumberASP = 210,
            //                Bearing = 16,
            //                Level = -90,
            //                Std = 123.2209F,
            //                DistanceKM = 500,
            //                IsOwn = true
            //            },
            //            new JamDirect
            //            {
            //                NumberASP = 215,
            //                Bearing = 17,
            //                Level = -80,
            //                Std = 34.4532F,
            //                DistanceKM = 1500,
            //                IsOwn = false
            //            },
            //            new JamDirect
            //            {
            //                NumberASP = 214,
            //                Bearing = 109,
            //                Level = -94,
            //                Std = 53.88F,
            //                DistanceKM = 5600,
            //                IsOwn = false
            //            },
            //            new JamDirect
            //            {
            //                NumberASP = 222,
            //                Bearing = 39,
            //                Level = -90,
            //                Std = 13.8989F,
            //                DistanceKM = 7500,
            //                IsOwn = false
            //            }
            //        }
            //    }
            //};
        }
    }

    public class TempFWSRow : TempFWS, INotifyPropertyChanged
    {
        private JamDirect selectedJamDirect = new JamDirect();
        private int selectedASP = -2;

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }
        #endregion

        public int SelectedASP
        {
            get
            {
                if (selectedASP < 0 && ListQ != null && ListQ.Count != 0)
                    selectedASP = ListQ.First().NumberASP;
                return selectedASP;
            }
            set
            {
                if (selectedASP == value) return;
                selectedASP = value;
                //OnPropertyChanged(nameof(SelectedASP));
            }
        }

        public JamDirect SelectedJamDirect
        {
            get
            {
                if (!CompareJams(selectedJamDirect, ListQ.FirstOrDefault(rec => rec.NumberASP == SelectedASP)))
                {
                    SelectedJamDirect = ListQ.FirstOrDefault(rec => rec.NumberASP == SelectedASP);
                }
                
                return selectedJamDirect;
            }
            set
            {
                if (value == null) return;
                if (selectedJamDirect == value) return;

                selectedJamDirect.Bearing = value.Bearing;
                selectedJamDirect.DistanceKM = value.DistanceKM;
                selectedJamDirect.IsOwn = value.IsOwn;
                selectedJamDirect.Level = value.Level;
                selectedJamDirect.NumberASP = value.NumberASP;
                selectedJamDirect.Std = value.Std;
                OnPropertyChanged(nameof(SelectedJamDirect));
            }
        }

        private bool CompareJams(JamDirect jam1, JamDirect jam2)
        {
            if (jam1 == null || jam2 == null)
                return true;
            if (jam1.Bearing != jam2.Bearing ||
                jam1.DistanceKM != jam2.DistanceKM ||
                jam1.IsOwn != jam2.IsOwn ||
                jam1.Level != jam2.Level ||
                jam1.NumberASP != jam2.NumberASP ||
                jam1.Std != jam2.Std)
                return false;
            return true;
        }

        public TempFWSRow()
        {
            if (ListQ != null && ListQ.Count != 0)
                selectedJamDirect = ListQ.First();
        }

        public TempFWSRow(TempFWS tempFWS)
        {
            Checking = tempFWS.Checking;
            Id = tempFWS.Id;
            Control = tempFWS.Control;
            FreqKHz = tempFWS.FreqKHz;
            Deviation = tempFWS.Deviation;
            Coordinates = tempFWS.Coordinates;
            Time = tempFWS.Time;
            Type = tempFWS.Type;
            ListQ = tempFWS.ListQ;
            IsSelected = tempFWS.IsSelected;
        }
    }

    public class GlobalTempFWSCurRow
    {
        public ObservableCollection<TempFWSRow> CollectionTempFWSCurRow { get; set; }

        public GlobalTempFWSCurRow()
        {
            CollectionTempFWSCurRow = new ObservableCollection<TempFWSRow> { };
        }
    }
}
