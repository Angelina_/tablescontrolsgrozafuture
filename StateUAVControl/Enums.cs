﻿namespace StateUAVControl
{
    public enum Led : byte
    {
        Empty = 0,
        Green = 1,
        Red = 2,
        Blue = 3,
        Yellow = 4,
        Gray = 5,
        White = 6
    }

    public enum Language : byte
    {
        RU = 0,
        EN = 1,
        AZ = 2
    }
    public enum Frequency : byte
    {
        FreqMin = 0,
        FreqMax = 1
    }
}
