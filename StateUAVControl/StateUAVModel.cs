﻿namespace StateUAVControl
{
    using System.Collections.Generic;

    public class StateUAVModel
    {
        public string Name { get; set; }

        public bool IsActive { get; set; }

        public List<FreqsRange> FreqsRange { get; set; }

        //public double FreqMinKHz { get; set; }

        //public double FreqMaxKHz { get; set; }
       
        //public Led Control { get; set; }

        public Led Suppress { get; set; }

        public string InterferenceParam { get; set; }
    }

    public class FreqsRange
    {
        public double FreqMinKHz { get; set; }

        public double FreqMaxKHz { get; set; }
    }
}
