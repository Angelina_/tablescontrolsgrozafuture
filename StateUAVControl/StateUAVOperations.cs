﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;

namespace StateUAVControl
{
    using System.Windows;

    public partial class UserControlStateUAV : UserControl
    {
        #region StateUAV
        private List<StateUAVModel> listStateUAV = new List<StateUAVModel> { };
        public List<StateUAVModel> ListStateUAV
        {
            get { return listStateUAV; }
            set
            {
                listStateUAV = value;
                UpdateStateUAV();
            }
        }

        /// <summary>
        /// Обновить контрол
        /// </summary>
        private void UpdateStateUAV()
        {
            try
            {
                if (this.ListStateUAV == null)
                    return;

                ((GlobalStateUAV)this.DgvStateUAV.DataContext).CollectionStateUAV.Clear();

                for (int i = 0; i < this.ListStateUAV.Count; i++)
                {
                    ((GlobalStateUAV)this.DgvStateUAV.DataContext).CollectionStateUAV.Add(this.ListStateUAV[i]);
                }

                if (this.ListStateUAV.All(x => x.IsActive))
                {
                    this.TBCheckedAll.IsChecked = true;
                }
                else 
                {
                    this.TBCheckedAll.IsChecked = false;
                }

                AddEmptyRows();
                GC.Collect(1, GCCollectionMode.Optimized);
            }
            catch { }
        }
        #endregion

        /// <summary>
        /// Добавить пустые строки в таблицу
        /// </summary>
        private void AddEmptyRows()
        {
            int сountRowsAll = DgvStateUAV.Items.Count; // количество всех строк в таблице
            double hs = 23; // высота строки
            double ah = DgvStateUAV.ActualHeight; // визуализированная высота dataGrid
            double chh = DgvStateUAV.ColumnHeaderHeight; // высота заголовка

            int countRows = Convert.ToInt32(Math.Floor(Convert.ToDouble(ah) - Convert.ToDouble(chh)) / hs); // количество строк, которое помещается в область видимости = (высота таблицы - заголовок таблицы) / высота строки

            int count = сountRowsAll - countRows; // сколько пустых строк нужно удалить
            int index = -1;
            for (int i = 0; i < count; i++)
            {
                // Удалить пустые строки в dgv
                index = ((GlobalStateUAV)DgvStateUAV.DataContext).CollectionStateUAV.ToList().FindIndex(x => x.Name == String.Empty);
                if (index != -1)
                {
                    ((GlobalStateUAV)DgvStateUAV.DataContext).CollectionStateUAV.RemoveAt(index);
                }
            }

            List<StateUAVModel> list = new List<StateUAVModel>();
            for (int i = 0; i < countRows - сountRowsAll; i++)
            {
                StateUAVModel stateUAV = new StateUAVModel
                {
                    Name = string.Empty,
                    FreqsRange = new List<FreqsRange>() 
                                     { 
                                         new FreqsRange()
                                             {
                                                FreqMinKHz = -2F,
                                                FreqMaxKHz = -2F
                                             }
                                         },
                    //Control = Led.Empty,
                    Suppress = Led.Empty,
                    InterferenceParam = string.Empty
                };
                
                list.Add(stateUAV);
            }

            for (int i = 0; i < list.Count; i++)
            {
                ((GlobalStateUAV)DgvStateUAV.DataContext).CollectionStateUAV.Add(list[i]);
            }
        }

        public void ChangeFreqHeaderMHz_kHz()
        {
            switch (ChangeFreqMHz_kHz.FreqMHz_kHz)
            {
                case 0: // kHz
                    lHeaderFreq.Content = SMeaning.meaningFkHz;
                    lHeaderBand.Content = SMeaning.meaningBandkHz;
                    break;

                case 1: // MHz
                    lHeaderFreq.Content = SMeaning.meaningFMHz;
                    lHeaderBand.Content = SMeaning.meaningBandMHz;
                    break;
            }
        }
    }

    /// <summary>
    /// Отображение названий F, MHz / F, kHz
    /// </summary>
    public class ChangeFreqMHz_kHz
    {
        private static byte freqMHz_kHz = 0;
        public static byte FreqMHz_kHz
        {
            get { return freqMHz_kHz; }
            set
            {
                if (freqMHz_kHz == value)
                    return;

                freqMHz_kHz = value;
            }
        }
    }
}
