﻿using System.Windows.Controls;

namespace StateUAVControl
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows;
    using System.Windows.Controls.Primitives;

    /// <summary>
    /// Логика взаимодействия для UserControl1.xaml
    /// </summary>
    public partial class UserControlStateUAV : UserControl
    {
        public event EventHandler<List<StateUAVModel>> OnClickJamUAV = (object sender, List<StateUAVModel> data) => { };

        public UserControlStateUAV()
        {
            InitializeComponent();

            SMeaning.InitSMeaning();
            STypeModulation.InitSTypeModulation();

            DgvStateUAV.DataContext = new GlobalStateUAV();
            
            ListStateUAV.AddRange(((GlobalStateUAV)this.DgvStateUAV.DataContext).CollectionStateUAV); 
        }
       
        private void ToggleButtonAll_OnClick(object sender, RoutedEventArgs e)
        {
            foreach (var item in this.ListStateUAV)
            {
                item.IsActive = this.TBCheckedAll.IsChecked.Value;
            }

            this.OnClickJamUAV(this, this.ListStateUAV);

            this.UpdateStateUAV();
        }

        private void ToggleButton_OnClick(object sender, RoutedEventArgs e)
        {
            if ((StateUAVModel)this.DgvStateUAV.SelectedItem == null) return;

            bool tbIsClick = (sender as ToggleButton).IsChecked.Value;

            int ind = this.ListStateUAV.FindIndex(x => x.Name == ((StateUAVModel)this.DgvStateUAV.SelectedItem).Name &&
                                                           ((StateUAVModel)this.DgvStateUAV.SelectedItem).IsActive != tbIsClick);

            if (ind != -1)
            {
                this.ListStateUAV[ind].IsActive = tbIsClick;
                this.OnClickJamUAV(this, this.ListStateUAV);

                this.UpdateStateUAV();
            }
        }

        private void DgvStateUAV_OnSizeChanged(object sender, SizeChangedEventArgs e)
        {
            AddEmptyRows();
        }
    }
}
