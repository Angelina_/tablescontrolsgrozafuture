﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;

namespace StateUAVControl
{
    public partial class UserControlStateUAV : UserControl, INotifyPropertyChanged
    {
        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }
        #endregion

        #region Language

        public static readonly DependencyProperty LanguageProperty = DependencyProperty.Register("Language", typeof(byte), typeof(UserControlStateUAV),
                                                                       new PropertyMetadata((byte)0, new PropertyChangedCallback(LanguageChanged)));


        public byte Language
        {
            get { return (byte)GetValue(LanguageProperty); }
            set { SetValue(LanguageProperty, value); }
        }

        private static void LanguageChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                UserControlStateUAV userControlStateUAV  = (UserControlStateUAV)d;
                userControlStateUAV.UpdateLanguage();
            }
            catch
            { }

        }

        /// <summary>
        /// Обновить язык в таблице
        /// </summary>
        private void UpdateLanguage()
        {
            try
            {
                Language lng = StateUAVControl.Language.RU;

                switch (Language)
                {
                    case 0:
                        lng = StateUAVControl.Language.RU;
                        break;

                    case 1:
                        lng = StateUAVControl.Language.EN;
                        break;

                    case 2:
                        lng = StateUAVControl.Language.AZ;
                        break;
                }
             
                SetTranslation(lng);
                LoadDictionary(lng);
                ChangeFreqHeaderMHz_kHz();
                UpdateInterferenceParam();
            }
            catch { }
        }

        /// <summary>
        /// Обновить InterferenceParam при выборе языка
        /// </summary>
        private void UpdateInterferenceParam()
        {
            foreach (var item in this.ListStateUAV)
            {
                switch (item.Name)
                {
                    case "2.4":
                        item.InterferenceParam = STypeModulation.paramLChM + " +/- 43 " + SMeaning.meaningMHz + " +/- 50 " + SMeaning.meaningkHz + " 0.5 " + SMeaning.meaningms;
                        break;
                    case "5.2":
                        item.InterferenceParam = STypeModulation.paramLChM + " +/- 65 " + SMeaning.meaningMHz + " +/- 50 " + SMeaning.meaningkHz + " 0.5 " + SMeaning.meaningms;
                        break;
                    case "5.8":
                        item.InterferenceParam = STypeModulation.paramLChM + " +/- 75 " + SMeaning.meaningMHz + " +/- 50 " + SMeaning.meaningkHz + " 0.5 " + SMeaning.meaningms;
                        break;
                    case "NAV":
                        item.InterferenceParam = STypeModulation.paramLChM + " +/- 8 " + SMeaning.meaningMHz + " +/- 100 " + SMeaning.meaningkHz + " 0.5 " + SMeaning.meaningms + "\r\n"
                                                 + STypeModulation.paramLChM + " +/- 4 " + SMeaning.meaningMHz + " +/- 100 " + SMeaning.meaningkHz + " 0.5 " + SMeaning.meaningms;
                        break;
                }
            }

            this.UpdateStateUAV();
        }
        #endregion


    }
}
