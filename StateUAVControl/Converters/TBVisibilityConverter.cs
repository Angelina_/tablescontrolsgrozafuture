﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace StateUAVControl
{
    using System.Windows;

    public class TBVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //try
            //{
            switch (value)
            {
                case "":
                    return Visibility.Hidden;

                default:
                    return Visibility.Visible;
            }
            //}
            //catch { }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
