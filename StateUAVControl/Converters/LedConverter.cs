﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace StateUAVControl
{
    [ValueConversion(sourceType: typeof(Led), targetType: typeof(Uri))]
    public class LedConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //try
            //{
            Uri uri;
            switch (value)
            {
                case Led.Empty:
                        uri = new Uri(@"pack://application:,,,/"
                                  + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                                  + ";component/"
                                  + "Resources/empty.png", UriKind.Absolute); 
                        break;

                    case Led.Green:
                        uri = new Uri(@"pack://application:,,,/"
                                   + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                                   + ";component/"
                                   + "Resources/green.png", UriKind.Absolute);
                        break;

                    case Led.Red:
                        uri = new Uri(@"pack://application:,,,/"
                                  + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                                  + ";component/"
                                  + "Resources/red.png", UriKind.Absolute);
                        break;

                    case Led.Blue:
                        uri = new Uri(@"pack://application:,,,/"
                                  + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                                  + ";component/"
                                  + "Resources/blue.png", UriKind.Absolute);
                        break;

                    case Led.Yellow:
                        uri = new Uri(@"pack://application:,,,/"
                                  + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                                  + ";component/"
                                  + "Resources/yellow.png", UriKind.Absolute);
                        break;

                    case Led.Gray:
                        uri = new Uri(@"pack://application:,,,/"
                                  + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                                  + ";component/"
                                  + "Resources/gray.png", UriKind.Absolute);
                        break;

                case Led.White:
                    uri = new Uri(@"pack://application:,,,/"
                              + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                              + ";component/"
                              + "Resources/white.png", UriKind.Absolute);
                    break;

                default:
                        uri = new Uri(@"pack://application:,,,/"
                                  + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                                  + ";component/"
                                  + "Resources/empty.png", UriKind.Absolute);
                        break;
            }

            return uri;
            //}
            //catch { return null; }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
