﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace StateUAVControl
{
    [ValueConversion(sourceType: typeof(string), targetType: typeof(string))]
    public class TBTextConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //try
            //{
            switch (value)
            {
                case "2.4":
                    return "2, 4";

                case "5.2":
                    return "5, 2";

                case "5.8":
                    return "5, 8";

                case "NAV":
                    return "NAV";

                default:
                    return string.Empty;
            }
            //}
            //catch { }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
