﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Collections.Generic;

namespace StateUAVControl
{
    [ValueConversion(sourceType: typeof(List<FreqsRange>), targetType: typeof(string))]
    public class FreqMinConverter : IValueConverter
    {
        /// <summary>
        /// Преобразовать значение double частоты в строку string 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return FreqResult.GetFreqs(value as List<FreqsRange>, Frequency.FreqMin, culture);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //Int32 result;

            //if (Int32.TryParse(((FreqsRange)value).FreqMinKHz.ToString(), NumberStyles.Any, culture, out result))
            //{
            //    return result;
            //}
            return value;
        }
    }

    [ValueConversion(sourceType: typeof(List<FreqsRange>), targetType: typeof(string))]
    public class FreqMaxConverter : IValueConverter
    {
        /// <summary>
        /// Преобразовать значение double частоты в строку string 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return FreqResult.GetFreqs(value as List<FreqsRange>, Frequency.FreqMax, culture);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //Int32 result;

            //if (Int32.TryParse(((FreqsRange)value).FreqMaxKHz.ToString(), NumberStyles.Any, culture, out result))
            //{
            //    return result;
            //}
            return value;
        }
    }

    [ValueConversion(sourceType: typeof(List<FreqsRange>), targetType: typeof(string))]
    public class BandConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            List<FreqsRange> list = value as List<FreqsRange>;
            string strBand = string.Empty;

            if (list.Count == 2)
            {
                return "GPS L1, Beidou L1\r\nGLONASS L1";
            }
            
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].FreqMinKHz == -2 || list[i].FreqMaxKHz == -2)
                {
                    return string.Empty;
                }

                strBand += FreqResult.GetFreq(list[i].FreqMinKHz, culture) + " - " + FreqResult.GetFreq(list[i].FreqMaxKHz, culture) + "\r\n";
            }

            return strBand.Substring(0, strBand.Length - 2);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }
    }

    [ValueConversion(sourceType: typeof(string), targetType: typeof(string))]
    public class FreqConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            switch (value)
            {
                case "2.4":
                    return ChangeFreqMHz_kHz.FreqMHz_kHz == 0 ? "2 442 500,0" : "2 442,5";

                case "5.2":
                    return ChangeFreqMHz_kHz.FreqMHz_kHz == 0 ? "5 185 000,0" : "5 185,0";

                case "5.8":
                    return ChangeFreqMHz_kHz.FreqMHz_kHz == 0 ? "5 800 000,0" : "5 800,0";

                case "NAV":

                    string L1 = (ChangeFreqMHz_kHz.FreqMHz_kHz == 0 ? "1 568 000,0" : "1 568,0") + "\r\n";
                    string L2 = ChangeFreqMHz_kHz.FreqMHz_kHz == 0 ? "1 602 000,0" : "1 602,0";
                    return L1 + L2;
                           
                default:
                    return string.Empty;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }
    }

    public class FreqResult
    {
        public static string GetFreqs(List<FreqsRange> list, Frequency frequency, CultureInfo culture)
        {
            string strFreqResult = string.Empty;
            double dFreq = 0;

            for (int i = 0; i < list.Count; i++)
            {
                switch (frequency)
                {
                    case Frequency.FreqMin:
                        dFreq = System.Convert.ToDouble(list[i].FreqMinKHz);
                        break;

                    case Frequency.FreqMax:
                        dFreq = System.Convert.ToDouble(list[i].FreqMaxKHz);
                        break;
                }

                strFreqResult += GetFreq(dFreq, culture);
            }

            return strFreqResult.Substring(0, strFreqResult.Length - 2);
        }

        public static string GetFreq(double dFreq, CultureInfo culture)
        {
            string strFreqResult = string.Empty;

            if (dFreq == -2F)
            {
                return string.Empty;
            }

            bool bTemp = dFreq.ToString().Contains(",");
            int Freq = 0;
                   
            string strFreq = string.Empty;
            int iLength;

            switch (ChangeFreqMHz_kHz.FreqMHz_kHz)
            {
                case 0:

                    if (bTemp) { Freq = (int)(dFreq * 10d); }
                    else Freq = System.Convert.ToInt32(dFreq * 10);
                            
                    strFreq = System.Convert.ToString(Freq);
                    iLength = strFreq.Length;

                    try
                    {
                        if (strFreq == "0")
                            return strFreq;

                        if (iLength == 8)
                        {
                            strFreqResult = strFreq.Substring(0, 1) + " " + strFreq.Substring(1, 3) + " " + strFreq.Substring(iLength - 4, 3) + ",".ToString(culture) + strFreq.Substring(iLength - 1, 1);
                        }    
                        else
                        {
                            strFreqResult = strFreq.Substring(0, iLength - 4) + " " + strFreq.Substring(iLength - 4, 3) + ",".ToString(culture) + strFreq.Substring(iLength - 1, 1);
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message.ToString());
                    }
                    break;

                case 1:

                    if (bTemp) { Freq = (int)(dFreq / 1000 * 10d); }
                    else Freq = System.Convert.ToInt32(dFreq / 1000 * 10);    

                    strFreqResult = "";
                    strFreq = System.Convert.ToString(Freq);
                    iLength = strFreq.Length;

                    try
                    {
                        if (strFreq == "0")
                            return strFreq; //string.Empty;

                        if (iLength == 5)
                        {
                            strFreqResult = strFreq.Substring(0, 1) + " " + strFreq.Substring(1, 3) + ",".ToString(culture) + strFreq.Substring(iLength - 1, 1);
                        }
                        else
                        {
                            strFreqResult = strFreq.Substring(0, iLength - 1) + ",".ToString(culture) + strFreq.Substring(iLength - 1, 1);
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message.ToString());
                    }    
                    break;
            }

            return strFreqResult;
        }







        //public static string GetFreqs(List<FreqsRange> list, Frequency frequency, CultureInfo culture)
        //{
        //    string strFreqResult = string.Empty;
        //    double dFreq = 0;

        //    for (int i = 0; i < list.Count; i++)
        //    {
        //        switch (frequency)
        //        {
        //            case Frequency.FreqMin:
        //                dFreq = System.Convert.ToDouble(list[i].FreqMinKHz);
        //                break;

        //            case Frequency.FreqMax:
        //                dFreq = System.Convert.ToDouble(list[i].FreqMaxKHz);
        //                break;
        //        }

        //        if (dFreq == -2F)
        //        {
        //            return string.Empty;
        //        }

        //        bool bTemp = dFreq.ToString().Contains(",");
        //        int Freq = 0;
                   
        //        string strFreq = string.Empty;
        //        int iLength;

        //        switch (PropLocalFreqMHz_kHz.FreqMHz_kHz)
        //        {
        //            case 0:

        //                if (bTemp) { Freq = (int)(dFreq * 10d); }
        //                else Freq = System.Convert.ToInt32(dFreq * 10);
                            
        //                strFreq = System.Convert.ToString(Freq);
        //                iLength = strFreq.Length;

        //                try
        //                {
        //                    if (strFreq == "0")
        //                        return strFreq;

        //                    if (iLength == 8)
        //                    {
        //                        strFreqResult += strFreq.Substring(0, 1) + " " + strFreq.Substring(1, 3) + " " + strFreq.Substring(iLength - 4, 3) + ",".ToString(culture) + strFreq.Substring(iLength - 1, 1) + "\r\n";
        //                    }
        //                    else
        //                    {
        //                        strFreqResult += strFreq.Substring(0, iLength - 4) + " " + strFreq.Substring(iLength - 4, 3) + ",".ToString(culture) + strFreq.Substring(iLength - 1, 1) + "\r\n";
        //                    }
        //                }
        //                catch (Exception ex)
        //                {
        //                    Console.WriteLine(ex.Message.ToString());
        //                }
        //                break;

        //            case 1:

        //                if (bTemp) { Freq = (int)(dFreq / 1000 * 10d); }
        //                else Freq = System.Convert.ToInt32(dFreq / 1000 * 10);

        //                strFreqResult = "";
        //                strFreq = System.Convert.ToString(Freq);
        //                iLength = strFreq.Length;

        //                try
        //                {
        //                    if (strFreq == "0")
        //                        return strFreq; //string.Empty;

        //                    if (iLength == 5)
        //                    {
        //                        strFreqResult += strFreq.Substring(0, 1) + " " + strFreq.Substring(1, 3) + ",".ToString(culture) + strFreq.Substring(iLength - 1, 1) + "\r\n";
        //                    }
        //                    else
        //                    {
        //                        strFreqResult += strFreq.Substring(0, iLength - 1) + ",".ToString(culture) + strFreq.Substring(iLength - 1, 1) + "\r\n";
        //                    }
        //                }
        //                catch (Exception ex)
        //                {
        //                    Console.WriteLine(ex.Message.ToString());
        //                }
        //                break;
        //        }
        //    }


        //    return strFreqResult.Substring(0, strFreqResult.Length - 2);
        //}
    }
}