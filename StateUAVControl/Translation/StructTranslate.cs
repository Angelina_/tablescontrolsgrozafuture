﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StateUAVControl
{
    #region Значения
    public struct SMeaning
    {
        public static string meaningFminMHz;
        public static string meaningFmaxMHz;
        public static string meaningFMHz;
        public static string meaningFkHz;
        public static string meaningFminkHz;
        public static string meaningFmaxkHz;
        public static string meaningBandkHz;
        public static string meaningBandMHz;

        public static string meaningkHz;
        public static string meaningMHz;

        public static string meaningms;

        public static void InitSMeaning()
        {
            meaningFminMHz = "F мин., МГц";
            meaningFmaxMHz = "F макс., МГц";
            meaningFminkHz = "F мин., кГц";
            meaningFmaxkHz = "F макс., кГц";
            meaningFMHz = "F, МГц";
            meaningFkHz = "F, кГц";
            meaningBandkHz = "Диапазон, кГц";
            meaningBandMHz = "Диапазон, МГц";

            meaningkHz = "кГц";
            meaningMHz = "МГц";

            meaningms = "мс";
        }
    }
    #endregion

    #region Параметры
    public struct STypeModulation
    {
        public static string paramLChM;

        public static void InitSTypeModulation()
        {
            paramLChM = "ЛЧМ";
        }
    }
    #endregion

}
