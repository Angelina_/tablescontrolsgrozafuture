﻿using System.Collections.Generic;

namespace StateUAVControl
{
    public class FunctionsTranslate
    {
        /// <summary>
        /// Переименование значений при смене языка
        /// </summary>
        /// <param name="TranslateDic"></param>
        public static void RenameMeaning(Dictionary<string, string> TranslateDic)
        {
            if (TranslateDic.ContainsKey("meaningFminMHz"))
                SMeaning.meaningFminMHz = TranslateDic["meaningFminMHz"];

            if (TranslateDic.ContainsKey("meaningFmaxMHz"))
                SMeaning.meaningFmaxMHz = TranslateDic["meaningFmaxMHz"];

            if (TranslateDic.ContainsKey("meaningFminkHz"))
                SMeaning.meaningFminkHz = TranslateDic["meaningFminkHz"];

            if (TranslateDic.ContainsKey("meaningFmaxkHz"))
                SMeaning.meaningFmaxkHz = TranslateDic["meaningFmaxkHz"];

            if (TranslateDic.ContainsKey("meaningFkHz"))
                SMeaning.meaningFkHz = TranslateDic["meaningFkHz"];

            if (TranslateDic.ContainsKey("meaningFMHz"))
                SMeaning.meaningFMHz = TranslateDic["meaningFMHz"];

            if (TranslateDic.ContainsKey("meaningBandkHz"))
                SMeaning.meaningBandkHz = TranslateDic["meaningBandkHz"];

            if (TranslateDic.ContainsKey("meaningBandMHz"))
                SMeaning.meaningBandMHz = TranslateDic["meaningBandMHz"];

            if (TranslateDic.ContainsKey("meaningMHz"))
                SMeaning.meaningMHz = TranslateDic["meaningMHz"];

            if (TranslateDic.ContainsKey("meaningkHz"))
                SMeaning.meaningkHz = TranslateDic["meaningkHz"];

            if (TranslateDic.ContainsKey("meaningms"))
                SMeaning.meaningms = TranslateDic["meaningms"];

            if (TranslateDic.ContainsKey("paramLChM"))
                STypeModulation.paramLChM = TranslateDic["paramLChM"];
        }
    }
}
