﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace StateUAVControl
{
    using System.Collections.Generic;
    using System.Xml;

    public partial class UserControlStateUAV : UserControl
    {
        public void SetTranslation(Language language)
        {
            ResourceDictionary dict = new ResourceDictionary();
            try
            {
                switch (language)
                {
                    case StateUAVControl.Language.RU:
                        dict.Source = new Uri("/StateUAVControl;component/Translation/TranslatorTables.RU.xaml",
                                      UriKind.Relative);
                        break;
                    case StateUAVControl.Language.EN:
                        dict.Source = new Uri("/StateUAVControl;component/Translation/TranslatorTables.EN.xaml",
                                           UriKind.Relative);
                        break;
                    case StateUAVControl.Language.AZ:
                        dict.Source = new Uri("/StateUAVControl;component/Translation/TranslatorTables.AZ.xaml",
                                           UriKind.Relative);
                        break;
                    default:
                        dict.Source = new Uri("/StateUAVControl;component/Translation/TranslatorTables.RU.xaml",
                                          UriKind.Relative);
                        break;
                }

                this.Resources.MergedDictionaries.Add(dict);
            }
            catch (Exception ex)
            { }
        }

        static Dictionary<string, string> TranslateDic;
        public static void LoadDictionary(StateUAVControl.Language language)
        {
            XmlDocument xDoc = new XmlDocument();

            var translation = Properties.Resources.TranslatorTables;
            xDoc.LoadXml(translation);

            TranslateDic = new Dictionary<string, string>();

            // получим корневой элемент
            XmlElement xRoot = xDoc.DocumentElement;
            foreach (XmlNode x2Node in xRoot.ChildNodes)
            {
                if (x2Node.NodeType == XmlNodeType.Comment)
                    continue;

                // получаем атрибут ID
                if (x2Node.Attributes.Count > 0)
                {
                    XmlNode attr = x2Node.Attributes.GetNamedItem("ID");
                    if (attr != null)
                    {
                        foreach (XmlNode childnode in x2Node.ChildNodes)
                        {
                            // если узел - language
                            if (childnode.Name == language.ToString())
                            {
                                if (!TranslateDic.ContainsKey(attr.Value))
                                    TranslateDic.Add(attr.Value, childnode.InnerText);
                            }
                        }
                    }
                }
            }

            if (TranslateDic.Count > 0 && TranslateDic != null)
            {
                FunctionsTranslate.RenameMeaning(TranslateDic);
            }
        }
    }
}
