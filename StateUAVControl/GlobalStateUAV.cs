﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace StateUAVControl
{
    using System.Collections.Generic;

    public class GlobalStateUAV : INotifyPropertyChanged
    {
        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }
        #endregion

        public ObservableCollection<StateUAVModel> CollectionStateUAV { get; set; }

        public GlobalStateUAV()
        {
            //CollectionStateUAV = new ObservableCollection<StateUAVModel> { };

            #region Test
            CollectionStateUAV = new ObservableCollection<StateUAVModel>
            {
                new StateUAVModel
                {
                    Name = "2.4",
                    FreqsRange = new List<FreqsRange>()
                                    {
                                        new FreqsRange()
                                        {
                                            FreqMinKHz = 2400000, FreqMaxKHz = 2480000
                                        }

                                    },
                    //Control = Led.Empty,
                    Suppress = Led.Empty,
                    InterferenceParam = STypeModulation.paramLChM + " +/- 43 " + SMeaning.meaningMHz + " +/- 50 " + SMeaning.meaningkHz + " 0.5 " + SMeaning.meaningms
                },
                new StateUAVModel
                {
                    Name = "5.2",
                    FreqsRange = new List<FreqsRange>()
                                     {
                                         new FreqsRange()
                                             {
                                                 FreqMinKHz = 5125000, FreqMaxKHz = 5250000
                                             }
                                     },
                    //Control = Led.Empty,
                    Suppress = Led.Empty,
                    InterferenceParam = STypeModulation.paramLChM + " +/- 65 " + SMeaning.meaningMHz + " +/- 50 " + SMeaning.meaningkHz + " 0.5 " + SMeaning.meaningms
                },
                new StateUAVModel
                {
                    Name = "5.8",
                    FreqsRange = new List<FreqsRange>()
                                     {
                                         new FreqsRange()
                                             {
                                                 FreqMinKHz = 5725000, FreqMaxKHz = 5850000
                                             }

                                     },
                    //Control = Led.Empty,
                    Suppress = Led.Empty,
                    InterferenceParam = STypeModulation.paramLChM + " +/- 75 " + SMeaning.meaningMHz + " +/- 50 " + SMeaning.meaningkHz + " 0.5 " + SMeaning.meaningms
                },
                new StateUAVModel
                    {
                        Name = "NAV",
                        FreqsRange = new List<FreqsRange>()
                                         {
                                             new FreqsRange()
                                                 {
                                                     FreqMinKHz = 1500, FreqMaxKHz = 1550
                                                 },
                                             new FreqsRange()
                                                 {
                                                     FreqMinKHz = 1600, FreqMaxKHz = 1650
                                                 }
                                         },
                        //Control = Led.Empty,
                        Suppress = Led.Empty,
                        InterferenceParam = STypeModulation.paramLChM + " +/- 8 " + SMeaning.meaningMHz + " +/- 100 " + SMeaning.meaningkHz + " 0.5 " + SMeaning.meaningms + "\r\n" +
                                            STypeModulation.paramLChM + " +/- 4 " + SMeaning.meaningMHz + " +/- 100 " + SMeaning.meaningkHz + " 0.5 " + SMeaning.meaningms
                    }
            };
            #endregion
        }

    }
}
