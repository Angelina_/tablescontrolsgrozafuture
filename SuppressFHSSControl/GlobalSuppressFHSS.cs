﻿using ModelsTablesDBLib;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace SuppressFHSSControl
{
    public class TableSuppressFHSSAll
    {
        public TableSuppressFHSS TableSuppressFHSS { get; set; } = new TableSuppressFHSS();
        public TempSuppressFHSS TempSuppressFHSS { get; set; } = new TempSuppressFHSS();
    }

    public class GlobalSuppressFHSS : INotifyPropertyChanged
    {
        public ObservableCollection<TableSuppressFHSSAll> CollectionSuppressFHSS { get; set; }

        public GlobalSuppressFHSS()
        {
            CollectionSuppressFHSS = new ObservableCollection<TableSuppressFHSSAll>() { };

            //CollectionSuppressFHSS = new ObservableCollection<TableSuppressFHSSAll>()
            //{
            //    new TableSuppressFHSSAll
            //    {
            //        TableSuppressFHSS = new TableSuppressFHSS
            //        {
            //            Id = 1,
            //            FreqMinKHz = 35887.5,
            //            FreqMaxKHz = 88000.6,
            //            Letters = new byte[] { 1, 2 },
            //            Threshold = -65,
            //            EPO = new byte[] { 1, 3 },
            //            StepKHz = 100,
            //            InterferenceParam = new InterferenceParam
            //            {
            //                Modulation = 2,
            //                Manipulation = 0,
            //                Deviation = 1,
            //                Duration = 2
            //            }
            //        },
            //        TempSuppressFHSS = new TempSuppressFHSS
            //        {
            //            Control = Led.Red,
            //            Suppress = Led.Green,
            //            Radiation = Led.Blue
            //        }
            //    },
            //    new TableSuppressFHSSAll
            //    {
            //        TableSuppressFHSS = new TableSuppressFHSS
            //        {
            //            Id = 2,
            //            FreqMinKHz = 120000.25,
            //            FreqMaxKHz = 177000.77,
            //            Letters = new byte[] { 4, 5 },
            //            Threshold = -80,
            //            EPO = new byte[] { 4, 6 },
            //            StepKHz = 400,
            //            InterferenceParam = new InterferenceParam
            //            {
            //                Modulation = 1,
            //                Manipulation = 1,
            //                Deviation = 2,
            //                Duration = 1
            //            }
            //        },
            //        TempSuppressFHSS = new TempSuppressFHSS
            //        {
            //            Control = Led.Yellow,
            //            Suppress = Led.Green,
            //            Radiation = Led.Blue
            //        }
            //    }
            //};
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
    }

    public class GlobalFHSSExcludedFreq : INotifyPropertyChanged
    {
        public ObservableCollection<TableFHSSExcludedFreq> CollectionFHSSExcludedFreq { get; set; }

        public GlobalFHSSExcludedFreq()
        {
            CollectionFHSSExcludedFreq = new ObservableCollection<TableFHSSExcludedFreq>() { };

            //CollectionFHSSExcludedFreq = new ObservableCollection<TableFHSSExcludedFreq>()
            //{
            //    new TableFHSSExcludedFreq
            //    {
            //        Id = 1,
            //        IdFHSS = 1,
            //        Deviation = 300,
            //        FreqKHz = 36000.0F
            //    },
            //    new TableFHSSExcludedFreq
            //    {
            //        Id = 2,
            //        IdFHSS = 1,
            //        Deviation = 700,
            //        FreqKHz = 79005.6F
            //    },
            //    new TableFHSSExcludedFreq
            //    {
            //        Id = 3,
            //        IdFHSS = 2,
            //        Deviation = 250,
            //        FreqKHz = 111000.0F
            //    },
            //    new TableFHSSExcludedFreq
            //    {
            //        Id = 4,
            //        IdFHSS = 2,
            //        Deviation = 700,
            //        FreqKHz = 157300.6F
            //    }
            //};
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
    }
}
