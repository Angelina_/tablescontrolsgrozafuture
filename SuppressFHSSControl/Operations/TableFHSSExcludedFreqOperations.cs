﻿using ModelsTablesDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using TableEvents;

namespace SuppressFHSSControl
{
    public partial class UserControlSuppressFHSS : UserControl
    {
        public List<TableFHSSExcludedFreq> listExcludedFreq = new List<TableFHSSExcludedFreq>();

        /// <summary>
        /// Обновить записи в контроле (Выколотые частоты)
        /// </summary>
        /// <param name="listFHSSExcludedFreq"></param>
        public void UpdateFHSSExcludedFreq(List<TableFHSSExcludedFreq> listFHSSExcludedFreq)
        {
            try
            {
                if (listFHSSExcludedFreq == null)
                    return;

                listExcludedFreq = listFHSSExcludedFreq;

                List<TableFHSSExcludedFreq> list = listExcludedFreq.Where(x => x.IdFHSS == PropSelectedIdFHSS.SelectedIdSupprFHSS).ToList();

                if (list != null && list.Count > 0)
                {
                    ((GlobalFHSSExcludedFreq)dgvExcludedFreq.DataContext).CollectionFHSSExcludedFreq.Clear();

                    for (int i = 0; i < list.Count; i++)
                    {
                        ((GlobalFHSSExcludedFreq)dgvExcludedFreq.DataContext).CollectionFHSSExcludedFreq.Add(list[i]);
                    }

                    AddEmptyRowsExcludedFreq();
                }
            }
            catch { }
        }

        /// <summary>
        /// Обновть таблицу (Выколотые частоты)
        /// </summary>
        /// <param name="listFHSSExcludedFreqs"></param>
        private void UpdateTableExcludedFreqs(int id)
        {
            try
            {
                ((GlobalFHSSExcludedFreq)dgvExcludedFreq.DataContext).CollectionFHSSExcludedFreq.Clear();

                List<TableFHSSExcludedFreq> list = listExcludedFreq.Where(x => x.IdFHSS == id).ToList();

                for (int i = 0; i < list.Count; i++)
                {
                    ((GlobalFHSSExcludedFreq)dgvExcludedFreq.DataContext).CollectionFHSSExcludedFreq.Add(list[i]);
                }

                AddEmptyRowsExcludedFreq();
            }
            catch { }
        }

        /// <summary>
        /// Добавить несколько записей в контрол (Выколотые частоты)
        /// </summary>
        /// <param name="listFHSSExcludedFreqs"></param>
        public void AddFHSSExcludedFreqs(List<TableFHSSExcludedFreq> listFHSSExcludedFreq)
        {
            try
            {
                DeleteEmptyRowsExcludedFreq();

                for (int i = 0; i < listFHSSExcludedFreq.Count; i++)
                {
                    ((GlobalFHSSExcludedFreq)dgvExcludedFreq.DataContext).CollectionFHSSExcludedFreq.Add(listFHSSExcludedFreq[i]);
                }

                AddEmptyRowsExcludedFreq();
            }
            catch { }
        }

        /// <summary>
        /// Добавить одну запись в контрол (Выколотые частоты)
        /// </summary>
        /// <param name="FHSSExcludedFreq"></param>
        public void AddFHSSExcludedFreq(TableFHSSExcludedFreq FHSSExcludedFreq)
        {
            try
            {
                listExcludedFreq.Add(FHSSExcludedFreq);

                DeleteEmptyRowsExcludedFreq();

                ((GlobalFHSSExcludedFreq)dgvExcludedFreq.DataContext).CollectionFHSSExcludedFreq.Add(FHSSExcludedFreq);

                AddEmptyRowsExcludedFreq();
            }
            catch { }
        }

        /// <summary>
        /// Удалить все записи из контрола (Выколотые частоты)
        /// </summary>
        public void ClearExcludedFreq()
        {
            try
            {
                ((GlobalFHSSExcludedFreq)dgvExcludedFreq.DataContext).CollectionFHSSExcludedFreq.Clear();

                AddEmptyRowsExcludedFreq();
            }
            catch { }
        }

        /// <summary>
        /// Изменить одну запись в контроле (Выколотые частоты)
        /// </summary>
        /// <param name="id"> id записи, которую надо заменить </param>
        /// <param name="supprReplace"> запись, на которую надо заменить </param>
        public void ChangeFHSSExcludedFreq(int id, TableFHSSExcludedFreq supprReplace)
        {
            try
            {
                int index = (((GlobalFHSSExcludedFreq)dgvExcludedFreq.DataContext).CollectionFHSSExcludedFreq).ToList().FindIndex(x => x.Id == id);

                if (index != -1)
                {
                    ((GlobalFHSSExcludedFreq)dgvExcludedFreq.DataContext).CollectionFHSSExcludedFreq.RemoveAt(index);
                    ((GlobalFHSSExcludedFreq)dgvExcludedFreq.DataContext).CollectionFHSSExcludedFreq.Insert(index, supprReplace);

                    AddEmptyRowsExcludedFreq();
                }
            }
            catch { }
        }

        /// <summary>
        /// Добавить пустые строки в таблицу (Выколотые частоты)
        /// </summary>
        private void AddEmptyRowsExcludedFreq()
        {
            int сountRowsAll = dgvExcludedFreq.Items.Count; // количество всех строк в таблице
            double hs = 23; // высота строки
            double ah = dgvExcludedFreq.ActualHeight; // визуализированная высота dataGrid
            double chh = dgvExcludedFreq.ColumnHeaderHeight; // высота заголовка
            int countRows = Convert.ToInt32(Math.Floor(Convert.ToDouble(ah) - Convert.ToDouble(chh)) / hs); // количество строк, которое помещается в область видимости = (высота таблицы - заголовок таблицы) / высота строки

            int count = сountRowsAll - countRows; // сколько пустых строк нужно удалить
            int index = -1;
            for (int i = 0; i < count; i++)
            {
                // Удалить пустые строки в dgv
                index = ((GlobalFHSSExcludedFreq)dgvExcludedFreq.DataContext).CollectionFHSSExcludedFreq.ToList().FindIndex(x => x.IdFHSS < 0);
                if (index != -1)
                {
                    ((GlobalFHSSExcludedFreq)dgvExcludedFreq.DataContext).CollectionFHSSExcludedFreq.RemoveAt(index);
                }
            }

            List<TableFHSSExcludedFreq> list = new List<TableFHSSExcludedFreq>();
            for (int i = 0; i < countRows - сountRowsAll; i++)
            {
                TableFHSSExcludedFreq strFHSSExcludedFreq = new TableFHSSExcludedFreq
                {
                    Id = -2,
                    IdFHSS = -2,
                    Deviation = -2,
                    FreqKHz = -2F
                };

                list.Add(strFHSSExcludedFreq);
            }

            for (int i = 0; i < list.Count; i++)
            {
                ((GlobalFHSSExcludedFreq)dgvExcludedFreq.DataContext).CollectionFHSSExcludedFreq.Add(list[i]);
            }
        }

        /// <summary>
        /// Удалить пустые строки из таблицы (Выколотые частоты)
        /// </summary>
        private void DeleteEmptyRowsExcludedFreq()
        {
            int countEmptyRows = ((GlobalFHSSExcludedFreq)dgvExcludedFreq.DataContext).CollectionFHSSExcludedFreq.Count(s => s.Id < 0);
            int countAllRows = ((GlobalFHSSExcludedFreq)dgvExcludedFreq.DataContext).CollectionFHSSExcludedFreq.Count;
            int iCount = countAllRows - countEmptyRows;
            for (int i = iCount; i < countAllRows; i++)
            {
                ((GlobalFHSSExcludedFreq)dgvExcludedFreq.DataContext).CollectionFHSSExcludedFreq.RemoveAt(iCount);
            }
        }
    }
}
