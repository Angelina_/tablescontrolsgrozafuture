﻿using ModelsTablesDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using TableEvents;
using TableOperations;
using ValuesConverter;
using ValuesCorrectLib;

namespace SuppressFHSSControl
{
    public partial class UserControlSuppressFHSS : UserControl
    {
        /// <summary>
        /// Обновить записи в контроле
        /// </summary>
        /// <param name="listSuppressFHSS"></param>
        public void UpdateSuppressFHSS(List<TableSuppressFHSS> listSuppressFHSS)
        {
            try
            {
                ((GlobalSuppressFHSS)dgvSuppressFHSS.DataContext).CollectionSuppressFHSS.Clear();

                List<TableSuppressFHSSAll> list = SuppressFHSSToSuppressFHSSAll(listSuppressFHSS);

                for (int i = 0; i < listSuppressFHSS.Count; i++)
                {
                    ((GlobalSuppressFHSS)dgvSuppressFHSS.DataContext).CollectionSuppressFHSS.Add(list[i]);
                }

                //dgvSuppressFHSS.ItemsSource = ((GlobalSuppressFHSS)dgvSuppressFHSS.DataContext).CollectionSuppressFHSS;
               
                AddEmptyRows();

                int ind = ((GlobalSuppressFHSS)dgvSuppressFHSS.DataContext).CollectionSuppressFHSS.ToList().FindIndex(x => x.TableSuppressFHSS.Id == PropSelectedIdFHSS.SelectedIdSupprFHSS);
                if (ind != -1)
                {
                    dgvSuppressFHSS.SelectedIndex = ind;
                }
                else
                {
                    dgvSuppressFHSS.SelectedIndex = 0;
                }

            }
            catch { }
        }

        /// <summary>
        /// ControlState -- КР
        /// RadiationState -- Излучение
        /// SuppressState -- РП
        /// </summary>
        /// <param name="answer"></param>
        public void UpdateRadioJamState(List<TempSuppressFHSS> answer)
        {
            Led ControlState;
            Led SuppressState;
            Led RadiationState;

            int ind = -1;

            if (answer.Count == 0)
            {
                ClearRadioJamState();
            }

            if (answer.Count > 0)
            {
                for (int i = 0; i < answer.Count; i++)
                {
                    ind = ((GlobalSuppressFHSS)dgvSuppressFHSS.DataContext).CollectionSuppressFHSS.ToList().FindIndex(x => x.TableSuppressFHSS.Id == answer[i].Id);

                    if (ind != -1)
                    {
                        ControlState = answer[ind].Control;
                        switch (ControlState)
                        {
                            case Led.Empty: // нет сигнала (ничего)
                                ((GlobalSuppressFHSS)dgvSuppressFHSS.DataContext).CollectionSuppressFHSS[ind].TempSuppressFHSS.Control = Led.Empty;
                                break;

                            case Led.Red: // есть сигнал (красный)
                                ((GlobalSuppressFHSS)dgvSuppressFHSS.DataContext).CollectionSuppressFHSS[ind].TempSuppressFHSS.Control = Led.Red;
                                break;

                            case Led.Green: // давно есть сигнал (зеленый)
                                ((GlobalSuppressFHSS)dgvSuppressFHSS.DataContext).CollectionSuppressFHSS[ind].TempSuppressFHSS.Control = Led.Green;
                                break;

                            case Led.Yellow: // давно нет сигнала (желтый)
                                ((GlobalSuppressFHSS)dgvSuppressFHSS.DataContext).CollectionSuppressFHSS[ind].TempSuppressFHSS.Control = Led.Yellow;
                                break;
                        }

                        SuppressState = answer[ind].Suppress;
                        switch (SuppressState)
                        {
                            case Led.Empty: // нет подавления (ничего)
                                ((GlobalSuppressFHSS)dgvSuppressFHSS.DataContext).CollectionSuppressFHSS[ind].TempSuppressFHSS.Suppress = Led.Empty;
                                break;

                            case Led.Red: // есть подавление (красный)
                                ((GlobalSuppressFHSS)dgvSuppressFHSS.DataContext).CollectionSuppressFHSS[ind].TempSuppressFHSS.Suppress = Led.Red;
                                break;

                            case Led.Green: // давно есть подавление (зеленый)
                                ((GlobalSuppressFHSS)dgvSuppressFHSS.DataContext).CollectionSuppressFHSS[ind].TempSuppressFHSS.Suppress = Led.Green;
                                break;
                        }

                        RadiationState = answer[ind].Radiation;
                        switch (RadiationState)
                        {
                            case Led.Empty: // нет излучения (ничего)
                                ((GlobalSuppressFHSS)dgvSuppressFHSS.DataContext).CollectionSuppressFHSS[ind].TempSuppressFHSS.Radiation = Led.Empty;
                                break;

                            case Led.Blue: // есть излучение (синий)
                                ((GlobalSuppressFHSS)dgvSuppressFHSS.DataContext).CollectionSuppressFHSS[ind].TempSuppressFHSS.Radiation = Led.Blue;
                                break;
                        }

                        ChangeSuppressFHSS(ind, ((GlobalSuppressFHSS)dgvSuppressFHSS.DataContext).CollectionSuppressFHSS[ind]);
                    }
                }
            }
        }

        /// <summary>
        /// Изменить запись в контроле (подавление)
        /// </summary>
        /// <param name="index"></param>
        /// <param name="supprReplace"></param>
        public void ChangeSuppressFHSS(int index, TableSuppressFHSSAll supprReplace)
        {
            try
            {
                ((GlobalSuppressFHSS)dgvSuppressFHSS.DataContext).CollectionSuppressFHSS.RemoveAt(index);
                ((GlobalSuppressFHSS)dgvSuppressFHSS.DataContext).CollectionSuppressFHSS.Insert(index, supprReplace);

                AddEmptyRows();
            }
            catch { }
        }

        /// <summary>
        /// Очистить RadioJamState
        /// </summary>
        public void ClearRadioJamState()
        {
            try
            {
                for (int i = 0; i < ((GlobalSuppressFHSS)dgvSuppressFHSS.DataContext).CollectionSuppressFHSS.Count; i++)
                {
                    ((GlobalSuppressFHSS)dgvSuppressFHSS.DataContext).CollectionSuppressFHSS[i].TempSuppressFHSS.Control = Led.Empty;
                    ((GlobalSuppressFHSS)dgvSuppressFHSS.DataContext).CollectionSuppressFHSS[i].TempSuppressFHSS.Suppress = Led.Empty;
                    ((GlobalSuppressFHSS)dgvSuppressFHSS.DataContext).CollectionSuppressFHSS[i].TempSuppressFHSS.Radiation = Led.Empty;
                }
            }
            catch { }
        }

        /// <summary>
        /// Преобразование TableSuppressFHSS к TableSuppressFHSSAll
        /// </summary>
        /// <param name="listSuppressFHSS"></param>
        /// <returns> List<TableSuppressFWSAll> </returns>
        private List<TableSuppressFHSSAll> SuppressFHSSToSuppressFHSSAll(List<TableSuppressFHSS> listSuppressFHSS)
        {
            List<TableSuppressFHSSAll> list = new List<TableSuppressFHSSAll>();
            for (int i = 0; i < listSuppressFHSS.Count; i++)
            {
                TableSuppressFHSSAll table = new TableSuppressFHSSAll();
                table.TableSuppressFHSS.Id = listSuppressFHSS[i].Id;
                table.TableSuppressFHSS.IdMission = listSuppressFHSS[i].IdMission;
                table.TableSuppressFHSS.NumberASP = listSuppressFHSS[i].NumberASP;
                table.TableSuppressFHSS.Letters = listSuppressFHSS[i].Letters;
                table.TableSuppressFHSS.FreqMinKHz = listSuppressFHSS[i].FreqMinKHz;
                table.TableSuppressFHSS.FreqMaxKHz = listSuppressFHSS[i].FreqMaxKHz;
                table.TableSuppressFHSS.Threshold = listSuppressFHSS[i].Threshold;
                table.TableSuppressFHSS.EPO = listSuppressFHSS[i].EPO;
                table.TableSuppressFHSS.StepKHz = listSuppressFHSS[i].StepKHz;
                table.TableSuppressFHSS.InterferenceParam.Modulation = listSuppressFHSS[i].InterferenceParam.Modulation;
                table.TableSuppressFHSS.InterferenceParam.Manipulation = listSuppressFHSS[i].InterferenceParam.Manipulation;
                table.TableSuppressFHSS.InterferenceParam.Duration = listSuppressFHSS[i].InterferenceParam.Duration;
                table.TableSuppressFHSS.InterferenceParam.Deviation = listSuppressFHSS[i].InterferenceParam.Deviation;

                list.Add(table);
            }

            return list;
        }

        /// <summary>
        /// Преобразование TableSuppressFHSS к TableSuppressFHSSAll
        /// </summary>
        /// <param name="listSuppressFHSS"></param>
        /// <returns> List<TableSuppressFWSAll> </returns>
        private List<TableSuppressFHSS> SuppressFHSSAllToSuppressFHSS(List<TableSuppressFHSSAll> listSuppressFHSSAll)
        {
            List<TableSuppressFHSS> list = new List<TableSuppressFHSS>();
            for (int i = 0; i < listSuppressFHSSAll.Count; i++)
            {
                TableSuppressFHSS table = new TableSuppressFHSS();

                if (listSuppressFHSSAll[i].TableSuppressFHSS.Id > 0)
                {
                    table.Id = listSuppressFHSSAll[i].TableSuppressFHSS.Id;
                    table.IdMission = listSuppressFHSSAll[i].TableSuppressFHSS.IdMission;
                    table.NumberASP = listSuppressFHSSAll[i].TableSuppressFHSS.NumberASP;
                    table.Letters = listSuppressFHSSAll[i].TableSuppressFHSS.Letters;
                    table.FreqMinKHz = listSuppressFHSSAll[i].TableSuppressFHSS.FreqMinKHz;
                    table.FreqMaxKHz = listSuppressFHSSAll[i].TableSuppressFHSS.FreqMaxKHz;
                    table.Threshold = listSuppressFHSSAll[i].TableSuppressFHSS.Threshold;
                    table.EPO = listSuppressFHSSAll[i].TableSuppressFHSS.EPO;
                    table.StepKHz = listSuppressFHSSAll[i].TableSuppressFHSS.StepKHz;
                    table.InterferenceParam.Modulation = listSuppressFHSSAll[i].TableSuppressFHSS.InterferenceParam.Modulation;
                    table.InterferenceParam.Manipulation = listSuppressFHSSAll[i].TableSuppressFHSS.InterferenceParam.Manipulation;
                    table.InterferenceParam.Duration = listSuppressFHSSAll[i].TableSuppressFHSS.InterferenceParam.Duration;
                    table.InterferenceParam.Deviation = listSuppressFHSSAll[i].TableSuppressFHSS.InterferenceParam.Deviation;

                    list.Add(table);
                }
            }

            return list;
        }

        /// <summary>
        /// Добавить несколько записей в контрол
        /// </summary>
        /// <param name="listSuppressFHSS"></param>
        public void AddSuppressFHSSs(List<TableSuppressFHSS> listSuppressFHSS)
        {
            try
            {
                DeleteEmptyRows();

                List<TableSuppressFHSSAll> list = SuppressFHSSToSuppressFHSSAll(listSuppressFHSS);

                for (int i = 0; i < listSuppressFHSS.Count; i++)
                {
                    ((GlobalSuppressFHSS)dgvSuppressFHSS.DataContext).CollectionSuppressFHSS.Add(list[i]);
                }

                //dgvSuppressFHSS.ItemsSource = ((GlobalSuppressFHSS)dgvSuppressFHSS.DataContext).CollectionSuppressFHSS;

                AddEmptyRows();
            }
            catch { }
        }

        /// <summary>
        /// Добавить одну запись в контрол 
        /// </summary>
        /// <param name="suppressFHSS"></param>
        public void AddSuppressFHSS(TableSuppressFHSS suppressFHSS)
        {
            try
            {
                DeleteEmptyRows();

                TableSuppressFHSSAll table = new TableSuppressFHSSAll();
                table.TableSuppressFHSS = suppressFHSS;

                ((GlobalSuppressFHSS)dgvSuppressFHSS.DataContext).CollectionSuppressFHSS.Add(table);

                //dgvSuppressFHSS.ItemsSource = ((GlobalSuppressFHSS)dgvSuppressFHSS.DataContext).CollectionSuppressFHSS;

                AddEmptyRows();
            }
            catch { }
        }

        /// <summary>
        /// Удалить одну запись из контрола
        /// </summary>
        /// <param name="specFreq"></param>
        public void DeleteSuppressFHSS(TableSuppressFHSS suppressFHSS)
        {
            try
            {
                int index = ((GlobalSuppressFHSS)dgvSuppressFHSS.DataContext).CollectionSuppressFHSS.ToList().FindIndex(x => x.TableSuppressFHSS.Id == suppressFHSS.Id);

                ((GlobalSuppressFHSS)dgvSuppressFHSS.DataContext).CollectionSuppressFHSS.RemoveAt(index);

                //dgvSuppressFHSS.ItemsSource = ((GlobalSuppressFHSS)dgvSuppressFHSS.DataContext).CollectionSuppressFHSS;

                AddEmptyRows();
            }
            catch { }
        }

        /// <summary>
        /// Удалить все записи из контрола
        /// </summary>
        public void ClearSuppressFHSS()
        {
            try
            {
                ((GlobalSuppressFHSS)dgvSuppressFHSS.DataContext).CollectionSuppressFHSS.Clear();

                AddEmptyRows();
            }
            catch { }
        }

        /// <summary>
        /// Изменить одну запись в контроле
        /// </summary>
        /// <param name="id"> id записи, которую надо заменить </param>
        /// <param name="supprReplace"> запись, на которую надо заменить </param>
        public void ChangeSuppressFHSS(int id, TableSuppressFHSS supprReplace)
        {
            try
            {
                TableSuppressFHSSAll table = new TableSuppressFHSSAll();
                table.TableSuppressFHSS = supprReplace;

                int index = (((GlobalSuppressFHSS)dgvSuppressFHSS.DataContext).CollectionSuppressFHSS).ToList().FindIndex(x => x.TableSuppressFHSS.Id == id);

                if (index != -1)
                {
                    ((GlobalSuppressFHSS)dgvSuppressFHSS.DataContext).CollectionSuppressFHSS.RemoveAt(index);
                    ((GlobalSuppressFHSS)dgvSuppressFHSS.DataContext).CollectionSuppressFHSS.Insert(index, table);

                    AddEmptyRows();
                }
            }
            catch { }
        }

        /// <summary>
        /// Добавить пустые строки в таблицу
        /// </summary>
        private void AddEmptyRows()
        {
            int сountRowsAll = dgvSuppressFHSS.Items.Count; // количество всех строк в таблице
            double hs = 23; // высота строки
            double ah = dgvSuppressFHSS.ActualHeight; // визуализированная высота dataGrid
            double chh = dgvSuppressFHSS.ColumnHeaderHeight; // высота заголовка
            int countRows = Convert.ToInt32(Math.Floor(Convert.ToDouble(ah) - Convert.ToDouble(chh)) / hs); // количество строк, которое помещается в область видимости = (высота таблицы - заголовок таблицы) / высота строки

            int count = сountRowsAll - countRows; // сколько пустых строк нужно удалить
            int index = -1;
            for (int i = 0; i < count; i++)
            {
                // Удалить пустые строки в dgv
                index = ((GlobalSuppressFHSS)dgvSuppressFHSS.DataContext).CollectionSuppressFHSS.ToList().FindIndex(x => x.TableSuppressFHSS.Id < 0);
                if (index != -1)
                {
                    ((GlobalSuppressFHSS)dgvSuppressFHSS.DataContext).CollectionSuppressFHSS.RemoveAt(index);
                }
            }

            List<TableSuppressFHSSAll> list = new List<TableSuppressFHSSAll>();
            for (int i = 0; i < countRows - сountRowsAll; i++)
            {
                TableSuppressFHSSAll strSuppressFHSSAll = new TableSuppressFHSSAll
                {
                    TableSuppressFHSS = new TableSuppressFHSS
                    {
                        Id = -2,
                        FreqMinKHz = -2F,
                        FreqMaxKHz = -2F,
                        Letters = new byte[] {},
                        Threshold = 255,
                        EPO = new byte[] {},
                        StepKHz = -2,
                        InterferenceParam = new InterferenceParam
                        {

                        }
                    },
                    TempSuppressFHSS = new TempSuppressFHSS
                    {
                        Control = Led.Empty,
                        Suppress = Led.Empty,
                        Radiation = Led.Empty
                    }
                };

                list.Add(strSuppressFHSSAll);
            }

            for (int i = 0; i < list.Count; i++)
            {
                ((GlobalSuppressFHSS)dgvSuppressFHSS.DataContext).CollectionSuppressFHSS.Add(list[i]);
            }
        }

        /// <summary>
        /// Удалить пустые строки из таблицы
        /// </summary>
        private void DeleteEmptyRows()
        {
            int countEmptyRows = ((GlobalSuppressFHSS)dgvSuppressFHSS.DataContext).CollectionSuppressFHSS.Count(s => s.TableSuppressFHSS.Id < 0);
            int countAllRows = ((GlobalSuppressFHSS)dgvSuppressFHSS.DataContext).CollectionSuppressFHSS.Count;
            int iCount = countAllRows - countEmptyRows;
            for (int i = iCount; i < countAllRows; i++)
            {
                ((GlobalSuppressFHSS)dgvSuppressFHSS.DataContext).CollectionSuppressFHSS.RemoveAt(iCount);
            }
        }

        private bool IsSelectedRowEmpty()
        {
            try
            {
                if (((TableSuppressFHSSAll)dgvSuppressFHSS.SelectedItem).TableSuppressFHSS.Id == -2)
                    return false;
            }
            catch { }

            return true;
        }

        private void SetLanguageTables(Languages language)
        {
            ResourceDictionary dict = new ResourceDictionary();
            try
            {
                switch (language)
                {
                    case Languages.Eng:

                        dict.Source = new Uri("/ProjectTestTables;component/TranslatorTables.EN.xaml",
                                      UriKind.Relative);
                        break;
                    case Languages.Rus:
                        dict.Source = new Uri("/ProjectTestTables;component/TranslatorTables.RU.xaml",
                                            UriKind.Relative);
                        break;
                    default:
                        dict.Source = new Uri("/ProjectTestTables;component/TranslatorTables.RU.xaml",
                                          UriKind.Relative);
                        break;
                }

                this.Resources.MergedDictionaries.Add(dict);
            }
            catch (Exception)
            { }
        }

        /// <summary>
        /// Добавить таблицу для отчета
        /// </summary>
        /// <returns></returns>
        private string[,] AddSuppressFHSSToTable()
        {
            int Columns = 9;
            int Rows = (((GlobalSuppressFHSS)dgvSuppressFHSS.DataContext).CollectionSuppressFHSS.Count(x => x.TableSuppressFHSS.Id > 0)) + 1;
            string[,] Table = new string[Rows, Columns];

            try
            {
                Table[0, 0] = SHeaders.headerNumAJS;
                Table[0, 1] = SHeaders.headerFreqMin;
                Table[0, 2] = SHeaders.headerFreqMax;
                Table[0, 3] = SHeaders.headerCount;
                Table[0, 4] = SHeaders.headerStep;
                Table[0, 5] = SHeaders.headerLetters;
                Table[0, 6] = SHeaders.headerLevel;
                Table[0, 7] = SHeaders.headerJammerParams;
                Table[0, 8] = SHeaders.headerSSR;

                for (int i = 1; i < Rows; i++)
                {
                    Table[i, 0] = ((GlobalSuppressFHSS)dgvSuppressFHSS.DataContext).CollectionSuppressFHSS[i - 1].TableSuppressFHSS.NumberASP.ToString();
                    Table[i, 1] = ((GlobalSuppressFHSS)dgvSuppressFHSS.DataContext).CollectionSuppressFHSS[i - 1].TableSuppressFHSS.FreqMinKHz.ToString();
                    Table[i, 2] = ((GlobalSuppressFHSS)dgvSuppressFHSS.DataContext).CollectionSuppressFHSS[i - 1].TableSuppressFHSS.FreqMaxKHz.ToString();
                    Table[i, 3] = FreqCountConverter.FreqCount(Convert.ToInt32(((GlobalSuppressFHSS)dgvSuppressFHSS.DataContext).CollectionSuppressFHSS[i - 1].TableSuppressFHSS.FreqMinKHz),
                        Convert.ToInt32(((GlobalSuppressFHSS)dgvSuppressFHSS.DataContext).CollectionSuppressFHSS[i - 1].TableSuppressFHSS.FreqMaxKHz),
                        ((GlobalSuppressFHSS)dgvSuppressFHSS.DataContext).CollectionSuppressFHSS[i - 1].TableSuppressFHSS.StepKHz);
                    Table[i, 4] = ((GlobalSuppressFHSS)dgvSuppressFHSS.DataContext).CollectionSuppressFHSS[i - 1].TableSuppressFHSS.StepKHz.ToString();
                    Table[i, 5] = LettersFHSSConverter.LettersFHSS(((GlobalSuppressFHSS)dgvSuppressFHSS.DataContext).CollectionSuppressFHSS[i - 1].TableSuppressFHSS.Letters);
                    Table[i, 6] = ((GlobalSuppressFHSS)dgvSuppressFHSS.DataContext).CollectionSuppressFHSS[i - 1].TableSuppressFHSS.Threshold.ToString();
                    Table[i, 7] = ParamsNoiseConverter.ParamHindrance(((GlobalSuppressFHSS)dgvSuppressFHSS.DataContext).CollectionSuppressFHSS[i - 1].TableSuppressFHSS.InterferenceParam.Modulation,
                        ((GlobalSuppressFHSS)dgvSuppressFHSS.DataContext).CollectionSuppressFHSS[i - 1].TableSuppressFHSS.InterferenceParam.Deviation,
                        ((GlobalSuppressFHSS)dgvSuppressFHSS.DataContext).CollectionSuppressFHSS[i - 1].TableSuppressFHSS.InterferenceParam.Manipulation,
                        ((GlobalSuppressFHSS)dgvSuppressFHSS.DataContext).CollectionSuppressFHSS[i - 1].TableSuppressFHSS.InterferenceParam.Duration);
                    Table[i, 8] = EPOConverter.EPO(((GlobalSuppressFHSS)dgvSuppressFHSS.DataContext).CollectionSuppressFHSS[i - 1].TableSuppressFHSS.EPO);
                }
            }
            catch { }

            return Table;
        }

        public void ChangeFreqHeaderMHz_kHz()
        {
            switch (PropLocalFreqMHz_kHz.FreqMHz_kHz)
            {
                case 0: // kHz
                    lHeaderFreq.Content = SMeaning.meaningFkHz;
                    lHeaderFreqMin.Content = SMeaning.meaningFminkHz;
                    lHeaderFreqMax.Content = SMeaning.meaningFmaxkHz;
                    break;

                case 1: // MHz
                    lHeaderFreq.Content = SMeaning.meaningFMHz;
                    lHeaderFreqMin.Content = SMeaning.meaningFminMHz;
                    lHeaderFreqMax.Content = SMeaning.meaningFmaxMHz;
                    break;
            }
        }
    }
}
