﻿using ModelsTablesDBLib;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.WpfPropertyGrid;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TableEvents;
using TableOperations;
using ValuesCorrectLib;

namespace SuppressFHSSControl
{
    /// <summary>
    /// Логика взаимодействия для ExcludedFreqProperty.xaml
    /// </summary>
    public partial class ExcludedFreqProperty : Window
    {
        private ObservableCollection<TableFHSSExcludedFreq> collectionTemp;
        public TableFHSSExcludedFreq tableExcludedFreq { get; private set; }

        public ExcludedFreqProperty(ObservableCollection<TableFHSSExcludedFreq> collectionExcludedFreq)
        {
            try
            {
                InitializeComponent();

                collectionTemp = collectionExcludedFreq;
                tableExcludedFreq = new TableFHSSExcludedFreq();
                propertyGrid.SelectedObject = tableExcludedFreq;
                propertyGrid.ShowReadOnlyProperties = true;

                //Title = "Add record";
                Title = SMeaning.meaningAddRecord;
                Icon = new BitmapImage(new Uri(@"pack://application:,,,/"
                                    + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                                    + ";component/"
                                    + "Resources/AddRec.ico", UriKind.Absolute));

                InitProperty();
                //ChangeCategories();
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
        }

        public ExcludedFreqProperty()
        {
            try
            {
                InitializeComponent();

                InitProperty();
                //ChangeCategories();
            }
            catch { }
        }

        private void InitProperty()
        {
            foreach (var property in propertyGrid.Properties)
            {
                try
                {
                    if (property.IsBrowsable == false)
                        continue;
                    if (property.PropertyValue.SubProperties.Count != 0)
                    {
                        foreach (var subProperty in property.PropertyValue.SubProperties)
                            subProperty.PropertyValue.PropertyValueException += PropertyGridSubException;
                        continue;
                    }
                    property.PropertyValue.PropertyValueException += PropertyGridException;
                }
                catch (Exception ex)
                { MessageBox.Show(ex.Message); }
            }
        }

        private void PropertyGridSubException(object sender, ValueExceptionEventArgs e)
        {
            propertyGrid.Properties[(sender as PropertyItemValue).ParentProperty.ParentValue.ParentProperty.Name].SetValue((sender as PropertyItemValue).Value);
        }

        private void PropertyGridException(object sender, ValueExceptionEventArgs e)
        {
            propertyGrid.Properties[(sender as PropertyItemValue).ParentProperty.Name].SetValue((sender as PropertyItemValue).Value);
        }

        private void ChangeCategories()
        {
            propertyGrid.Categories.FirstOrDefault(t => t.Name == "ФРЧ").HeaderCategoryName = "FF";

            propertyGrid.Properties[nameof(TableFHSSExcludedFreq.FreqKHz)].DisplayName = "F, kHz";
            propertyGrid.Properties[nameof(TableFHSSExcludedFreq.Deviation)].DisplayName = "Δf, kHz";
        }

        public void SetLanguagePropertyGrid(Languages language)
        {
            LoadTranslatorPropertyGrid(language);
            TranslatorTables.ChangeLanguagePropertyGrid(language, propertyGrid);
            
            SetDisplayNameFreqs();
        }

        public void SetLanguagePropertyGrid(Languages language, ResourceDictionary dict)
        {
            LoadTranslatorPropertyGrid(dict);
            TranslatorTables.ChangeLanguagePropertyGrid(language, propertyGrid);

            SetDisplayNameFreqs();
        }

        private void LoadTranslatorPropertyGrid(ResourceDictionary dict)
        {
            try
            {
                this.Resources.MergedDictionaries.Add(dict);
            }
            catch (Exception)
            { }
        }

        private void SetDisplayNameFreqs()
        {
            switch (PropLocalFreqMHz_kHz.FreqMHz_kHz)
            {
                case 0: // kHz
                    propertyGrid.Properties[nameof(TableFHSSExcludedFreq.FreqKHz)].DisplayName = SMeaning.meaningFkHz;
                    break;

                case 1: // MHz
                    propertyGrid.Properties[nameof(TableFHSSExcludedFreq.FreqKHz)].DisplayName = SMeaning.meaningFMHz;
                    break;

                default:
                    break;
            }
        }

        private void LoadTranslatorPropertyGrid(Languages language)
        {
            ResourceDictionary dict = new ResourceDictionary();
            try
            {
                switch (language)
                {
                    case Languages.Eng:
                        dict.Source = new Uri("/GrozaFuture;component/Languages/TranslatorTables/TranslatorTables.EN.xaml",
                                      UriKind.Relative);
                        break;
                    case Languages.Rus:
                        dict.Source = new Uri("/GrozaFuture;component/Languages/TranslatorTables/TranslatorTables.RU.xaml",
                                            UriKind.Relative);
                        break;
                    default:
                        dict.Source = new Uri("/GrozaFuture;component/Languages/TranslatorTables/TranslatorTables.RU.xaml",
                                          UriKind.Relative);
                        break;
                }

                // Отладка ----------------------------------------------------------------------------------
                //switch (language)
                //{
                //    case Languages.Eng:

                //        dict.Source = new Uri("/ProjectTestTables;component/TranslatorTables.EN.xaml",
                //                      UriKind.Relative);
                //        break;
                //    case Languages.Rus:
                //        dict.Source = new Uri("/ProjectTestTables;component/TranslatorTables.RU.xaml",
                //                            UriKind.Relative);
                //        break;
                //    default:
                //        dict.Source = new Uri("/ProjectTestTables;component/TranslatorTables.RU.xaml",
                //                          UriKind.Relative);
                //        break;
                //}
                // ---------------------------------------------------------------------------------- Отладка

                this.Resources.MergedDictionaries.Add(dict);
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
        }

        private void ButtonApply_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void ButtonNoApply_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void gridProperty_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Enter)
            {
                DialogResult = true;
            }
        }
    }
}
