﻿using ModelsTablesDBLib;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using TableEvents;
using TableOperations;
using ValuesCorrectLib;
using System.Linq;

namespace SuppressFHSSControl
{
    /// <summary>
    /// Логика взаимодействия для UserControlSuppressFHSS.xaml
    /// </summary>
    public partial class UserControlSuppressFHSS : UserControl
    {
        #region Events
        public event EventHandler<TableEvent> OnAddRecord = (object srnder, TableEvent data) => { };
        public event EventHandler<TableEvent> OnChangeRecord = (object srnder, TableEvent data) => { };
        public event EventHandler<TableEvent> OnDeleteRecord = (object srnder, TableEvent data) => { };
        public event EventHandler<NameTable> OnClearRecords = (object srnder, NameTable data) => { };
        // Открылось окно с PropertyGrid
        public event EventHandler<SuppressFHSSProperty> OnIsWindowPropertyOpen = (object sender, SuppressFHSSProperty data) => { };
        public event EventHandler<ExcludedFreqProperty> OnIsWindowPropertyOpenExc = (object sender, ExcludedFreqProperty data) => { };

        public event EventHandler<TableEventReport> OnAddTableToReport = (object sender, TableEventReport data) => { };
        #endregion

        #region Properties
        public NameTable NameTable { get; } = NameTable.TableSuppressFHSS;
        public NameTable NameTableFreq { get; } = NameTable.TableFHSSExcludedFreq;
        #endregion

        public UserControlSuppressFHSS()
        {
            InitializeComponent();

            dgvSuppressFHSS.DataContext = new GlobalSuppressFHSS();

            dgvExcludedFreq.DataContext = new GlobalFHSSExcludedFreq();
        }

        private void ButtonAdd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (PropNumberASP.SelectedNumASP != 0 && PropNumberASP.IsSelectedRowASP)
                {
                    var SuppressFHSSWindow = new SuppressFHSSProperty(((GlobalSuppressFHSS)dgvSuppressFHSS.DataContext).CollectionSuppressFHSS);
                    SuppressFHSSWindow.tableSuppressFHSS.NumberASP = PropNumberASP.SelectedNumASP;

                    ComboBoxEditor.IndComboBoxModulation = 4;
                    ComboBoxEditor.IndComboBoxManipulation = 8;
                    ComboBoxEditor.IndComboBoxDeviation = 0;
                    ComboBoxEditor.IndComboBoxDuration = 0;
                    ComboBoxEditor.ValueDecUpDownDeviation = 10;
                    ComboBoxEditor.ValueDecUpDownManipulation = 10;

                    OnIsWindowPropertyOpen(this, SuppressFHSSWindow);

                    if (SuppressFHSSWindow.ShowDialog() == true)
                    {
                        // Событие добавления одной записи
                        OnAddRecord(this, new TableEvent(SuppressFHSSWindow.tableSuppressFHSS));
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void ButtonChange_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (((TableSuppressFHSSAll)dgvSuppressFHSS.SelectedItem) != null)
                {
                    if (((TableSuppressFHSSAll)dgvSuppressFHSS.SelectedItem).TableSuppressFHSS.Id > 0)
                    {
                        var selected = ((TableSuppressFHSSAll)dgvSuppressFHSS.SelectedItem).TableSuppressFHSS;

                        var SuppressFHSSWindow = new SuppressFHSSProperty(((GlobalSuppressFHSS)dgvSuppressFHSS.DataContext).CollectionSuppressFHSS, selected.Clone());

                        OnIsWindowPropertyOpen(this, SuppressFHSSWindow);

                        if (SuppressFHSSWindow.ShowDialog() == true)
                        {
                            TableSuppressFHSS supprReplace = new TableSuppressFHSS();

                            supprReplace = SuppressFHSSWindow.tableSuppressFHSS;

                            // Проверка возможности изменения записи ------------------------------------------------------------------------------------
                            List<TableSuppressFHSS> lSuppressFHSS = SuppressFHSSAllToSuppressFHSS(((GlobalSuppressFHSS)dgvSuppressFHSS.DataContext).CollectionSuppressFHSS.ToList());
                                   
                            int ind = lSuppressFHSS.FindIndex(x => x.Id == supprReplace.Id);
                            if(ind != -1)
                            {
                                lSuppressFHSS.RemoveAt(ind);

                                if (!CorrectSuppress.IsCheckCountChannel(lSuppressFHSS, supprReplace))
                                {
                                    MessageBox.Show(SMessages.mesCountChannelsMax, SMessages.mesMessage, MessageBoxButton.OK, MessageBoxImage.Exclamation);
                                    return;
                                }

                                // Событие изменения одной записи
                                OnChangeRecord(this, new TableEvent(supprReplace));
                            }
                        }
                    }
                }
            }
            catch { }
        }

        private void ButtonDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!IsSelectedRowEmpty())
                    return;

                TableSuppressFHSS tableSuppressFHSS = new TableSuppressFHSS
                {
                    Id = ((TableSuppressFHSSAll)dgvSuppressFHSS.SelectedItem).TableSuppressFHSS.Id,
                    NumberASP = ((TableSuppressFHSSAll)dgvSuppressFHSS.SelectedItem).TableSuppressFHSS.NumberASP
                };

                // Событие удаления одной записи
                OnDeleteRecord(this, new TableEvent(tableSuppressFHSS));
            }
            catch { }
        }

        private void ButtonClear_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // Событие удаления всех записей
                OnClearRecords(this, NameTable);
            }
            catch { }
        }

        private void ButtonWord_Click(object sender, RoutedEventArgs e)
        {
            string[,] Table = AddSuppressFHSSToTable();

            OnAddTableToReport(this, new TableEventReport(Table, NameTable.TableSuppressFHSS));
        }

        private void ButtonPrint_Click(object sender, RoutedEventArgs e)
        {

        }

        private void dgvSuppressFHSS_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            AddEmptyRows();
        }
        
        private void dgvFHSSExcludedFreq_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            AddEmptyRowsExcludedFreq();
        }

        private void bAddFreq_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (((TableSuppressFHSSAll)dgvSuppressFHSS.SelectedItem) != null)
                {
                    if (((TableSuppressFHSSAll)dgvSuppressFHSS.SelectedItem).TableSuppressFHSS.Id > 0)
                    {
                        var ExcludedFreqWindow = new ExcludedFreqProperty(((GlobalFHSSExcludedFreq)dgvExcludedFreq.DataContext).CollectionFHSSExcludedFreq);

                        OnIsWindowPropertyOpenExc(this, ExcludedFreqWindow);

                        if (ExcludedFreqWindow.ShowDialog() == true)
                        {
                            if (PropLocalFreqMHz_kHz.FreqMHz_kHz == 1)
                            {
                                ExcludedFreqWindow.tableExcludedFreq.FreqKHz = ExcludedFreqWindow.tableExcludedFreq.FreqKHz * 1000;
                            }

                            if (CorrectAddRecord.IsCorrectFreqRanges(ExcludedFreqWindow.tableExcludedFreq.FreqKHz))
                            {
                                if (CorrectAddRecord.IsCorrectDeviation(ExcludedFreqWindow.tableExcludedFreq.Deviation))
                                {
                                    if (CorrectAddRecord.IsCorrectRangeForExcludedFreq(ExcludedFreqWindow.tableExcludedFreq.FreqKHz,
                                        ((TableSuppressFHSSAll)dgvSuppressFHSS.SelectedItem).TableSuppressFHSS.FreqMinKHz,
                                        ((TableSuppressFHSSAll)dgvSuppressFHSS.SelectedItem).TableSuppressFHSS.FreqMaxKHz))
                                    {
                                        ExcludedFreqWindow.tableExcludedFreq.IdFHSS = ((TableSuppressFHSSAll)dgvSuppressFHSS.SelectedItem).TableSuppressFHSS.Id;

                                        // Событие добавления одной записи
                                        OnAddRecord(this, new TableEvent(ExcludedFreqWindow.tableExcludedFreq));
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void bClearFreqs_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // Событие удаления всех записей
                OnClearRecords(this, NameTableFreq);
            }
            catch { }
        }

        private void dgvSuppressFHSS_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if ((TableSuppressFHSSAll)dgvSuppressFHSS.SelectedItem == null) { return; }

            PropSelectedIdFHSS.SelectedIdSupprFHSS = ((TableSuppressFHSSAll)dgvSuppressFHSS.SelectedItem).TableSuppressFHSS.Id;
            UpdateTableExcludedFreqs(PropSelectedIdFHSS.SelectedIdSupprFHSS);
        }
    }
}
