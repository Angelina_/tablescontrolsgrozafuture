﻿using ModelsTablesDBLib;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls.WpfPropertyGrid;
using System.Windows.Media.Imaging;
using TableEvents;
using TableOperations;
using ValuesCorrectLib;

namespace SuppressFHSSControl
{
    /// <summary>
    /// Логика взаимодействия для SuppressFHSSProperty.xaml
    /// </summary>
    public partial class SuppressFHSSProperty : Window
    {
        private ObservableCollection<TableSuppressFHSSAll> collectionTemp;
        public TableSuppressFHSS tableSuppressFHSS { get; private set; }

        public SuppressFHSSProperty(ObservableCollection<TableSuppressFHSSAll> collectionSuppressFHSS)
        {
            try
            {
                InitializeComponent();

                InitEditors();

                collectionTemp = collectionSuppressFHSS;
                tableSuppressFHSS = new TableSuppressFHSS();
                tableSuppressFHSS.StepKHz = 100;
                tableSuppressFHSS.Threshold = -100;
                propertyGrid.SelectedObject = tableSuppressFHSS;
                propertyGrid.ShowReadOnlyProperties = true;

                //Title = "Add record";
                Title = SMeaning.meaningAddRecord;
                Icon = new BitmapImage(new Uri(@"pack://application:,,,/"
                                    + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                                    + ";component/"
                                    + "Resources/AddRec.ico", UriKind.Absolute));

                InitProperty();
                propertyGrid.Categories.FirstOrDefault(t => t.Name == "InterferenceParam").HeaderCategoryName = SMeaning.meaningInterferenceParam;

                //ChangeCategories();
            }
            catch { }
        }

        public SuppressFHSSProperty(ObservableCollection<TableSuppressFHSSAll> collectionSuppressFHSS, TableSuppressFHSS SuppressFHSS)
        {
            try
            {
                InitializeComponent();

                InitEditors();

                collectionTemp = collectionSuppressFHSS;
                tableSuppressFHSS = SuppressFHSS;

                switch (PropLocalFreqMHz_kHz.FreqMHz_kHz)
                {
                    case 0: // kHz
                        tableSuppressFHSS.FreqMinKHz = Math.Round(tableSuppressFHSS.FreqMinKHz, 1);
                        tableSuppressFHSS.FreqMaxKHz = Math.Round(tableSuppressFHSS.FreqMaxKHz, 1);
                        break;

                    case 1: // MHz
                        tableSuppressFHSS.FreqMinKHz = Math.Round(tableSuppressFHSS.FreqMinKHz / 1000, 1);
                        tableSuppressFHSS.FreqMaxKHz = Math.Round(tableSuppressFHSS.FreqMaxKHz / 1000, 1);
                        break;

                    default:
                        tableSuppressFHSS.FreqMinKHz = Math.Round(tableSuppressFHSS.FreqMinKHz, 1);
                        tableSuppressFHSS.FreqMaxKHz = Math.Round(tableSuppressFHSS.FreqMaxKHz, 1);
                        break;
                }

                propertyGrid.SelectedObject = tableSuppressFHSS;
                propertyGrid.ShowReadOnlyProperties = false;

                //Title = "Change record";
                Title = SMeaning.meaningChangeRecord;
                Icon = new BitmapImage(new Uri(@"pack://application:,,,/"
                                    + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                                    + ";component/"
                                    + "Resources/ChangeRec.ico", UriKind.Absolute));

                propertyGrid.Categories.FirstOrDefault(t => t.Name == "InterferenceParam").HeaderCategoryName = SMeaning.meaningInterferenceParam;
                //ChangeCategories();
                InitProperty();

                PropChangeJammerParams.IsChangeJammerParams = true;
                CorrectJammerParams.PropertyGridJammerParams(tableSuppressFHSS.InterferenceParam);
            }
            catch { }
        }


        public SuppressFHSSProperty()
        {
            try
            {
                InitializeComponent();

                InitEditors();
                propertyGrid.Categories.FirstOrDefault(t => t.Name == "InterferenceParam").HeaderCategoryName = SMeaning.meaningInterferenceParam;
                //ChangeCategories();
                InitProperty();
            }
            catch { }
        }

        private void ButtonApply_Click(object sender, RoutedEventArgs e)
        {
            if (IsAddClick((TableSuppressFHSS)propertyGrid.SelectedObject) != null)
            {
                DialogResult = true;
            }
        }

        public double FreqMHz_kHz(double Freq)
        {
            return PropLocalFreqMHz_kHz.FreqMHz_kHz == 1 ? Freq * 1000 : Freq;
        }

        public TableSuppressFHSS IsAddClick(TableSuppressFHSS SuppressFHSSWindow)
        {
            SuppressFHSSWindow.Threshold = SuppressFHSSWindow.Threshold > 0 ? (SuppressFHSSWindow.Threshold * -1) : SuppressFHSSWindow.Threshold;

            if (CorrectMinMax.IsCorrectFreqMHz_kHz(SuppressFHSSWindow))
            {
                CorrectMinMax.IsCorrectMinMax(SuppressFHSSWindow, FreqMHz_kHz(SuppressFHSSWindow.FreqMinKHz), FreqMHz_kHz(SuppressFHSSWindow.FreqMaxKHz));
                if (CorrectMinMax.IsCorrectRangeMinMax(FreqMHz_kHz(SuppressFHSSWindow.FreqMinKHz), FreqMHz_kHz(SuppressFHSSWindow.FreqMaxKHz)))
                {
                    SuppressFHSSWindow.Threshold = CorrectAddRecord.IsCorrectThreshold((short)SuppressFHSSWindow.Threshold);

                    if (CorrectAddRecord.IsCorrectStep(SuppressFHSSWindow.StepKHz))
                    {
                        if (CorrectParamsNoise.GetParamsNoise(SuppressFHSSWindow.InterferenceParam, 0, 0))
                        {
                            SuppressFHSSWindow.Letters = DefinitionParams.DefineLetters(FreqMHz_kHz(SuppressFHSSWindow.FreqMinKHz), FreqMHz_kHz(SuppressFHSSWindow.FreqMaxKHz));
                            SuppressFHSSWindow.EPO = DefinitionParams.DefineEPO(FreqMHz_kHz(SuppressFHSSWindow.FreqMinKHz), FreqMHz_kHz(SuppressFHSSWindow.FreqMaxKHz));

                            SuppressFHSSWindow.FreqMinKHz = FreqMHz_kHz(SuppressFHSSWindow.FreqMinKHz);
                            SuppressFHSSWindow.FreqMaxKHz = FreqMHz_kHz(SuppressFHSSWindow.FreqMaxKHz);

                            PropSelectedIdFHSS.SelectedIdSupprFHSS = SuppressFHSSWindow.Id;

                            return SuppressFHSSWindow;
                        }
                    }
                }
            }
            return null;
        }

        private void ButtonNoApply_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void InitProperty()
        {
            foreach (var property in propertyGrid.Properties)
            {
                try
                {
                    if (property.IsBrowsable == false)
                        continue;
                    if (property.PropertyValue.SubProperties.Count != 0)
                    {
                        foreach (var subProperty in property.PropertyValue.SubProperties)
                            subProperty.PropertyValue.PropertyValueException += PropertyGridSubException;
                        continue;
                    }
                    property.PropertyValue.PropertyValueException += PropertyGridException;
                }
                catch (Exception ex)
                { MessageBox.Show(ex.Message); }
            }
        }

        private void PropertyGridSubException(object sender, ValueExceptionEventArgs e)
        {
            propertyGrid.Properties[(sender as PropertyItemValue).ParentProperty.ParentValue.ParentProperty.Name].SetValue((sender as PropertyItemValue).Value);
        }

        private void PropertyGridException(object sender, ValueExceptionEventArgs e)
        {
            propertyGrid.Properties[(sender as PropertyItemValue).ParentProperty.Name].SetValue((sender as PropertyItemValue).Value);
        }

        private void ChangeCategories()
        {
            //propertyGrid.Categories.FirstOrDefault(t => t.Name == "InterferenceParam").HeaderCategoryName = "Параметры помехи";
            //propertyGrid.Categories.FirstOrDefault(t => t.Name == "Прочее").HeaderCategoryName = "Общие";
            
            propertyGrid.Categories.FirstOrDefault(t => t.Name == "InterferenceParam").HeaderCategoryName = "Jammer parameters";
            propertyGrid.Categories.FirstOrDefault(t => t.Name == "Прочее").HeaderCategoryName = "General";

            propertyGrid.Properties[nameof(TableSuppressFHSS.FreqMinKHz)].DisplayName = "F min, kHz";
            propertyGrid.Properties[nameof(TableSuppressFHSS.FreqMaxKHz)].DisplayName = "F max, kHz";
            propertyGrid.Properties[nameof(TableSuppressFHSS.Threshold)].DisplayName = "U, dB";
            propertyGrid.Properties[nameof(TableSuppressFHSS.StepKHz)].DisplayName = "Net step, kHz";
        }

        public void SetLanguagePropertyGrid(Languages language)
        {
            LoadTranslatorPropertyGrid(language);
            TranslatorTables.ChangeLanguagePropertyGrid(language, propertyGrid);
            
            SetDisplayNameFreqs();
        }

        public void SetLanguagePropertyGrid(Languages language, ResourceDictionary dict)
        {
            LoadTranslatorPropertyGrid(dict);
            TranslatorTables.ChangeLanguagePropertyGrid(language, propertyGrid);

            SetDisplayNameFreqs();
        }

        private void LoadTranslatorPropertyGrid(ResourceDictionary dict)
        {
            try
            {
                this.Resources.MergedDictionaries.Add(dict);
            }
            catch (Exception)
            { }
        }

        private void SetDisplayNameFreqs()
        {
            switch (PropLocalFreqMHz_kHz.FreqMHz_kHz)
            {
                case 0: // kHz
                    propertyGrid.Properties[nameof(TableSuppressFHSS.FreqMinKHz)].DisplayName = SMeaning.meaningFminkHz;
                    propertyGrid.Properties[nameof(TableSuppressFHSS.FreqMaxKHz)].DisplayName = SMeaning.meaningFmaxkHz;
                    break;

                case 1: // MHz
                    propertyGrid.Properties[nameof(TableSuppressFHSS.FreqMinKHz)].DisplayName = SMeaning.meaningFminMHz;
                    propertyGrid.Properties[nameof(TableSuppressFHSS.FreqMaxKHz)].DisplayName = SMeaning.meaningFmaxMHz;
                    break;

                default:
                    break;
            }
        }

        private void LoadTranslatorPropertyGrid(Languages language)
        {
            ResourceDictionary dict = new ResourceDictionary();
            try
            {
                switch (language)
                {
                    case Languages.Eng:
                        dict.Source = new Uri("/GrozaFuture;component/Languages/TranslatorTables/TranslatorTables.EN.xaml",
                                      UriKind.Relative);
                        break;
                    case Languages.Rus:
                        dict.Source = new Uri("/GrozaFuture;component/Languages/TranslatorTables/TranslatorTables.RU.xaml",
                                            UriKind.Relative);
                        break;
                    case Languages.Azr:
                        dict.Source = new Uri("/GrozaFuture;component/Languages/TranslatorTables/TranslatorTables.AZ.xaml",
                                            UriKind.Relative);
                        break;
                    default:
                        dict.Source = new Uri("/GrozaFuture;component/Languages/TranslatorTables/TranslatorTables.RU.xaml",
                                          UriKind.Relative);
                        break;
                }

                // Отладка ----------------------------------------------------------------------------------
                //switch (language)
                //{
                //    case Languages.Eng:

                //        dict.Source = new Uri("/ProjectTestTables;component/TranslatorTables.EN.xaml",
                //                      UriKind.Relative);
                //        break;
                //    case Languages.Rus:
                //        dict.Source = new Uri("/ProjectTestTables;component/TranslatorTables.RU.xaml",
                //                            UriKind.Relative);
                //        break;
                //    default:
                //        dict.Source = new Uri("/ProjectTestTables;component/TranslatorTables.RU.xaml",
                //                          UriKind.Relative);
                //        break;
                //}
                // ---------------------------------------------------------------------------------- Отладка

                this.Resources.MergedDictionaries.Add(dict);
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
        }

        private void InitEditors()
        {
            propertyGrid.Editors.Add(new GeneralFreqMinEditor(nameof(tableSuppressFHSS.FreqMinKHz), typeof(TableSuppressFHSS)));
            propertyGrid.Editors.Add(new GeneralFreqMaxEditor(nameof(tableSuppressFHSS.FreqMaxKHz), typeof(TableSuppressFHSS)));
            propertyGrid.Editors.Add(new GeneralThresholdFHSSEditor(nameof(tableSuppressFHSS.Threshold), typeof(TableSuppressFHSS)));
            propertyGrid.Editors.Add(new GeneralStepEditor(nameof(tableSuppressFHSS.StepKHz), typeof(TableSuppressFHSS)));

            //propertyGrid.Editors.Add(new InterferenceParamFHSSEditor(nameof(InterferenceParam), typeof(TableSuppressFHSS)));
            propertyGrid.Editors.Add(new InterferenceParamFHSSEditor(nameof(tableSuppressFHSS.InterferenceParam), typeof(TableSuppressFHSS)));
        }

        private void gridProperty_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Enter)
            {
                if (IsAddClick((TableSuppressFHSS)propertyGrid.SelectedObject) != null)
                {
                    DialogResult = true;
                }
            }
        }
    }
}
