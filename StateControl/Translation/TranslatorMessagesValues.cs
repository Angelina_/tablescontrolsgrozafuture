﻿using System.Collections.Generic;

namespace StateNAVControl
{
    public class FunctionsTranslate
    {
        /// <summary>
        /// Переименование значений при смене языка
        /// </summary>
        /// <param name="TranslateDic"></param>
        public static void RenameMeaning(Dictionary<string, string> TranslateDic)
        {
            if (TranslateDic.ContainsKey("meaningNAV"))
                SMeaning.meaningNAV = TranslateDic["meaningNAV"];

            if (TranslateDic.ContainsKey("meaningSPUF"))
                SMeaning.meaningSPUF = TranslateDic["meaningSPUF"];
        }

        /// <summary>
        /// Переименование заголовкок при смене языка
        /// </summary>
        /// <param name="TranslateDic"></param>
        public static void RenameHeader(Dictionary<string, string> TranslateDic)
        {
            if (TranslateDic.ContainsKey("HeaderType"))
                SHeader.HeaderType = TranslateDic["HeaderType"];

            if (TranslateDic.ContainsKey("HeaderTemperature"))
                SHeader.HeaderTemperature = TranslateDic["HeaderTemperature"];

            if (TranslateDic.ContainsKey("HeaderAmperage"))
                SHeader.HeaderAmperage = TranslateDic["HeaderAmperage"];

            if (TranslateDic.ContainsKey("HeaderSnt"))
                SHeader.HeaderSnt = TranslateDic["HeaderSnt"];

            if (TranslateDic.ContainsKey("HeaderRad"))
                SHeader.HeaderRad = TranslateDic["HeaderRad"];

            if (TranslateDic.ContainsKey("HeaderPow"))
                SHeader.HeaderPow = TranslateDic["HeaderPow"];
        }
    }
}
