﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Xml;

namespace StateNAVControl
{
    public partial class UserControlStateNAV : UserControl
    {
        static Dictionary<string, string> TranslateDic;
        private void LoadDictionary(Language language)
        {
            XmlDocument xDoc = new XmlDocument();

            var translation = Properties.Resources.TranslatorStateNAV;
            xDoc.LoadXml(translation);

            TranslateDic = new Dictionary<string, string>();

            // получим корневой элемент
            XmlElement xRoot = xDoc.DocumentElement;
            foreach (XmlNode x2Node in xRoot.ChildNodes)
            {
                if (x2Node.NodeType == XmlNodeType.Comment)
                    continue;

                // получаем атрибут ID
                if (x2Node.Attributes.Count > 0)
                {
                    XmlNode attr = x2Node.Attributes.GetNamedItem("ID");
                    if (attr != null)
                    {
                        foreach (XmlNode childnode in x2Node.ChildNodes)
                        {
                            // если узел - language
                            if (childnode.Name == language.ToString())
                            {
                                if (!TranslateDic.ContainsKey(attr.Value))
                                    TranslateDic.Add(attr.Value, childnode.InnerText);
                            }
                        }
                    }
                }
            }

            if (TranslateDic.Count > 0 && TranslateDic != null)
            {
                FunctionsTranslate.RenameMeaning(TranslateDic);
                FunctionsTranslate.RenameHeader(TranslateDic);
            }
        }

        private void LoadTranslatorTable(Language language)
        {
            ResourceDictionary dict = new ResourceDictionary();
            try
            {
                switch (language)
                {
                    case StateNAVControl.Language.RU:
                        dict.Source = new Uri("/StateNAVControl;component/Translation/TranslatorStateNAV.RU.xaml",
                                      UriKind.Relative);
                        break;
                    case StateNAVControl.Language.EN:
                        dict.Source = new Uri("/StateNAVControl;component/Translation/TranslatorStateNAV.EN.xaml",
                                           UriKind.Relative);
                        break;
                    case StateNAVControl.Language.AZ:
                        dict.Source = new Uri("/StateNAVControl;component/Translation/TranslatorStateNAV.AZ.xaml",
                                           UriKind.Relative);
                        break;
                    default:
                        dict.Source = new Uri("/StateNAVControl;component/Translation/TranslatorStateNAV.RU.xaml",
                                          UriKind.Relative);
                        break;
                }

                this.Resources.MergedDictionaries.Add(dict);
            }
            catch (Exception ex)
            { }
        }

        public void SetTranslation(Language language)
        {
            LoadDictionary(language);
            LoadTranslatorTable(language);
        }
    }
}
