﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StateNAVControl
{
    #region Значения
    public struct SMeaning
    {
        public static string meaningNAV;
        public static string meaningSPUF;

        public static void InitSMeaning()
        {
            meaningNAV = "Навигация";
            meaningSPUF = "Спуфинг";
        }
    }
    #endregion

    #region Заголовок
    public struct SHeader
    {
        public static string HeaderType;
        public static string HeaderTemperature;
        public static string HeaderAmperage;
        public static string HeaderSnt;
        public static string HeaderRad;
        public static string HeaderPow;

        public static void InitSHeader()
        {
            HeaderType = "Тип";
            HeaderTemperature = "T, °C";
            HeaderAmperage = "I, A";
            HeaderSnt = "Синт.";
            HeaderRad = "Изл.";
            HeaderPow = "Мощ.";
        }
    }
    #endregion
}
