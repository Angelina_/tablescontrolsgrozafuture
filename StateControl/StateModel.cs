﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StateNAVControl
{
    public class StateModel
    {
        public int Id { get; set; }
        public Type Type { get; set; }
        public short Temperature { get; set; }
        public float Amperage { get; set; }
        public Led Snt { get; set; }
        public Led Rad { get; set; }
        public Led Pow { get; set; }
    }
}
