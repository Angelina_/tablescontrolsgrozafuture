﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace StateNAVControl
{
    public partial class UserControlStateNAV : UserControl
    {
        private List<StateModel> listStateModel = new List<StateModel> { };
        public List<StateModel> ListStateModel
        {
            get { return listStateModel; }
            set
            {
                if (listStateModel != null && listStateModel.Equals(value)) return;
                listStateModel = value;
                UpdateState();
            }
        }

        /// <summary>
        /// Обновить контрол
        /// </summary>
        private void UpdateState()
        {
            try
            {
                if (ListStateModel == null)
                    return;

                ((GlobalState)DgvState.DataContext).CollectionState.Clear();

                if (ListStateModel.Count == 0)
                {
                    AddEmptyRows();
                    return;
                }

                for (int i = 0; i < ListStateModel.Count; i++)
                {
                    ((GlobalState)DgvState.DataContext).CollectionState.Add(ListStateModel[i]);
                }
            }
            catch { }
        }


        /// <summary>
        /// Добавить в таблицу пустые строки
        /// </summary>
        private void AddEmptyRows()
        {
            try
            {
                int сountRowsAll = DgvState.Items.Count; // количество всех строк в таблице
                double hs = 23; // высота строки
                double ah = DgvState.ActualHeight; // визуализированная высота dataGrid
                double chh = DgvState.ColumnHeaderHeight; // высота заголовка

                int countRows = Convert.ToInt32(Math.Floor(Convert.ToDouble(ah) - Convert.ToDouble(chh)) / hs); // количество строк, которое помещается в область видимости = (высота таблицы - заголовок таблицы) / высота строки

                int count = сountRowsAll - countRows; // сколько пустых строк нужно удалить
                int index = -1;
                for (int i = 0; i < count; i++)
                {
                    // Удалить пустые строки в dgv
                    index = ((GlobalState)DgvState.DataContext).CollectionState.ToList().FindIndex(x => x.Id < 0);
                    if (index != -1)
                    {
                        ((GlobalState)DgvState.DataContext).CollectionState.RemoveAt(index);
                    }
                }

                List<StateModel> list = new List<StateModel>()
                {
                    new StateModel
                    {
                        Id = -2,
                        Type = Type.NAV,
                        Temperature = -102,
                        Amperage = -2F,
                        Snt = Led.Empty,
                        Rad = Led.Empty,
                        Pow = Led.Empty
                    },
                    new StateModel
                    {
                        Id = -2,
                        Type = Type.SPUF,
                        Temperature = -102,
                        Amperage = -2F,
                        Snt = Led.Empty,
                        Rad = Led.Empty,
                        Pow = Led.Empty
                    }
                };

                for (int i = 0; i < list.Count; i++)
                {
                    ((GlobalState)DgvState.DataContext).CollectionState.Add(list[i]);
                }
            }
            catch { }
        }
    }
}
