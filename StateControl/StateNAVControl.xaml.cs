﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace StateNAVControl
{
    /// <summary>
    /// Логика взаимодействия для UserControl1.xaml
    /// </summary>
    public partial class UserControlStateNAV : UserControl
    {
        public UserControlStateNAV()
        {
            InitializeComponent();

            DgvState.DataContext = new GlobalState();
        }

        private void DgvState_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            AddEmptyRows();
        }

        private void DgvState_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DgvState.UnselectAllCells();
        }
    }
}
