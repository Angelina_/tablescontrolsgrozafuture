﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace StateNAVControl
{
    [ValueConversion(sourceType: typeof(byte), targetType: typeof(string))]
    public class TypeConverter : IValueConverter
    {
        public object Convert(object value, System.Type targetType, object parameter, CultureInfo culture)
        {
            string sType = string.Empty;

            //try
            //{

            if ((byte)value == 255)
                return sType;

            switch ((byte)value)
            {
                case 0:
                    sType = SMeaning.meaningNAV;// "Навигация";
                    break;

                case 1:
                    sType = SMeaning.meaningSPUF;// "Спуфинг";
                    break;

                default:
                    sType = string.Empty;
                    break;
            }

            //}
            //    catch { }

            return sType;
        }

        public object ConvertBack(object value, System.Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
