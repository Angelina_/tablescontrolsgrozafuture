﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace StateNAVControl
{
    public class TemperatureConverter : IValueConverter
    {
        public object Convert(object value, System.Type targetType, object parameter, CultureInfo culture)
        {
            //try
            //{
            if (System.Convert.ToInt16(value) == -101)
            {
                return "-";
            }
            if (System.Convert.ToInt16(value) == -102)
            {
                return string.Empty;
            }
            //}
            //catch { }

            return System.Convert.ToString(value);
        }

        public object ConvertBack(object value, System.Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
