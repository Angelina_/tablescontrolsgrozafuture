﻿namespace StateNAVControl
{
    public enum Type : byte
    {
        NAV = 0,
        SPUF = 1,
        Empty = 2
    }
    
    public enum Led : byte
    {
        Gray = 0,
        Green = 1,
        Red = 2,
        Empty = 3
    }

    public enum Language : byte
    {
        RU = 0,
        EN = 1,
        AZ = 2
    }

}
