﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace StateNAVControl
{
    public class GlobalState : INotifyPropertyChanged
    {
        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }
        #endregion

        public ObservableCollection<StateModel> CollectionState { get; set; }

        public GlobalState()
        {
            CollectionState = new ObservableCollection<StateModel> { };

            #region Test
            //CollectionState = new ObservableCollection<StateModel>
            //{
            //    new StateModel
            //    {
            //        Type = Type.NAV,
            //        Temperature = 23,
            //        Amperage = 12.5F,
            //        Snt = Led.Gray,
            //        Rad = Led.Green,
            //        Pow = Led.Red
            //    },
            //    new StateModel
            //    {
            //        Type = Type.SPUF,
            //        Temperature = 45,
            //        Amperage = 67.8F,
            //        Snt = Led.Green,
            //        Rad = Led.Red,
            //        Pow = Led.Red
            //    }
            //};
            #endregion
        }
    }
}
