﻿using ModelsTablesDBLib;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using TableEvents;
using TableOperations;
using ValuesCorrectLib;

namespace ReconFWSControl
{
    /// <summary>
    /// Логика взаимодействия для UserControl1.xaml
    /// </summary>
    public partial class UserControlReconFWS : UserControl, ITableReconEvent
    {
        #region Events
        public event EventHandler<TableEvent> OnDeleteRecord = (object sender, TableEvent data) => { };
        public event EventHandler<TableEvent> OnChangeRecord = (object sender, TableEvent data) => { };
        public event EventHandler<NameTable> OnClearRecords = (object sender, NameTable data) => { };
        // Добавить запись в таблицу ИРИ ФРЧ РП
        public event EventHandler<TableSuppressFWS> OnAddFWS_RS = (object sender, TableSuppressFWS data) => { };
        // Отправить частоту на КРПУ1
        public event EventHandler<TempFWS> OnSendFreqCRRD = (object sender, TempFWS data) => { };
        // Отправить частоту на КРПУ2
        public event EventHandler<TempFWS> OnSendFreqCRRD2 = (object sender, TempFWS data) => { };
        // Отправить запрос на исполнительное пеленгование
        public event EventHandler<TableReconFWS> OnGetExecBear = (object sender, TableReconFWS data) => { };
        // Отправить запрос на квазиодновременное пеленгование
        public event EventHandler<TableReconFWS> OnGetKvBear = (object sender, TableReconFWS data) => { };

        public event EventHandler<TableEvent> OnAddRecord = (object srnder, TableEvent data) => { };

        // Нажата кнопка Целераспределение
        public event EventHandler OnClickTDistribution;

        public event EventHandler<TableReconFWS> OnSelectedASPSuppr = (object srnder, TableReconFWS data) => { };

        // Нажата кнопка на отправление ИРИ ЦР на РП
        public event EventHandler<List<TableSuppressFWS>> OnSendFWS_TD_RS = (object sender, List<TableSuppressFWS> data) => { };

        // Нажата кнопка Радиосети
        public event EventHandler<bool> OnClickRS = (object sender, bool b) => { };
        // Нажата кнопка Узлы связи
        public event EventHandler<bool> OnClickUS = (object sender, bool b) => { };

        public event EventHandler<TableEventReport> OnAddTableToReport = (object sender, TableEventReport data) => { };
        #endregion

        #region Properties
        public NameTable NameTable { get; } = NameTable.TableReconFWS;
        #endregion

        public UserControlReconFWS()
        {
            InitializeComponent();

            dgvReconFWS.DataContext = new GlobalReconFWS();
            
        }

        private void ButtonDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if ((TableReconFWSAll)dgvReconFWS.SelectedItem != null)
                {
                    if (!IsSelectedRowEmpty())
                        return;

                    TableReconFWS tableReconFWS = new TableReconFWS
                    {
                        Id = ((TableReconFWSAll)dgvReconFWS.SelectedItem).TableReconFWSRow.Id,
                        Coordinates = new Coord { },
                        ListJamDirect = new ObservableCollection<TableJamDirect> { }
                    };

                    // Событие удаления одной записи
                    OnDeleteRecord(this, new TableEvent(tableReconFWS));
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        private void ButtonClear_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // Событие удаления всех записей
                OnClearRecords(this, NameTable);
            }
            catch { }
            //catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        private void ButtonAddFWS_RS_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if ((TableReconFWSAll)dgvReconFWS.SelectedItem != null)
                {
                    if (!IsSelectedRowEmpty())
                        return;

                    InterferenceParam interferenceParam = new InterferenceParam();
                    if (((TableReconFWSAll)dgvReconFWS.SelectedItem).TableReconFWSRow.Sender == SignSender.Cicada)
                    {
                        interferenceParam = CorrectReconFWS.GetParamsForModulationKondor(((TableReconFWSAll)dgvReconFWS.SelectedItem).TableReconFWSRow.Modulation);
                    }
                    else
                    {
                        interferenceParam.Manipulation = 0;
                        interferenceParam.Modulation = 1;
                        interferenceParam.Deviation = 8;
                        interferenceParam.Duration = 4;
                    }

                    TableSuppressFWS tableSuppressFWS = new TableSuppressFWS
                    {
                        Id = 0,
                        Sender = SignSender.OtherTable,
                        NumberASP = ((TableReconFWSAll)dgvReconFWS.SelectedItem).TableReconFWSRow.SelectedJamDirect.JamDirect.NumberASP,
                        FreqKHz = ((TableReconFWSAll)dgvReconFWS.SelectedItem).TableReconFWSRow.FreqKHz,
                        Bearing = ((TableReconFWSAll)dgvReconFWS.SelectedItem).TableReconFWSRow.SelectedJamDirect.JamDirect.Bearing,
                        Letter = DefinitionParams.DefineLetter(((TableReconFWSAll)dgvReconFWS.SelectedItem).TableReconFWSRow.FreqKHz),
                        Threshold = (short)(((TableReconFWSAll)dgvReconFWS.SelectedItem).TableReconFWSRow.SelectedJamDirect.JamDirect.Level - 10),
                        Priority = 2,
                        Coordinates = new Coord
                        {
                            Latitude = ((TableReconFWSAll)dgvReconFWS.SelectedItem).TableReconFWSRow.Coordinates.Latitude,
                            Longitude = ((TableReconFWSAll)dgvReconFWS.SelectedItem).TableReconFWSRow.Coordinates.Longitude,
                            Altitude = ((TableReconFWSAll)dgvReconFWS.SelectedItem).TableReconFWSRow.Coordinates.Altitude
                        },
                        InterferenceParam = interferenceParam
                        //InterferenceParam = new InterferenceParam
                        //{
                        //    Manipulation = 0,
                        //    Modulation = 1,
                        //    Deviation = 8,
                        //    Duration = 4
                        //},
                    };

                    if (tableSuppressFWS.Threshold < ConstValues.constLevel) { tableSuppressFWS.Threshold = ConstValues.constLevel; }

                    // Событие Отправить ИРИ на РП
                    OnAddFWS_RS(this, tableSuppressFWS);
                }
            }
            catch { }
        }

        private void ButtonAddToWord_Click(object sender, RoutedEventArgs e)
        {
            string[,] Table = AddReconFWSToTable();

            OnAddTableToReport(this, new TableEventReport(Table, NameTable.TableReconFWS));
        }

        private void ButtonPrint_Click(object sender, RoutedEventArgs e)
        {

        }

        private void ComboBoxASPRecon_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int dgvRowIndex = dgvReconFWS.SelectedIndex;

            try
            {
                if (dgvRowIndex != -1)
                {
                    ComboBox cellComboBox = sender as ComboBox;
                    if (cellComboBox.SelectedValue != null)
                    {
                        string strNumberASP = Convert.ToString(cellComboBox.SelectedValue);
                        if (strNumberASP.Substring(0, 1) == "*")
                        {
                            strNumberASP = strNumberASP.Remove(0, 1);
                        }

                        int iNumberASP = Convert.ToInt32(strNumberASP);

                        ((TableReconFWSAll)dgvReconFWS.SelectedItem).TableReconFWSRow.SelectedASP = iNumberASP;
                        ((TableReconFWSAll)dgvReconFWS.SelectedItem).TableReconFWSRow.SelectedJamDirect = ((TableReconFWSAll)dgvReconFWS.SelectedItem).TableReconFWSRow.ListJamDirect.FirstOrDefault(rec => rec.JamDirect.NumberASP == iNumberASP);
                    }
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        private void ComboBoxASPSuppr_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int dgvRowIndex = dgvReconFWS.SelectedIndex;

            try
            {
                if (dgvRowIndex != -1)
                {
                    ComboBox cellComboBox = sender as ComboBox;
                    
                    if (cellComboBox.SelectedValue != null)
                    {
                        string strNumberASP = Convert.ToString(cellComboBox.SelectedValue);
                        ((TableReconFWSAll)dgvReconFWS.SelectedItem).TableNumASP.SelectedASPRP = strNumberASP;
                       
                        int iNumberASP = -2;

                        if (strNumberASP != string.Empty)
                        {
                            if (strNumberASP.Substring(0, 1) == "*") { strNumberASP = strNumberASP.Remove(0, 1);  }
                            iNumberASP = Convert.ToInt32(strNumberASP);
                        }
                    }
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        private void dgvReconFWS_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            AddEmptyRows();

            UpdateRS_USAfterScroll();
        }

        private void ButtonAddFWS_CRRD_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if ((TableReconFWSAll)dgvReconFWS.SelectedItem != null)
                {
                    if (!IsSelectedRowEmpty())
                        return;

                    TempFWS tempFWS = new TempFWS
                    {
                        FreqKHz = ((TableReconFWSAll)dgvReconFWS.SelectedItem).TableReconFWSRow.FreqKHz
                    };

                    // Событие На КРПУ
                    OnSendFreqCRRD(this, tempFWS);
                }
            }
            catch { }
        }

        private void ButtonAddFWS_CRRD2_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if ((TableReconFWSAll)dgvReconFWS.SelectedItem != null)
                {
                    if (!IsSelectedRowEmpty())
                        return;

                    TempFWS tempFWS = new TempFWS
                    {
                        FreqKHz = ((TableReconFWSAll)dgvReconFWS.SelectedItem).TableReconFWSRow.FreqKHz
                    };

                    // Событие На КРПУ
                    OnSendFreqCRRD2(this, tempFWS);
                }
            }
            catch { }
        }

        private void ButtonGetExecBear_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if ((TableReconFWSAll)dgvReconFWS.SelectedItem != null)
                {
                    if (!IsSelectedRowEmpty())
                        return;

                    ObservableCollection<TableJamDirect> listJamDirect = new ObservableCollection<TableJamDirect>();
                    for (int i = 0; i < ((TableReconFWSAll)dgvReconFWS.SelectedItem).TableReconFWSRow.ListJamDirect.Count; i++)
                    {
                        TableJamDirect tableJamDirect = new TableJamDirect
                        {
                            ID = ((TableReconFWSAll)dgvReconFWS.SelectedItem).TableReconFWSRow.ListJamDirect[i].ID,
                            JamDirect = new JamDirect
                            {

                                NumberASP = ((TableReconFWSAll)dgvReconFWS.SelectedItem).TableReconFWSRow.ListJamDirect[i].JamDirect.NumberASP,
                                Bearing = ((TableReconFWSAll)dgvReconFWS.SelectedItem).TableReconFWSRow.ListJamDirect[i].JamDirect.Bearing,
                                Level = ((TableReconFWSAll)dgvReconFWS.SelectedItem).TableReconFWSRow.ListJamDirect[i].JamDirect.Level,
                                Std = ((TableReconFWSAll)dgvReconFWS.SelectedItem).TableReconFWSRow.ListJamDirect[i].JamDirect.Std,
                                DistanceKM = ((TableReconFWSAll)dgvReconFWS.SelectedItem).TableReconFWSRow.ListJamDirect[i].JamDirect.DistanceKM,
                                IsOwn = ((TableReconFWSAll)dgvReconFWS.SelectedItem).TableReconFWSRow.ListJamDirect[i].JamDirect.IsOwn
                            }
                        };

                        listJamDirect.Add(tableJamDirect);
                    }

                    TableReconFWS tableReconFWS = new TableReconFWS
                    {
                        Id = ((TableReconFWSAll)dgvReconFWS.SelectedItem).TableReconFWSRow.Id,
                        FreqKHz = ((TableReconFWSAll)dgvReconFWS.SelectedItem).TableReconFWSRow.FreqKHz,
                        Time = DateTime.Now.ToLocalTime(),
                        Deviation = ((TableReconFWSAll)dgvReconFWS.SelectedItem).TableReconFWSRow.Deviation,
                        Coordinates = new Coord
                        {
                            Latitude = ((TableReconFWSAll)dgvReconFWS.SelectedItem).TableReconFWSRow.Coordinates.Latitude,
                            Longitude = ((TableReconFWSAll)dgvReconFWS.SelectedItem).TableReconFWSRow.Coordinates.Longitude,
                            Altitude = ((TableReconFWSAll)dgvReconFWS.SelectedItem).TableReconFWSRow.Coordinates.Altitude
                        },
                        ListJamDirect = listJamDirect
                    };

                    // Событие Запрос на исполнительное пеленгование
                    OnGetExecBear(this, tableReconFWS);
                }
            }
            catch { }
        }

        private void dgvReconFWS_SelectedCellsChanged(object sender, SelectedCellsChangedEventArgs e)
        {
            try
            {
                if (((TableReconFWSAll)dgvReconFWS.SelectedItem) != null)
                {
                    PropNumberASP.SelectedASPforListQ = ((TableReconFWSAll)dgvReconFWS.SelectedItem).TableReconFWSRow.SelectedASP;
                    ((TableReconFWSAll)dgvReconFWS.SelectedItem).TableReconFWSRow.SelectedJamDirect = ((TableReconFWSAll)dgvReconFWS.SelectedItem).TableReconFWSRow.ListJamDirect.FirstOrDefault(rec => rec.JamDirect.NumberASP == PropNumberASP.SelectedASPforListQ);

                    if (((TableReconFWSAll)dgvReconFWS.SelectedItem).TableNumASP.SelectedASPRP == string.Empty)
                    {
                        if (((TableReconFWSAll)dgvReconFWS.SelectedItem).TableNumASP.NumberASP != null)
                        {
                            if (((TableReconFWSAll)dgvReconFWS.SelectedItem).TableNumASP.NumberASP.Count > 0)
                            {
                                string strNumberASP = ((TableReconFWSAll)dgvReconFWS.SelectedItem).TableNumASP.NumberASP[0];
                                ((TableReconFWSAll)dgvReconFWS.SelectedItem).TableNumASP.SelectedASPRP = strNumberASP;
                                
                            }
                        }
                    }

                    //if (((TableReconFWSAll)dgvReconFWS.SelectedItem).TableNumASP.SelectedASPRP == -2)
                    //{
                    //    if (((TableReconFWSAll)dgvReconFWS.SelectedItem).TableNumASP.NumberASP != null)
                    //    {
                    //        if (((TableReconFWSAll)dgvReconFWS.SelectedItem).TableNumASP.NumberASP.Count > 0)
                    //        {
                    //            string str = ((TableReconFWSAll)dgvReconFWS.SelectedItem).TableNumASP.NumberASP[0];
                    //            int iNumberASP = -2;

                    //            if (str != " ")
                    //            {
                    //                if (str.Substring(0, 1) == "*")
                    //                {
                    //                    str = str.Remove(0, 1);
                    //                }

                    //                iNumberASP = Convert.ToInt32(str);
                    //            }

                    //            ((TableReconFWSAll)dgvReconFWS.SelectedItem).TableNumASP.SelectedASPRP = iNumberASP;
                    //        }
                    //    }
                    //}
                }
            }
            catch { }
        }

        /// <summary>
        /// Целераспределение
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonFWS_TD_Click(object sender, RoutedEventArgs e)
        {
            OnClickTDistribution(this, null);
        }

        private void ButtonFWS_TD_RS_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if(((GlobalReconFWS)dgvReconFWS.DataContext).CollectionReconFWS.Count > 0)
                {
                    if(((GlobalReconFWS)dgvReconFWS.DataContext).CollectionReconFWS[0].TableReconFWSRow.Id != -2)
                    {
                        List<TableSuppressFWS> listSuppressFWS = new List<TableSuppressFWS>();

                        for(int i = 0; i < ((GlobalReconFWS)dgvReconFWS.DataContext).CollectionReconFWS.Count; i++)
                        {
                            if (((GlobalReconFWS)dgvReconFWS.DataContext).CollectionReconFWS[i].TableReconFWSRow.Id != -2)
                            {
                                //if (((GlobalReconFWS)dgvReconFWS.DataContext).CollectionReconFWS[i].TableNumASP.NumberASP[0] != " ")
                                if (((GlobalReconFWS)dgvReconFWS.DataContext).CollectionReconFWS[i].TableNumASP.SelectedASPRP != string.Empty)
                                {
                                    //int NumASP = int.Parse(string.Join(string.Empty, (((GlobalReconFWS)dgvReconFWS.DataContext).CollectionReconFWS[i].TableNumASP.NumberASP[0]).Where(c => char.IsDigit(c))));
                                    int NumASP = int.Parse(string.Join(string.Empty, (((GlobalReconFWS)dgvReconFWS.DataContext).CollectionReconFWS[i].TableNumASP.SelectedASPRP).Where(c => char.IsDigit(c))));
                                    if (NumASP > 0)
                                        if (NumASP > 0)
                                        {
                                        TableSuppressFWS tableSuppressFWS = new TableSuppressFWS
                                        {
                                            Id = ((GlobalReconFWS)dgvReconFWS.DataContext).CollectionReconFWS[i].TableReconFWSRow.Id,
                                            Sender = SignSender.OtherTable,
                                            NumberASP = NumASP,
                                            FreqKHz = ((GlobalReconFWS)dgvReconFWS.DataContext).CollectionReconFWS[i].TableReconFWSRow.FreqKHz,
                                            Bearing = ((GlobalReconFWS)dgvReconFWS.DataContext).CollectionReconFWS[i].TableReconFWSRow.SelectedJamDirect.JamDirect.Bearing,
                                            Letter = DefinitionParams.DefineLetter(((GlobalReconFWS)dgvReconFWS.DataContext).CollectionReconFWS[i].TableReconFWSRow.FreqKHz),
                                            Threshold = (short)(((GlobalReconFWS)dgvReconFWS.DataContext).CollectionReconFWS[i].TableReconFWSRow.SelectedJamDirect.JamDirect.Level - 10),
                                            Priority = 2,
                                            Coordinates = new Coord
                                            {
                                                Latitude = ((GlobalReconFWS)dgvReconFWS.DataContext).CollectionReconFWS[i].TableReconFWSRow.Coordinates.Latitude,
                                                Longitude = ((GlobalReconFWS)dgvReconFWS.DataContext).CollectionReconFWS[i].TableReconFWSRow.Coordinates.Longitude,
                                                Altitude = ((GlobalReconFWS)dgvReconFWS.DataContext).CollectionReconFWS[i].TableReconFWSRow.Coordinates.Altitude
                                            },
                                            InterferenceParam = new InterferenceParam
                                            {
                                                Manipulation = 0,
                                                Modulation = 1,
                                                Deviation = 8,
                                                Duration = 4
                                            }
                                        };
                                        if (tableSuppressFWS.Threshold < ConstValues.constLevel) { tableSuppressFWS.Threshold = ConstValues.constLevel; }

                                        listSuppressFWS.Add(tableSuppressFWS);
                                    }
                                }
                            }
                        }

                        OnSendFWS_TD_RS(this, listSuppressFWS);
                    }
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }
      
        private void ButtonGetKvBear_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if ((TableReconFWSAll)dgvReconFWS.SelectedItem != null)
                {
                    if (!IsSelectedRowEmpty())
                        return;

                    ObservableCollection<TableJamDirect> listJamDirect = new ObservableCollection<TableJamDirect>();
                    for (int i = 0; i < ((TableReconFWSAll)dgvReconFWS.SelectedItem).TableReconFWSRow.ListJamDirect.Count; i++)
                    {
                        TableJamDirect tableJamDirect = new TableJamDirect
                        {
                            ID = ((TableReconFWSAll)dgvReconFWS.SelectedItem).TableReconFWSRow.ListJamDirect[i].ID,
                            JamDirect = new JamDirect
                            {

                                NumberASP = ((TableReconFWSAll)dgvReconFWS.SelectedItem).TableReconFWSRow.ListJamDirect[i].JamDirect.NumberASP,
                                Bearing = ((TableReconFWSAll)dgvReconFWS.SelectedItem).TableReconFWSRow.ListJamDirect[i].JamDirect.Bearing,
                                Level = ((TableReconFWSAll)dgvReconFWS.SelectedItem).TableReconFWSRow.ListJamDirect[i].JamDirect.Level,
                                Std = ((TableReconFWSAll)dgvReconFWS.SelectedItem).TableReconFWSRow.ListJamDirect[i].JamDirect.Std,
                                DistanceKM = ((TableReconFWSAll)dgvReconFWS.SelectedItem).TableReconFWSRow.ListJamDirect[i].JamDirect.DistanceKM,
                                IsOwn = ((TableReconFWSAll)dgvReconFWS.SelectedItem).TableReconFWSRow.ListJamDirect[i].JamDirect.IsOwn
                            }
                        };

                        listJamDirect.Add(tableJamDirect);
                    }

                    TableReconFWS tableReconFWS = new TableReconFWS
                    {
                        Id = ((TableReconFWSAll)dgvReconFWS.SelectedItem).TableReconFWSRow.Id,
                        FreqKHz = ((TableReconFWSAll)dgvReconFWS.SelectedItem).TableReconFWSRow.FreqKHz,
                        Time = DateTime.Now.ToLocalTime(),
                        Deviation = ((TableReconFWSAll)dgvReconFWS.SelectedItem).TableReconFWSRow.Deviation,
                        Coordinates = new Coord
                        {
                            Latitude = ((TableReconFWSAll)dgvReconFWS.SelectedItem).TableReconFWSRow.Coordinates.Latitude,
                            Longitude = ((TableReconFWSAll)dgvReconFWS.SelectedItem).TableReconFWSRow.Coordinates.Longitude,
                            Altitude = ((TableReconFWSAll)dgvReconFWS.SelectedItem).TableReconFWSRow.Coordinates.Altitude
                        },
                        ListJamDirect = listJamDirect
                    };

                    // Событие Запрос на исполнительное пеленгование
                    OnGetKvBear(this, tableReconFWS);
                }
            }
            catch { }
        }

        private void bRS_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                bRS.IsChecked = true;
                PropRS_US.CheckRS = true;
                OnClickRS(this, PropRS_US.CheckRS);
            }
            catch { }
        }

        private void bRS_Unchecked(object sender, RoutedEventArgs e)
        {
            try
            {
                bRS.IsChecked = false;
                PropRS_US.CheckRS = false;
                OnClickRS(this, PropRS_US.CheckRS);
            }
            catch { }
        }

        private void bUS_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                bUS.IsChecked = true;
                PropRS_US.CheckUS = true;
                OnClickUS(this, PropRS_US.CheckUS);
            }
            catch { }
        }

        private void bUS_Unchecked(object sender, RoutedEventArgs e)
        {
            try
            {
                bUS.IsChecked = false;
                PropRS_US.CheckUS = false;
                OnClickUS(this, PropRS_US.CheckUS);
            }
            catch { }
        }

        private void dgvReconFWS_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            UpdateRS_USAfterScroll();
        }

        private void grid_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == System.Windows.Input.Key.Space)
                    ButtonAddFWS_CRRD_Click(this, null);

                if (e.Key == System.Windows.Input.Key.Q)
                    ButtonGetKvBear_Click(this, null);

                if (e.Key == System.Windows.Input.Key.E)
                    ButtonGetExecBear_Click(this, null);

                if (e.Key == System.Windows.Input.Key.R)
                    ButtonAddFWS_RS_Click(this, null);
            }
            catch { }
        }

       
    }
}
