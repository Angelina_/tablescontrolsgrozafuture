﻿using Formation_RN_RD_CC;
using ModelsTablesDBLib;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;
using TableEvents;
using ValuesCorrectLib;

namespace ReconFWSControl
{
    public partial class UserControlReconFWS : UserControl
    {
        public List<SRNet> listRS = new List<SRNet>();
        public List<SRNet> listUS = new List<SRNet>();


        public List<TableReconFWS> TempListReconFWS { get; set; }
        /// <summary>
        /// Обновить записи в контроле
        /// </summary>
        public void UpdateReconFWS(List<TableReconFWS> listReconFWS)
        {
            try
            {
                TempListReconFWS = listReconFWS;

                ((GlobalReconFWS)dgvReconFWS.DataContext).CollectionReconFWS.Clear();
                
                for (int i = 0; i < listReconFWS.Count; i++)
                {
                    TableReconFWSAll table = ReconFWSToReconFWSAll(listReconFWS[i]);
                    ((GlobalReconFWS)dgvReconFWS.DataContext).CollectionReconFWS.Add(table);

                    ///////////////////////////////
                    //((GlobalReconFWS)dgvReconFWS.DataContext).CollectionReconFWS[i].TableNumASP.SelectedASPRP = table.TableReconFWSRow.ASPSuppr.ToString();
                    /////////////////////////////////
                }

                AddEmptyRows();
            }
            catch { }
           
        }

        /// <summary>
        /// Добавить записи в контрол
        /// </summary>
        public void AddReconFWSs(List<TableReconFWS> listReconFWS)
        {
            try
            {
                DeleteEmptyRows();

                for (int i = 0; i < listReconFWS.Count; i++)
                {
                    int ind = ((GlobalReconFWS)dgvReconFWS.DataContext).CollectionReconFWS.ToList().FindIndex(x => x.TableReconFWSRow.Id == listReconFWS[i].Id);
                    if (ind != -1)
                    {
                        var newRec = ReconFWSToReconFWSAll(listReconFWS[i]);

                        newRec.TableReconFWSRow.SelectedASP = ((GlobalReconFWS)dgvReconFWS.DataContext).CollectionReconFWS[ind].TableReconFWSRow.SelectedASP;
                        ((GlobalReconFWS)dgvReconFWS.DataContext).CollectionReconFWS[ind].TableReconFWSRow = newRec.TableReconFWSRow;

                    }
                    else
                    {
                        TableReconFWSAll table = ReconFWSToReconFWSAll(listReconFWS[i]);
                        ((GlobalReconFWS)dgvReconFWS.DataContext).CollectionReconFWS.Add(table);
                    }
                }

                AddEmptyRows();
            }
            catch { }
            //catch(Exception ex) { MessageBox.Show(ex.Message); }
        }

        /// <summary>
        /// Преобразование List<TableReconFWS> к List<TableReconFWSAll>
        /// </summary>
        /// <param name="listReconFWS"></param>
        /// <returns> List<TableReconFWSAll> </returns>
        private List<TableReconFWSAll> ReconFWSToReconFWSAll(List<TableReconFWS> listReconFWS)
        {
            List<TableReconFWSAll> list = new List<TableReconFWSAll>();
            for (int i = 0; i < listReconFWS.Count; i++)
            {
                ObservableCollection<TableJamDirect> listJamDirect = new ObservableCollection<TableJamDirect>();
                for (int j = 0; j < listReconFWS[i].ListJamDirect.Count; j++)
                {
                    TableJamDirect tableJamDirect = new TableJamDirect
                    {
                        ID = listReconFWS[i].ListJamDirect[i].ID,
                        JamDirect = new JamDirect
                        {

                            NumberASP = listReconFWS[i].ListJamDirect[i].JamDirect.NumberASP,
                            Bearing = listReconFWS[i].ListJamDirect[i].JamDirect.Bearing,
                            Level = listReconFWS[i].ListJamDirect[i].JamDirect.Level,
                            Std = listReconFWS[i].ListJamDirect[i].JamDirect.Std,
                            DistanceKM = listReconFWS[i].ListJamDirect[i].JamDirect.DistanceKM,
                            IsOwn = listReconFWS[i].ListJamDirect[i].JamDirect.IsOwn
                        }
                    };

                    listJamDirect.Add(tableJamDirect);
                }

                TableReconFWSAll table = new TableReconFWSAll();
                table.TableReconFWSRow.Id = listReconFWS[i].Id;
                table.TableReconFWSRow.IdMission = listReconFWS[i].IdMission;
                table.TableReconFWSRow.FreqKHz = listReconFWS[i].FreqKHz;
                table.TableReconFWSRow.Deviation = listReconFWS[i].Deviation;
                table.TableReconFWSRow.Type = listReconFWS[i].Type;
                table.TableReconFWSRow.ASPSuppr = listReconFWS[i].ASPSuppr;
                table.TableReconFWSRow.Modulation = listReconFWS[i].Modulation;
                table.TableReconFWSRow.Time = listReconFWS[i].Time;
                table.TableReconFWSRow.Coordinates = new Coord
                {
                    Latitude = listReconFWS[i].Coordinates.Latitude,
                    Longitude = listReconFWS[i].Coordinates.Longitude,
                    Altitude = listReconFWS[i].Coordinates.Altitude
                };
                table.TableReconFWSRow.ListJamDirect = listJamDirect;

                list.Add(table);
            }

            return list;
        }

        /// <summary>
        /// Преобразование TableReconFWS к TableReconFWSAll
        /// </summary>
        /// <param name="tableReconFWS"></param>
        /// <returns> TableReconFWSAll </returns>
        private TableReconFWSAll ReconFWSToReconFWSAll(TableReconFWS tableReconFWS)
        {
            
            ObservableCollection<TableJamDirect> listJamDirect = new ObservableCollection<TableJamDirect>();
            for (int i = 0; i < tableReconFWS.ListJamDirect.Count; i++)
            {
                TableJamDirect tableJamDirect = new TableJamDirect
                {
                    ID = tableReconFWS.ListJamDirect[i].ID,
                    JamDirect = new JamDirect
                    {

                        NumberASP = tableReconFWS.ListJamDirect[i].JamDirect.NumberASP,
                        Bearing = tableReconFWS.ListJamDirect[i].JamDirect.Bearing,
                        Level = tableReconFWS.ListJamDirect[i].JamDirect.Level,
                        Std = tableReconFWS.ListJamDirect[i].JamDirect.Std,
                        DistanceKM = tableReconFWS.ListJamDirect[i].JamDirect.DistanceKM,
                        IsOwn = tableReconFWS.ListJamDirect[i].JamDirect.IsOwn
                    }
                };

                listJamDirect.Add(tableJamDirect);
            }

            TableReconFWSAll table = new TableReconFWSAll();
            table.TableReconFWSRow.Id = tableReconFWS.Id;
            table.TableReconFWSRow.IdMission = tableReconFWS.IdMission;
            table.TableReconFWSRow.Sender = tableReconFWS.Sender;
            table.TableReconFWSRow.FreqKHz = tableReconFWS.FreqKHz;
            table.TableReconFWSRow.Deviation = tableReconFWS.Deviation;
            table.TableReconFWSRow.Type = tableReconFWS.Type;
            table.TableReconFWSRow.ASPSuppr = tableReconFWS.ASPSuppr;
            table.TableReconFWSRow.Modulation = tableReconFWS.Modulation;
            table.TableReconFWSRow.Time = tableReconFWS.Time;
            table.TableReconFWSRow.Coordinates = new Coord
            {
                Latitude = tableReconFWS.Coordinates.Latitude,
                Longitude = tableReconFWS.Coordinates.Longitude,
                Altitude = tableReconFWS.Coordinates.Altitude
            };
            table.TableReconFWSRow.ListJamDirect = listJamDirect;


            return table;
        }

        /// <summary>
        /// Преобразование TableReconFWSAll к TableReconFWS
        /// </summary>
        /// <param name="tableReconFWS"></param>
        /// <returns> TableReconFWSAll </returns>
        private TableReconFWS ReconFWSAllToReconFWS(TableReconFWSAll tableReconFWSAll)
        {

            ObservableCollection<TableJamDirect> listJamDirect = new ObservableCollection<TableJamDirect>();
            for (int i = 0; i < tableReconFWSAll.TableReconFWSRow.ListJamDirect.Count; i++)
            {
                TableJamDirect tableJamDirect = new TableJamDirect
                {
                    ID = tableReconFWSAll.TableReconFWSRow.ListJamDirect[i].ID,
                    JamDirect = new JamDirect
                    {

                        NumberASP = tableReconFWSAll.TableReconFWSRow.ListJamDirect[i].JamDirect.NumberASP,
                        Bearing = tableReconFWSAll.TableReconFWSRow.ListJamDirect[i].JamDirect.Bearing,
                        Level = tableReconFWSAll.TableReconFWSRow.ListJamDirect[i].JamDirect.Level,
                        Std = tableReconFWSAll.TableReconFWSRow.ListJamDirect[i].JamDirect.Std,
                        DistanceKM = tableReconFWSAll.TableReconFWSRow.ListJamDirect[i].JamDirect.DistanceKM,
                        IsOwn = tableReconFWSAll.TableReconFWSRow.ListJamDirect[i].JamDirect.IsOwn
                    }
                };

                listJamDirect.Add(tableJamDirect);
            }

            TableReconFWS table = new TableReconFWS();
            table.Id = tableReconFWSAll.TableReconFWSRow.Id;
            table.IdMission = tableReconFWSAll.TableReconFWSRow.IdMission;
            table.Sender = tableReconFWSAll.TableReconFWSRow.Sender;
            table.FreqKHz = tableReconFWSAll.TableReconFWSRow.FreqKHz;
            table.Deviation = tableReconFWSAll.TableReconFWSRow.Deviation;
            table.Type = tableReconFWSAll.TableReconFWSRow.Type;
            table.ASPSuppr = tableReconFWSAll.TableReconFWSRow.ASPSuppr;
            table.Time = tableReconFWSAll.TableReconFWSRow.Time;
            table.Coordinates = new Coord
            {
                Latitude = tableReconFWSAll.TableReconFWSRow.Coordinates.Latitude,
                Longitude = tableReconFWSAll.TableReconFWSRow.Coordinates.Longitude,
                Altitude = tableReconFWSAll.TableReconFWSRow.Coordinates.Altitude
            };
            table.ListJamDirect = listJamDirect;


            return table;
        }

        /// <summary>
        /// Обновить колонку SupprASP в таблице
        /// </summary>
        /// <param name="lASPRP"></param>
        public void UpdateASPRP(List<TableASP> tableASP, List<TableReconFWS> tableReconFWS)
        {
            dgvReconFWS.UnselectAllCells();

            DeleteEmptyRows();

            if (tableReconFWS.Count > 0 && ((GlobalReconFWS)dgvReconFWS.DataContext).CollectionReconFWS.Count == tableReconFWS.Count)
            {
                for (int i = 0; i<tableReconFWS.Count; i++)
                {
                    string strSelectedASPRP = string.Empty;
                    ObservableCollection<string> list = new ObservableCollection<string>();
                    list.Add(string.Empty);

                    for (int j = 0; j<tableASP.Count; j++)
                    {
                        if (tableASP[j].ISOwn)
                        { 
                            list.Add("*" + (tableASP[j].Id).ToString());

                            if (((GlobalReconFWS)dgvReconFWS.DataContext).CollectionReconFWS[i].TableReconFWSRow.ASPSuppr == tableASP[j].Id)
                            {
                                strSelectedASPRP = "*" + (tableASP[j].Id).ToString();
                            } 
                        }
                        else 
                        { 
                            list.Add(tableASP[j].Id.ToString());

                            if (((GlobalReconFWS)dgvReconFWS.DataContext).CollectionReconFWS[i].TableReconFWSRow.ASPSuppr == tableASP[j].Id)
                            {
                                strSelectedASPRP = (tableASP[j].Id).ToString();
                            }
                        }
                    }
                    ((GlobalReconFWS)dgvReconFWS.DataContext).CollectionReconFWS[i].TableNumASP.NumberASP = list;
                    ((GlobalReconFWS)dgvReconFWS.DataContext).CollectionReconFWS[i].TableNumASP.SelectedASPRP = strSelectedASPRP;


                }
            }
            AddEmptyRows();
        }
       
        /// <summary>
        /// Добавить одну запись в контрол
        /// </summary>
        /// <param name="suppressFWS"></param>
        public void AddReconFWS(TableReconFWS reconFWS)
        {
            try
            {
                DeleteEmptyRows();

                ((GlobalReconFWS)dgvReconFWS.DataContext).CollectionReconFWS.Add(ReconFWSToReconFWSAll(reconFWS));

                AddEmptyRows();
            }
            catch { }

        }

        /// <summary>
        /// Удалить одну запись из контрола
        /// </summary>
        public void DeleteReconFWS(TableReconFWS tableReconFWS)
        {
            try
            {
                int index = ((GlobalReconFWS)dgvReconFWS.DataContext).CollectionReconFWS.ToList().FindIndex(x => x.TableReconFWSRow.Id == tableReconFWS.Id);
                ((GlobalReconFWS)dgvReconFWS.DataContext).CollectionReconFWS.RemoveAt(index);
            }
            catch { }
        }

        /// <summary>
        /// Удалить все записи из контрола
        /// </summary>
        public void ClearReconFWS()
        {
            try
            {
                ((GlobalReconFWS)dgvReconFWS.DataContext).CollectionReconFWS.Clear();

                AddEmptyRows();
            }
            catch { }
        }

        /// <summary>
        /// Добавить / удалить пустую строку в dgvTempFWSCurRow или dgvTempFWS
        /// </summary>
        private void AddEmptyRows()
        {
            try
            {
                int сountRowsAll = dgvReconFWS.Items.Count; // количество всех строк в таблице
                double hs = 23; // высота строки
                double ah = dgvReconFWS.ActualHeight; // визуализированная высота dataGrid
                double chh = dgvReconFWS.ColumnHeaderHeight; // высота заголовка
                int countRows = -1;
                int index = -1;
                int count = -1;

                List<TableReconFWSAll> list = new List<TableReconFWSAll>();

                countRows = Convert.ToInt32(Math.Floor(Convert.ToDouble(ah) - Convert.ToDouble(chh)) / hs); // количество строк, которое помещается в область видимости = (высота таблицы - заголовок таблицы) / высота строки
                count = сountRowsAll - countRows; // сколько пустых строк нужно удалить

                for (int i = 0; i < count; i++)
                {
                    // Удалить пустые строки в dgv
                    index = ((GlobalReconFWS)dgvReconFWS.DataContext).CollectionReconFWS.ToList().FindIndex(x => x.TableReconFWSRow.Id < 0);
                    if (index != -1)
                    {
                        ((GlobalReconFWS)dgvReconFWS.DataContext).CollectionReconFWS.RemoveAt(index);
                    }
                }

                for (int i = 0; i < (countRows - сountRowsAll) + 1; i++)
                {
                    TableReconFWSAll strReconFWS = new TableReconFWSAll
                    {
                        TableReconFWSRow = new TableReconFWSRow
                        {
                            Id = -2,
                            Deviation = -2F,
                            FreqKHz = -2F,
                            Coordinates = new Coord
                            {
                                Latitude = -2,
                                Longitude = -2,
                                Altitude = -2
                            },
                            Modulation = ModulationKondor.None,
                            ListJamDirect = new ObservableCollection<TableJamDirect>
                            {
                                new TableJamDirect
                                {
                                    JamDirect = new JamDirect
                                    {
                                        NumberASP = -2,
                                        Bearing = -2F,
                                        Level = -2,
                                        Std = -2,
                                        DistanceKM = -2
                                    }
                                }
                            },
                            Type = 33 // 33 - пустое значение
                        },
                        TableNumASP = new TableNumASP
                        {
                            NumberASP = new ObservableCollection<string> { }

                        }
                    };

                    list.Add(strReconFWS);
                }

                for (int i = 0; i < list.Count; i++)
                {
                    ((GlobalReconFWS)dgvReconFWS.DataContext).CollectionReconFWS.Add(list[i]);
                }
            }
            catch { }
        }

        /// <summary>
        /// Удалить пустые строки из таблицы
        /// </summary>
        private void DeleteEmptyRows()
        {
            try
            {
                int countEmptyRows = ((GlobalReconFWS)dgvReconFWS.DataContext).CollectionReconFWS.Count(s => s.TableReconFWSRow.Id < 0);
                int countAllRows = ((GlobalReconFWS)dgvReconFWS.DataContext).CollectionReconFWS.Count;
                int iCount = countAllRows - countEmptyRows;
                for (int i = iCount; i < countAllRows; i++)
                {
                    ((GlobalReconFWS)dgvReconFWS.DataContext).CollectionReconFWS.RemoveAt(iCount);
                }
            }
            catch { }
        }

        private bool IsSelectedRowEmpty()
        {
            try
            {
                if (((TableReconFWSAll)dgvReconFWS.SelectedItem).TableReconFWSRow.Id == -2)
                    return false;
            }
            catch { }

            return true;
        }

        /// <summary>
        /// Очистить Радиосети
        /// </summary>
        /// <param name="listReconFWS"> Лист ИРИ ФРЧ ЦР </ИРИ> </param>
        /// <param name="lUS"> Лист Узлы связи </param>
        public void ClearRS(List<SRNet> lUS)
        {
            try
            {
                listUS = new List<SRNet>(lUS);

                UpdateStyleDataGrid();

                if (PropRS_US.CheckUS)
                    UpdateUS(lUS);
            }
            catch { }
        }

        /// <summary>
        /// Очистить Узлы связи
        /// </summary>
        /// <param name="listReconFWS"> Лист ИРИ ФРЧ ЦР </param>
        /// <param name="lRS"> Лист Радиосети </param>
        public void ClearUS(List<SRNet> lRS)
        {
            try
            {
                listRS = new List<SRNet>(lRS);

                UpdateStyleDataGrid();

                if (PropRS_US.CheckRS)
                    UpdateRS(lRS);
            }
            catch { }
        }

        Dictionary<string, string> dictKeyDataStyle = new Dictionary<string, string>()
        {
            { nameof(DataGrid), "DataGrid" },
            { nameof(DataGridCell), "DataGridCell"},
            { nameof(DataGridRow), "DataGridRow"},
            { nameof(DataGridRowHeader), "DataGridRowHeader"},
            { nameof(DataGridColumnHeader), "DataGridColumnHeader"}
        };
        private Style GetStyle(string NameProperty)
        {
            try
            { 
                var resource = new ResourceDictionary
                {
                    Source = new Uri("/ReconFWSControl;component/Themes/ThemeGreen.xaml",
                              UriKind.RelativeOrAbsolute)
                };
                foreach (var key in resource.Keys)
                {
                    var typeKey = key.GetType();
                    var KeyVal = typeKey.GetProperties().FirstOrDefault(rec => rec.Name == "Name");
                    if(KeyVal != null)
                    {
                        if(KeyVal.GetValue(key).ToString() == NameProperty)
                            return (Style)resource[key];
                    }
                }
                return null;
            }
            catch(Exception)
            { return null; }
        }

        //public void UpdateStyleDataGrid()
        //{
        //    try
        //    {
        //        Color colorAlternatingRowBackground = (Color)ColorConverter.ConvertFromString("#FF272626");
        //        Color colorRowBackground = (Color)ColorConverter.ConvertFromString("#FF343434");

        //        Style styleDataGrid = new Style();
        //        //styleDataGrid.Setters.Add(new Setter
        //        //{
        //        //    Property = DataGrid.AlternatingRowBackgroundProperty,
        //        //    Value = new LinearGradientBrush(new GradientStopCollection(new List<GradientStop>()
        //        //    {
        //        //        new GradientStop(colorAlternatingRowBackground, 0),
        //        //        new GradientStop(colorAlternatingRowBackground, 1)
        //        //    }),
        //        //        new Point(0, 0.5), new Point(1, 0.5))
        //        //});

        //        //styleDataGrid.Setters.Add(new Setter
        //        //{
        //        //    Property = DataGrid.RowBackgroundProperty,
        //        //    Value = new LinearGradientBrush(new GradientStopCollection(new List<GradientStop>()
        //        //    {
        //        //        new GradientStop(colorRowBackground, 0),
        //        //        new GradientStop(colorRowBackground, 1)
        //        //    }),
        //        //        new Point(0, 0), new Point(0, 1))
        //        //});


        //        dgvReconFWS.Style = GetStyle(nameof(DataGrid));
        //        dgvReconFWS.UpdateLayout();
        //    }
        //    catch { }
        //}

        /// <summary>
        /// for test
        /// </summary>
        private void ChangeBackgroundTESTdgv()
        {
            try
            {
                for (int i = 0; i < dgvReconFWS.Items.Count; i++)
                {
                    var dataGridRow = dgvReconFWS.ItemContainerGenerator.ContainerFromIndex(i) as DataGridRow;
                    //var dataGridRow1 = dgvReconFWS.ItemContainerGenerator.ContainerFromItem(dgvReconFWS.Items[i]) as DataGridRow;

                    if (dataGridRow != null)
                        dataGridRow.Background = Brushes.Red;
                }
            }
            catch { }
        }

        /// <summary>
        /// Радиосети
        /// Генерирование цвета от желтого до зеленого 
        /// </summary>
        public void UpdateRS(List<SRNet> lRS)
        {
            try
            {
                listRS = new List<SRNet>(lRS);

                Random randA = new Random();
                Random randR = new Random();
                Random randG = new Random();
                Random randB = new Random();
                Color color = new Color();

                if (lRS != null)
                {
                    for (int i = 0; i < lRS.Count; i++)
                    {
                        byte a, r, g, b = new byte();

                        if (i % 2 == 0) // желтый
                        {
                            a = (byte)randA.Next(200, 255);  // яркость
                            r = (byte)randR.Next(200, 255); // красный
                            g = (byte)randG.Next(170, 255); // зеленый
                            b = (byte)randB.Next(0, 150);     // синий
                        }
                        else // зеленый
                        {
                            a = (byte)randA.Next(200, 255);   // яркость
                            r = (byte)randR.Next(0, 150);    // красный
                            g = (byte)randG.Next(200, 255); // зеленый
                            b = (byte)randB.Next(60, 800);   // синий
                        }

                        color = Color.FromArgb(a, r, g, b);

                        SolidColorBrush solidBrush = new SolidColorBrush(color); //new Color { R = 255, G = 100, B = 0, A = 0 });

                        for (int j = 0; j < lRS[i].list_IRI_ID.Count; j++)
                        {
                            int id = lRS[i].list_IRI_ID[j].ID;

                            for (int row = 0; row < dgvReconFWS.Items.Count; row++)
                            {

                                DataGridRow dataGridRow = dgvReconFWS.ItemContainerGenerator.ContainerFromItem(dgvReconFWS.Items[row]) as DataGridRow;

                                if (dataGridRow != null)
                                {
                                    var idRec = (dataGridRow.Item as TableReconFWSAll).TableReconFWSRow.Id;
                                    if (idRec == id)
                                        dataGridRow.Foreground = solidBrush;
                                }
                            }
                        }
                    }
                }
            }
            catch { }
        }

        private void UpdateStyleDataGrid()
        {
            try
            {
                SolidColorBrush solidBrushWhite = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FFD9F9E3"));

                for (int row = 0; row < dgvReconFWS.Items.Count; row++)
                {
                    var dataGridRow = dgvReconFWS.ItemContainerGenerator.ContainerFromItem(dgvReconFWS.Items[row]) as DataGridRow;

                    if (dataGridRow != null)
                    {
                        dataGridRow.Foreground = solidBrushWhite;
                    }
                }
            }
            catch { }
        }
         
        /// <summary>
        /// Узлы связи
        /// Генерирование цвета от розового до фиолетового
        /// </summary>
        public void UpdateUS(List<SRNet> lUS)
        {
            try
            {
                listUS = new List<SRNet>(lUS);

                Random randA = new Random();
                Random randR = new Random();
                Random randG = new Random();
                Random randB = new Random();
                Color color = new Color();

                if (lUS != null)
                {
                    for (int i = 0; i < lUS.Count; i++)
                    {
                        byte a, r, g, b = new byte();

                        if (i % 2 == 0) // розовый
                        {
                            a = (byte)randA.Next(200, 255);  // яркость
                            r = (byte)randR.Next(140, 255); // красный
                            g = (byte)randG.Next(0, 4);     // красный
                            b = (byte)randB.Next(40, 130);  // синий
                        }
                        else // фиолетовый
                        {
                            a = (byte)randA.Next(200, 255);   // яркость
                            r = (byte)randR.Next(170, 220);  // красный
                            g = (byte)randG.Next(0, 0);     // красный
                            b = (byte)randB.Next(160, 255); // синий
                        }

                        color = Color.FromArgb(a, r, g, b);

                        SolidColorBrush solidBrush = new SolidColorBrush(color); //new Color { R = 255, G = 100, B = 0, A = 0 });

                        for (int j = 0; j < lUS[i].list_IRI_ID.Count; j++)
                        {
                            int id = lUS[i].list_IRI_ID[j].ID;

                            for (int row = 0; row < dgvReconFWS.Items.Count; row++)
                            {
                                DataGridRow dataGridRow = dgvReconFWS.ItemContainerGenerator.ContainerFromItem(dgvReconFWS.Items[row]) as DataGridRow;

                                if (dataGridRow != null)
                                {
                                    var idRec = (dataGridRow.Item as TableReconFWSAll).TableReconFWSRow.Id;
                                    if (idRec == id)
                                        dataGridRow.Foreground = solidBrush;
                                }
                            }
                        }
                    }
                }
            }
            catch { }
        }

        /// <summary>
        /// Обновить таблицу с УС/РС после прокрутки
        /// </summary>
        private void UpdateRS_USAfterScroll()
        {
            try
            {
                if (bRS.IsChecked == true || bUS.IsChecked == true)
                {
                    UpdateStyleDataGrid();

                    if (bRS.IsChecked == true)
                    {
                        if (listRS.Count > 0)
                        {
                            UpdateRS(listRS);
                        }
                    }
                    if (bUS.IsChecked == true)
                    {
                        if (listUS.Count > 0)
                        {
                            UpdateUS(listUS);
                        }
                    }
                }
            }
            catch { }
        }

        /// <summary>
        /// Добавить таблицу для отчета
        /// </summary>
        /// <returns></returns>
        private string[,] AddReconFWSToTable()
        {
            int Columns = 11;
            int Rows = (((GlobalReconFWS)dgvReconFWS.DataContext).CollectionReconFWS.Count(x => x.TableReconFWSRow.Id > 0)) + 1;
            string[,] Table = new string[Rows, Columns];

            try
            {
                Table[0, 0] = SHeaders.headerFreq;
                Table[0, 1] = SHeaders.headerDeltaF;
                Table[0, 2] = SHeaders.headerBearing;
                Table[0, 3] = SHeaders.headerRMSE;
                Table[0, 4] = SHeaders.headerLevel;
                Table[0, 5] = SHeaders.headerDistance;
                Table[0, 6] = SHeaders.headerRI_AJS;
                Table[0, 7] = SHeaders.headerLatLon;
                Table[0, 8] = SHeaders.headerAlt;
                Table[0, 9] = SHeaders.headerTime;
                Table[0, 10] = SHeaders.headerJ_AJS;

                for (int i = 1; i < Rows; i++)
                {
                    Table[i, 0] = ((GlobalReconFWS)dgvReconFWS.DataContext).CollectionReconFWS[i - 1].TableReconFWSRow.FreqKHz.ToString();
                    Table[i, 1] = ((GlobalReconFWS)dgvReconFWS.DataContext).CollectionReconFWS[i - 1].TableReconFWSRow.Deviation.ToString();
                    Table[i, 2] = ((GlobalReconFWS)dgvReconFWS.DataContext).CollectionReconFWS[i - 1].TableReconFWSRow.SelectedJamDirect.JamDirect.Bearing.ToString();
                    Table[i, 3] = ((GlobalReconFWS)dgvReconFWS.DataContext).CollectionReconFWS[i - 1].TableReconFWSRow.SelectedJamDirect.JamDirect.Std.ToString();
                    Table[i, 4] = ((GlobalReconFWS)dgvReconFWS.DataContext).CollectionReconFWS[i - 1].TableReconFWSRow.SelectedJamDirect.JamDirect.Level.ToString();
                    Table[i, 5] = ((GlobalReconFWS)dgvReconFWS.DataContext).CollectionReconFWS[i - 1].TableReconFWSRow.SelectedJamDirect.JamDirect.DistanceKM.ToString();
                    Table[i, 6] = ((GlobalReconFWS)dgvReconFWS.DataContext).CollectionReconFWS[i - 1].TableReconFWSRow.SelectedJamDirect.JamDirect.NumberASP.ToString();
                    Table[i, 7] = ((GlobalReconFWS)dgvReconFWS.DataContext).CollectionReconFWS[i - 1].TableReconFWSRow.Coordinates.Latitude.ToString() + "   " + ((GlobalReconFWS)dgvReconFWS.DataContext).CollectionReconFWS[i - 1].TableReconFWSRow.Coordinates.Longitude.ToString();
                    Table[i, 8] = ((GlobalReconFWS)dgvReconFWS.DataContext).CollectionReconFWS[i - 1].TableReconFWSRow.Coordinates.Altitude.ToString();
                    Table[i, 9] = ((GlobalReconFWS)dgvReconFWS.DataContext).CollectionReconFWS[i - 1].TableReconFWSRow.Time.ToString();
                    Table[i, 10] = ((GlobalReconFWS)dgvReconFWS.DataContext).CollectionReconFWS[i - 1].TableReconFWSRow.ASPSuppr.ToString();
                }
            }
            catch { }

            return Table;
        }

        /// <summary>
        /// Узлы связи
        /// Генерирование цвета от розового до фиолетового (закрашена вся строка)
        /// </summary>
        //public void UpdateUS(List<SRNet> lUS)
        //{
        //    try
        //    {
        //        listUS = new List<SRNet>(lUS);

        //        Random randA = new Random();
        //        Random randR = new Random();
        //        Random randG = new Random();
        //        Random randB = new Random();
        //        Color color = new Color();

        //        if (lUS != null)
        //        {
        //            for (int i = 0; i < lUS.Count; i++)
        //            {
        //                byte a, r, g, b = new byte();

        //                if (i % 2 == 0) // розовый
        //                {
        //                    a = (byte)randA.Next(66, 122);  // яркость
        //                    r = (byte)randR.Next(140, 255); // красный
        //                    g = (byte)randG.Next(0, 4);     // красный
        //                    b = (byte)randB.Next(40, 130);  // синий
        //                }
        //                else // фиолетовый
        //                {
        //                    a = (byte)randA.Next(30, 80);   // яркость
        //                    r = (byte)randR.Next(70, 140);  // красный
        //                    g = (byte)randG.Next(0, 0);     // красный
        //                    b = (byte)randB.Next(140, 255); // синий
        //                }

        //                color = Color.FromArgb(a, r, g, b);

        //                SolidColorBrush solidBrush = new SolidColorBrush(color); //new Color { R = 255, G = 100, B = 0, A = 0 });

        //                for (int j = 0; j < lUS[i].list_IRI_ID.Count; j++)
        //                {
        //                    int id = lUS[i].list_IRI_ID[j].ID;

        //                    for (int row = 0; row < dgvReconFWS.Items.Count; row++)
        //                    {
        //                        DataGridRow dataGridRow = dgvReconFWS.ItemContainerGenerator.ContainerFromItem(dgvReconFWS.Items[row]) as DataGridRow;

        //                        if (dataGridRow != null)
        //                        {
        //                            var idRec = (dataGridRow.Item as TableReconFWSAll).TableReconFWSRow.Id;
        //                            if (idRec == id)
        //                                dataGridRow.Background = solidBrush;
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    catch { }
        //}

        /// <summary>
        /// Радиосети
        /// Генерирование цвета от желтого до зеленого (закрашена вся строка)
        /// </summary>
        //public void UpdateRS(List<SRNet> lRS)
        //{
        //    try
        //    {
        //        listRS = new List<SRNet>(lRS);

        //        Random randA = new Random();
        //        Random randR = new Random();
        //        Random randG = new Random();
        //        Random randB = new Random();
        //        Color color = new Color();

        //        if (lRS != null)
        //        {
        //            for (int i = 0; i < lRS.Count; i++)
        //            {
        //                byte a, r, g, b = new byte();

        //                if (i % 2 == 0) // желтый
        //                {
        //                    a = (byte)randA.Next(75, 120);  // яркость
        //                    r = (byte)randR.Next(150, 255); // красный
        //                    g = (byte)randG.Next(80, 128);  // зеленый
        //                    b = (byte)randB.Next(0, 0);     // синий
        //                }
        //                else // зеленый
        //                {
        //                    a = (byte)randA.Next(47, 90);   // яркость
        //                    r = (byte)randR.Next(0, 0);     // красный
        //                    g = (byte)randG.Next(100, 190); // зеленый
        //                    b = (byte)randB.Next(0, 0);     // синий
        //                }

        //                color = Color.FromArgb(a, r, g, b);

        //                SolidColorBrush solidBrush = new SolidColorBrush(color); //new Color { R = 255, G = 100, B = 0, A = 0 });

        //                for (int j = 0; j < lRS[i].list_IRI_ID.Count; j++)
        //                {
        //                    int id = lRS[i].list_IRI_ID[j].ID;

        //                    for (int row = 0; row < dgvReconFWS.Items.Count; row++)
        //                    {

        //                        DataGridRow dataGridRow = dgvReconFWS.ItemContainerGenerator.ContainerFromItem(dgvReconFWS.Items[row]) as DataGridRow;

        //                        if (dataGridRow != null)
        //                        {
        //                            var idRec = (dataGridRow.Item as TableReconFWSAll).TableReconFWSRow.Id;
        //                            if (idRec == id)
        //                                dataGridRow.Background = solidBrush;
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    catch { }
        //}

        /// <summary>
        /// Видимость кнопок в зависимости от заданного состояния КРПУ
        /// </summary>
        public void ButtonsCRRDVisible()
        {
            // КРПУ1
            if (PropLocalCRRD.CRRD1)
            {
                gridButtons.ColumnDefinitions[7].Width = new GridLength(30);
            }
            else
            {
                gridButtons.ColumnDefinitions[7].Width = new GridLength(0);
            }

            // КРПУ2
            if (PropLocalCRRD.CRRD2)
            {
                gridButtons.ColumnDefinitions[8].Width = new GridLength(30);
            }
            else
            {
                gridButtons.ColumnDefinitions[8].Width = new GridLength(0);
            }
        }

        public void ChangeFreqHeaderMHz_kHz()
        {
            switch (PropLocalFreqMHz_kHz.FreqMHz_kHz)
            {
                case 0: // kHz
                    lHeaderFreq.Content = SMeaning.meaningFkHz;
                    break;

                case 1: // MHz
                    lHeaderFreq.Content = SMeaning.meaningFMHz;
                    break;
            }

        }

        private bool isRoss = false;
        public bool IsRoss
        {
            get { return isRoss; }
            set
            {
                if (isRoss == value)
                    return;

                isRoss = value;
                UpdateButtonsForRoss(isRoss);
            }
        }

        private void UpdateButtonsForRoss(bool isRoss)
        {
            if (isRoss)
            {
                bAddFWS_CRRD.Visibility = Visibility.Hidden;
                bAddFWS_CRRD2.Visibility = Visibility.Hidden;
                bGetExecBear.Visibility = Visibility.Hidden;
                bGetKvBear.Visibility = Visibility.Hidden;
                lSeparator2.Visibility = Visibility.Hidden;
                bFWS_TD.Visibility = Visibility.Hidden;
                bFWS_TD_RS.Visibility = Visibility.Hidden;
                bRS.Visibility = Visibility.Hidden;
                bUS.Visibility = Visibility.Hidden;
            }
            else
            {
                bAddFWS_CRRD.Visibility = Visibility.Visible;
                bAddFWS_CRRD2.Visibility = Visibility.Visible;
                bGetExecBear.Visibility = Visibility.Visible;
                bGetKvBear.Visibility = Visibility.Visible;
                lSeparator2.Visibility = Visibility.Visible;
                bFWS_TD.Visibility = Visibility.Visible;
                bFWS_TD_RS.Visibility = Visibility.Visible;
                bRS.Visibility = Visibility.Visible;
                bUS.Visibility = Visibility.Visible;
            }
            
        }

    }
}
