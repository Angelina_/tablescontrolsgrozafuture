﻿using ModelsTablesDBLib;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using TableEvents;

namespace ReconFWSControl
{
    public class TableReconFWSAll : INotifyPropertyChanged
    {
        private TableNumASP tableNumASP = new TableNumASP();
        private TableReconFWSRow tableReconFWSRow = new TableReconFWSRow();
        [NotifyParentProperty(true)]
        public TableReconFWSRow TableReconFWSRow
        {
            get => tableReconFWSRow;
            set
            {
                if (tableReconFWSRow == value) return;
                tableReconFWSRow = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        public TableNumASP TableNumASP
        {
            get => tableNumASP;
            set
            {
                if (tableNumASP == value) return;
                tableNumASP = value;
                OnPropertyChanged();
            }
        }

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }
        #endregion
    }

    public class GlobalReconFWS
    {
        public ObservableCollection<TableReconFWSAll> CollectionReconFWS { get; set; }

        public GlobalReconFWS()
        {
            CollectionReconFWS = new ObservableCollection<TableReconFWSAll> { };

            CollectionReconFWS = new ObservableCollection<TableReconFWSAll>
            {
                new TableReconFWSAll
                {
                    TableReconFWSRow = new TableReconFWSRow
                    {
                        Id = 1,
                        Sender = SignSender.Cicada,
                        FreqKHz = 35000.9F,
                        Time = DateTime.Now.ToLocalTime(),
                        Deviation = 100,
                        ASPSuppr = 209,
                        Type = 2,
                        Modulation = ModulationKondor.FM,
                        Coordinates = new Coord
                        {
                            Latitude = 55.675566,
                            Longitude = 27.565656,
                            Altitude = 150
                        },
                        ListJamDirect = new ObservableCollection<TableJamDirect>
                        {
                            new TableJamDirect
                            {
                                JamDirect = new JamDirect
                                {
                                    NumberASP = 209,
                                    Bearing = 150,
                                    Level = -70,
                                    Std = 100.9F,
                                    DistanceKM = 2000,
                                    IsOwn = true
                                }
                            },
                            new TableJamDirect
                            {
                                JamDirect = new JamDirect
                                {
                                    NumberASP = 210,
                                    Bearing = 166,
                                    Level = -90,
                                    Std = 123.88F,
                                    DistanceKM = 500,
                                    IsOwn = false
                                }
                            },
                            new TableJamDirect
                            {
                                JamDirect = new JamDirect
                                {
                                    NumberASP = 211,
                                    Bearing = 170,
                                    Level = -80,
                                    Std = 34.88F,
                                    DistanceKM = 1500,
                                    IsOwn = false
                                }
                            },
                            new TableJamDirect
                            {
                                JamDirect = new JamDirect
                                {
                                    NumberASP = 212,
                                    Bearing = 10,
                                    Level = -94,
                                    Std = 53.88F,
                                    DistanceKM = 5600,
                                    IsOwn = false
                                }
                            }
                        }
                    },
                    TableNumASP = new TableNumASP
                    {
                        NumberASP = new ObservableCollection<string>{"*209", "210", "211"}
                    }
                },
                new TableReconFWSAll
                {
                    TableReconFWSRow = new TableReconFWSRow
                    {
                        Id = 2,
                        Sender = SignSender.RadioRecevier,
                        FreqKHz = 43555.7F,
                        Time = DateTime.Now.ToLocalTime(),
                        Deviation = 200,
                        ASPSuppr = 211,
                        Type = 1,
                        Modulation = ModulationKondor.TETRA,
                        Coordinates = new Coord
                        {
                            Latitude = 53.444444,
                            Longitude = 28.888999,
                            Altitude = 1220
                        },
                        ListJamDirect = new ObservableCollection<TableJamDirect>
                        {
                            new TableJamDirect
                            {
                                JamDirect = new JamDirect
                                {
                                    NumberASP = 210,
                                    Bearing = 220,
                                    Level = -60,
                                    Std = 144.9F,
                                    DistanceKM = 2000,
                                    IsOwn = true
                                }
                            },
                            new TableJamDirect
                            {
                                JamDirect = new JamDirect
                                {
                                    NumberASP = 218,
                                    Bearing = 166,
                                    Level = -70,
                                    Std = 15.88F,
                                    DistanceKM = 20,
                                    IsOwn = false
                                }
                            },
                            new TableJamDirect
                            {
                                JamDirect = new JamDirect
                                {
                                    NumberASP = 217,
                                    Bearing = 130,
                                    Level = -90,
                                    Std = 34.88F,
                                    DistanceKM = 300,
                                    IsOwn = false
                                }
                            }
                        }
                    },
                     TableNumASP = new TableNumASP
                     {
                         NumberASP = new ObservableCollection<string>{"*209", "210", "211"}
                     }
                }
            };
        }
    }

    public class TableNumASP : INotifyPropertyChanged
    {
        private ObservableCollection<string> numberASP;

        public ObservableCollection<string> NumberASP
        {
            get => numberASP;
            set
            {
                if (numberASP == value) return;

                if (value != null)
                {
                    if (numberASP == null)
                        numberASP = new ObservableCollection<string>();
                    numberASP.Clear();
                    foreach (var rec in value)
                        numberASP.Add(rec);
                    OnPropertyChanged();
                    return;
                }

                numberASP = value;
                OnPropertyChanged();
            }
        }


        private string selectedASPRP = string.Empty;
        public string SelectedASPRP
        {
            get
            {
                return selectedASPRP;
            }
            set
            {
                if (selectedASPRP == value) return;
                selectedASPRP = value;
                OnPropertyChanged("SelectedASPRP");
            }
        }

        //private int selectedASPRP = -2;

        //public int SelectedASPRP
        //{
        //    get
        //    {
        //       return selectedASPRP;
        //    }
        //    set
        //    {
        //        if (selectedASPRP == value) return;
        //        selectedASPRP = value;
        //        OnPropertyChanged("SelectedASPRP");
        //    }
        //}

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }
        #endregion
    }

    public class TableReconFWSRow : TableReconFWS, INotifyPropertyChanged
    {
        private TableJamDirect selectedJamDirect = new TableJamDirect { JamDirect = new JamDirect() { } };
        private int selectedASP = -2;

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }
        #endregion


        public int SelectedASP
        {
            get
            {
                if (selectedASP < 0 && ListJamDirect != null && ListJamDirect.Count != 0)
                    selectedASP = ListJamDirect.First().JamDirect.NumberASP;
                return selectedASP;
            }
            set
            {
                if (selectedASP == value) return;
                selectedASP = value;
            }
        }

        public TableJamDirect SelectedJamDirect
        {
            get
            {
                if (!CompareJams(selectedJamDirect, ListJamDirect.FirstOrDefault(rec => rec.JamDirect.NumberASP == SelectedASP)))
                {
                    SelectedJamDirect = ListJamDirect.FirstOrDefault(rec => rec.JamDirect.NumberASP == SelectedASP);
                }

                return selectedJamDirect;
            }
            set
            {
                if (value == null) return;
                if (selectedJamDirect == value) return;

                selectedJamDirect.JamDirect.Bearing = value.JamDirect.Bearing;
                selectedJamDirect.JamDirect.DistanceKM = value.JamDirect.DistanceKM;
                selectedJamDirect.JamDirect.IsOwn = value.JamDirect.IsOwn;
                selectedJamDirect.JamDirect.Level = value.JamDirect.Level;
                selectedJamDirect.JamDirect.NumberASP = value.JamDirect.NumberASP;
                selectedJamDirect.JamDirect.Std = value.JamDirect.Std;
                OnPropertyChanged(nameof(SelectedJamDirect));
            }
        }

        private bool CompareJams(TableJamDirect jam1, TableJamDirect jam2)
        {
            if (jam1.JamDirect == null || jam2.JamDirect == null)
                return true;
            if (jam1.JamDirect.Bearing != jam2.JamDirect.Bearing ||
                jam1.JamDirect.DistanceKM != jam2.JamDirect.DistanceKM ||
                jam1.JamDirect.IsOwn != jam2.JamDirect.IsOwn ||
                jam1.JamDirect.Level != jam2.JamDirect.Level ||
                jam1.JamDirect.NumberASP != jam2.JamDirect.NumberASP ||
                jam1.JamDirect.Std != jam2.JamDirect.Std)
                return false;
            return true;
        }

        public TableReconFWSRow()
        {
            if (ListJamDirect != null && ListJamDirect.Count != 0)
                selectedJamDirect = ListJamDirect.First();
        }

        public TableReconFWSRow(TableReconFWS tableReconFWS)
        {
            Id = tableReconFWS.Id;
            FreqKHz = tableReconFWS.FreqKHz;
            Deviation = tableReconFWS.Deviation;
            Coordinates = tableReconFWS.Coordinates;
            Time = tableReconFWS.Time;
            Type = tableReconFWS.Type;
            ListJamDirect = tableReconFWS.ListJamDirect;
            ASPSuppr = tableReconFWS.ASPSuppr;
        }
    }
}
